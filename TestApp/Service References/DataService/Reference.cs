﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestApp.DataService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DataService.IDataService")]
    public interface IDataService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDataService/ImportJSON", ReplyAction="http://tempuri.org/IDataService/ImportJSONResponse")]
        int ImportJSON(string dataset, string JSONdata, bool verbose);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDataService/ImportJSON", ReplyAction="http://tempuri.org/IDataService/ImportJSONResponse")]
        System.Threading.Tasks.Task<int> ImportJSONAsync(string dataset, string JSONdata, bool verbose);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDataService/GetAvailableDatasets", ReplyAction="http://tempuri.org/IDataService/GetAvailableDatasetsResponse")]
        string[] GetAvailableDatasets();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDataService/GetAvailableDatasets", ReplyAction="http://tempuri.org/IDataService/GetAvailableDatasetsResponse")]
        System.Threading.Tasks.Task<string[]> GetAvailableDatasetsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IDataServiceChannel : TestApp.DataService.IDataService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DataServiceClient : System.ServiceModel.ClientBase<TestApp.DataService.IDataService>, TestApp.DataService.IDataService {
        
        public DataServiceClient() {
        }
        
        public DataServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DataServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int ImportJSON(string dataset, string JSONdata, bool verbose) {
            return base.Channel.ImportJSON(dataset, JSONdata, verbose);
        }
        
        public System.Threading.Tasks.Task<int> ImportJSONAsync(string dataset, string JSONdata, bool verbose) {
            return base.Channel.ImportJSONAsync(dataset, JSONdata, verbose);
        }
        
        public string[] GetAvailableDatasets() {
            return base.Channel.GetAvailableDatasets();
        }
        
        public System.Threading.Tasks.Task<string[]> GetAvailableDatasetsAsync() {
            return base.Channel.GetAvailableDatasetsAsync();
        }
    }
}
