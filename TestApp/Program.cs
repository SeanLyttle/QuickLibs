﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using TestApp.DataService;
using QuickLibs.Javascript;
using QuickLibs.Data;
using QuickLibs.Web;
using QuickLibs.Extensions.String;
using QuickLibs.Utils;
using QuickLibs.MSOffice;
using System.IO;
using ClosedXML.Excel;
using QuickLibs.Web.API.Facebook;
using System.Web.UI.HtmlControls;
using QuickLibs.Cryptography;
using System.Diagnostics;
using DocumentFormat.OpenXml.Presentation;
using System.Text;
using System.Text.RegularExpressions;

namespace QuickLibs.TestApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            QuickHtmlParsing();
            
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void QuickHtmlParsing()
        {
            QuickHtml test = new QuickHtml("", "This is a <b style=\"color:red\">bold</b> statement.");
            Console.WriteLine(test);
            Console.WriteLine(test.Html());
            Console.WriteLine(test.Length);
            foreach (QuickHtml control in test)
            {
                Console.WriteLine($"'{control}'");
            }
        }

        private static void JSONDeserialize()
        {
            string jString = "{\"Numeral\":\"123,456\",\"Number\":\"0123456\",\"Zero\":\"0\",\"Decimal\":\"0.5\",\"IP\":\"12.34.56\"}";
            JSON j = new JSON(jString);
            Console.WriteLine(jString);
            Console.WriteLine(j.ToString());
        }

        private static void TestHubspotRequest()
        {
            APIRequest Request = new APIRequest("https://api.hubapi.com/crm/v3/");
            Request.Auth.QueryString("hapikey", "81d46cc9-0e1f-4c30-9714-238dca570d6f");
            Request.RouteParameters.Add("ObjectType", "contacts");

            JSON Response = Request.Get("/Properties/{ObjectType}/");
            Response.Throw();

            string Properties = Response["results"].Aggregate(
                new StringBuilder(),
                (sb, j) => sb.Append(j["name"].String).Append(","),
                sb => sb.ToString(0, sb.Length - 1)
            );
        }

        private static void TestAPIRequest()
        {
            APIRequest api = new APIRequest("https://api.hubapi.com/crm/v3/");
            api.QueryString.Add("hapikey", "e19d3e88-02e4-4652-9ee7-aad9bc03e854").Add("limit", "10");
            api.Get("/objects/contacts/");

            api.Response.DoResponse();
        }

        private static void QuickHtmlIDs()
        {
            System.Web.UI.Control ctrl = new System.Web.UI.Control();
            QuickHtml test = new QuickHtml("section");
            QuickHtml h = new QuickHtml("h1", "This is a test");
            h.SetID(null);
            h.AppendTo(test);
            test.AppendTo(ctrl);
        }

        private static void FacebookAPI()
        {
            FBAPI fb = new FBAPI("EAALStC6RIVABAAKBmSGV4vdYDc8ox6mA4ZBZCZC0MX2ZAZCRIC2GmZARTrsZCZAxZCryviEfxIti3uNGsJIogiaVE6nBwYITFSTebfK9uAObQ24aEJwg0yE9tsJSrWEJhEtZBuOyVcXnNGk6uHfybfNWMG5BItcxoa7qtph4TD1xdkmPkKLoveekRkxSZBRVCSMIwwJJC1WzKlm0wZDZD");
            fb.Get("me");
            Console.WriteLine("\nme");
            Console.WriteLine(fb.Response.ToJSON());
            fb.Get("me/accounts");
            Console.WriteLine("\nme/accounts");
            Console.WriteLine(fb.Response.ToJSON());

            string PageToken = fb.GetPageToken(
                fb["data"][0]["id"].String,
                "794621270630736",                 //Collage FB App ID
                "bc17ad50c89eab59dd0c458b2fb3bea0" //Collage FB App Secret
            );
            fb.AccessToken = PageToken;
            fb.Get("me");
            Console.WriteLine("\nme (page)");
            Console.WriteLine(fb.Response.ToJSON());
            fb.Get("me/accounts");
            Console.WriteLine("\nme/accounts (page)");
            Console.WriteLine(fb.Response.ToJSON());

        }

        private static void TestHash()
        {
            Console.WriteLine("Basic Hash");
            Console.WriteLine(Crypto.GetHash("TEST"));
            Console.WriteLine(Crypto.GetHashSHA512("TEST",""));

            Console.WriteLine("Hash With Salt");
            Console.WriteLine(Crypto.GetHash("TEST","SALT",1));
            Console.WriteLine(Crypto.GetHashSHA512("TEST", "SALT"));

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Console.WriteLine(Crypto.GetHash("TEST", "SALT", 10000));
            Console.WriteLine("10,000 iterations: " + sw.ElapsedMilliseconds.ToString() + "ms"); 
        }

        private static void TestChangeDatabase()
        {
            QuickDB qDB = new QuickDB("Server=hmpedemo.database.windows.net;Database=MDM;User Id=pedemo;Password=5VYPBai4NdPY");
            Console.WriteLine(qDB.DatabaseName);
            qDB.Select("SELECT * FROM TestTable");
            Console.WriteLine(qDB.GetString());
            qDB.DatabaseName = "CDP";
            Console.WriteLine(qDB.DatabaseName);
            qDB.Select("SELECT * FROM ctr.Licence");
            Console.WriteLine(qDB.GetString());
            qDB.Use("MDM").Select("SELECT * FROM TestTable");
            Console.WriteLine(qDB.GetString());
        }

        private static void TestTableNormalization()
        {
            Console.WriteLine(QuickDB.NormalizeObjectName("Th[i]s", "Is", "A", "Test"));
            Console.WriteLine(QuickDB.NormalizeObjectName("[This]", "Is", "[A", "Test]"));
            Console.WriteLine(QuickDB.NormalizeObjectName("[This]", "", null, "Test]"));
            Console.WriteLine(QuickDB.NormalizeTableName("[DB].[Schema].[Table]"));
            Console.WriteLine(QuickDB.NormalizeTableName("DB.Schema.Table"));
            Console.WriteLine(QuickDB.NormalizeTableName("DB..Table"));
            Console.WriteLine(QuickDB.NormalizeTableName("[Table]"));
            Console.WriteLine(QuickDB.NormalizeTableName("[Table.Name]"));
            Console.WriteLine(QuickDB.NormalizeTableName("Tab[le.Na]me"));
            Console.WriteLine(QuickDB.NormalizeTableName("DB.[Schema].Table]"));
            Console.WriteLine(QuickDB.DenormalizeObjectName("[This[is]]a].[test].too")[0]);
            Console.WriteLine(QuickDB.DenormalizeObjectName("[This[is]]a].[test].too")[1]);
            Console.WriteLine(QuickDB.DenormalizeObjectName("[This[is]]a].[test].too")[2]);
        }

        private static void JSONGetValue()
        {
            JSON j = new JSON("[1,True,3.5,4,5]");

            Console.WriteLine("String Array");
            string[] str = j.ToObject<string[]>();
            foreach (string s in str)
                Console.WriteLine(s);

            Console.WriteLine("\nInt? Array");
            int?[] ints = j.ToObject<int?[]>();
            foreach (int? i in ints)
                Console.WriteLine(i);

            Console.WriteLine("\nDouble? Array");
            double?[] dbls = j.ToObject<double?[]>();
            foreach (double? i in dbls)
                Console.WriteLine(i);

            Console.WriteLine("\nBool Array");
            bool[] bol = j.ToObject<bool[]>();
            foreach (bool b in bol)
                Console.WriteLine(b);

            object o = j.ToObject();

            j = new JSON("{key:value}");
            Dictionary<string, string> test = j.ToObject<Dictionary<string, string>>();
        }

        private static void QuickDBConnectionStrings()
        {
            QuickDB qDB = new QuickDB("Srv", "DB", "Usr", "Pwd", true);
            Console.WriteLine(qDB.ToString());
        }

        private static void TestQDBRetry()
        {
            QuickDB qDB = new QuickDB("Server=hmpedemo.database.windows.net;Database=MDM;User Id=pedemo;Password=5VYPBai4NdPY");
            qDB.SetParameter("ID", 1);
            qDB.SetParameter("Height", "");
            qDB.Select("UPDATE [DemoTable] SET [Height] = @Height OUTPUT INSERTED.* WHERE [ID] = @ID;");

            if (qDB.HasErrors())
                Console.WriteLine(new JSON(qDB.LastException).ToJSON());
            else
                Console.WriteLine(JSON.FromDataRow(qDB.GetRow(), false).ToJSON());
        }

        private static void QuickDBMaskConnection()
        {
            Console.WriteLine(QuickDB.MaskConnectionString("Server=Test;DB=Test;Username=MyUser;Password=MyPassword;"));
            Console.WriteLine(QuickDB.MaskConnectionString("Server=Test;DB=Test;Username=MyUser;Password=MyPassword;", "user"));
            Console.WriteLine(QuickDB.MaskConnectionString("Server=Test;DB=Test;Username=MyUser;Password=MyPassword;", "password"));
            Console.WriteLine(QuickDB.MaskConnectionString("Server=Test;DB=Test;Username=MyUser;Password=MyPassword;", "user", "password"));
            Console.WriteLine(QuickDB.MaskConnectionString("Server=Test;DB=Test;Username=MyUser;Password=MyPassword", "nothing"));
        }

        private static void JSONAddChain()
        {
            new JSON()
                .Add("Key", new JSON().Add("Key3", "Value"))
                .Add("Key", "Value2")
                .Add("Key2", "Val1")
                .Add("Key", "Value3")
                .DoResponse();
        }

        private static void TestLog()
        {
            Logging Log = new Logging(true, true);
            Log.VerboseOutput = true;
            Log.DebugMode = true;

            Log.Prefix = "TEST";
            Log.Suffix = ";";

            Log.Info("Some information");
            Log.Warning("A warning message");
            Log.Error("An error message");
            Log.Debug("A debug message");


            Log.LOG.DoResponse();
        }

        private static void TestAPIResponses()
        {
            JSON response;
            JSON payload = new JSON("{Namespace:HollandMountain,Class:ClientRecon,Method:RunReconciliation,Parameters:{Tolerance:0.01,IgnoreTables:[table 1,table 2]}");

            JSON.Get("https://cdprestapi-dev.azurewebsites.net/debug/?error=Test%20Message").DoResponse();
            JSON.Put("https://cdprestapi-dev.azurewebsites.net/debug/", null, payload).DoResponse(); //Payload will take precidence over postData if both are supplied.

            JSON.Get("https://cdprestapi-dev.azurewebsites.net/debug/").DoResponse();
            JSON.Post("https://cdprestapi-dev.azurewebsites.net/debug/").DoResponse();
            JSON.Put("https://cdprestapi-dev.azurewebsites.net/debug/").DoResponse();
            JSON.Patch("https://cdprestapi-dev.azurewebsites.net/debug/").DoResponse();
            JSON.Delete("https://cdprestapi-dev.azurewebsites.net/debug/").DoResponse();
            JSON.Load("https://cdprestapi-dev.azurewebsites.net/debug/", method: "post").DoResponse();

            //response = JSON.Get("https://pm2.preqinsolutions.com/apiCore/api/metrics/values/6883/2019-09-01");
            //response.DoResponse();
        }

        private static void TestRequestBody()
        {
            JSON response = new JSON();
            try
            {
                //GET Source iSheet
                string Endpoint = "https://flow.ircp-it.com/ircp/api/3/";
                string APICall = "/isheets/admin?siteid=729";

                JSON h = new JSON("Authorization", "Bearer " + "tinQiV1iArPfubOinRmRBzL95jQsutk0");

                response = JSON.Get(Endpoint.TrimEnd('/', '\\') + "/" + APICall.TrimStart('/', '\\'), headers: h);
                response.Throw();

                JSON iSheetSource = response["isheet"].Where(j => "659".Equals(j["id"].String)).FirstOrDefault();

                //POST New iSheet
                APICall = "/isheets/admin?siteid=729"; //Same url!
                response = JSON.Post(Endpoint.TrimEnd('/', '\\') + "/" + APICall.TrimStart('/', '\\'), jsonData: iSheetSource, headers: h);
                response.Throw();

                string newSheetId = response["id"].String;

                //GET Source Columns
                APICall = "/isheets/admin/659/columns";
                response = JSON.Get(Endpoint.TrimEnd('/', '\\') + "/" + APICall.TrimStart('/', '\\'), headers: h);
                response.Throw();

                //POST Columns to New iSheet
                APICall = "/isheets/admin/" + newSheetId + "/columns";
                response = JSON.Post(Endpoint.TrimEnd('/', '\\') + "/" + APICall.TrimStart('/', '\\'), jsonData: response, headers: h);
                response.Throw();


                //POST Columns ProgressiveKeyStatus
                if (response["progressivekey"].IsString)
                {
                    APICall = "/progressivekeystatus/" + response["progressivekey"].String;
                    response = JSON.Post(Endpoint.TrimEnd('/', '\\') + "/" + APICall.TrimStart('/', '\\'), headers: h);
                    response.Throw();
                }

                response.DoResponse();
            }
            catch (Exception exp)
            {
                new JSON(exp).DoResponse();
            }
        }

        private static void TestJSONException()
        {
            JSON j = new JSON(new Exception("My Exception", new JSONException("InnerExcepiton", new InvalidCastException("Cast was invalid"))));
            Console.WriteLine("ORIGINAL");
            j.DoResponse();

            Console.WriteLine("EXCEPTION");
            new JSON(j.Exception).DoResponse();

            Console.WriteLine("TO EXCEPTION");
            new JSON(j.ToException()).DoResponse();

            j.Throw();
        }

        private static void SQLException()
        {
            try
            {
                QuickDB q = new QuickDB("DWH").Throw(true);
                q.Select("SELECT * FROM Whatever");
            }
            catch (Exception exp)
            {
                Console.WriteLine("ORIGINAL");
                JSON j = new JSON(exp);
                j.DoResponse();

                Console.WriteLine("EXCEPTION");
                new JSON(j.Exception).DoResponse();

                Console.WriteLine("TO EXCEPTION");
                new JSON(j.ToException()).DoResponse();
            }
        }

        private static void TestHttpsPost()
        {
            JSON h = new JSON("Authorization", "Bearer j/KG2ptPa7quwQ12Uea5zh_u3PtdQAtd");

            JSON response = JSON.Get("https://flow.ircp-it.com/ircp/api/2/sites", headers: h);
            response.DoResponse();

        }

        private static void JSONComparisons()
        {
            JSON j = new JSON("[1,2,1.5,1.5,2.0,2,2.01,\"2.0100\",0,00,-2]");
            for (int i = 0; i < j.Length; i++)
            {
                Console.WriteLine(j[i].String + " == " + (j[i + 1].String ?? "null") + ": " + (j[i] == j[i + 1]).ToString());
                Console.WriteLine(j[i].String + " != " + (j[i + 1].String ?? "null") + ": " + (j[i] != j[i + 1]).ToString());
                Console.WriteLine(j[i].String + " >  " + (j[i + 1].String ?? "null") + ": " + (j[i] >  j[i + 1]).ToString());
                Console.WriteLine(j[i].String + " >= " + (j[i + 1].String ?? "null") + ": " + (j[i] >= j[i + 1]).ToString());
                Console.WriteLine(j[i].String + " <  " + (j[i + 1].String ?? "null") + ": " + (j[i] <  j[i + 1]).ToString());
                Console.WriteLine(j[i].String + " <= " + (j[i + 1].String ?? "null") + ": " + (j[i] <= j[i + 1]).ToString());
                Console.WriteLine();
            }
        }

        private static void QuickHtmlTables()
        {
            QuickHtml tab = new QuickHtml("table");
            HtmlTable htab = new HtmlTable();

            QuickHtml rows = new QuickHtml();
            QuickHtml tr = new QuickHtml("tr");
            tr.Append(new QuickHtml("td", "Test"));
            tr.AppendTo(rows);
            rows.AppendTo(tab);
            rows.AppendTo(htab);
        }

        private static void Crypto_GetPassword()
        {
            string Password;
            //Password = Crypto.Encode("TestPassword");
            Password = Crypto.Encrypt("TestPassword");

            Console.WriteLine(String.IsNullOrEmpty(Crypto.GetPassword(null, null, null)));
            Console.WriteLine(Crypto.GetPassword("TestPassword", null, null));
            Console.WriteLine(Crypto.GetPassword(null, "VGVzdFBhc3N3b3Jk", null));
            Console.WriteLine(Crypto.GetPassword(null, null, "2f3mZ1dp8cG6IdOVyodE2h1GA1VXB+IEhKHXzB81ygZxOn8L6cNhzVvqcm5R5aoyMm2uHOLnf1z+UBcuW1ExUfWaTOL6yVdbQjYQyV1M1BiMULs1v55Nk5cQSHM3X8IX"));
            Console.WriteLine();

            Console.WriteLine(Crypto.GetPassword("Test.Password", "Test.EncodedPassword", "Test.EncryptedPassword"));
            Console.WriteLine(Crypto.GetPassword("Test.Password"));
        }

        private static void QuickDB_GetSchema()
        {
            QuickDB qDB = new QuickDB(@"Server=10.8.21.6;Database=CDP;User ID=CDPPortalAccount;EncodedPassword=fCxuRG42N3dtN2hGO35QdHJOMkU;");
            qDB.GetSchema("[Source].[dbo].[TEMP_TestCSV]");
            qDB.AddRow();

            qDB.AddTable("MyTable");
            qDB.AddTable("MyTable");

        }

        private static void StringExtensionUnderline()
        {
            Console.WriteLine("This is a test".Underline("=", 0));
            Console.WriteLine("NEXT LINE");
            Console.WriteLine("This is a test\n".Underline("=", -1));
            Console.WriteLine("NEXT LINE");
            Console.WriteLine("\nThis is not a test\nThis is a test\n".Underline("^", 2));
            Console.WriteLine("NEXT LINE");

        }

        private static void qDBReturnParameters()
        {
            QuickDB qDB = new QuickDB("WCT");
            qDB.SetParameter("loop", 1, ParamDir.Input);
            qDB.SetParameter("test", "val1234567", ParamDir.Output);
            qDB.SetParameter("return", "value", ParamDir.RetrunValue);

            Console.WriteLine("Loop: " + qDB.GetString("@loop"));
            Console.WriteLine("Test: " + qDB.GetString("@test"));
            Console.WriteLine("Return: " + qDB.GetString("@return"));

            int val = qDB.Update("SRC.dbo.test");
            if(qDB.HasErrors())
                Console.WriteLine(qDB.LastException.ToString());

            Console.WriteLine("Val: " + val);

            Console.WriteLine("Loop: " + qDB.GetString("@loop"));
            Console.WriteLine("Test: " + qDB.GetString("@test"));
            Console.WriteLine("Return: " + qDB.GetString("@return"));
        }

        private static void TestArgs()
        {
            ArgsHelper args = new ArgsHelper("?");
            //ArgsHelper args = new ArgsHelper("MyString","?");
            //ArgsHelper args = new ArgsHelper("help");

            args.DoHelp();

            //args.DoHelp("help");
        }

        private static void XLSTest()
        {
            //FileStream fs = File.Open(@"..\..\XLSX\DataDemo.xlsx", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //ExcelWorkbook xl = new ExcelWorkbook(fs);

            ExcelWorkbook xl = new ExcelWorkbook(@"..\..\XLSX\DataDemo.xlsx", true);
            foreach (IXLCell cell in xl.xlWorkbook.Worksheets.First().Rows(2,2).CellsUsed())
                Console.WriteLine("[" + cell.Value + "]");
        }

        private static void TestQHtml()
        {
            QuickHtml top = new QuickHtml("div");
            List<QuickHtml> list = new List<QuickHtml>();
            for (int i = 1; i <= 5; i++)
                list.Add(new QuickHtml("span").Attr("title", "Span" + i.ToString()).AppendTo(top));
            Console.WriteLine(top.ToString());
            Console.WriteLine();

            QuickHtml additional = new QuickHtml();
            additional.Append(new QuickHtml("span", "TEST1")).Append(new QuickHtml("span", "TEST1"));
            Console.WriteLine(additional.ToString());
            Console.WriteLine();

            QuickHtml Span3 = list[2];
            QuickHtml p = new QuickHtml("p").Css("text-align", "center");
            Span3.Ctrl.Parent.Controls.AddAt(Span3.Ctrl.Parent.Controls.IndexOf(Span3.Ctrl), p.Ctrl);
            p.Append(Span3);
            Span3.Append(additional);
            Console.WriteLine(top.ToString());
            Console.WriteLine();

            foreach (QuickHtml span in top)
                Console.WriteLine(span.ToString());
            Console.WriteLine();

            foreach (QuickHtml span in list)
                Console.WriteLine(span.ToString());
            Console.WriteLine();

            QuickHtml test = new QuickHtml("option", "Select One...");
            test.Attr(new JSON("style", "display:none;").Add("value", "").Add("selected", "selected").Add("disabled", "true"));
            Console.WriteLine(test.ToString());
            Console.WriteLine();

            Console.WriteLine(test.Attr().ToJSON());
            Console.WriteLine();
        }

        private static void QuickDB_OleDBExcel97()
        {
            //QuickDB qDB = new QuickDB(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=F:\Users\SLyttle\Documents\Repositories\SLy\TestApp\XLSX\DataDemo.xls;Extended Properties=Excel 8.0;");
            //QuickDB qDB = new QuickDB(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\cdpvmUNIData01\f$\DATA\UNI\PROD\Metadata\2018\201801\oeic trades 201801.xls;Extended Properties=Excel 8.0;");
            QuickDB qDB = new QuickDB(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\cdpvmUNIData01\c$\Program Files\Consolidata\oeic trades 201801.xls;Extended Properties=""Excel 8.0;HDR=NO;IMEX=1""");
            qDB.GetSchema();

            qDB.Select("SELECT * FROM [" + qDB.GetString("TABLE_NAME", 0) + "]");

            if (qDB.HasErrors())
                Console.WriteLine(qDB.LastException.ToString());
            else
            {
                Console.WriteLine(qDB.RowCount());
                foreach (QuickDB cell in qDB[0][1])
                    Console.WriteLine(cell.GetColumnName() + ": " + cell.GetString());
            }
        }

        private static void PasswordGen()
        {
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.UCase));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.LCase));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Numerals));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Symbols));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Specials));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Exotics));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Hex));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.LCaseHex));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.UCaseHex));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.AlphaNumeric));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.Complex));
            Console.WriteLine(Crypto.GeneratePassword(20, CharTypes.ALL));
        }

        private static void JSONCrypt()
        {
            JSON enc = new JSON("Test");
            JSON test = new JSON("key", "MyKey");
            test.Add("enc", enc.ToEncrypted(test["key"].String));
            test.Add("cod", enc.ToEncoded());
            test.Add("One", JSON.FromEncrypted(test["enc"].String, test["key"].String));
            test.Add("Two", JSON.FromEncoded(test["cod"].String));

            JSON j = JSON.Load(@"F:\Users\SLyttle\Documents\Repositories\SLy\TestApp\PPTX\demo.json");
            string crypt = j.ToEncrypted();
            string cryptKey = j.ToEncrypted("Key");
            j = JSON.FromEncrypted(crypt);
            JSON e = JSON.FromEncrypted(cryptKey, "Wrong");
            JSON k = JSON.FromEncrypted(cryptKey, "Key");
        }

        private static void JSONParents()
        {
            JSON a = new JSON("SubA1", new JSON("KeyA1", "ValA1")).Add("SubA2", new JSON("KeyA2", "ValA2"));
            JSON b = new JSON("SubB1", new JSON("KeyB1", "ValB1")).Add("SubB2", new JSON("KeyB2", "ValB2"));
            JSON ca = a["SubA1"];
            JSON cb = b["SubB1"];
            JSON arr = new JSON(a).Add(b);
            JSON dic = new JSON("A", a).Add("B", b);

            ca.Copy(cb);
        }

        private static void XLSXDemo()
        {
            string path = @"F:\Users\SLyttle\Documents\Repositories\SLy\TestApp\XLSX\";

            ExcelWorkbook xl = new ExcelWorkbook();
            xl.Load(path + "DemoPW.xlsx", "password");
            Console.WriteLine(xl.xlWorkbook.Worksheets.First().FirstCell().Value);
            
            //ExcelWorkbook xl = new ExcelWorkbook(JSON.Load(path + "demo.json"));
            //xl.Save(path + "Demo.xlsx");

            //QuickDB qDB = new QuickDB("DWH");
            //qDB.Select("SELECT 1 AS First, 2 AS Second, 3 AS Third; SELECT 'row' AS _header, 1 AS This, 2 AS That;");
            //JSON tables = new JSON(qDB.GetDataSet());

            //xl = new ExcelWorkbook(tables);
            //xl.Save(path + "DataDemo.xlsx");
        }

        private static void PPTXDemo()
        {
            string path = @"F:\Users\SLyttle\Documents\Repositories\SLy\TestApp\PPTX\";
            PowerpointPresentation pptx = new PowerpointPresentation(path + "DefaultTheme.pptx");
            pptx.Log.RecordLog = true;
            pptx.Log.VerboseOutput = true;
            pptx.GeneratePPTX(JSON.Load(path + "demo.json"));
            pptx.Save(path + "Demo.pptx");

            Console.WriteLine(pptx.Log.LOG.ToJSON());
        }

        private static void TestDecoding()
        {
            QuickDB qDB = new QuickDB("DWH");
            Console.WriteLine(qDB);

            string cipher = Crypto.Encode("Password");
            string decipher = Crypto.Decode(cipher);
            string another = Crypto.Decode("UGFzc3dvcmQ=");

            Console.WriteLine(cipher);
            Console.WriteLine(decipher);
            Console.WriteLine(another);
        }

        private static void TestAzureAPIRequest()
        {
            JSON headers = new JSON("Authorization", "Bearer AQABAAAAAAABlDrqfEFlSaui6xnRjX5E8zekdI57uzjsHC9NLn_oSDk32Hs7ci7T8PmkOfXf1ePHdqLNTLi-Nw-HqyXZFDy9v-Nm_-sKo0_p2uw4RKSX2yctwEBWfacgxpgyCDwcAuC9dDDBoWej5P_AGruLJ75EMl1bP6pQ6KkxdodsiLnXbF4oVe-dJ-RReemkh7Dc9WT772AZDl2HiNiHKunsLQ6ArZnj3udL8_R9opHc_bmq9a0asAKo27EFjf2_Tl1KwGdNl2tDZrU1sw63TRA2fzgLakLKqsyMGQfmoaQ08qpT1VCfaNgQD_8R13ANIZakHYluPqoKbyyvvzktdKEnGpNAYP66OqkW1P0m6sz5ARab4LK1EO_i7gT1Y3R-2Ct35anjHDRqdAvOKUH6Bi_aUCTRBx4eFOjMgHvToyp10Tr_abL5TVE5COdQveChWQ_a0SuvOzyLIq8ntbaDLTIMpc4RkVWPi0HpR0gjxgzgdZvlDwKniENezJOHjVOwgfTAAnCD3DkaCZ4VLk0_9GnnB9uY4V4MbCIE2_wF133JFjCC-J2YjJ63ycs9okDJLY1IcSt-HaMoj63mes5IugShmNTjIAA");
            JSON response = JSON.Load("https://graph.windows.net/me?api-version=1.6", headers);
            response.DoResponse();
        }

        private static void TestResponse1()
        {
            Console.WriteLine("Test1");
        }

        private static void TestResponse2()
        {
            Console.WriteLine("Test2");
        }

        private static void TestAjaxHelper()
        {
            AjaxHandler.Do("Test1",
                "Test1", (Action)TestResponse1,
                "Test2", (Action)TestResponse2
            );
            AjaxHandler.Do("Test2",
                "Test1", (Action)TestResponse1,
                "Test2", (Action)TestResponse2
            );
            AjaxHandler.Do("Test3",
                "Test1", (Action)TestResponse1,
                "Test2", (Action)TestResponse2
            );
            AjaxHandler.Do(null,
                "Test1", (Action)TestResponse1,
                "Test2", (Action)TestResponse2
            );
            AjaxHandler.Do(null,
                "Test1", (Action)TestResponse1,
                (Action)TestResponse2
            );
            AjaxHandler.Do("Test3",
                "Test1", (Action)TestResponse1,
                (Action)TestResponse2
            );
        }

        private static void TestArgsHelper()
        {
            ArgsHelper args = new ArgsHelper("-value", "1984-06-16", "-string:test", "-bool");

            Console.WriteLine(args.GetDateTime("value"));
            Console.WriteLine(args.GetString("string"));
            Console.WriteLine(args.GetStringOrBool("string"));
            Console.WriteLine(args.GetStringOrBool("bool"));
            Console.WriteLine(args.GetBool("bool"));

            Console.WriteLine(args.GetString(0));
            Console.WriteLine(args.GetString(1));
            Console.WriteLine(args.GetString(2));
            Console.WriteLine(args.GetString(3));
            Console.WriteLine(args.GetString(4));
            Console.WriteLine(args.GetString(5));
            Console.WriteLine(args.GetString(-1));
        }

        static void TestDataManipulation2()
        {
            QuickDB qDB = new QuickDB(@"Server=cdpvmIRCPData02\MVPUnitTest;Database=CDP;Trusted_Connection=True;");
            qDB.GetSchema("[CDP].[temp].[CompanyWatch_Test1]","CDP.temp.CompanyWatch_Test1","[CDP]..[CompanyWatch_Test1]","CompanyWatch_Test1");
            qDB.ImportMode = ImportMode.DropAndCreate;

            //qDB.SetValue("Dat1", "Col1");
            //qDB.SetValue("Dat3", "Col2");
            //qDB.SetValue("Dat3", "Col3");
            //qDB.Import();


            //qDB["feed.CompanyWatch_TEST"].AddRow().AddRow().AddRow();
            //qDB["feed.CompanyWatch_TEST"].FirstChild.FirstChild.SetValue("Row1Col1")
            //    .Next.SetValue("Row1Col2")
            //    .Next.SetValue("Row1Col3")
            //    .Next.SetValue("Row1Col4")
            //    .Next.SetValue("Row1Col5")
            //    .Parent.Next.LastChild.Prev.SetValue("Row2Col5")
            //    .Prev.SetValue("Row2Col4")
            //    .Prev.SetValue("Row2Col3")
            //    .Prev.SetValue("Row2Col2")
            //    .Parent.Prev["Source"].SetValue("Done");
            //qDB.Import();


            //foreach (QuickDB col in qDB[1][2].Where(q => q.Prev != null && q.Next != null))
            //    col.SetValue(col.GetColumnName().ToTitleCase());

            Console.WriteLine(new JSON(qDB["api.WebServiceLog"].Table).ToJSON());
        }

        static void TestDataManipulation()
        {
            QuickDB qDB = new QuickDB();
            Console.WriteLine(new JSON(qDB.GetDataSet()).ToJSON());
            qDB.SetTableNames("My First Table", "My Second Table");
            Console.WriteLine(new JSON(qDB.GetDataSet()).ToJSON());
            qDB.SetTableName("My 2nd Table", 1);
            Console.WriteLine(new JSON(qDB.GetDataSet()).ToJSON());
            qDB.SetColumnNames("Col 1", "Col 2", "Col 3");
            qDB.SetValue(null);
            Console.WriteLine(new JSON(qDB.GetDataSet()).ToJSON());
            qDB[1].SetColumnNames("My Col", "Another");
            qDB[1].SetValue(null);
            qDB[1].SetTableName("My Second Table");
            qDB.RemoveColumn("Col 2");
            Console.WriteLine(new JSON(qDB.GetDataSet()).ToJSON());
        }

        static void TestStringExtensions()
        {
            string test = null;
            //Regex test = new Regex("");
            Console.WriteLine("This\n is a  replacement \ttest".Replace(@"\s+", " ", true));
            Console.WriteLine("<b>This is a replacement test</b>".Replace(@"<([^)]+)>(.*)<\/\1>", "<strong>$2</strong> (not $1)", true));
            Console.WriteLine("This is a test".WordCount());
            Console.WriteLine("this is a test. oF SENTENCE? Case.".ToLower().ToSentenceCase());
            Console.WriteLine("This is a test of title case.".ToTitleCase());
            Console.WriteLine("thomas mcwilliams macloud o'donerly".ToNameCase());
            Console.WriteLine("This is a test and I like it".ToCamelCase());
            Console.WriteLine("ThisIsATestAndILikeIt".ToSentenceCase());
            Console.WriteLine("ThisIsATestAndILikeIt".BreakWords());
            Console.WriteLine("ThisIsATestAndILikeIt".ToSnakeCase());
            Console.WriteLine("This_Is_A_Test_And_ILikeIt".ToCamelCase());
            Console.WriteLine("ThisIsATestAndILikeIt".ToTitleCase());
            Console.WriteLine("ToCamelCase".ToCamelCase());
            Console.WriteLine("toPascalCase".ToPascalCase());
            Console.WriteLine("A".ToCamelCase());
            Console.WriteLine(test.ToCamelCase());
        }

        static void TestDataService()
        {
            //This callback replaces the default certificate validation and is used to ignore any certificate errors.
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate (Object sender1, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

            DataServiceClient DataSvc = new DataServiceClient();
            DataSvc.ClientCredentials.UserName.UserName = "mPSuDr35tn31bN";
            DataSvc.ClientCredentials.UserName.Password = "i+y~MyCs(n}D75|]wdt-(W/nxV$2.dr5BU^a=SvScEKQd6%-)R}a$8NXZl2K|f2j]|iV80pj#rK^NI9uQDAWdYfu/Z3]q{H5F8_ESsBYdYWLy3zE9-Ilfk0m$ZZ$XQL2";
            string[] test = DataSvc.GetAvailableDatasets();
            foreach (string str in test)
                Console.WriteLine(str);
            //int rows = DataSvc.ImportJSON("groups", "[{ GROP_REF: LiveTest1, GROP_TAX1_NUM:Test },{ GROP_REF: LiveTest2, GROP_AC_PERIODS: Test },{ GROP_REF: LiveTest3, GROP_TAX2_NUM: FAIL }]", true);
            DataSvc.Close();
        }

        static void TestConcat()
        {
            JSON Test = new JSON("[{Items:[a1,a2,a3]},{Items:[b1,b2,b3]},{Items:[c1,c2,c3]}]");
            JSON Concat = new JSON(Test.SelectMany(j => j["Items"]).ToArray());
            JSON First = JSON.Safe(Concat.FirstOrDefault(j => j.String == "a2"));
            //Concat.Filter("");

            JSON NULL = new JSON("{\"test\":null}");
            JSON first = new JSON("a", "1").Add("b", "2").Add("c", "3");
            JSON second = new JSON("d", "4").Add("e", "5").Add("f", "6");
            JSON third = new JSON("g", "7").Add("h", "8").Add("c", "9");
            JSON test = new JSON().Concat(new JSON(), first, new JSON(), second, third);
            test = new JSON(test.Where(j => j.Number % 2 == 0).ToDictionary(j => j.Key));
            test = new JSON().Concat(new JSON(), new JSON());
        }

        static void TestCallbackResponse(JSON response)
        {
            Console.WriteLine("Async Response");
            Console.WriteLine(response.ToJSON());

        }

        static void TestCallback()
        {
            Console.WriteLine("Request Response");
            JSON test = JSON.Load("http://localhost:50225/Utilities/IRCP/Flow/Debug.aspx", TestCallbackResponse);
            //test.DoResponse();

            Console.WriteLine("GET Response");
            //JSON.Get("http://localhost:50225/Utilities/IRCP/Flow/Debug.aspx").DoResponse();
            JSON.Get("http://localhost:50225/Utilities/IRCP/Flow/Debug.aspx", new JSON("Test", "Testing")).DoResponse();
            Console.WriteLine("POST Response");
            JSON.Post("http://localhost:50225/Utilities/IRCP/Flow/Debug.aspx", null).DoResponse();
            JSON.Post("http://localhost:50225/Utilities/IRCP/Flow/Debug.aspx?Test=Test", new JSON("Test", "Testing")).DoResponse();
        }

        static JSON TestPassByReference(JSON input)
        {
            JSON output = new JSON();
            output = input.Add("Another");
            output[-1].String += "One";
            return output;
        }

        static void TestEquality()
        {
            JSON test = new JSON();
            JSON source = new JSON();

            test.Add(source);
            source.Add("Second");
            test.Add(source.Copy());
            source.Add("Third");
            test.Add(source);
            JSON output = TestPassByReference(source);
            output.Add("Final");


            //WATCH TESTS
            //test[1] == source;
            //source == test[1];
            //source.Equals(test[1]);
            //source.Equals(test[0]);
            //object.ReferenceEquals(source, test[1]);
            //object.ReferenceEquals(source, test[0]);
            //source.Equals("[Second]");
            //source.Is(test[1]);
            //source.Is(test[0]);
        }

        static void TestSort()
        {
            JSON Test = new JSON("{a:5,d:4,c:3,e:2,b:1}");
            Test.DoResponse();
            Test.Sort();
            Test.DoResponse();

            JSON test = new JSON("{D:{X:{S:2}},C:{X:{S:1}},A:{Y:{S:4}},E:{X:{S:3}},B:{X:{S:5}}}");
            test.DoResponse();
            test.Sort();
            test.DoResponse();
            test.SortDesc();
            test.DoResponse();
            test.Sort("X.S");
            test.DoResponse();
            test.SortDesc("0.0");
            test.DoResponse();
        }

        static void TestDateTimeParsing()
        {
            JSON test = new JSON(DateTime.Today).Add(new JSON(DateTime.Now)).Add(new JSON(DateTime.Now.TimeOfDay));
        }

        static void TestDynamicAllocation()
        {
            JSON test = new JSON().Add("0").Add("1").Add("2");
            test.SetDynamic();
            test[5].String = "5";

            test = new JSON();
            test["One", true]["Two"]["Three"].String = "This Should Appear";
            test["One"]["Four"]["Five"].String = "This Should Too";
            test.SetDynamic();
            test["Six"]["Seven"]["Eight"].String = "This will appear";
            test["Six", false]["Nine"]["Ten"].String = "This should NOT.";
            Console.WriteLine(test.ToJSON());
        }

        static void TestQDBDefaults()
        {
            QuickDB qDB = new QuickDB("");
            QuickDB.DefaultString = "Test";
        }

        static void TestJSONMisc()
        {
            JSON test = new JSON(new Exception("Message1", new Exception("Message1.1", new Exception("Message1.1.1"))));
            test.Clear();

            test.Add("This/is/a/test", new JSON("[A,B,C]"), '/');
            test.Add("This/is/another/test", "[1,2,3]", '/');

            test = JSON.Load(@"C:\temp\test.jsonx");
            test.Clear();
        }

        static void TestQDBImport()
        {
            QuickDB qDB = new QuickDB(@"localhost", "RSAuthentication");
            JSON table = new JSON("[{col1:val11,col2:val12,col3:val13},{col1:val21,col2:val22,col3:val23}]");
            int rowNumber = 0;
            foreach (JSON row in table)
            {
                foreach (JSON col in row)
                {
                    qDB.SetValue(col.String, col.Key, rowNumber, "DBTableName");
                }
                rowNumber++;
            }

            String SQL = "CREATE TABLE DBTableName (";
            foreach (JSON col in table[0])
            {
                SQL += col.Key + " varchar(100),";
            }
            SQL = SQL.TrimEnd(',') + ");";

            qDB.Execute(SQL);

            qDB.Import("DBTableName");
        }

        static void TestJSONParsing()
        {
            JSON j = new JSON("{\"path\":\"C:\\\\testing\\\\works\",\"quote\":\"This \\\"contains\\\" a quote.\",\"special\":\"This\tcontains\nspecial/characters.");
            j.Add("path2", @"C:\testin\works");
            j["path3", true].String = @"c:\testing\works";

            JSON k = new JSON(j.ToString());
            k.Clear();
        }

        static void TestQDBStrings()
        {
            ////LEGACY - Moved to Utils String Extension.
            //String test = QuickDB.FromCamel("ILikeACamelText");
            //test = QuickDB.ToCamel(test.ToLower());
            //test = QuickDB.FromCamel(test);
            //test = QuickDB.ToCamel(test);

            //if (test == "") { }
        }

        static void qDB_InfoMessage(object sender, string message)
        {
            Console.WriteLine(message);
        }

        static void TestInfoMessage()
        {
            QuickDB qDB = new QuickDB("TEST");
            qDB.InfoMessage += qDB_InfoMessage;
            qDB.Timeout = 300;
            qDB.SetParameter("Loop", 5);
            qDB.SetParameter("Loop", 7);
            qDB.Execute("spTest");
        }

        static void TestNullJSONs()
        {
            bool b = false;
            b = new JSON(1) == null;
            b = null == new JSON(1);
            b = new JSON() == null;
            b = null == new JSON();
        }

        static void TestPivots()
        {
            JSON json = new JSON("[[11,12,13,14],[21,22,23,24],[31,32]");
            //JSON json = new JSON("{\"Male\":{\"Name\":\"Sean\",\"Age\":\"31\"},\"Female\":{\"Name\":\"Rachel\",\"Age\":\"26\",\"Hair\": \"Yes\"}}");
            //JSON json = new JSON(qDB.GetTable(2));
            json.Pivot("Stub");
            //json = new JSON(qDB.GetTable(2));
            json.Pivot("Invalid Key");
            //json = new JSON(qDB.GetTable(0));
            json.Pivot("Placeholder", "Text");
            json.Pivot();
            json.Pivot();
            json.Clear();
        }
    }
}
