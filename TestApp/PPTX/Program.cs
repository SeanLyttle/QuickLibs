﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Spreadsheet;
using Drawing = DocumentFormat.OpenXml.Drawing;
using Charts = DocumentFormat.OpenXml.Drawing.Charts;
using ClosedXML.Excel;
using SLy;
#endregion

namespace PPTXVisualiser
{
    class Program
    {
        #region Error Codes

        /*
         * 100: General information
         * 
         * 101: General exception fault.
         * 201: Null values retured in chart data.
         * 202: No chart data returned.
         * 301: Template slide does not exist.
         * 302: Element missing from template slide.
         * 
         * 401: Data count mismatch.
         * 
         */

        #endregion

        static bool verbose;
        static int kVisualisation = -1;

        static void Main(string[] args)
        {
            //Read commandline parameters
            bool help = args.Contains("-?") || args.Contains("/?");
            bool pptx = args.Contains("-k") || args.Contains("/k");
            bool json = args.Contains("-p") || args.Contains("/p");
            verbose = args.Contains("-v") || args.Contains("/v");
            JSON input = new JSON();

            if (json)
            {
                Log("Loading file: " + GetArg(args, "-p"));
                input = JSON.Load(GetArg(args, "-p"));
            }

            if (pptx)
            {
                Log("Loading from database: kVisualisation = " + GetArg(args, "-k"));
                kVisualisation = int.Parse(GetArg(args, "-k"));
            }

            //DEBUGGING
            //verbose = true;
            //input = JSON.Load(@"C:\temp\test.json");

            //Read piped input
            if (Console.IsInputRedirected)
            {
                Log("Reading piped file.");
                input = new JSON(Console.In.ReadToEnd());
            }

            //new QuickDB(@"KODA60GB", "VIS").Delete("DELETE FROM PPTX.Log;");

            //Do stuff.
            if (help)
                ReturnHelp();
            else if (!input.IsNull)
                GeneratePPTX(input);
            else if (kVisualisation != -1)
                FetchAndGeneratePPTX(kVisualisation);
            else
                ReturnHelp();

            //Pause
            //Console.WriteLine("Press any key to exit...");
            //Console.ReadKey();


        }

        #region Commandline features

        private static string GetArg(string[] args, string arg)
        {
            for (int i = 0; i < args.Length - 1; i++)
            {
                if (args[i].Replace('/', '-') == arg)
                    return args[i + 1]; 
            }
            return "";
        }

        private static void ReturnHelp()
        {
            Console.WriteLine("This program can run in one of two modes:");
            Console.WriteLine("1. Pass a filepath to JSON data (-p path) to generate visualisation.");
            Console.WriteLine("2. Pipe JSON visualisation data in to generate visualisation.");
            Console.WriteLine();
            Console.WriteLine("Command line options:");
            Console.WriteLine("  -?      : This help information.");
            Console.WriteLine("  -v      : Verbose logging - returns extra messages during runtime.");
            Console.WriteLine("  -p path : Generate a Visualisation using the given filepath (path).");
            Console.WriteLine("  -k kVis : Generate a Visualisation using the given kVisualisation.");
        }

        private static void FetchAndGeneratePPTX(int kVisualisation)
        {
            try
            {
                Log(String.Concat("Fetching Visualisation data for kVisualisation=", kVisualisation));
                QuickDB qDB = new QuickDB(@"KODA60GB\FFNdev", "VIS"); //Open data connection to KODA60GB
                qDB.Timeout = 3600;
                qDB.SetSelectParameter("kVisualisation", kVisualisation);
                DataSet presDataSet = qDB.Select("pptx.spFetchVisualisationData");

                /*  pptx.spFetchVisualisationData definition:

                    --Get PPTX File Data (table index 0)
                    SELECT
                        v.kVisualisation AS kPresentation,
                        v.Title,
                        '' AS Subject,
                        v.ChartType AS Category,
                        '' AS Tags,
                        '' AS Comments,
                        t.FilePath AS TemplateFilePath,
                        '\\KODA60GB\D$\DATA\FFN\DEV\Output\PPTX\' + v.ChartType + '-' + ChartFileName + '.pptx' AS OutputFilePath
                    FROM
                        [vis].[ufnVisualisationData] () v -- A table, similar to the old Loop File.
                        LEFT JOIN pptx.Template t ON t.ChartType = v.ChartType
                    WHERE
                        kVisualisation = @kVisualisation;



                    --Get Visualisation Data (table index 1)
                    SELECT
                        v.kVisualisation AS kSlide,
                        v.QuestionCode,
                        t.Template,

                        v.Title,
                        v.Subtitle,
                        v.MainQuestion,
                        v.Description,

                        '\\10.183.160.49\d$\DATA\FFN\DEV\References\Images\' + v.Images AS Images,
                        v.Indent,

                        --Base: <Slide.Respondants> online respondants aged <Slide.Age> (<Slide.Specifics>), <Slide.Wave>
                        ISNULL(CAST(v.MinRespondants AS varchar) + '-' + CAST(NULLIF(v.MaxRespondants,v.MinRespondants) AS varchar), CAST(v.MinRespondants AS varchar)) AS Respondants,
                        ISNULL(CAST(v.MinAge AS varchar) + '-' + CAST(NULLIF(v.MaxAge,v.MinAge) AS varchar), CAST(v.MinAge AS varchar)) AS Age,
                        v.Locale,
                        v.Wave,

                        'Reference for Consolidata: kVisualisation = ' + CAST(v.kVisualisation AS varchar) AS Notes,

                        ISNULL(v.Demog1,'No demographic title available.') AS Demographic,
                        v.Execution,
                        v.SerGroupCol,
                        v.CatGroupCol,
                        v.SeriesCol,
                        v.CategoryCol,
                        v.ValueCol,
                        v.ValueFormat AS [Format],

                        ISNULL(v.Transpose,CAST('False' as bit)) AS Transpose,
                        t.SlideSchema,
                        t.SheetsSchema,
                        v.SchemaColors
                    FROM   
                        [vis].[ufnVisualisationData] () v
                        LEFT JOIN pptx.Template t ON t.ChartType = v.ChartType
                    WHERE
                        v.kVisualisation = @kVisualisation;


                    --Append chart data where available (table indexes 2+)
                    DECLARE @SQL AS nvarchar(max);

                    SELECT
                        @SQL = v.Execution
                    FROM   
                        [vis].[ufnVisualisationData] () v
                    WHERE
                        v.kVisualisation = @kVisualisation;

                    EXEC sp_ExecuteSql @SQL;

                */

                if (qDB.HasErrors())
                    throw qDB.LastException;

                JSON presData = GetPresentationData(presDataSet);
                GeneratePPTX(presData);
            }
            catch (Exception e)
            {
                Log(101, "Error Generating Presentation: " + e.Message);
            }
        }


        #endregion

        #region DATA TO JSON

        /*************************************************************************************
        **************************************************************************************
        ***                                                                                ***
        ***   DDDD    AAA   TTTTT   AAA      TTTTT   OOO      JJJJJ   SSS    OOO   N   N   ***
        ***   D   D  A   A    T    A   A       T    O   O        J   S      O   O  NN  N   ***
        ***   D   D  AAAAA    T    AAAAA       T    O   O        J    SSS   O   O  N N N   ***
        ***   D   D  A   A    T    A   A       T    O   O     J  J       S  O   O  N  NN   ***
        ***   DDDD   A   A    T    A   A       T     OOO       JJ     SSS    OOO   N   N   ***
        ***                                                                                ***
        **************************************************************************************
        *************************************************************************************/

        /// <summary>
        ///     Returns a JSON containing all the information necessary to generate the PPTX.
        /// </summary>
        private static JSON GetPresentationData(DataSet presDataSet)
        {
            //Load Data into QuickDB object for easy interrogation.
            QuickDB qDB = new QuickDB(presDataSet);

            //Ensure the dataset has been populated
            if (!qDB.HasResult())
                throw new Exception("No Data Available. ", qDB.GetLastException());

            Log(String.Concat("Presentation data contains ", qDB.TableCount(), qDB.pl(" table", " tables"), "."));

            //Fetch placeholder data - split by kID.  For global placeholders kID = -1.
            //JSON meta = new JSON(qDB.GetTable(2)).Pivot("kID", true); //Legacy placeholders table replaced with columns in the Presentation and Slide table.

            //Prepare slide data.
            JSON slides = new JSON();
            if (qDB.RowCount(1) > 0) //Slides Table.
            {
                int DataTableIndex = 2; //First possile data table.

                //foreach slide
                for (int row = 0; row < qDB.RowCount(1); row++)
                {
                    int TableCount = qDB.TableCount() - 2;

                    Log(String.Concat("Preparing slide ", row + 1, " of ", qDB.RowCount(1), ". This slide uses ", TableCount, " data table", qDB.pl(".", "s.")));

                    JSON chartData = new JSON();
                    JSON additionalData = new JSON();

                    //Fetch Chart Data if it exists.
                    if (TableCount > 0)
                    {
                        Log(String.Concat("Fetching chart data from data table index: ", DataTableIndex));

                        //Fetch chartData and format it for use in the chart.
                        chartData = ExtractChartData(qDB, DataTableIndex, row);

                        Log(String.Concat("Fetching additional data from data table indicies: ", DataTableIndex, " to ", DataTableIndex + TableCount - 1));

                        //Fetch raw chartdata and additional data tables.
                        if (TableCount > 1)
                            for (int index = 1; index < TableCount; index++)
                                additionalData.Add(new JSON(qDB.GetTable(DataTableIndex + index), "-"));
                    }

                    Log("Constructing JSON data structure for slide.");

                    slides.Add(
                        new JSON(qDB.GetRow(row, 1))
                        //.Add("placeholders", meta[qDB.GetString("kSlide", row, 1)].Pivot("Placeholder", qDB.GetString("Language", "en-gb"))) //Default to English translations.
                        .Add("chartData", chartData)
                        .Add("additionalData", additionalData)
                        .Add("slideSchema", new JSON(qDB.GetString("SlideSchema", "[]", row, 1)))
                        .Add("sheetsSchema", new JSON(qDB.GetString("SheetsSchema", "[]", row, 1)))
                        .Remove("SlideSchema") //Remove string version of the JSON from the database.
                        .Remove("SheetsSchema") //Remove string version of the JSON from the database.
                    );

                    DataTableIndex += TableCount;
                }

                if (qDB.TableCount() > DataTableIndex)
                    Log(401, String.Concat("Total number of data tables (", qDB.TableCount() - 2, ") is greater than the number of tables used (", DataTableIndex - 2, "), the presentation may not appear as expected. Please ensure each slide has the correct TableCount for its data set."), LogType.Warning);
                else if (qDB.TableCount() < DataTableIndex)
                    Log(401, String.Concat("Total number of data tables (", qDB.TableCount() - 2, ") is less than the number of tables expected (", DataTableIndex - 2, "), the presentation may not appear as expected. Please ensure each slide has the correct TableCount for its data set."), LogType.Warning);
            }

            return new JSON(qDB.GetRow(0))
                .Add("slides", slides);
            //.Add("placeholders", meta[String.Concat("-", qDB.GetString("kPresentation"))].Pivot("Placeholder", qDB.GetString("Language", "en-gb"))); //Default to English translations.
        }

        /// <summary>
        ///     Returns a JSON relevent data from given table index for use in the chart.
        /// </summary>
        private static JSON ExtractChartData(QuickDB qDB, int dataTableIndex, int slideRow)
        {
            JSON data = new JSON().SetDynamic(true); //Allows you to write directly to keys that don't exist yet.
            JSON categories = new JSON().SetDynamic(true);
            JSON SeriesOrder = new JSON().SetDynamic(true);
            int nullCount = 0;
            int catBreakCount = 0;
            int serBreakCount = 0;

            //Catch errors generated in the data execution.
            if (!String.IsNullOrEmpty(qDB.GetString("Error_Number", 0, dataTableIndex)))
                return new JSON(qDB.GetRow(0, dataTableIndex), '_');

            //Get relevant column names
            string serGroupCol = qDB.GetString("SerGroupCol", slideRow, 1);
            string catGroupCol = qDB.GetString("CatGroupCol", slideRow, 1);
            string[] categoryCols = qDB.GetString("CategoryCol", slideRow, 1).Split(',');
            string[] seriesCols = qDB.GetString("SeriesCol", slideRow, 1).Split(',');
            List<string> valueCols = new List<string>();
            List<string> methods = new List<string>();

            //Do a quick bit of manipulation to extract the methods from each of the Value Expressions.
            foreach (string expression in qDB.GetString("ValueCol", slideRow, 1).Split(','))
            {
                if (expression.Contains('('))
                {
                    methods.Add(expression.Split('(')[0].ToUpper());
                    valueCols.Add(expression.Split('(')[1].Replace(")", ""));
                }
                else
                {
                    methods.Add("SUM");//Default
                    valueCols.Add(expression);
                }
            }

            Log(String.Concat("Formatting Data - Categories: ", String.Join(", ", categoryCols), " - Series: ", String.Join(", ", seriesCols), " - Values: ", String.Join(", ", valueCols.ToArray()), " - Methods: ", String.Join(", ", methods.ToArray())));

            //Keep track of each value retrieved. (ValueCol will repeat)
            int valueCount = 0;
            //string lastGroup = qDB.GetString(groupCol.Trim(), 0, dataTableIndex);

            JSON CurrentSeriesOrder = new JSON().SetDynamic(true);

            //For each data row
            for (int i = 0; i < qDB.RowCount(dataTableIndex); i++)
            {
                foreach (string categoryCol in categoryCols)
                {
                    foreach (string seriesCol in seriesCols)
                    {
                        string valueCol = valueCols[valueCount % valueCols.Count];
                        string method = methods[valueCount++ % methods.Count];

                        //Get relevant data values
                        string serGroup = qDB.GetString(serGroupCol.Trim(), i, dataTableIndex);
                        string catGroup = qDB.GetString(catGroupCol.Trim(), i, dataTableIndex);
                        string category = qDB.GetString(categoryCol.Trim(), null, i, dataTableIndex); //null used to be "_"
                        string series = qDB.GetString(seriesCol.Trim(), null, i, dataTableIndex); //null used to be "_"
                        JSON value = new JSON(qDB.GetString(valueCol, null, i, dataTableIndex));

                        //Add gap between category groups (using null data)
                        if (string.IsNullOrEmpty(category))
                            category = string.Concat("_break", ++catBreakCount);

                        //Add gap between series groups (using null data)
                        if (string.IsNullOrEmpty(series))
                            series = string.Concat("_break", ++serBreakCount);

                        if (value.IsNull)
                            nullCount++;

                        //Record values in JSON
                        switch (method)
                        {
                            case "LAST":
                                data[seriesCol + serGroup][series][categoryCol + catGroup][category] = value;
                                break;
                            case "FIRST":
                                if (data[seriesCol + serGroup][series][categoryCol + catGroup][category].IsNull)
                                    data[seriesCol + serGroup][series][categoryCol + catGroup][category] = value;
                                break;
                            case "MAX":
                                if (value > data[seriesCol + serGroup][series][categoryCol + catGroup][category])
                                    data[seriesCol + serGroup][series][categoryCol + catGroup][category] = value;
                                break;
                            case "MIN":
                                if (value < data[seriesCol + serGroup][series][categoryCol + catGroup][category])
                                    data[seriesCol + serGroup][series][categoryCol + catGroup][category] = value;
                                break;
                            case "SUM":
                                data[seriesCol + serGroup][series][categoryCol + catGroup][category].Number += value.Number;
                                break;
                            default:
                                Log(100, String.Concat("[kSlide: ", qDB.GetString("kSlide", slideRow, 1), ", Title: ", qDB.GetString("Title", slideRow, 1), "]: Invalid method \"", method, "\" in ValueCol for data table index ", dataTableIndex, ". Using \"SUM\" instead."), LogType.Warning);
                                data[seriesCol + serGroup][series][categoryCol + catGroup][category].Number += value.Number;
                                break;
                        }

                        //Build a definitive list of categories across series.
                        categories[categoryCol + catGroup][category] = new JSON();
                        //Build a definitive list of series in order.
                        if (CurrentSeriesOrder[0].Key != category)
                            CurrentSeriesOrder.Clear();
                        CurrentSeriesOrder[category][seriesCol + serGroup][series] = new JSON();
                    }
                }

                //Assume that the longest Series order contains all of the available series.
                if (CurrentSeriesOrder[0].ToString().Length > SeriesOrder.ToString().Length)
                    SeriesOrder = CurrentSeriesOrder[0];
            }

            if (nullCount > 0)
            {
                if (nullCount == qDB.RowCount(dataTableIndex))  //Log error.
                    Log(201, String.Concat("[kSlide: ", qDB.GetString("kSlide", slideRow, 1), ", Title: ", qDB.GetString("Title", slideRow, 1), "]: All records returned a null value. Please ensure the column names \"", String.Join("\", \"", valueCols.ToArray()), "\" are correct for data table index ", dataTableIndex, "."), LogType.Error);
                else //Log warning.
                    Log(201, String.Concat("[kSlide: ", qDB.GetString("kSlide", slideRow, 1), ", Title: ", qDB.GetString("Title", slideRow, 1), "]: A total of ", nullCount, " out of ", qDB.RowCount(dataTableIndex), " rows returned a null. Please ensure this is the correct behaviour for the column names \"", String.Join("\", \"", valueCols.ToArray()), "\" in data table index ", dataTableIndex, "."), LogType.Warning);
            }

            //Normalise the categories across each series (in case there are differences)
            foreach (JSON SeriesGroup in data)
            {
                foreach (JSON Series in SeriesGroup)
                {
                    //Create new categories structure from the definitive list of categories.
                    JSON newCategories = categories.Copy();

                    //Copy the data from this set of categories in to the definitive structure.
                    foreach (JSON CategoryGroup in Series)
                        foreach (JSON Category in CategoryGroup)
                            newCategories[CategoryGroup.Key][Category.Key].String = Category.String;

                    //Replace this list with the new list.
                    SeriesOrder[SeriesGroup.Key][Series.Key].Copy(newCategories);
                }
            }

            //Turn off dynamic allocation so that checks on data do not create null values.
            SeriesOrder.SetDynamic(false);

            return SeriesOrder;
        }

        #endregion

        #region JSON TO PPTX

        /*************************************************************************************
        **************************************************************************************
        ***                                                                                ***
        ***   JJJJJ   SSS    OOO   N   N     TTTTT   OOO      PPPP   PPPP   TTTTT  X   X   ***
        ***      J   S      O   O  NN  N       T    O   O     P   P  P   P    T     X X    ***
        ***      J    SSS   O   O  N N N       T    O   O     PPPP   PPPP     T      X     ***
        ***   J  J       S  O   O  N  NN       T    O   O     P      P        T     X X    ***
        ***    JJ     SSS    OOO   N   N       T     OOO      P      P        T    X   X   ***
        ***                                                                                ***
        **************************************************************************************
        *************************************************************************************/

        /// <summary>
        ///     Generates a PPTX file from the given JSON presentation data.
        /// </summary>
        private static void GeneratePPTX(JSON presData)
        {
            //Log template file path.
            Log(String.Concat("Using template file \"", presData["TemplateFilePath"].String, "\""));

            if (File.Exists(presData["TemplateFilePath"].SafeString))
            {
                //Create directory if it doesn't already exist.
                if (presData["OutputFilePath"].String.Contains("\\") && !Directory.Exists(presData["OutputFilePath"].String.Substring(0, presData["OutputFilePath"].String.LastIndexOf('\\'))))
                {
                    Log(String.Concat("Creating output directory \"", presData["OutputFilePath"].String.Substring(0, presData["OutputFilePath"].String.LastIndexOf('\\')), "\""));
                    Directory.CreateDirectory(presData["OutputFilePath"].String.Substring(0, presData["OutputFilePath"].String.LastIndexOf('\\')));
                }
                else
                    Log(String.Concat("Output directory already exists."));

                //Log output file path.
                Log(String.Concat("Copying template to \"", presData["OutputFilePath"].String, "\""));

                //Copy the template presentation to working file.
                File.Copy(presData["TemplateFilePath"].SafeString, presData["OutputFilePath"].SafeString, true);

                Log(String.Concat("Opening working file."));

                //Open the working file.
                PresentationDocument pptx = PresentationDocument.Open(presData["OutputFilePath"].SafeString, true);
                if (pptx == null)
                    throw new ArgumentNullException("PresentationDocument", "Unable to access PresentationDocument: " + presData["OutputFilePath"].SafeString);

                //Reference the presentation XML
                PresentationPart pptxPart = pptx.PresentationPart;

                try
                {
                    if (pptxPart == null)// || pptxPart.Presentation.SlideIdList == null)
                        throw new InvalidOperationException("The template presentation document was empty.");

                    //Generate Slides from JSON slide data.
                    foreach (JSON slide in presData["slides"])
                        GenerateSlide(pptxPart, slide, presData);

                    //Update Presentation Properties
                    pptx.PackageProperties.Title = ReplacePlaceholders(pptx.PackageProperties.Title, new JSON(), presData); //Title
                    pptx.PackageProperties.Subject = ReplacePlaceholders(pptx.PackageProperties.Subject, new JSON(), presData); //Subject
                    pptx.PackageProperties.Category = ReplacePlaceholders(pptx.PackageProperties.Category, new JSON(), presData); //Category
                    pptx.PackageProperties.Keywords = ReplacePlaceholders(pptx.PackageProperties.Keywords, new JSON(), presData); //Tags
                    pptx.PackageProperties.Description = ReplacePlaceholders(pptx.PackageProperties.Description, new JSON(), presData); //Comments
                }
                catch (Exception exp)
                {
                    AddSlideToPresentation(pptxPart,
                        CreateErrorSlide(pptxPart, exp)
                    );
                }

                //Remove teplate slides
                RemoveTemplateSlides(pptxPart);

                //Log saving.
                Log(String.Concat("Saving changes to file: \"", presData["OutputFilePath"].String, "\""));

                // Save and close the modified presentation.
                pptxPart.Presentation.Save();
                pptx.Close(); //nb. failure to close document results in a corrupted file - alternatively, wrap the everything between open and close in a Using scope. (Took 3 horus to figure that out!)

                //Log completion.
                Log(String.Concat("Process complete for file: \"", presData["OutputFilePath"].String, "\""));
            }
            else
                throw new FileNotFoundException("Template file does not exist: \"" + presData["TemplateFilePath"].SafeString + "\" for ChartType: \"" + presData["ChartType"].SafeString + "\"");
        }

        /// <summary>
        ///     Returns the template slide with the given name (if no name is given, the first template slide will be returned).
        /// </summary>
        private static SlidePart GetTemplateSlide(PresentationPart pptxPart, string templateName)
        {
            Log(String.Concat("Fetching template slide: \"", templateName, "\""));
            //SlidePart lastSlide = null;

            if (pptxPart.Presentation.SlideIdList != null)
            {
                foreach (SlideId slide in pptxPart.Presentation.SlideIdList)
                {
                    SlidePart slidePart = (SlidePart)pptxPart.GetPartById(slide.RelationshipId);
                    if (slidePart.NotesSlidePart != null && slidePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template" + (String.IsNullOrEmpty(templateName) ? "" : "=" + templateName.ToLower())))
                        return slidePart;

                    //lastSlide = slidePart;
                }
            }

            return null; //lastSlide;
        }

        /// <summary>
        ///     Generates a single presentation slide based on JSON slide data and inserts it into the given Presentation.
        /// </summary>
        private static void GenerateSlide(PresentationPart pptxPart, JSON slideData, JSON presData)
        {
            try
            {
                // Fetch templateSlidePart
                SlidePart templateSlidePart = GetTemplateSlide(pptxPart, slideData["Template"].SafeString);

                //Check template exists.
                if (templateSlidePart == null)
                    throw new InvalidDataException(String.Concat("Unable to generate slide: ", slideData["Title"].String, " [", slideData["kSlide"].String, "]\nTemplate slide \"", slideData["Template"].String, "\" does not exist."));

                //Create a new slide based on the given template if available.
                AddSlideToPresentation(pptxPart,
                    CreateTemplateSlide(pptxPart, templateSlidePart, slideData, presData)
                );
            }
            catch (Exception exp)
            {
                AddSlideToPresentation(pptxPart,
                    CreateErrorSlide(pptxPart, exp)
                );
            }
        }

        /// <summary>
        /// Adds the given slide to the given presentation.
        /// This process will create a slide list if one does not already exist, and 
        /// </summary>
        private static void AddSlideToPresentation(PresentationPart pptxPart, Slide slide)
        {
            //Fetch a reference to the presentation slides list and creat one if it's not already there.
            SlideIdList slideIdList = pptxPart.Presentation.SlideIdList;
            if (slideIdList == null)
                slideIdList = pptxPart.Presentation.SlideIdList = new SlideIdList();


            // Find the highest slide ID in the current list and pick the first template slide in the process.
            uint newSlideId = 256; //Minimum SlideID value must be less than or equal to 256.
            SlideId firstTemplate = null;

            foreach (SlideId slideId in slideIdList.ChildElements)
            {
                if (slideId.Id >= newSlideId)
                    newSlideId = slideId.Id + 1;

                SlidePart templatePart = (SlidePart)pptxPart.GetPartById(slideId.RelationshipId);
                if (firstTemplate == null && templatePart.NotesSlidePart != null && templatePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template"))
                    firstTemplate = slideId;
            }

            //If current slide has no loayout part defined, use the same layout as the first slide in the presentation. (Fixes CreateErrorSlide).
            if (slide.SlidePart.SlideLayoutPart == null)
                slide.SlidePart.AddPart(((SlidePart)pptxPart.GetPartById(((SlideId)(slideIdList.ChildElements[0])).RelationshipId)).SlideLayoutPart);

            //Insert the new slide into the slide list before the first template slide (if firstTemplate is null, this will Append instead).
            slideIdList.InsertBefore(
                new SlideId() { Id = newSlideId, RelationshipId = pptxPart.GetIdOfPart(slide.SlidePart) },
                firstTemplate
            );
        }

        /// <summary>
        ///     Removes all "_template" slides from the given presentation.
        /// </summary>
        private static void RemoveTemplateSlides(PresentationPart pptxPart)
        {
            Log("Removing template slide parts:");

            List<SlideId> templateSlides = new List<SlideId>();
            foreach (SlideId slide in pptxPart.Presentation.SlideIdList)
            {
                SlidePart slidePart = (SlidePart)pptxPart.GetPartById(slide.RelationshipId);
                if (slidePart.NotesSlidePart != null && slidePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template"))
                {
                    Log(slidePart.NotesSlidePart.NotesSlide.Descendants<Drawing.Paragraph>().Where(p => p.InnerText.Contains("_template")).First().InnerText);
                    templateSlides.Add(slide);
                    pptxPart.DeletePart(slidePart);
                }
            }

            foreach (SlideId slide in templateSlides)
                slide.Remove();
        }


        //Create slides

        /// <summary>
        ///     Creates a new blank slide from scratch containing the error message.
        /// </summary>
        private static Slide CreateErrorSlide(PresentationPart pptxPart, Exception exp)
        {
            Log(101, "Creating error slide: " + exp.Message);

            //Slide contents:
            string TitleText = "Error generating slide";
            string BodyText = exp.Message;

            //Everything else is just generating the slide.
            uint drawingObjectId = 1;

            // Declare and instantiate a new slide.
            Slide slide = new Slide();
            slide.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");
            slide.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            slide.AddNamespaceDeclaration("p", "http://schemas.openxmlformats.org/presentationml/2006/main");

            slide.Append(
                new CommonSlideData(
                    new ShapeTree(
                        new NonVisualGroupShapeProperties(
                            new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "" },
                            new NonVisualGroupShapeDrawingProperties(),
                            new ApplicationNonVisualDrawingProperties()
                        ),
                        new GroupShapeProperties(
                            new Drawing.TransformGroup(
                                new Drawing.Offset() { X = 0L, Y = 0L },
                                new Drawing.Extents() { Cx = 0L, Cy = 0L },
                                new Drawing.ChildOffset() { X = 0L, Y = 0L },
                                new Drawing.ChildExtents() { Cx = 0L, Cy = 0L }
                            )
                        )
                    )
                ),
                new ColorMapOverride(
                    new Drawing.MasterColorMapping()
                )
            );


            // Declare and instantiate the title shape of the new slide.
            slide.CommonSlideData.ShapeTree.AppendChild(
                new Shape(
                    new NonVisualShapeProperties(
                        new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "Title" },
                        new NonVisualShapeDrawingProperties(
                            new Drawing.ShapeLocks() { NoGrouping = true }
                        ),
                        new ApplicationNonVisualDrawingProperties(
                            new PlaceholderShape() { Type = PlaceholderValues.Title }
                        )
                    ),
                    new ShapeProperties(
                        new Drawing.Transform2D(
                            new Drawing.Offset() { X = 287524L, Y = 332656L },
                            new Drawing.Extents() { Cx = 8568952L, Cy = 684076L }
                        )
                    ),
                    new TextBody(
                        new Drawing.BodyProperties(),
                        new Drawing.ListStyle(),
                        new Drawing.Paragraph(
                            new Drawing.Run(
                                new Drawing.RunProperties(
                                    new Drawing.SolidFill(
                                        new Drawing.RgbColorModelHex() { Val = "FF0000" } //RED
                                    )
                                ) { Dirty = false },
                                new Drawing.Text(TitleText)
                            )
                        )
                    )
                )
            );

            // Declare and instantiate the body shape of the new slide.
            slide.CommonSlideData.ShapeTree.AppendChild(
                new Shape(
                    new NonVisualShapeProperties(
                        new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "Content Placeholder" },
                        new NonVisualShapeDrawingProperties(
                            new Drawing.ShapeLocks() { NoGrouping = true }
                        ),
                        new ApplicationNonVisualDrawingProperties(
                            new PlaceholderShape() { Index = (UInt32Value)1U }
                        )
                    ),
                    new ShapeProperties(
                        new Drawing.Transform2D(
                            new Drawing.Offset() { X = 287524L, Y = 1016732L },
                            new Drawing.Extents() { Cx = 8568952L, Cy = 5256584L }
                        )
                    ),
                    new TextBody(
                        new Drawing.BodyProperties(),
                        new Drawing.ListStyle(),
                        new Drawing.Paragraph(
                            new Drawing.Run(
                                new Drawing.RunProperties(
                                    new Drawing.SolidFill(
                                        new Drawing.RgbColorModelHex() { Val = "000000" } //BLACK
                                    )
                                ) { FontSize = 1200, Dirty = false },
                                new Drawing.Text(BodyText)
                            )
                        )
                    )
                )
            );


            // Create the slide part for the new slide.
            SlidePart slidePart = pptxPart.AddNewPart<SlidePart>();


            // Save the new slide part.
            slide.Save(slidePart);
            return slidePart.Slide;
        }

        /// <summary>
        ///     Create a new slide based on the contents of the given template slide.
        /// </summary>
        private static Slide CreateTemplateSlide(PresentationPart pptxPart, SlidePart templateSlidePart, JSON slideData, JSON presData)
        {
            Log("Creating a new slide from template.");

            ChartPart chartPart = null;

            // Copy template to a new slide
            Slide slide = (Slide)templateSlidePart.Slide.CloneNode(true);

            // Create new slide part to copy into
            SlidePart slidePart = pptxPart.AddNewPart<SlidePart>();

            try
            {
                // Copy layout part
                slidePart.AddPart(templateSlidePart.SlideLayoutPart);

                // Copy the image parts only if they are reqired. TODO: Hanle multiple images better?

                JSON ImageFiles = new JSON("[" + slideData["Images"].SafeString + "]");

                foreach (ImagePart part in templateSlidePart.ImageParts)
                {
                    //Grab next filepath from list.
                    string ImageFilePath = ImageFiles[true].String;
                    ImagePartType ImageType = ImagePartType.Icon; //Use ICON as "Invalid Type"

                    //Ensure file exists
                    if (File.Exists(ImageFilePath))
                    {
                        switch (new FileInfo(ImageFilePath).Extension.ToLower())
                        {
                            case ".bmp": ImageType = ImagePartType.Bmp; break;
                            case ".emf": ImageType = ImagePartType.Emf; break;
                            case ".gif": ImageType = ImagePartType.Gif; break;
                            //case ".ico": ImageType = ImagePartType.Icon; break;
                            case ".jpg": ImageType = ImagePartType.Jpeg; break;
                            case ".jpeg": ImageType = ImagePartType.Jpeg; break;
                            case ".pcx": ImageType = ImagePartType.Pcx; break;
                            case ".png": ImageType = ImagePartType.Png; break;
                            case ".tif": ImageType = ImagePartType.Tiff; break;
                            case ".wmf": ImageType = ImagePartType.Wmf; break;
                            default:
                                Log(402, "Unsupported image file type: [" + slideData["kSlide"] + "] " + ImageFilePath);
                                break;
                        }
                    }


                    //If file exists and image type is valid, embed it - otherwise remove the placeholder image.
                    if (File.Exists(ImageFilePath) && ImageType != ImagePartType.Icon)
                    {
                        //Embed the image data and copy the reference from the template.
                        ImagePart newPart = slidePart.AddImagePart(ImageType, templateSlidePart.GetIdOfPart(part));
                        newPart.FeedData(new FileStream(ImageFilePath, FileMode.Open));
                    }
                    else
                    {
                        //Find and remove picture from the slide.
                        foreach (DocumentFormat.OpenXml.Presentation.Picture pic in slide.Descendants<DocumentFormat.OpenXml.Presentation.Picture>())
                            if (pic.Descendants<Drawing.Blip>().FirstOrDefault().Embed.Value == templateSlidePart.GetIdOfPart(part))
                                pic.Remove();
                    }
                }

                // Copy the chart parts
                foreach (ChartPart part in templateSlidePart.ChartParts)
                {
                    ChartPart newPart = slidePart.AddNewPart<ChartPart>(templateSlidePart.GetIdOfPart(part));
                    newPart.FeedData(part.GetStream());
                }

                // Copy the notes part
                NotesSlidePart notesPart = slidePart.AddNewPart<NotesSlidePart>(templateSlidePart.NotesSlidePart.ContentType, templateSlidePart.GetIdOfPart(templateSlidePart.NotesSlidePart));
                notesPart.FeedData(templateSlidePart.NotesSlidePart.GetStream());

                // and delete the "_template" reference
                List<Drawing.Paragraph> templatePara = new List<Drawing.Paragraph>();

                foreach (Drawing.Paragraph para in notesPart.NotesSlide.Descendants<Drawing.Paragraph>())
                    if (para.InnerText.Contains("_template"))
                        templatePara.Add(para);

                foreach (Drawing.Paragraph para in templatePara)
                    para.Remove();

                // An empty NotesSlidePart causes an error, so remove it
                if (notesPart.NotesSlide.Descendants<Drawing.Run>().Count() == 0)
                    slidePart.DeletePart(slidePart.NotesSlidePart);

                // Fetch a reference to the first chart part on the templat slide if it exists. (Only handling for one chart per slide.)
                if (slidePart.ChartParts.Count<ChartPart>() > 0)
                    chartPart = slidePart.ChartParts.First();

                //slidePart.DeletePart(chartPart);

                // Save the new slide part to link slidePart and Slide.
                slide.Save(slidePart);

                // Replace placeholders
                ReplaceSlidePlaceholders(slidePart.Slide, slideData, presData);

                //Generate error slide if chartData contains errors.
                if (!slideData["chartData"]["Error"].IsNull)
                    throw new InvalidDataException("Invalid Data: [" + slideData["kSlide"] + "] " + slideData["Execution"] + "\n" + slideData["chartData"]["Error"]["Message"].SafeString);

                //Log warning if chart will be blank.
                if (slideData["chartData"].IsNull)
                    Log(202, String.Concat("[kSlide: ", slideData["kSlide"].String, ", Title: ", slideData["Title"].String, "]: This template slide contains a chart, but no chart data was returned."), LogType.Warning);

                //Generate Spreadsheet from data
                if (chartPart != null)
                    GenerateChart(chartPart, slideData, presData);
                else if (!slideData["chartData"].IsNull)
                    Log(302, String.Concat("[kSlide: ", slideData["kSlide"].String, ", Title: ", slideData["Title"].String, "]: This slide contains chart data, but no chart was found on the template slide."), LogType.Warning);
            }
            catch (Exception exp)
            {
                //clean up unfinished slide.
                pptxPart.DeletePart(slidePart);
                throw new Exception(exp.Message + " [" + slideData["kSlide"].String + "]");
            }

            return slidePart.Slide;
        }

        /// <summary>
        ///     Find all placeholders ("&lt;name&gt;") within the given slide and replace them with the meta data of the same name (case sensitive).
        /// </summary>
        private static void ReplaceSlidePlaceholders(Slide slide, JSON slideData, JSON presData)
        {
            //Regular expression to find placeholder tags.
            Regex rex = new Regex("<([^>]*)>");
            //Iterate through the paragraphs in the slide AND the notes.
            foreach (Drawing.Paragraph para in slide.Descendants<Drawing.Paragraph>().Concat(slide.SlidePart.NotesSlidePart.NotesSlide.Descendants<Drawing.Paragraph>()))
            {
                if (rex.IsMatch(para.InnerText))
                {
                    //Fetch plain text from each paragraph (this could be fragmented over numerous Drawing.Text objects).
                    string txt = para.Descendants<Drawing.Text>().Aggregate("", (s, t) => s + t.Text);

                    //Replace placeholders.
                    txt = ReplacePlaceholders(txt, slideData, presData);

                    //Place the new text back into the first text object and delete the rest.
                    foreach (Drawing.Text text in para.Descendants<Drawing.Text>())
                        if (text == para.Descendants<Drawing.Text>().First())
                            text.Text = txt;
                        else
                            text.Text = ""; //Deleting text objects caused an error, so for now, I've cleared the text instead.
                }
            }
        }


        //Create Chart / Datasheets

        /// <summary>
        ///     Generates the given chart based on the given JSON chart data.
        /// </summary>
        private static void GenerateChart(ChartPart chartPart, JSON slideData, JSON presData)
        {
            JSON chartData = slideData["chartData"];
            JSON chartSchema = slideData["slideSchema"]["chart"];
            JSON seriesSchema = chartSchema["seriesSchema"];
            JSON schemaColors = new JSON("[" + slideData["SchemaColors"].SafeString + "]");
            JSON additionalData = slideData["additionalData"];
            JSON sheetsSchema = slideData["sheetsSchema"];
            string numFormat = slideData["Format"].SafeString;
            bool Transpose = slideData["Transpose"].Boolean;

            double cellWidth = 12.14; //90px;


            //////////////////////////////
            // ADDITIONAL EMBEDDED DATA //
            //////////////////////////////

            XLWorkbook xl = new XLWorkbook();
            IXLWorksheet chartSheet = null;
            int chartRow = 1;
            int chartCol = 1;

            List<int> processedData = new List<int>();

            //Generate sheets from schema first
            foreach (JSON sheetDef in sheetsSchema)
            {
                Log(String.Concat("Creating sheet: ", sheetDef.Key));

                //Keep track of relative positioning.
                int deltaRow = 0;

                //Select existing or create new worksheet.
                IXLWorksheet sheet = xl.Worksheets.Where(w => w.Name == sheetDef.Key).FirstOrDefault();
                if (sheet == null)
                    sheet = xl.AddWorksheet(sheetDef.Key);


                //Alter specific ranges from the sheet definition.
                foreach (JSON rangeDef in sheetDef)
                {
                    IXLRange range = sheet.Range(rangeDef.Key);
                    if (range != null)
                    {
                        //Shift current selection down if other previous elements used more than one row.
                        if (!rangeDef.Key.Contains("$"))
                            range = sheet.Range(range.FirstCell().CellBelow(Math.Max(deltaRow,0)), range.LastCell().CellBelow(Math.Max(deltaRow,0)));

                        IXLCell cell = range.FirstCell();
                        IXLCell lastCell = range.LastCell();

                        //Set cell text/formula
                        if (rangeDef.IsString)
                            lastCell = PopulateCells(range, ReplacePlaceholders(rangeDef.String, slideData, presData));
                        else if (rangeDef["text"].IsString)
                            lastCell = PopulateCells(range, ReplacePlaceholders(rangeDef["text"].String, slideData, presData));
                        else if (rangeDef["data"].IsInteger)
                        {
                            if (rangeDef["data"].Integer == 0)
                            {
                                chartSheet = sheet;
                                chartRow = cell.Address.RowNumber;
                                chartCol = cell.Address.ColumnNumber;
                                lastCell = CalculateLastChartDataCell(cell, chartData, Transpose);
                            }
                            else
                            {
                                lastCell = PopulateCells(cell, additionalData[(int)rangeDef["data"].Integer - 1], numFormat, cellWidth);
                                processedData.Add((int)rangeDef["data"].Integer - 1);
                            }
                        }

                        //Update range if necessary.
                        if (range.LastCell() != lastCell)
                            range = sheet.Range(cell, lastCell);

                        //Add totals
                        if (!rangeDef["totals"].IsNull && range.RowCount() > 1)
                        {
                            //Set up defaults
                            string formula = "=SUM()";
                            string title = "Total";
                            int space = 1;

                            //Or use defined values if given.
                            if (rangeDef["totals"]["formula"].IsString)
                                formula = rangeDef["totals"]["formula"].String;
                            if (rangeDef["totals"]["title"].IsString)
                                title = rangeDef["totals"]["title"].String;
                            if (rangeDef["totals"]["space"].IsInteger)
                                space = (int)rangeDef["totals"]["space"].Integer;

                            if (!formula.StartsWith("="))
                                formula = String.Concat("=", formula);

                            int row = range.LastCell().Address.RowNumber + space + 1;

                            for (int col = range.FirstCell().Address.ColumnNumber; col <= range.LastCell().Address.ColumnNumber; col++)
                            {
                                IXLCell totalCell = sheet.Cell(row, col);
                                string totalFormula = formula.Replace("()", String.Concat("(", sheet.Range(range.FirstCell().Address.RowNumber + 1, col, range.LastCell().Address.RowNumber, col).RangeAddress.ToString(), ")"));

                                //Use title for first cell if available.
                                if (!string.IsNullOrEmpty(title))
                                {
                                    totalFormula = title;
                                    totalCell.Style.Font.SetBold();
                                    title = null;
                                }

                                totalCell.Style.NumberFormat.SetFormat(numFormat);

                                lastCell = PopulateCells(totalCell, totalFormula);
                            }
                            //Update the range again
                            range = sheet.Range(cell, lastCell);
                        }

                        //Increment delta row if more than one row was added.
                        deltaRow += range.Rows().Count() - 1;

                        //Apply formatting from definition.
                        ApplyRangeFormatting(rangeDef, range.Style);

                        if (rangeDef["hidden"].Boolean)
                            sheet.Rows(range.FirstCell().Address.RowNumber, range.LastCell().Address.RowNumber).Hide();

                        if (rangeDef["conditional"].IsArray)
                        {
                            foreach (JSON condition in rangeDef["conditional"])
                            {
                                IXLStyle style = sheet.Range(range.FirstCell().CellBelow(), range.LastCell()) //First row is always column headings, so skip it.
                                    .AddConditionalFormat() //Add condition
                                    .WhenIsTrue(FixRelativeFormula(condition["formula"].SafeString, deltaRow + 1, sheet)); //Delta row +1 skip an extra row because we want to skip column headings.

                                //Apply formatting form definition.
                                ApplyRangeFormatting(condition, style);
                            }
                        }
                    }
                    else
                        Log(501, string.Concat("Invalid cell reference '", rangeDef.Key, "' in sheet '", sheetDef.Key, "' on slide '", slideData["Title"].String, "' [", slideData["kSlide"].Integer, "]"), LogType.Warning);
                }
            }


            //If chart data has not yet been added, create the sheet now (before additional data sheets).
            if (chartSheet == null)
                chartSheet = xl.AddWorksheet("Chart Data");

            //If there are any remaining additional data sheets, add them now.
            int sheetNum = 1;
            for (int index = 0; index < additionalData.Length; index++)
            {
                if (!processedData.Contains(index))
                {
                    Log(String.Concat("Creating an additional data sheet for table index: ", index));

                    IXLWorksheet sheet = xl.AddWorksheet(String.Concat("Additional Data", (additionalData.Length - processedData.Count) > 1 ? sheetNum++.ToString(" 0") : ""));

                    PopulateCells(sheet.Cell("A1"), additionalData[index], numFormat, cellWidth);
                }
            }

            /////////////////////////////////
            //            CHART            //
            /////////////////////////////////

            //Find chart reference in slide
            Charts.ChartSpace chartSpace = chartPart.ChartSpace;
            Dictionary<string, OpenXmlCompositeElement> charts = FindCharts(chartSpace);

            if (charts.Count == 0)
                throw new NullReferenceException("Chart missing or unsupported chart type found in template.");

            //Position objects on screen
            if (chartSchema["plotArea"]["top"].IsNumber && chartSchema["plotArea"]["left"].IsNumber && chartSchema["plotArea"]["width"].IsNumber && chartSchema["plotArea"]["height"].IsNumber)
                ApplyLayout(chartSchema["plotArea"], chartSpace.Descendants<Charts.PlotArea>().FirstOrDefault().GetFirstChild<Charts.Layout>());
            else if (chartSchema["plotArea"].IsDictionary)
                Log(303, "Slide Schema > Charts > Plot Area definition is incomplete. Location has not been modified", LogType.Warning);

            if (chartSchema["legend"]["top"].IsNumber && chartSchema["legend"]["left"].IsNumber && chartSchema["legend"]["width"].IsNumber && chartSchema["legend"]["height"].IsNumber)
            {
                ApplyPosition(chartSchema["legend"]["position"].SafeString, chartSpace.Descendants<Charts.Legend>().FirstOrDefault());
                ApplyLayout(chartSchema["legend"], chartSpace.Descendants<Charts.Legend>().FirstOrDefault().GetFirstChild<Charts.Layout>());
            }
            else if (chartSchema["legend"]["position"].IsString)
                ApplyPosition(chartSchema["legend"]["position"].String, chartSpace.Descendants<Charts.Legend>().FirstOrDefault());
            else if (chartSchema["legend"].IsDictionary)
                Log(303, "Slide Schema > Charts > Legend definition is incomplete. Location has not been modified", LogType.Warning);

            if (chartSchema["legend"]["hidden"].Boolean)
                chartSpace.Descendants<Charts.Legend>().First().Remove();


            //Clear current cached chart data.
            List<OpenXmlCompositeElement> CachedElements = new List<OpenXmlCompositeElement>();
            //      Find child series elements in each chart.
            foreach (KeyValuePair<string, OpenXmlCompositeElement> chart in charts)
                foreach (OpenXmlElement child in chart.Value.ChildElements)
                    if (child.GetType().Name.Contains("Series"))
                        CachedElements.Add((OpenXmlCompositeElement)child);
            //      Remove found elements.
            foreach (OpenXmlCompositeElement child in CachedElements)
                child.Remove();

            uint i = 0;

            //Hide legend and remove first column if only 1 series.
            if (chartData.Length <= 1 && chartData[0].Length <= 1)
            {
                if (Transpose)
                    chartRow--;
                else
                    chartCol--;

                //Check the legend hasn't already been removed by schema.
                if(chartSpace.Descendants<Charts.Legend>().Count() > 0)
                    chartSpace.Descendants<Charts.Legend>().First().Remove();
            }

            foreach (JSON seriesGroup in chartData)
            {
                foreach (JSON series in seriesGroup)
                {
                    bool blank = series.Key.StartsWith("_break"); //Legacy
                    int SchemaIndex;
                    //If no limitations are defined, select incremental schema index by default. Otherwise use the limited set if it is available.
                    if (schemaColors.Length == 0)
                        SchemaIndex = seriesSchema.Length == 0 ? 0 : (int)i % seriesSchema.Length;
                    else
                        SchemaIndex = (int)schemaColors[(int)i % schemaColors.Length].Integer - 1; //Zero based index.

                    //Get the correct chart.
                    OpenXmlCompositeElement chart = charts[charts.Keys.First()]; //Default
                    if (charts.Count > 0 && seriesSchema.Length > 0 && seriesSchema[SchemaIndex]["secondary"].Boolean)
                        chart = charts[charts.Keys.Last()]; //Alternative Axis  //Limits handling to only two graph types.

                    //Append a new chart series.
                    OpenXmlCompositeElement chartSeries = AppendSeries(chart, i, seriesSchema[SchemaIndex]["color"].SafeString);
                    if (chartSeries == null)
                        throw new NullReferenceException("Chart series is unsupported."); //This should never happen - but would highligh missed types in coding.

                    //If more than one series - add series labels.
                    if (chartData.Length > 1 || chartData[0].Length > 1)
                    {
                        IXLCell SeriesCell;
                        if (Transpose)
                            SeriesCell = chartSheet.Cell(Math.Max(chartRow, 1), (int)i + chartCol + 1);
                        else
                            SeriesCell = chartSheet.Cell((int)i + chartRow + 1, Math.Max(chartCol, 1));

                        //Update worksheet.
                        SeriesCell.Value = blank ? "" : "'" + series.Key;
                        SeriesCell.Style.Font.SetBold(true);
                        if (SeriesCell.Value.ToString().Contains(" "))
                            SeriesCell.Style.Alignment.WrapText = true;

                        //Update chart.
                        chartSeries.Append(
                            new Charts.SeriesText(
                                new Charts.StringReference(
                                    new Charts.Formula(SeriesCell.Address.ToStringRelative(true)),  //B1, C1, D1 etc.
                                    new Charts.StringCache(
                                        new Charts.PointCount() { Val = new UInt32Value(1U) },
                                        new Charts.StringPoint(
                                            new Charts.NumericValue(SeriesCell.Value.ToString())
                                        ) { Index = new UInt32Value(0U) }
                                    )
                                )
                            )
                        );
                    }

                    uint j = 0;

                    Charts.StringCache strCache = new Charts.StringCache(new Charts.PointCount() { Val = new UInt32Value(0U) });
                    Charts.NumberingCache numCache = new Charts.NumberingCache(new Charts.FormatCode(numFormat), new Charts.PointCount() { Val = new UInt32Value(0U) });

                    Charts.Formula strRange = new Charts.Formula(); //A2:A5
                    Charts.Formula numRange = new Charts.Formula(); //B2:B5, C2:C5, D2:D5 etc.

                    chartSeries.Append(
                        new Charts.CategoryAxisData(
                            new Charts.StringReference(
                                strRange,
                                strCache
                            )
                        ),
                        new Charts.Values(
                            new Charts.NumberReference(
                                numRange,
                                numCache
                            )
                        )
                    );

                    foreach (JSON categoryGroup in series)
                    {
                        foreach (JSON value in categoryGroup)
                        {
                            blank = series.Key.StartsWith("_break"); //Legacy
                            if (value.Key.StartsWith("_break"))
                                blank = true;

                            IXLCell CategoryCell;
                            IXLCell ValueCell;
                            if (Transpose)
                            {
                                CategoryCell = chartSheet.Cell((int)j + chartRow + 1, chartCol); //B1, C1, D1...
                                ValueCell = chartSheet.Cell((int)j + chartRow + 1, (int)i + chartCol + 1); //B2,C2,D2 .. B3,C3,D3 ..

                                strRange.Text = chartSheet.Range(
                                    chartRow + 1,                                       //2
                                    CategoryCell.WorksheetColumn().ColumnNumber(), //A
                                    CategoryCell.WorksheetRow().RowNumber(), //2,3,4,5
                                    CategoryCell.WorksheetColumn().ColumnNumber() //A
                                ).RangeAddress.ToStringRelative(true); //A2:A5

                                numRange.Text = chartSheet.Range(
                                    chartRow + 1,                                    //2
                                    ValueCell.WorksheetColumn().ColumnNumber(), //B
                                    ValueCell.WorksheetRow().RowNumber(), //2,3,4,5
                                    ValueCell.WorksheetColumn().ColumnNumber() //B .. C ..
                                ).RangeAddress.ToStringRelative(true); //B2:B5 .. C2:C5 ..
                            }
                            else //not transposed.
                            {
                                CategoryCell = chartSheet.Cell(chartRow, (int)j + chartCol + 1); //A2, A3, A4...
                                ValueCell = chartSheet.Cell((int)i + chartRow + 1, (int)j + chartCol + 1); //B2,B3,B4 .. C2,C3,C4 ..

                                strRange.Text = chartSheet.Range(
                                    CategoryCell.WorksheetRow().RowNumber(), //1
                                    chartCol + 1,                                //B
                                    CategoryCell.WorksheetRow().RowNumber(), //1
                                    CategoryCell.WorksheetColumn().ColumnNumber() //B,C,D,E
                                ).RangeAddress.ToStringRelative(true); //B1:E1

                                numRange.Text = chartSheet.Range(
                                    ValueCell.WorksheetRow().RowNumber(), //2 .. 3 ..
                                    chartCol + 1,                             //B
                                    ValueCell.WorksheetRow().RowNumber(), //2 .. 3 .. 
                                    ValueCell.WorksheetColumn().ColumnNumber() //B,C,D,E
                                ).RangeAddress.ToStringRelative(true); //B2:E2 .. B3:E3 ..
                            }

                            if (i == 0) //Only use first series to label categories
                            {
                                //Update worksheet.                            
                                CategoryCell.Value = blank ? "" : "'" + value.Key;
                                CategoryCell.Style.Font.SetBold(true);
                                if (CategoryCell.Value.ToString().Contains(" "))
                                    CategoryCell.Style.Alignment.WrapText = true;
                            }

                            ValueCell.Style.NumberFormat.SetFormat(numFormat);

                            ValueCell.Value = blank ? "" : value.SafeString;


                            //Add chart cached data.
                            strCache.Append(
                                new Charts.StringPoint(
                                    new Charts.NumericValue(CategoryCell.Value.ToString())
                                ) { Index = new UInt32Value(j) }
                            );

                            numCache.Append(
                                new Charts.NumericPoint(
                                    new Charts.NumericValue(ValueCell.Value.ToString())
                                ) { Index = new UInt32Value(j) }
                            );

                            strCache.Descendants<Charts.PointCount>().First().Val++;
                            numCache.Descendants<Charts.PointCount>().First().Val++;
                            j++;
                        }
                        j++;
                    }
                    i++;
                }
                i++;
            }

            //Set all columns to default width.
            chartSheet.ColumnsUsed().Width = cellWidth;
            //chartSheet.ColumnsUsed().AdjustToContents(); //auto size columns.

            //Save spreadsheet and add it as an embedded part.
            EmbeddedPackagePart embeddedPart = chartPart.AddNewPart<EmbeddedPackagePart>("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "rId1");

            MemoryStream ms = new MemoryStream();
            xl.SaveAs(ms); //Save workbook to memory stream.
            ms.Position = 0; //Reset memory stream so we can start reading again from the beginning.
            embeddedPart.FeedData(ms); //Load memory stream into embeded data part.

            ms.Close();

            // link the excel to the chart
            chartPart.ChartSpace.Descendants<Charts.ExternalData>().First().Id = chartPart.GetIdOfPart(embeddedPart);
            chartPart.ChartSpace.Save();
        }

        private static void ApplyPosition(string position, Charts.Legend legend)
        {
            Charts.LegendPosition legendPos = new Charts.LegendPosition();

            switch (position.ToUpper())
            {
                case "TOP": legendPos.Val = Charts.LegendPositionValues.Top; break;
                case "LEFT": legendPos.Val = Charts.LegendPositionValues.Left; break;
                case "RIGHT": legendPos.Val = Charts.LegendPositionValues.Right; break;
                case "BOTTOM": legendPos.Val = Charts.LegendPositionValues.Bottom; break;
                case "TOPRIGHT": legendPos.Val = Charts.LegendPositionValues.TopRight; break;
                default: break;
            }

            if (legendPos.Val != null)
                legend.LegendPosition = legendPos;
        }

        /// <summary>
        /// Sets the position of the given element using the given schema.
        /// </summary>
        private static void ApplyLayout(JSON layoutSchema, Charts.Layout Layout)
        {
            if (Layout.ManualLayout == null) //Chart is using default layout, so create manual layout.
            {
                Layout.Append(
                    new Charts.ManualLayout( //The order of creation is important: Left, Top, Width, Height.
                        Layout.Parent.GetType().Name.Contains("PlotArea") ? new Charts.LayoutTarget() { Val = Charts.LayoutTargetValues.Inner } : null,
                        new Charts.LeftMode() { Val = Charts.LayoutModeValues.Edge },
                        new Charts.TopMode() { Val = Charts.LayoutModeValues.Edge },
                        new Charts.Left() { Val = Math.Min(Math.Max(layoutSchema["left"].Number, 0), 100) / 100 },
                        new Charts.Top() { Val = Math.Min(Math.Max(layoutSchema["top"].Number, 0), 100) / 100 },
                        new Charts.Width() { Val = Math.Min(Math.Max(layoutSchema["width"].Number, 0), 100) / 100 },
                        new Charts.Height() { Val = Math.Min(Math.Max(layoutSchema["height"].Number, 0), 100) / 100 }
                    )
                );
            }
            else //Adjust existing manual layout values.
            {
                Layout.ManualLayout.Top.Val = Math.Min(Math.Max(layoutSchema["top"].Number, 0), 100) / 100;
                Layout.ManualLayout.Left.Val = Math.Min(Math.Max(layoutSchema["left"].Number, 0), 100) / 100;
                Layout.ManualLayout.Width.Val = Math.Min(Math.Max(layoutSchema["width"].Number, 0), 100) / 100;
                Layout.ManualLayout.Height.Val = Math.Min(Math.Max(layoutSchema["height"].Number, 0), 100) / 100;
            }
        }

        /// <summary>
        /// Inserts data into a worksheet, starting at the specified cell, from a string ('\n' will move down to the next cell).
        /// </summary>
        private static IXLCell PopulateCells(IXLCell cell, string value)
        {
            foreach (string val in value.Split('\n'))
            {
                if (!String.IsNullOrEmpty(cell.Value + cell.FormulaA1))
                    Log(503, String.Concat("Cell '", cell.Address.ToStringRelative(true), "' already contained some data ('", cell.Value, cell.FormulaA1, "'), which will be overritten with '", val, "'."), LogType.Warning);

                if (val.StartsWith("="))
                    cell.FormulaA1 = val;
                else
                    cell.Value = val;

                cell = cell.CellBelow();
            }

            return cell.CellAbove();
        }

        /// <summary>
        /// Inserts repeated data into a worksheet, into each cell in the range, from a string ('\n' will move down to the next cell.
        /// </summary>
        private static IXLCell PopulateCells(IXLRange range, string value)
        {
            IXLCell lastCell = range.FirstCell();
            foreach (IXLCell cell in range.Cells())
                lastCell = PopulateCells(cell, value);

            return lastCell;
        }

        /// <summary>
        /// Inserts data into a worksheet, starting at the specified cell, from a JSON table.
        /// </summary>
        private static IXLCell PopulateCells(IXLCell cell, JSON data, string numFormat, double cellWidth)
        {
            //Check that the data is not empty.
            if (!data.HasData())
                return cell;

            IXLWorksheet sheet = cell.Worksheet;
            int r = cell.Address.RowNumber;
            int c = cell.Address.ColumnNumber;

            //Column Headers
            foreach (JSON col in data[0])
            {
                sheet.Cell(r, c).Value = col.Key.StartsWith("_") ? "" : col.Key.Replace("_", " ");
                sheet.Cell(r, c++).Style.Font.SetBold(true);
            }

            //Data Rows
            foreach (JSON row in data)
            {
                r++;
                c = cell.Address.ColumnNumber;
                foreach (JSON col in row)
                {
                    IXLCell ValueCell = sheet.Cell(r, c);

                    ValueCell.Value = col.SafeString;
                    ValueCell.Style.NumberFormat.SetFormat(numFormat);
                    if (ValueCell.Value.ToString().Equals("-") && !string.IsNullOrEmpty(numFormat))
                        ValueCell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    else if (ValueCell.Value.ToString().Contains(" "))
                        ValueCell.Style.Alignment.WrapText = true;

                    //If header cell is blank - column contains row headers.
                    if (sheet.Cell(cell.Address.RowNumber, c).Value.ToString().Trim() == "")
                    {
                        ValueCell.Style.Font.SetBold(true);

                        IXLCell CellAbove = ValueCell.CellAbove();
                        if (ValueCell.Value.ToString() == CellAbove.Value.ToString())
                        {
                            while (ValueCell.Value.ToString() == CellAbove.Value.ToString())
                                CellAbove = CellAbove.CellAbove();

                            sheet.Range(CellAbove.CellBelow(), ValueCell).Merge();
                            CellAbove.CellBelow().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        }
                    }

                    c++;
                }
            }

            //Set all columns to default width.
            sheet.ColumnsUsed().Width = cellWidth;
            //sheet.ColumnsUsed().AdjustToContents(); //Auto size columns.

            return sheet.Cell(r, c - 1);
        }

        /// <summary>
        /// Applys formatting to the given cell range based on the given JSON definition.
        /// </summary>
        private static void ApplyRangeFormatting(JSON styleDef, IXLStyle rangeStyle)
        {
            if (styleDef["bold"].IsBoolean)
                rangeStyle.Font.SetBold(styleDef["bold"].Boolean);
            if (styleDef["color"].IsString && IsColor(styleDef["color"].String))
                rangeStyle.Font.SetFontColor(GetXLColor(styleDef["color"].String));
            if (styleDef["background"].IsString && IsColor(styleDef["background"].String))
                rangeStyle.Fill.SetBackgroundColor(GetXLColor(styleDef["background"].String));
            if (styleDef["format"].IsString)
                rangeStyle.NumberFormat.SetFormat(styleDef["format"].String);
            if (styleDef["borders"].IsBoolean)
                if (styleDef["borders"].Boolean)
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.Medium)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin);
                else
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.None)
                        .Border.SetInsideBorder(XLBorderStyleValues.None);
            else if (styleDef["borders"].IsDictionary)
            {
                if (styleDef["borders"]["color"].IsString && IsColor(styleDef["borders"]["color"].String))
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.Medium)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetOutsideBorderColor(GetXLColor(styleDef["borders"]["color"].String))
                        .Border.SetInsideBorderColor(GetXLColor(styleDef["borders"]["color"].String));

                if (styleDef["borders"]["style"].IsString)
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.None)
                        .Border.SetInsideBorder(BorderStyleFromString(styleDef["borders"]["style"].String));


                if (styleDef["borders"]["top"].IsDictionary)
                {
                    if (styleDef["borders"]["top"]["color"].IsString && IsColor(styleDef["borders"]["top"]["color"].String))
                        rangeStyle.Border.SetTopBorderColor(GetXLColor(styleDef["borders"]["top"]["color"].String));
                    if (styleDef["borders"]["top"]["style"].IsString)
                        rangeStyle.Border.SetTopBorder(BorderStyleFromString(styleDef["borders"]["top"]["style"].String));
                }
                if (styleDef["borders"]["bottom"].IsDictionary)
                {
                    if (styleDef["borders"]["bottom"]["color"].IsString && IsColor(styleDef["borders"]["bottom"]["color"].String))
                        rangeStyle.Border.SetBottomBorderColor(GetXLColor(styleDef["borders"]["bottom"]["color"].String));
                    if (styleDef["borders"]["bottom"]["style"].IsString)
                        rangeStyle.Border.SetBottomBorder(BorderStyleFromString(styleDef["borders"]["bottom"]["style"].String));
                }
                if (styleDef["borders"]["left"].IsDictionary)
                {
                    if (styleDef["borders"]["left"]["color"].IsString && IsColor(styleDef["borders"]["left"]["color"].String))
                        rangeStyle.Border.SetLeftBorderColor(GetXLColor(styleDef["borders"]["left"]["color"].String));
                    if (styleDef["borders"]["left"]["style"].IsString)
                        rangeStyle.Border.SetLeftBorder(BorderStyleFromString(styleDef["borders"]["left"]["style"].String));
                }
                if (styleDef["borders"]["right"].IsDictionary)
                {
                    if (styleDef["borders"]["right"]["color"].IsString && IsColor(styleDef["borders"]["right"]["color"].String))
                        rangeStyle.Border.SetRightBorderColor(GetXLColor(styleDef["borders"]["right"]["color"].String));
                    if (styleDef["borders"]["right"]["style"].IsString)
                        rangeStyle.Border.SetRightBorder(BorderStyleFromString(styleDef["borders"]["right"]["style"].String));
                }
            }
        }


        /// <summary>
        ///     Returns the expected last cell based on the size of the chart data.
        /// </summary>
        private static IXLCell CalculateLastChartDataCell(IXLCell cell, JSON chartData, bool Transpose)
        {
            int seriesCount = chartData.Length; //one row/col space between groups, includs one for heading.
            foreach (JSON seriesGroup in chartData)
                seriesCount += seriesGroup.Length;

            int categoryCount = chartData[0][0].Length; //one col/row space between groups, includs one for heading.
            foreach (JSON categoryGroup in chartData[0][0])
                categoryCount += categoryGroup.Length;

            if (Transpose)
                return cell.CellBelow(Math.Max(categoryCount - 1,0)).CellRight(Math.Max(seriesCount - 1,0));
            else
                return cell.CellBelow(Math.Max(seriesCount - 1,0)).CellRight(Math.Max(categoryCount - 1,0));
        }

        /// <summary>
        /// Finds relative cell references in the given formula and shifts them down, based on previous entries.
        /// </summary>
        private static string FixRelativeFormula(string formula, int deltaRow, IXLWorksheet sheet)
        {
            Regex rex = new Regex(@"(?:\b|\$)[a-zA-Z]+[0-9]+\b(?!\()(?=(?:[^""']*(?:""|')[^""']*(?:""|'))*[^""']*$)"); //Regex to find Excel cell references with a relative row number.
            foreach (Match reference in rex.Matches(formula))
            {
                formula = formula.Replace(reference.Value, sheet.Cell(reference.Value).CellBelow(Math.Max(deltaRow,0)).Address.ToString()); //Loses fixed column relativity (not sure if this is an issue).
            }
            return formula;
        }

        /// <summary>
        /// Returns a list of all charts on the slide regardless of chart type.
        /// </summary>
        private static Dictionary<string, OpenXmlCompositeElement> FindCharts(Charts.ChartSpace chartSpace)
        {
            Dictionary<string, OpenXmlCompositeElement> charts = new Dictionary<string, OpenXmlCompositeElement>();

            //Find all charts within the plot area.
            //Area3DChart, AreaChart, Bar3DChart, BarChart, BubbleChart, DoughnutChart, Line3DChart, LineChart, OfPieChart, Pie3DChart, PieChart, RadarChart, ScatterChart
            foreach (OpenXmlCompositeElement chart in chartSpace.Descendants<Charts.PlotArea>().FirstOrDefault().ChildElements.Where(e => e.GetType().Name.EndsWith("Chart")))
                charts.Add(chart.GetType().Name, chart);

            return charts;
        }

        /// <summary>
        ///     Creates the relevant chart series within the given chart reglardless or chart type.
        /// </summary>
        private static OpenXmlCompositeElement AppendSeries(OpenXmlCompositeElement chart, uint index, string color)
        {
            Charts.ChartShapeProperties shapeProp = null;
            if (IsColor(color))
                shapeProp =
                    new Charts.ChartShapeProperties(
                        new Drawing.SolidFill(
                            GetSlideColor(color)
                        ),
                        new Drawing.Outline(
                            new Drawing.NoFill()
                        )
                    );

            switch (chart.GetType().Name)
            {
                case "Area3DChart":
                case "AreaChart":
                    return chart.AppendChild(
                        new Charts.AreaChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp
                        )
                    );
                case "Bar3DChart":
                case "BarChart":
                    return chart.AppendChild(
                        new Charts.BarChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp
                        )
                    );
                case "BubbleChart":
                    return chart.AppendChild(
                        new Charts.BubbleChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp
                        )
                    );
                case "Line3DChart":
                case "LineChart":
                    return chart.AppendChild(
                        new Charts.LineChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            new Charts.ChartShapeProperties(
                                new Drawing.Outline(
                                    new Drawing.NoFill()
                                )
                            ),
                            new Charts.Marker(
                                new Charts.Symbol() { Val = Charts.MarkerStyleValues.Circle },
                                new Charts.Size() { Val = 7 },
                                shapeProp
                            ),
                            new Charts.Smooth() { Val = false }
                        )
                    );
                case "DoughnutChart":
                case "OfPieChart":
                case "Pie3DChart":
                case "PieChart":
                    return chart.AppendChild(
                        new Charts.PieChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp
                        )
                    );
                case "RadarChart":
                    return chart.AppendChild(
                        new Charts.RadarChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp
                        )
                    );
                case "ScatterChart":
                    return chart.AppendChild(
                        new Charts.ScatterChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            new Charts.Marker(
                                new Charts.Symbol() { Val = Charts.MarkerStyleValues.Circle },
                                new Charts.Size() { Val = 7 },
                                shapeProp
                            )
                        )
                    );
                default:
                    return null;
            }
        }


        //Common functions

        /// <summary>
        ///     Find all placeholders ("&lt;name&gt;") within the given string and replace them with the meta data of the same name (case sensitive).
        /// </summary>
        private static string ReplacePlaceholders(string txt, JSON slideData, JSON presData)
        {
            txt = txt.Replace("&gt;", ">").Replace("&lt;", "<");

            //Regular expression to find placeholder tags.
            Regex rex = new Regex("<([^>]*)>");

            foreach (Match match in rex.Matches(txt))
            {
                //if (slideData["placeholders"].Contains(match.Groups[1].Value)) //Slide placeholders (overides all other options)
                //    txt = txt.Replace(match.Value, slideData["placeholders"][match.Groups[1].Value].SafeString);
                //else if (presData["placeholders"].Contains(match.Groups[1].Value)) //Presentation placeholders
                //    txt = txt.Replace(match.Value, presData["placeholders"][match.Groups[1].Value].SafeString);
                //else 

                if (match.Groups[1].Value.StartsWith("Global.") || match.Groups[1].Value.StartsWith("Pres.") || match.Groups[1].Value.StartsWith("Presentation.")) //Presentation table data - column mapping to pptx.Presentation table.
                    txt = txt.Replace(match.Value, presData[match.Groups[1].Value.Split('.')[1]].SafeString);
                else if (match.Groups[1].Value.StartsWith("Slide.")) //Slide table data - column mapping to pptx.Slide table.
                    txt = txt.Replace(match.Value, slideData[match.Groups[1].Value.Split('.')[1]].SafeString);
                else //Otherwise leave it blank.
                    txt = txt.Replace(match.Value, "");
            }

            return txt.Replace(">", "&gt;").Replace("<", "&lt;");
        }


        /// <summary>
        /// Returns a valid border style based on the given string. (Default: None)
        /// </summary>
        private static XLBorderStyleValues BorderStyleFromString(string borderStyle)
        {
            switch (borderStyle.ToLower())
            {
                case "hair":
                    return XLBorderStyleValues.Hair;
                case "thin":
                    return XLBorderStyleValues.Thin;
                case "medium":
                    return XLBorderStyleValues.Medium;
                case "thick":
                    return XLBorderStyleValues.Thick;
                case "double":
                    return XLBorderStyleValues.Double;
                case "dashed":
                    return XLBorderStyleValues.Dashed;
                case "dotted":
                    return XLBorderStyleValues.Dotted;
                case "dotdash":
                case "dashdot":
                    return XLBorderStyleValues.DashDot;
                case "dashdotdot":
                case "dotdashdot":
                case "dotdotdash":
                    return XLBorderStyleValues.DashDotDot;
                case "mediumdashed":
                    return XLBorderStyleValues.MediumDashed;
                case "mediumdotdash":
                case "mediumdashdot":
                    return XLBorderStyleValues.MediumDashDot;
                case "mediumdashdotdot":
                case "mediumdotdashdot":
                case "mediumdotdotdash":
                    return XLBorderStyleValues.MediumDashDotDot;
                default:
                    return XLBorderStyleValues.None;
            }
        }


        /// <summary>
        /// Returns true if the given string can be matched againsta a color.
        /// </summary>
        private static bool IsColor(string color)
        {
            if (String.IsNullOrEmpty(color))
            {
                Log(502, String.Concat("Template sheet definition contins an unrecognised color: \"\""), LogType.Warning);
                return false;
            }

            //Remove tint if specified.
            color = color.Split(' ')[0].Split(',')[0].Trim();

            try
            {
                if (",accent1,accent2,accent3,accent4,accent5,accent6,background1,background2,followedhyperlink,hyperlink,text1,text2,dark1,dark2,light1,light2,".Contains(String.Concat(",", color.ToLower(), ",")))
                    return true;

                XLColor.FromHtml(color);
                return true;
            }
            catch (Exception e)
            {
                Log(502, String.Concat("Template sheet definition contins an unrecognised color: ", color), LogType.Warning);
                return false;
            }
        }

        /// <summary>
        /// Returns an XL Color based on the given string ("Color Tint": where Color = any color string and positive Tint = brightness%  and negative Tint = darkness%)
        /// </summary>
        private static XLColor GetXLColor(string color)
        {
            String colorPart = color.Split(' ')[0].Split(',')[0].Trim();

            int tint = 0;
            int.TryParse(color.Substring(colorPart.Length).Trim(), out tint);

            tint = Math.Min(Math.Max(tint, -100000), 100000); //Clip value if out of range.

            XLColor xlColor;

            switch (colorPart.ToLower())
            {
                case "accent1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent1, 1.0 * tint / 100);
                    break;
                case "accent2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent2, 1.0 * tint / 100);
                    break;
                case "accent3":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent3, 1.0 * tint / 100);
                    break;
                case "accent4":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent4, 1.0 * tint / 100);
                    break;
                case "accent5":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent5, 1.0 * tint / 100);
                    break;
                case "accent6":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent6, 1.0 * tint / 100);
                    break;
                case "background1":
                case "light1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Background1, 1.0 * tint / 100);
                    break;
                case "background2":
                case "light2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Background2, 1.0 * tint / 100);
                    break;
                case "followedhyperlink":
                    xlColor = XLColor.FromTheme(XLThemeColor.FollowedHyperlink, 1.0 * tint / 100);
                    break;
                case "hyperlink":
                    xlColor = XLColor.FromTheme(XLThemeColor.Hyperlink, 1.0 * tint / 100);
                    break;
                case "text1":
                case "dark1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Text1, 1.0 * tint / 100);
                    break;
                case "text2":
                case "dark2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Text2, 1.0 * tint / 100);
                    break;
                default:
                    xlColor = XLColor.FromHtml(colorPart);
                    break;
            }

            return xlColor;
        }

        /// <summary>
        /// Returns an XML Color based on the given string ("Color Tint": where Color = any color string and positive Tint = brightness%  and negative Tint = darkness%)
        /// </summary>
        private static OpenXmlElement GetSlideColor(string color)
        {
            String colorPart = color.Split(' ')[0].Split(',')[0].Trim();

            int tint = 0;
            int.TryParse(color.Substring(colorPart.Length).Trim(), out tint);

            tint = Math.Min(Math.Max(tint, -100), 100); //Clip value if out of range.

            OpenXmlElement xmlColor;

            switch (colorPart.ToLower())
            {
                case "accent1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent1 };
                    break;
                case "accent2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent2 };
                    break;
                case "accent3":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent3 };
                    break;
                case "accent4":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent4 };
                    break;
                case "accent5":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent5 };
                    break;
                case "accent6":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent6 };
                    break;
                case "background1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Background1 };
                    break;
                case "background2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Background2 };
                    break;
                case "followedhyperlink":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.FollowedHyperlink };
                    break;
                case "hyperlink":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Hyperlink };
                    break;
                case "text1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Text1 };
                    break;
                case "text2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Text2 };
                    break;
                case "dark1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Dark1 };
                    break;
                case "dark2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Dark2 };
                    break;
                case "light1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Light1 };
                    break;
                case "light2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Light2 };
                    break;
                default:
                    xmlColor = new Drawing.RgbColorModelHex() { Val = colorPart.Replace("#", "") };
                    break;
            }

            //Apply tint / shade if available.
            if (tint > 0)
                xmlColor.Append(
                    new Drawing.LuminanceModulation() { Val = (100 - tint) * 1000 },
                    new Drawing.LuminanceOffset() { Val = tint * 1000 }
                );
            else if (tint < 0)
                xmlColor.Append(
                    new Drawing.LuminanceModulation() { Val = (100 + tint) * 1000 }
                );


            return xmlColor;
        }

        #endregion

        #region Logging

        /// <summary>
        ///     Returns a code 100 information message to the console log.
        /// </summary>
        static void Log(String Message)
        {
            Log(100, Message, LogType.Information);
        }

        /// <summary>
        ///     Returns an error message to the console log with the given code.
        /// </summary>
        static void Log(int Code, String Message)
        {
            Log(Code, Message, LogType.Error);
        }

        /// <summary>
        ///     Returns a message to the consolde log with the given code and type.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Type">INFORMATION, WARNING or ERROR. If unspecified: Defaults to INFORMATION when no code is given. Defaults to ERROR when code is supplied.</param>
        static void Log(int Code, String Message, LogType Type)
        {
            switch (Type)
            {
                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(String.Concat("ERROR (", Code, "):", Message));
                    break;
                case LogType.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(String.Concat("WARNING (", Code, "):", Message));
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    if (verbose)
                        Console.WriteLine(Message);
                    break;
            }

            Console.ResetColor();

            //if (Type == LogType.Error || Type == LogType.Warning)
            //{
            //    QuickDB qDB = new QuickDB(@"KODA60GB\FFNdev", "VIS");
            //    qDB.SetInsertParameter("kVisualisation", kVisualisation);
            //    qDB.SetInsertParameter("Code", Code);
            //    qDB.SetInsertParameter("Type", Type);
            //    qDB.SetInsertParameter("Message", Message);
            //    qDB.Insert("pptx.spLog");
            //}
        }

        #endregion

    }

    enum LogType
    {
        Information,
        Warning,
        Error
    };
}
