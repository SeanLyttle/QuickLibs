﻿using QuickLibs.Javascript;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace QuickLibs.Web
{
    /// <summary>
    /// Quickly generates and output HTML to the client.  Write less, do more HTML library.
    /// </summary>
    public class QuickHtml : IEnumerable<QuickHtml>
    {
        protected Control ctrl;

        #region CONSTRUCTORS
        /// <summary>
        ///     Creates an HTML element with the tagName "div".
        /// </summary>
        public QuickHtml()
        {
            ctrl = MakeControl(null);
            ctrl.ClientIDMode = ClientIDMode.Static;
        }

        protected Control MakeControl(string tagName)
        {
            if (string.IsNullOrEmpty(tagName))
                return new Control();

            switch (tagName)
            {
                case "table":
                    return new HtmlTable();
                case "tr":
                    return new HtmlTableRow();
                case "th":
                case "td":
                    return new HtmlTableCell(tagName);
                case "br":
                case "hr":
                    return new LiteralControl($"<{ tagName } />");
                default:
                    return new HtmlGenericControl(tagName);
            }
        }

        /// <summary>
        ///     Creates an HTML element with the given tagName.
        /// </summary>
        /// <param name="tagName">Html tag name for the element. Default: "div".</param>
        public QuickHtml(string tagName)
        {
            ctrl = MakeControl(tagName);
            ctrl.ClientIDMode = ClientIDMode.Static;
        }

        /// <summary>
        ///     Creates an HTML element with the given tagName and innerHtml.
        /// </summary>
        /// <param name="tagName">Html tag name for the element. Default: "div".</param>
        /// <param name="innerHtml">Html content for the element. Default: "".</param>
        public QuickHtml(string tagName, string innerHtml)
        {
            ctrl = MakeControl(tagName);
            ctrl.ClientIDMode = ClientIDMode.Static;
            this.Html(innerHtml);
        }

        /// <summary>
        ///     Creates an HTML element with the given tagName, innerHtml and id attribute.
        /// </summary>
        /// <param name="tagName">Html tag name for the element. Default: "div".</param>
        /// <param name="innerHtml">Html content for the element. Default: "".</param>
        /// <param name="id">Html identifier for the element. Default: "".</param>
        public QuickHtml(string tagName, string innerHtml, string id)
        {
            ctrl = new HtmlGenericControl(tagName);
            ctrl.ClientIDMode = ClientIDMode.Static;
            this.Html(innerHtml).SetID(id);
        }

        /// <summary>
        ///     Creates an HTML element with the given tagName and child elements.
        /// </summary>
        /// <param name="tagName">Html tag name for the element. Default: "div".</param>
        /// <param name="childElements">List of child element to append.</param>
        public QuickHtml(string tagName, params QuickHtml[] childElements)
        {
            ctrl = new HtmlGenericControl(tagName);
            ctrl.ClientIDMode = ClientIDMode.Static;
            foreach (QuickHtml element in childElements)
                this.Append(element);
        }

        /// <summary>
        ///     Creates an HTML element from the given HtmlGenericControl.
        /// </summary>
        /// <param name="element">Any HtmlGenericControl.</param>
        public QuickHtml(Control element)
        {
            ctrl = element;
            ctrl.ClientIDMode = ClientIDMode.Static;

            //Clean up ID Attribute
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                {
                    HtmlControl hCtrl = (HtmlControl)ctrl;
                    if (!string.IsNullOrEmpty(hCtrl.Attributes["id"]))
                    {
                        if (ctrl.ID == null)
                            ctrl.ID = hCtrl.Attributes["id"];
                        hCtrl.Attributes.Remove("id");
                    }
                }
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                {
                    WebControl wCtrl = (WebControl)ctrl;
                    if (!string.IsNullOrEmpty(wCtrl.Attributes["id"]))
                    {
                        if (ctrl.ID == null)
                            ctrl.ID = wCtrl.Attributes["id"];
                        wCtrl.Attributes.Remove("id");
                    }
                }
            }
        }

        /// <summary>
        ///     Generates an HTML table row from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow containing data.</param>
        public QuickHtml(DataRow dr)
        {
            ctrl = new QuickHtml(dr, "").ctrl;
        }

        /// <summary>
        ///     Generates an HTML table row from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow containing data.</param>
        /// <param name="RowClass">CSS Class to apply to individual rows.</param>
        public QuickHtml(DataRow dr, string RowClass)
        {
            ctrl = MakeControl("tr");
            ctrl.ClientIDMode = ClientIDMode.Static;
            this.AddClass(RowClass);

            foreach (DataColumn dc in dr.Table.Columns)
                new QuickHtml("td", dr[dc.ColumnName] == DBNull.Value ? "&nbsp;" : dr[dc.ColumnName].ToString()).AppendTo(ctrl);
        }

        /// <summary>
        ///     Generates an HTML table from the given DataTable.
        /// </summary>
        /// <param name="dt">DataTable containing rows of data.</param>
        public QuickHtml(DataTable dt)
        {
            ctrl = new QuickHtml(dt, "", "", "").ctrl;
        }

        /// <summary>
        ///     Generates an HTML table from the given DataTable.
        /// </summary>
        /// <param name="dt">DataTable containing rows of data.</param>
        /// <param name="TableClass">CSS Class to apply to the table.</param>
        /// <param name="RowClass">CSS Class to apply to individual rows.</param>
        /// <param name="AltClass">CSS Class to apply to alternating rows.</param>
        public QuickHtml(DataTable dt, string TableClass, string RowClass, string AltClass)
        {
            ctrl = MakeControl("table");
            ctrl.ClientIDMode = ClientIDMode.Static;
            this.AddClass(TableClass);

            QuickHtml htr = new QuickHtml("tr").AppendTo(ctrl);

            foreach (DataColumn dc in dt.Columns)
                htr.Append(new QuickHtml("th", dc.ColumnName));

            bool alt = true;
            foreach (DataRow dr in dt.Rows)
                new QuickHtml(dr, $"{ RowClass }{ ((alt = !alt) ? $" { AltClass }" : "") }").AppendTo(ctrl);
        }

        ///// <summary>
        ///// Destructor
        ///// </summary>
        //~QuickHtml()
        //{
        //    if(ctrl != null)
        //        ctrl.Dispose();
        //}

        #endregion

        #region qHTML Building

        /// <summary>
        ///     Inserts the given element into the current QuickHtml at the given index.
        /// </summary>
        /// <param name="index">0 inserts before first child, -1 inserts before last child.</param>
        /// <param name="element">element to insert</param>
        /// <returns>This element.</returns>
        public QuickHtml Insert(int index, Control element)
        {
            if (ctrl.GetType().Name.Equals("LiteralControl"))
            {
                Control content = MakeControl(null);

                if (index >= ctrl.Controls.Count)
                {   //Append
                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text));
                    content.Controls.Add(element);
                }
                else if (index == 0 || Math.Abs(index) >= ctrl.Controls.Count)
                {   //Prepend
                    content.Controls.Add(element);
                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text));
                }
                else if (index < 0 && ctrl.Controls.Count > 0)
                {   //Insert
                    index = (index % ctrl.Controls.Count) + ctrl.Controls.Count;

                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text.Substring(0, index)));
                    content.Controls.Add(element);
                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text.Substring(index)));
                }
                else
                {   //Insert
                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text.Substring(0, index)));
                    content.Controls.Add(element);
                    content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text.Substring(index)));
                }

                ctrl.Parent.Controls.AddAt(ctrl.Parent.Controls.IndexOf(ctrl), content);
                ctrl.Parent.Controls.Remove(ctrl);
                ctrl = content;
            }
            else if (!element.GetType().Name.Equals("Control") || element.Controls.Count > 0)
            {
                if (index >= ctrl.Controls.Count)
                    ctrl.Controls.Add(element);
                else if (index < 0 && ctrl.Controls.Count > 0)
                    ctrl.Controls.AddAt((index % ctrl.Controls.Count) + ctrl.Controls.Count, element);
                else
                    ctrl.Controls.AddAt(index, element);
            }
            return this;
        }

        /// <summary>
        ///     Inserts the given element into the current QuickHtml at the given index.
        /// </summary>
        /// <param name="index">0 inserts before first child, -1 appends after last child, -2 inserts before last child.</param>
        /// <param name="element">element to insert</param>
        /// <returns>This element.</returns>
        public QuickHtml Insert(int index, QuickHtml element)
        {
            return Insert(index, element.Ctrl);
        }

        /// <summary>
        ///     Inserts literal text at the given index.
        /// </summary>
        /// <param name="index">0 inserts before first child, -1 appends after last child, -2 inserts before last child.</param>
        /// <param name="text">Literal text to insert.</param>
        /// <returns></returns>
        public QuickHtml Insert(int index, string text)
        {
            if (!string.IsNullOrEmpty(text))
                if (ctrl.GetType().Name.Equals("LiteralControl"))
                {
                    if (index >= ((LiteralControl)ctrl).Text.Length)
                        ((LiteralControl)ctrl).Text += text;
                    else if (index == 0 || Math.Abs(index) >= ((LiteralControl)ctrl).Text.Length)
                        ((LiteralControl)ctrl).Text = string.Concat(text, ((LiteralControl)ctrl).Text);
                    else if (index < 0)
                    {
                        index = (index % ((LiteralControl)ctrl).Text.Length) + ((LiteralControl)ctrl).Text.Length;
                        ((LiteralControl)ctrl).Text = ((LiteralControl)ctrl).Text.Insert(index, text);
                    }
                    else
                        ((LiteralControl)ctrl).Text = ((LiteralControl)ctrl).Text.Insert(index, text);
                }
                else
                    return Insert(index, new LiteralControl(text));
            return this;
        }

        /// <summary>
        ///     Appends an element after the last child.
        /// </summary>
        /// <param name="element">Element to append.</param>
        /// <returns>This element.</returns>
        public QuickHtml Append(Control element)
        {
            if (ctrl.GetType().Name.Equals("LiteralControl"))
            {
                Control content = new Control();
                content.Controls.Add(new LiteralControl(((LiteralControl)ctrl).Text));
                content.Controls.Add(element);
                ctrl.Parent.Controls.AddAt(ctrl.Parent.Controls.IndexOf(ctrl), content);
                ctrl.Parent.Controls.Remove(ctrl);
                ctrl = content;
            }
            else if (!element.GetType().Name.Equals("Control") || element.Controls.Count > 0)
                ctrl.Controls.Add(element);
            return this;
        }

        /// <summary>
        ///     Appends an element after the last child.
        /// </summary>
        /// <param name="element">Element to append.</param>
        /// <returns>This element.</returns>
        public QuickHtml Append(QuickHtml element)
        {
            return Append(element.Ctrl);
        }

        /// <summary>
        ///     Appends literal text after the last child.
        /// </summary>
        /// <param name="text">Literal text to append.</param>
        /// <returns></returns>
        public QuickHtml Append(string text)
        {
            if (!string.IsNullOrEmpty(text))
                if (ctrl.GetType().Name.Equals("LiteralControl"))
                    ((LiteralControl)ctrl).Text += text;
                else
                    ctrl.Controls.Add(new LiteralControl(text));
            return this;
        }

        /// <summary>
        ///     Prepends an element before the first child.
        /// </summary>
        /// <param name="element">Element to prepend.</param>
        /// <returns>This element.</returns>
        public QuickHtml Prepend(Control element)
        {
            return Insert(0, element);
        }

        /// <summary>
        ///     Prepends an element before the first child.
        /// </summary>
        /// <param name="element">Element to prepend.</param>
        /// <returns>This element.</returns>
        public QuickHtml Prepend(QuickHtml element)
        {
            return Insert(0, element.Ctrl);
        }

        /// <summary>
        ///     Prepends literal text before the first child.
        /// </summary>
        /// <param name="text">Literal text to prepend.</param>
        /// <returns></returns>
        public QuickHtml Prepend(string text)
        {
            return Insert(0, text);
        }

        /// <summary>
        ///     Appends this element after the last child of the given element.
        /// </summary>
        /// <param name="element">Element to append to.</param>
        /// <returns>This element.</returns>
        public QuickHtml AppendTo(Control element)
        {
            if (element != null)
                element.Controls.Add(this.Ctrl);
            return this;
        }

        /// <summary>
        ///     Appends this element after the last child of the given element.
        /// </summary>
        /// <param name="element">Element to append to.</param>
        /// <returns>This element.</returns>
        public QuickHtml AppendTo(QuickHtml element)
        {
            if (element != null)
                element.Append(this.Ctrl);
            return this;
        }

        /// <summary>
        ///     Prepends this element after the last child of the given element.
        /// </summary>
        /// <param name="element">Element to prepend to.</param>
        /// <returns>This element.</returns>
        public QuickHtml PrependTo(Control element)
        {
            if (element != null)
                element.Controls.AddAt(0, this.Ctrl);
            return this;
        }

        /// <summary>
        ///     Prepends this element after the last child of the given element.
        /// </summary>
        /// <param name="element">Element to prepend to.</param>
        /// <returns>This element.</returns>
        public QuickHtml PrependTo(QuickHtml element)
        {
            if (element != null)
                element.Prepend(this.Ctrl);
            return this;
        }

        /// <summary>
        ///     Inserts this element beofre the given element.
        /// </summary>
        /// <param name="element">Element to insert before.</param>
        /// <returns>This element.</returns>
        public QuickHtml InsertBefore(Control element)
        {
            if (element != null)
            {
                if (element.Parent != null)
                    element.Parent.Controls.AddAt(element.Parent.Controls.IndexOf(element), this.Ctrl);
                else
                {
                    new Control().Controls.Add(this.Ctrl);
                    this.Ctrl.Parent.Controls.Add(element);
                }
            }
            return this;
        }

        /// <summary>
        ///     Inserts this element beofre the given element.
        /// </summary>
        /// <param name="element">Element to insert before.</param>
        /// <returns>This element.</returns>
        public QuickHtml InsertBefore(QuickHtml element)
        {
            return InsertBefore(element.Ctrl);
        }

        /// <summary>
        ///     Inserts this element after the given element.
        /// </summary>
        /// <param name="element">Element to insert after.</param>
        /// <returns>This element.</returns>
        public QuickHtml InsertAfter(Control element)
        {
            if (element != null)
            {
                if (element.Parent != null)
                {
                    if (element.Parent.Controls.IndexOf(element) + 1 >= element.Parent.Controls.Count)
                        element.Parent.Controls.Add(this.Ctrl);
                    else
                        element.Parent.Controls.AddAt(element.Parent.Controls.IndexOf(element) + 1, this.Ctrl);
                }
                else
                {
                    new Control().Controls.Add(element);
                    element.Parent.Controls.Add(this.Ctrl);
                }
            }

            return this;
        }

        /// <summary>
        ///     Inserts this element after the given element.
        /// </summary>
        /// <param name="element">Element to insert after.</param>
        /// <returns>This element.</returns>
        public QuickHtml InsertAfter(QuickHtml element)
        {
            return InsertAfter(element.Ctrl);
        }

        /// <summary>
        ///     Removes all child elements within this element.
        /// </summary>
        /// <returns>This element.</returns>
        public QuickHtml Clear()
        {
            if (ctrl.GetType().Name.Equals("LiteralControl"))
                ((LiteralControl)ctrl).Text = null;
            else
                ctrl.Controls.Clear();

            if (ctrl.Controls.Count == 1) //Empty LiteralControl always left behind.
                ctrl.Controls.Remove(ctrl.Controls[0]);

            return this;
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength)
        {
            return Truncate(TruncLength, (int)Math.Round(TruncLength * 1.25), "See more", null, null, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MaxLength">Maximum length of text before applying truncation. (Default: 125% of TruncLength.)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, int MaxLength)
        {
            return Truncate(TruncLength, MaxLength, "See more", null, null, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MaxLength">Maximum length of text before applying truncation. (Default: 125% of TruncLength.)</param>
        /// <param name="MoreLinkText">Text to display in the "see more" link (Default: "See more", null = no link.)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, int MaxLength, string MoreLinkText)
        {
            return Truncate(TruncLength, MaxLength, MoreLinkText, null, null, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MoreLinkText">Text to display in the "see more" link (Default: "See more", null = no link.)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, string MoreLinkText)
        {
            return Truncate(TruncLength, (int)Math.Round(TruncLength * 1.25), MoreLinkText, null, null, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MoreLinkText">Text to display in the "see more" link (Default: "See more", null = no link.)</param>
        /// <param name="MoreLinkURL">URL to use for "see more" e.g. a link to full article. (Default: jQuery script to show hidden text.)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, string MoreLinkText, string MoreLinkURL)
        {
            return Truncate(TruncLength, (int)Math.Round(TruncLength * 1.25), MoreLinkText, MoreLinkURL, null, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MoreLinkText">Text to display in the "see more" link (Default: "See more", null = no link.)</param>
        /// <param name="MoreLinkURL">URL to use for "see more" e.g. a link to full article. (Default: jQuery script to show hidden text.)</param>
        /// <param name="MoreLinkTarget">Link Target eg "_blank" (Default: null)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, string MoreLinkText, string MoreLinkURL, string MoreLinkTarget)
        {
            return Truncate(TruncLength, (int)Math.Round(TruncLength * 1.25), MoreLinkText, MoreLinkURL, MoreLinkTarget, null);
        }

        /// <summary>
        ///     Truncates the text within this control to the given length, hiding anything longer and providing a link to show it if required.
        /// </summary>
        /// <param name="TruncLength">Length of text to remain visible.</param>
        /// <param name="MaxLength">Maximum length of text before applying truncation. (Default: 125% of TruncLength.)</param>
        /// <param name="MoreLinkText">Text to display in the "see more" link (Default: "See more", null = no link.)</param>
        /// <param name="MoreLinkURL">URL to use for "see more" e.g. a link to full article. (Default: jQuery script to show hidden text.)</param>
        /// <param name="MoreLinkTarget">Link Target eg "_blank" (Default: null)</param>
        /// <param name="hiddenClass">Name of class used to identify hidden elements. (Default: Randomly generated.)</param>
        /// <returns>This element.</returns>
        public QuickHtml Truncate(int TruncLength, int MaxLength, string MoreLinkText, string MoreLinkURL, string MoreLinkTarget, string hiddenClass)
        {
            if (Text().Length > MaxLength)
            {
                int Len = 0;
                if (this.Length == 0)
                {
                    string hidden = Html();
                    string visible = hidden;
                    int index = hidden.IndexOf(' ', TruncLength) > 0 ? hidden.IndexOf(' ', TruncLength) : TruncLength; //Try and break at a space if possible.

                    if (index < hidden.Length)
                    {
                        visible = hidden.Substring(0, index);
                        hidden = hidden.Substring(index);
                    }
                    else
                        hidden = "";

                    this.Clear();
                    this.Append(new QuickHtml().Html(visible));
                    if (!string.IsNullOrEmpty(MoreLinkText))
                    {
                        this.Append(new QuickHtml("span").Append("... ").Append(
                            new QuickHtml("a", MoreLinkText)
                                .Attr("href", MoreLinkURL, !string.IsNullOrEmpty(MoreLinkURL))
                                .Attr("href", "javascript:", string.IsNullOrEmpty(MoreLinkURL))
                                .Attr("onclick", "$(this).parent().next().show();$(this).parent().remove();", string.IsNullOrEmpty(MoreLinkURL))
                                .Attr("target", MoreLinkTarget, !string.IsNullOrEmpty(MoreLinkTarget))
                            ).Append(".")
                        );
                    }
                    this.Append(new QuickHtml("span", hidden).AddClass(hiddenClass, !string.IsNullOrEmpty(hiddenClass)).Hide());
                }
                else
                {
                    for (int i = 0; i < this.Length; i++) //foreach won't work because if truncate needs to append to a literal control it will change this list.
                    {
                        int childLength = this[i].Text().Length;

                        if (Len > TruncLength)
                            this[i].AddClass(hiddenClass).Hide();
                        else if (Len + childLength > TruncLength)
                            this[i].Truncate(TruncLength - Len, TruncLength - Len, MoreLinkText, MoreLinkURL, MoreLinkTarget, hiddenClass);

                        Len += childLength;
                    }
                }
            }
            return this;
        }

        #endregion

        #region qHTML Attributes

        /// <summary>
        ///     Adds or ammends the attributes of the current control.
        /// </summary>
        /// <param name="attributes">JSON containing a dictionary of attributes.</param>
        /// <returns>This element.</returns>
        public QuickHtml Attr(JSON attributes)
        {
            if (attributes.IsDictionary)
            {
                while (!attributes.IsOutOfRange)
                    if (attributes[false].Boolean)
                        Attr(attributes[false].Key, attributes[true].Key);
                    else
                        Attr(attributes[false].Key, attributes[true].SafeString);
            }
            return this;
        }

        /// <summary>
        ///     Gets the attributes of the current control.
        /// </summary>
        /// <returns>This element.</returns>
        public JSON Attr()
        {
            JSON json = JSON.EmptyDictionary();
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                AttributeCollection attributes;
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    attributes = ((HtmlControl)ctrl).Attributes;
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    attributes = ((WebControl)ctrl).Attributes;
                else
                    return json;

                foreach (string key in attributes.Keys)
                {
                    if (key.Equals(attributes[key], StringComparison.CurrentCultureIgnoreCase))
                        json.Add(key, "true");
                    else
                        json.Add(key, attributes[key]);
                }

                return json;
            }
            else
                return json;
        }

        /// <summary>
        ///     Gets the attributes of the current control.
        /// </summary>
        /// <param name="attribute">An HTML attribute eg. "title"</param>
        /// <returns>Attribute value.</returns>
        public string Attr(string attribute)
        {
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    return string.IsNullOrEmpty(((HtmlControl)ctrl).Attributes[attribute]) ? "" : ((HtmlControl)ctrl).Attributes[attribute];
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    return string.IsNullOrEmpty(((WebControl)ctrl).Attributes[attribute]) ? "" : ((WebControl)ctrl).Attributes[attribute];
                else
                    return "";
            }
            else
                return "";
        }

        /// <summary>
        ///     Gets the state of a boolean attribute of the current control.
        /// </summary>
        /// <param name="attribute">An HTML attribute eg. "disabled"</param>
        /// <returns>True if the attribute exists.</returns>
        public bool Is(string attribute)
        {
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    return !string.IsNullOrEmpty(((HtmlControl)ctrl).Attributes[attribute]);
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    return !string.IsNullOrEmpty(((WebControl)ctrl).Attributes[attribute]);
                else
                    return false;
            }
            else
                return false;
        }


        /// <summary>
        ///     Adds or ammends an attribute of the current control.
        /// </summary>
        /// <param name="attribute">An HTML attribute eg. "title"</param>
        /// <param name="value">Attribute value eg. "My Title"</param>
        /// <returns>This element.</returns>
        public QuickHtml Attr(string attribute, string value)
        {
            if (string.IsNullOrEmpty(attribute))
                return this;

            if (attribute.ToLower() == "id")
                Ctrl.ID = value; //Set Ctrl ID also.

            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    ((HtmlControl)ctrl).Attributes[attribute] = value;
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    ((WebControl)ctrl).Attributes[attribute] = value;
                else if (ctrl.GetType().Name.Equals("LiteralControl"))
                    return new QuickHtml("span").InsertAfter(ctrl).Append(ctrl).Attr(attribute, value);
            }
            else //if container, pass attribute to children.
                for (int i = 0; i < this.Length; i++)
                    this[i].Attr(attribute, value);
            return this;
        }

        /// <summary>
        ///     Adds or removes a boolean attribute of the current control.
        /// </summary>
        /// <param name="attribute">A boolean HTML attribute eg. "disabled"</param>
        /// <param name="value">Attribute value eg. true</param>
        /// <returns>This element.</returns>
        public QuickHtml Attr(string attribute, bool value)
        {
            if (string.IsNullOrEmpty(attribute))
                return this;

            if (attribute.ToLower() == "id")
                return this; // id is not a boolean value.

            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    ((HtmlControl)ctrl).Attributes[attribute] = value ? attribute : null;
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    ((WebControl)ctrl).Attributes[attribute] = value ? attribute : null;
                else if (ctrl.GetType().Name.Equals("LiteralControl"))
                    return new QuickHtml("span").InsertAfter(ctrl).Append(ctrl).Attr(attribute, value);
            }
            else //if container, pass attribute to children.
                for (int i = 0; i < this.Length; i++)
                    this[i].Attr(attribute, value);
            return this;
        }

        /// <summary>
        ///     Adds or ammends an attribute of the current control depending on the onlyIf bool.
        /// </summary>
        /// <param name="attribute">An HTML attribute eg. "title"</param>
        /// <param name="value">Attribute value eg. "My Title"</param>
        /// <param name="onlyIf">True = Add / ammend attribute. False = Do nothing.</param>
        /// <returns>This element.</returns>
        public QuickHtml Attr(string attribute, string value, bool onlyIf)
        {
            if (onlyIf)
                return Attr(attribute, value);
            else
                return this;
        }

        /// <summary>
        ///     Adds or removes a boolean attribute of the current control.
        /// </summary>
        /// <param name="attribute">A boolean HTML attribute eg. "disabled"</param>
        /// <param name="value">Attribute value eg. true</param>
        /// <param name="onlyIf">True = Add / remove attribute. False = Do nothing.</param>
        /// <returns>This element.</returns>
        public QuickHtml Attr(string attribute, bool value, bool onlyIf)
        {
            if (onlyIf)
                return Attr(attribute, value);
            else
                return this;
        }

        /// <summary>
        ///     Removes an attrubute from the current element.
        /// </summary>
        /// <param name="attribute">Attribute value eg. "My Title"</param>
        /// <returns></returns>
        public QuickHtml RemoveAttr(string attribute)
        {
            if (string.IsNullOrEmpty(attribute))
                return this;

            if (attribute.ToLower() == "id")
                ctrl.ID = null;

            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    ((HtmlControl)ctrl).Attributes.Remove(attribute);
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    ((WebControl)ctrl).Attributes.Remove(attribute);
            }
            else //if container, pass attribute to children.
                foreach (QuickHtml child in this)
                    child.RemoveAttr(attribute);

            return this;
        }

        /// <summary>
        ///     Removes an attrubute from the current element.
        /// </summary>
        /// <param name="attribute">Attribute value eg. "My Title"</param>
        /// <param name="onlyIf">True = Remove attribute. False = Do nothing.</param>
        /// <returns></returns>
        public QuickHtml RemoveAttr(string attribute, bool onlyIf)
        {
            if (onlyIf)
                return RemoveAttr(attribute);
            else
                return this;
        }

        /// <summary>
        ///     Adds or ammends the style of the current control.
        /// </summary>
        /// <param name="style">JSON containing a dictionary of attributes.</param>
        /// <returns>This element.</returns>
        public QuickHtml Css(JSON style)
        {
            if (style.IsDictionary)
            {
                while (!style.IsOutOfRange)
                    Css(style[false].Key, style[true].SafeString);
            }
            return this;
        }

        /// <summary>
        ///     Gets the style of the current control.
        /// </summary>
        /// <returns>Style value.</returns>
        public JSON Css()
        {
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    return new JSON(((HtmlControl)ctrl).Style);
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    return new JSON(((WebControl)ctrl).Style);
                else
                    return new JSON();
            }
            else
                return new JSON();
        }

        /// <summary>
        ///     Gets the style of the current control.
        /// </summary>
        /// <param name="style">HTML Style eg. "color"</param>
        /// <returns>Style value.</returns>
        public string Css(string style)
        {
            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    return string.IsNullOrEmpty(((HtmlControl)ctrl).Style[style]) ? "" : ((HtmlControl)ctrl).Style[style];
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    return string.IsNullOrEmpty(((WebControl)ctrl).Style[style]) ? "" : ((WebControl)ctrl).Style[style];
                else
                    return "";
            }
            else
                return "";
        }

        /// <summary>
        ///     Adds or ammends the style of the current control.
        /// </summary>
        /// <param name="style">HTML Style eg. "color"</param>
        /// <param name="value">Style value eg. "Red"</param>
        /// <returns>This element.</returns>
        public QuickHtml Css(string style, string value)
        {
            if (string.IsNullOrEmpty(style))
                return this;

            if (string.IsNullOrEmpty(value))
                return RemoveCss(style);

            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    ((HtmlControl)ctrl).Style[style] = value;
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    ((WebControl)ctrl).Style[style] = value;
                else if (ctrl.GetType().Name.Equals("LiteralControl"))
                    return new QuickHtml("span").InsertAfter(ctrl).Append(ctrl).Css(style, value);
            }
            else //if container, pass attribute to children.
                for (int i = 0; i < this.Length; i++)
                    this[i].Css(style, value);

            return this;
        }

        /// <summary>
        ///     Adds, ammends or removes the style of the current control depending on the onlyIf bool.
        /// </summary>
        /// <param name="style">HTML Style eg. "color"</param>
        /// <param name="value">Style value eg. "Red"</param>
        /// <param name="onlyIf">True = Add / ammend style. False = Do nothing.</param>
        /// <returns>This element.</returns>
        public QuickHtml Css(string style, string value, bool onlyIf)
        {
            if (onlyIf)
                return Css(style, value);
            else
                return this;
        }

        /// <summary>
        ///     Removes a style from the current element.
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public QuickHtml RemoveCss(string style)
        {
            if (string.IsNullOrEmpty(style))
                return this;

            if (!ctrl.GetType().Name.Equals("Control"))
            {
                if (ctrl.GetType().FullName.Contains("HtmlControls"))
                    ((HtmlControl)ctrl).Style.Remove(style);
                else if (ctrl.GetType().FullName.Contains("WebControls"))
                    ((WebControl)ctrl).Style.Remove(style);
            }
            else //if container, pass attribute to children.
                foreach (QuickHtml child in this)
                    child.Css(style);

            return this;
        }

        /// <summary>
        ///     Removes the style of the current control depending on the onlyIf bool.
        /// </summary>
        /// <param name="style">HTML Style eg. "title"</param>
        /// <param name="onlyIf">True = Remove style. False = Do nothing.</param>
        /// <returns>This element.</returns>
        public QuickHtml RemoveCss(string style, bool onlyIf)
        {
            if (onlyIf)
                return RemoveCss(style);
            else
                return this;
        }

        /// <summary>
        ///     Adds a CSS class to the current element.
        /// </summary>
        /// <param name="cssClass">CSS class name.</param>
        public QuickHtml AddClass(string cssClass)
        {
            if (!HasClass(cssClass))
                return Attr("class", $"{ Attr("class") } { cssClass }".Trim());
            else
                return this;
        }

        /// <summary>
        ///     Adds a CSS class to the current element.
        /// </summary>
        /// <param name="cssClass">CSS class name.</param>
        /// <param name="onlyIf">True = Add class. False = Do nothing.</param>
        public QuickHtml AddClass(string cssClass, bool onlyIf)
        {
            if (onlyIf)
                return AddClass(cssClass);
            else
                return this;
        }

        /// <summary>
        ///     Removes a CSS class from the current element.
        /// </summary>
        /// <param name="cssClass">CSS class name.</param>
        public QuickHtml RemoveClass(string cssClass)
        {
            return Attr("class", $" { Attr("class") } ".Replace($" { cssClass } ", " ").Trim());
        }

        /// <summary>
        ///     Removes a CSS class from the current element.
        /// </summary>
        /// <param name="cssClass">CSS class name.</param>
        /// <param name="onlyIf">True = Add class. False = Do nothing.</param>
        public QuickHtml RemoveClass(string cssClass, bool onlyIf)
        {
            if (onlyIf)
                return RemoveCss(cssClass);
            else
                return this;
        }

        /// <summary>
        ///     Returns true if the current element is assigned the given cssClass.
        /// </summary>
        /// <param name="cssClass">CSS class name.</param>
        public bool HasClass(string cssClass)
        {
            return !string.IsNullOrEmpty(cssClass) && $" { Attr("class") } ".Contains($" { cssClass } ");
        }

        /// <summary>
        ///     Returns the innerText within this element.
        /// </summary>
        public string Text()
        {
            if (ctrl.HasControls())
            {
                StringBuilder sb = new StringBuilder("");
                foreach (QuickHtml child in this)
                    sb.Append(child.Text().Trim() + " ");
                return sb.ToString().Trim();
            }
            else if (ctrl.GetType().Name.Equals("HtmlGenericControl"))
                return HttpUtility.HtmlDecode(((HtmlGenericControl)ctrl).InnerText);
            else if (ctrl.GetType().Name.Equals("HtmlTableCell"))
                return HttpUtility.HtmlDecode(((HtmlTableCell)ctrl).InnerText);
            else if (ctrl.GetType().Name.Equals("LiteralControl"))
                return HttpUtility.HtmlDecode(((LiteralControl)ctrl).Text);
            else
                return string.Empty;
        }

        /// <summary>
        ///     Populates the element with the given text.
        /// </summary>
        /// <param name="innerText">Text to place within the HTML tags.</param>
        /// <returns>This Element.</returns>
        public QuickHtml Text(string innerText)
        {
            ctrl.Controls.Clear();
            if (ctrl.GetType().Name.Equals("HtmlGenericControl"))
                ((HtmlGenericControl)ctrl).InnerText = HttpUtility.HtmlEncode(innerText);
            else if (ctrl.GetType().Name.Equals("HtmlTableCell"))
                ((HtmlTableCell)ctrl).InnerText = HttpUtility.HtmlEncode(innerText);
            else if (ctrl.GetType().Name.Equals("LiteralControl"))
                ((LiteralControl)ctrl).Text = HttpUtility.HtmlEncode(innerText);
            else if (ctrl.GetType().Name.Equals("Control"))
                ctrl = new LiteralControl(HttpUtility.HtmlEncode(innerText));
            return this;
        }

        /// <summary>
        ///     Returns the innerHTML within this element.
        /// </summary>
        public string Html()
        {
            if (ctrl.HasControls())
            {
                StringBuilder sb = new StringBuilder("");
                foreach (QuickHtml child in this)
                    sb.Append(child.ToString());
                return sb.ToString();
            }
            else if (ctrl.GetType().Name.Equals("HtmlGenericControl"))
                return ((HtmlGenericControl)ctrl).InnerHtml;
            else if (ctrl.GetType().Name.Equals("HtmlTableCell"))
                return ((HtmlTableCell)ctrl).InnerHtml;
            else if (ctrl.GetType().Name.Equals("LiteralControl"))
                return ((LiteralControl)ctrl).Text;
            else
                return string.Empty;
        }

        /// <summary>
        ///     Populates the element with the given HTML.
        /// </summary>
        /// <param name="innerHtml">HTML to place within the HTML tags.</param>
        /// <returns>This Element.</returns>
        public QuickHtml Html(string innerHtml)
        {
            if (string.IsNullOrEmpty(innerHtml))
            {
                if (ctrl.GetType().Name.Equals("HtmlGenericControl"))
                    ((HtmlGenericControl)ctrl).InnerHtml = innerHtml;
                else if (ctrl.GetType().Name.Equals("HtmlTableCell"))
                    ((HtmlTableCell)ctrl).InnerHtml = innerHtml;
                else if (ctrl.GetType().Name.Equals("LiteralControl"))
                    ((LiteralControl)ctrl).Text = innerHtml;

                return this;
            }

            // http://regexr.com/39vcn // Regex to find html tags: Groups[1] opening tag, Groups[2] attributes, Groups[3] self closing tag, Group[4] closing tag.
            Regex rxHtml = new Regex("<(?:([a-zA-Z\\?][\\w:\\-]*)(\\s(?:\\s*[a-zA-Z][\\w:\\-]*(?:\\s*=(?:\\s*\"(?:\\\\\"|[^\"])*\"|\\s*'(?:\\\\'|[^'])*'|[^\\s>]+))?)*)?(\\s*[\\/\\?]?)|\\/([a-zA-Z][\\w:\\-]*)\\s*|!--((?:[^\\-]|-(?!->))*)--|!\\[CDATA\\[((?:[^\\]]|\\](?!\\]>))*)\\]\\])>", RegexOptions.Compiled | RegexOptions.Singleline);
            MatchCollection matches = rxHtml.Matches(innerHtml);

            ctrl.Controls.Clear();
            if (matches.Count == 0)
            {   //No HTML tags detected
                if (ctrl.GetType().Name.Equals("HtmlGenericControl"))
                    ((HtmlGenericControl)ctrl).InnerHtml = innerHtml;
                else if (ctrl.GetType().Name.Equals("HtmlTableCell"))
                    ((HtmlTableCell)ctrl).InnerHtml = innerHtml;
                else if (ctrl.GetType().Name.Equals("LiteralControl"))
                    ((LiteralControl)ctrl).Text = innerHtml;
                else if (ctrl.GetType().Name.Equals("Control"))
                    ctrl = new LiteralControl(innerHtml);
            }
            else
            {
                int index = 0;
                Control target;

                if (ctrl.GetType().Name.Equals("LiteralControl"))
                {
                    target = ctrl = new Control();
                }
                else
                {
                    target = ctrl;
                }

                foreach (Match match in matches)
                {
                    if (match.Index > index) //Text between this and the last tag (or the beginning).
                    {
                        target.Controls.Add(new LiteralControl(innerHtml.Substring(index, match.Index - index)));
                    }

                    if (!string.IsNullOrEmpty(match.Groups[1].Value)) //Tag name
                    {
                        target = new QuickHtml(match.Groups[1].Value)
                            .Attr(ParseAttributes(match.Groups[2].Value))
                            .AppendTo(target).Ctrl;
                    }

                    if (!string.IsNullOrEmpty(match.Groups[3].Value + match.Groups[4].Value)) //Closing tag.
                    {
                        target = target.Parent;
                    }

                    index = match.Index + match.Length;
                }

                if (index < innerHtml.Length)
                {
                    target.Controls.Add(new LiteralControl(innerHtml.Substring(index)));
                }
            }

            return this;
        }

        private JSON ParseAttributes(string value)
        {
            JSON attributes = new JSON();

            //https://regexr.com/708ic - Attributes
            Regex rxAttributes = new Regex("(\\w+)\\s*=\\s*(\"|')([\\s\\S]*?)\\2", RegexOptions.Compiled);

            foreach (Match match in rxAttributes.Matches(value))
            {
                attributes.Add(match.Groups[1].Value, match.Groups[3].Value);
            }

            return attributes;
        }

        /// <summary>
        ///     Sets the ID of the current element.
        /// </summary>
        /// <param name="id">ID attribute.</param>
        /// <returns></returns>
        public QuickHtml SetID(string id)
        {
            ctrl.ID = id;
            Attr("id", id);
            return this;
        }

        /// <summary>
        ///     Gets or sets the visibility of this element. When visible is 'false' the HTML will not render on the client.
        /// </summary>
        /// <returns>This element's visibility.</returns>
        public bool Visible()
        {
            return ctrl.Visible;
        }

        /// <summary>
        ///     Gets or sets the visibility of this element. When visible is 'false' the HTML will not render on the client.
        /// </summary>
        /// <param name="visible">true = Render element, false = do not render.</param>
        /// <returns>This element.</returns>
        public QuickHtml Visible(bool visible)
        {
            ctrl.Visible = visible;
            return this;
        }

        /// <summary>
        ///     Sets the style display of this element to 'none' so that it will not be visible on the page.
        /// </summary>
        /// <returns>This element.</returns>
        public QuickHtml Hide()
        {
            return Css("display", "none");
        }

        /// <summary>
        ///     Clears the style display of this element so that it will be visible on the page.
        /// </summary>
        /// <returns>This element.</returns>
        public QuickHtml Show()
        {
            return RemoveCss("display");
        }

        #endregion

        #region qHTML Navigation

        /// <summary>
        ///     Gets or Sets the child element at the given index.
        /// </summary>
        /// <param name="index">0 = first child element, -1 = last child element.</param>
        /// <returns>Child element at index.</returns>
        public QuickHtml this[int index]
        {
            get
            {
                if (index >= ctrl.Controls.Count || Math.Abs(index) > ctrl.Controls.Count || ctrl.Controls.Count == 0) //Index out of range.
                    return null;
                else if (index < 0)
                    index = (index + ctrl.Controls.Count) % ctrl.Controls.Count;
                return new QuickHtml(ctrl.Controls[index]);
            }
            set
            {
                if (index < ctrl.Controls.Count)
                {
                    if (index < 0)
                        index = (index + ctrl.Controls.Count) % ctrl.Controls.Count;
                    ctrl.Controls.RemoveAt(index);
                    ctrl.Controls.AddAt(index, value.Ctrl);
                }
            }
        }

        /// <summary>
        ///     Gets the parent element of this element. If no parent exists return this element.
        /// </summary>
        /// <returns>A parent element.</returns>
        public QuickHtml Parent()
        {
            if (ctrl.Parent != null)
                return new QuickHtml(ctrl.Parent);
            else
                return new QuickHtml().Append(this);
        }

        /// <summary>
        ///     Returns the first child element with the given selector.
        /// </summary>
        /// <param name="Selector">CSS3 Selector eg tagName: "span", id: "#MySpan", class: ".MySpan", attribute: "[title]" or exact attribute: "[title:My Span]"</param>
        /// <returns>Child element, or null if not found.</returns>
        public QuickHtml Find(string Selector)
        {
            return Find(Selector, 0);
        }

        /// <summary>
        ///     Returns the nth child element with the given selector.
        /// </summary>
        /// <param name="Selector">CSS3 Selector eg tagName: "span", id: "#MySpan", class: ".MySpan", attribute: "[title]" or exact attribute: "[title:My Span]"</param>
        /// <param name="index">Zero based, 0 returns the first element found. 1 returns the second etc...</param>
        /// <returns>Child element, or null if not found.</returns>
        public QuickHtml Find(string Selector, int index)
        {
            for (int i = 0; i < ctrl.Controls.Count; i++)
            {
                QuickHtml element = new QuickHtml(ctrl.Controls[i]);

                if (element.TagName.Equals(Selector) ||
                  (Selector.StartsWith("#") && element.ID.Equals(Selector.Substring(1))) ||
                  (Selector.StartsWith(".") && element.HasClass(Selector.Substring(1))) ||
                  (Selector.StartsWith("[") && Selector.EndsWith("]") && !Selector.Contains(":") && !string.IsNullOrEmpty(element.Attr(Selector.Substring(1, Selector.Length - 2)))) ||
                  (Selector.StartsWith("[") && Selector.EndsWith("]") && Selector.Contains(":") && element.Attr(Selector.Substring(1).Split(':')[0]) == Selector.Split(':')[1].Substring(0, Selector.Split(':')[1].Length - 1))
                )
                {
                    if (index-- <= 0)
                        return element;
                }
            }

            return new QuickHtml();
        }

        #endregion

        #region Output and Operators

        //public static bool operator ==(QuickHtml a, QuickHtml b)
        //{
        //    if (a == null || b == null)
        //        return false;

        //    return a.ctrl == b.ctrl;
        //}

        //public static bool operator !=(QuickHtml a, QuickHtml b)
        //{
        //    return !(a == b);
        //}

        //public override bool Equals(Object obj)
        //{
        //    if (obj == null)
        //        return false;

        //    if (obj.GetType().Name == "QuickHtml")
        //        return this == (QuickHtml)obj;

        //    return false;
        //}

        //public override int GetHashCode()
        //{
        //    return ctrl.GetHashCode();
        //}


        /// <summary>
        ///     Returns the number of children within this element.
        /// </summary>
        public int Length
        {
            get
            {
                return ctrl.Controls.Count;
            }
        }

        /// <summary>
        ///     Returns true if this control contains other controls.
        /// </summary>
        public bool HasControls()
        {
            return (ctrl.Controls.Count > 0);
        }

        /// <summary>
        ///     Returns this element as a Control.
        /// </summary>
        public Control Ctrl
        {
            get
            {
                return this.ctrl;
            }
        }

        /// <summary>
        ///     Gets or sets the ID of the current element.
        /// </summary>
        public string ID
        {
            get
            {
                return ctrl.ID;
            }
            set
            {
                SetID(value);
            }
        }

        /// <summary>
        ///     Returns the tagName of the current element if available.
        /// </summary>
        public string TagName
        {
            get
            {
                if (!ctrl.GetType().Name.Equals("Control"))
                {
                    if (ctrl.GetType().FullName.Contains("HtmlControls"))
                        return ((HtmlControl)ctrl).TagName;
                    else if (ctrl.GetType().FullName.Contains("WebControls"))
                    {
                        System.IO.StringWriter sw = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(sw, "    ");
                        ((WebControl)ctrl).RenderBeginTag(tw);
                        return sw.ToString().Substring(1).Split(' ')[0];
                    }
                    else if (ctrl.GetType().Name.Equals("LiteralControl") && ((LiteralControl)ctrl).Text.StartsWith("<") && ((LiteralControl)ctrl).Text.EndsWith(" />"))
                        return ((LiteralControl)ctrl).Text.Substring(1, ((LiteralControl)ctrl).Text.IndexOf(' ') - 1);
                    else
                        return "";
                }
                return "";
            }
        }

        public Unit Width
        {
            get
            {
                if (!ctrl.GetType().Name.Equals("Control"))
                {
                    if (ctrl.GetType().FullName.Contains("HtmlControls"))
                        return Unit.Parse(((HtmlControl)ctrl).Style["width"]);
                    else if (ctrl.GetType().FullName.Contains("WebControls"))
                        return ((WebControl)ctrl).Width;
                    else
                        return Unit.Empty;
                }
                else
                    return Unit.Empty;
            }
            set
            {
                if (!ctrl.GetType().Name.Equals("Control"))
                {
                    if (ctrl.GetType().FullName.Contains("HtmlControls"))
                        ((HtmlControl)ctrl).Style["width"] = value.ToString();
                    else if (ctrl.GetType().FullName.Contains("WebControls"))
                        ((WebControl)ctrl).Width = value;
                }
            }
        }

        public Unit Height
        {
            get
            {
                if (!ctrl.GetType().Name.Equals("Control"))
                {
                    if (ctrl.GetType().FullName.Contains("HtmlControls"))
                        return Unit.Parse(((HtmlControl)ctrl).Style["height"]);
                    else if (ctrl.GetType().FullName.Contains("WebControls"))
                        return ((WebControl)ctrl).Height;
                    else
                        return Unit.Empty;
                }
                else
                    return Unit.Empty;
            }
            set
            {
                if (!ctrl.GetType().Name.Equals("Control"))
                {
                    if (ctrl.GetType().FullName.Contains("HtmlControls"))
                        ((HtmlControl)ctrl).Style["width"] = value.ToString();
                    else if (ctrl.GetType().FullName.Contains("WebControls"))
                        ((WebControl)ctrl).Width = value;
                }
            }
        }

        
        /// <summary>
        ///     Returns this elemented as a formatted HTML string.
        /// </summary>
        /// <returns>This outer HTML.</returns>
        public override string ToString()
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(sw, "    ");
            ctrl.RenderControl(tw);
            return sw.ToString();
        }

        /// <summary>
        ///     Sends the current QuickHtml back to the client via the given HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        public void DoResponse()
        {
            this.DoResponse(true);
        }

        /// <summary>
        ///     Sends the current QuickHtml back to the client via the given HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="completeResponse">Default: True = Send only the HTML and end the Response. False = Append the HTML and leave the Response open.</param>
        public void DoResponse(bool completeResponse)
        {
            if (completeResponse)
                HttpContext.Current.Response.Clear();

            HttpContext.Current.Response.ContentType = "text/html";
            HttpContext.Current.Response.Write(this.ToString());
            HttpContext.Current.Response.Write('\n');

            if (completeResponse)
            {
                //The following doesn't throw an exception, but allowes code execution to continue after this response.
                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                
                //The following ends execution but throws an exception, which should be ignored.
                HttpContext.Current.Response.End();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="markdown"></param>
        /// <returns></returns>
        public static QuickHtml FromMarkdown(string markdown)
        {
            QuickHtml html = new QuickHtml();

            return html;
        }

        //private QuickHtml AutoFormat(string text, bool H1)
        //{
        //    int h1Limit = H1 ? _H1Limit : -1;
        //    int h2Limit = _H2Limit;

        //    QuickHtml container = new QuickHtml();

        //    int i = 0; //Effective line number (skips images)
        //    string lastline = "";
        //    QuickHtml inner = new QuickHtml();

        //    QuickHtml target = container;

        //    foreach (string line in text.Split('\n'))
        //    {
        //        if (string.IsNullOrEmpty(line.Trim()))
        //        {
        //            i = 0;
        //            h2Limit = Math.Abs(h2Limit);
        //            if ((lastline.Trim().StartsWith("-") && !line.Trim().StartsWith("-")) || (lastline.Contains("|") && !line.Contains("|")))
        //                inner.AppendTo(target);
        //        }
        //        else
        //        {
        //            //Increment line number unless current line is an image tag.
        //            if (!line.Trim().StartsWith("<") || !line.Trim().EndsWith(">"))
        //                i++;

        //            //wrap lists
        //            if (line.Trim().StartsWith("-") && !lastline.Trim().StartsWith("-"))
        //                inner = new QuickHtml("ul");
        //            else if (lastline.Trim().StartsWith("-") && !line.Trim().StartsWith("-"))
        //                inner.AppendTo(target);

        //            //wrap tables
        //            if (line.Contains("|") && !lastline.Contains("|"))
        //                inner = new QuickHtml("table").AddClass("DataTable");
        //            else if (lastline.Contains("|") && !line.Contains("|"))
        //                new QuickHtml("div").AddClass("WideContent").Append(inner).AppendTo(target);

        //            //determine line formatting
        //            if (line.Trim().StartsWith("-")) //Un-ordered List Item
        //                new QuickHtml("li", line.Trim().Substring(1)).AppendTo(inner);
        //            else if (line.Contains("|")) //Table Row
        //                GenerateTableRow(new JSON(String.Concat("[\"", line.Replace("|", "\",\""), "\"]"))).AppendTo(inner);
        //            else if (new Regex("^<[0-9 .,-]*>$").IsMatch(line.Trim())) //Slide Show <{No of Images}.{width}.{height}> (Images [{Percentage Offset}]
        //                GenerateAnimation(new JSON(String.Concat("[", line.Trim().Substring(1, line.Trim().Length - 2).Replace('.', ','), "]"))).AppendTo(target);
        //            else if (line.Trim().StartsWith("<") && line.Trim().EndsWith(">")) //Image <{Title}>
        //                InlineImages.Add(new QuickHtml("span").Attr("title", line.Trim().Substring(1, line.Trim().Length - 2)).AppendTo(target));
        //            else if (line.Trim().StartsWith("*") && line.Trim().EndsWith("*") && line.Length > 1) //H3 Title
        //                new QuickHtml("h3", line.Trim().Substring(1, line.Trim().Length - 2).Trim()).AppendTo(target);
        //            else if ((i == 1) && (line.Length <= h1Limit) && !line.Trim().StartsWith("*")) //H1 Title
        //            {
        //                target.Append(new QuickHtml("h1", line));
        //                h1Limit = -1; //Only 1 H1 Title allowed per Section.
        //            }
        //            else if ((i <= 2) && (line.Length <= h2Limit) && !line.Trim().StartsWith("*")) //H2 Title
        //            {
        //                target.Append(new QuickHtml("h2", line));
        //                h2Limit *= -1; //Only 1 H2 Title per paragraph.
        //            }
        //            else if (i <= 2 && line.Trim().StartsWith("*"))
        //            {
        //                //Ignore Line / Skip Header.
        //            }
        //            else //Any other paragraph.
        //                target.Append(new QuickHtml("p", ReplaceKeywords(line)));

        //        }
        //        lastline = line;
        //    }

        //    return container;
        //}

        //private QuickHtml GenerateTableRow(JSON Cells)
        //{
        //    Boolean alt = false;
        //    QuickHtml tr = new QuickHtml("tr").AddClass("alt", alt = !alt);

        //    for (int i = 0; i < Cells.Length; i++)
        //    {
        //        String text = new Regex("^\\.*|\\.*$").Replace(Cells[i].SafeString, "");

        //        //Cell type
        //        if (String.IsNullOrEmpty(Cells[i].SafeString.Trim()))
        //        {
        //            int Span;
        //            int.TryParse(tr[-1].Attr("ColSpan"), out Span);
        //            Span = Math.Max(Span + 1, 2);
        //            tr[-1].Attr("colspan", Span.ToString());
        //        }
        //        else if (String.IsNullOrEmpty(text.Trim()))
        //            tr.Append(new QuickHtml("td", "&nbsp;"));
        //        else if (text.Trim().StartsWith("*") && text.Trim().StartsWith("*"))
        //            tr.Append(new QuickHtml("th", text.Trim().Substring(1, text.Trim().Length - 2)));
        //        else
        //            tr.Append(new QuickHtml("td", text.Trim()));

        //        //Alignment
        //        if (!String.IsNullOrEmpty(text.Trim()))
        //        {
        //            if (Cells[i].SafeString.StartsWith("..") && Cells[i].SafeString.EndsWith(".."))
        //                tr[-1].Css("text-align", "center");
        //            else if (Cells[i].SafeString.StartsWith(".."))
        //                tr[-1].Css("text-align", "right");
        //        }
        //    }

        //    return tr;
        //}


        /// <summary>
        ///     Formats plain text into html paragraphs split at the "\n" character and replaces keywords.
        /// </summary>
        public static QuickHtml AutoP(string text)
        {
            return new QuickHtml().Html($"<p>{ text.Replace("\n", "</p><p>") }</p>");
        }

        /// <summary>
        ///     Formats plain text into HTML paragraphs split at a double "\n\n" character and line breaks at the "\n" character and replaces keywords.
        /// </summary>
        public static QuickHtml AutoBR(string text)
        {
            return new QuickHtml().Html($"<p>{ text.Replace("\n\n", "</p><p>").Replace("\n", "<br />") }</p>");
        }

        /// <summary>
        ///     Converts urls within the text into links.
        /// </summary>
        public static string WrapLinks(string text)
        {
            Regex rx = new Regex("((([a-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[\\w.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[\\w.-]+)((?:\\/[\\+~%\\/\\w.-]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)");

            foreach (Match match in rx.Matches(text))
            {
                try
                {
                    Uri URL = new Uri((match.Value.Contains(":") ? "" : "http://") + match.Value);
                    text = text.Replace(match.Value, $"<a href=\"{ URL.AbsoluteUri }\"{ (URL.Host.Equals(HttpContext.Current.Request.Url.Host) ? "" : "target=\"_blank\"") }>{ match.Value }</a>");
                }
                catch (Exception exp)
                {
                    //Ignore anything that is incorrectly identified as a URL
                }
            }

            return text;
        }

        /// <summary>
        ///     Returns true if given text contains a url.
        /// </summary>
        public static bool HasLinks(string text)
        {
            return new Regex("((([a-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[\\w.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[\\w.-]+)((?:\\/[\\+~%\\/\\w.-]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)").IsMatch(text);
        }

        /// <summary>
        ///     Wraps the given image in the given div. Ensure the div has hidden overflow in order to crop the image.
        ///     Ensure Image and Div have both Width and Height dimentions, all in the same format (eg "px" or "%")
        /// </summary>
        public static QuickHtml CropImage(Image img, QuickHtml div, double PercOffsetX, double PercOffsetY)
        {
            PercOffsetX = Math.Max(Math.Min(100, PercOffsetX), 0);
            PercOffsetY = Math.Max(Math.Min(100, PercOffsetY), 0);

            double RangeX;
            double RangeY;

            if (div.Width.Type == UnitType.Percentage)
            {
                //Determine relative offset height of the div
                double divHeight = 100 * div.Height.Value / div.Width.Value; //Height as a percentage of the div width.

                //Image should be 100% width (or height) of div.
                RangeX = 100 - img.Width.Value;
                RangeY = divHeight - img.Height.Value;

                img.Style["margin-left"] = $"{ RangeX * PercOffsetX / 100.0 }%";
                img.Style["margin-top"] = $"{ RangeY * PercOffsetY / 100.0 }%";

                div.Append(new QuickHtml("div")
                    .Css("padding-top", $"{ divHeight }%") //Sets the relative height.
                ).RemoveCss("height"); //Remove psudo height.

            }
            else
            {
                RangeX = div.Width.Value - img.Width.Value;
                RangeY = div.Height.Value - img.Height.Value;

                img.Style["margin-left"] = $"{ RangeX * PercOffsetX / 100.0 }px";
                img.Style["margin-top"] = $"{ RangeY * PercOffsetY / 100.0 }px";
            }

            div.Append(img);

            return div;
        }

        #endregion


        IEnumerator<QuickHtml> IEnumerable<QuickHtml>.GetEnumerator()
        {
            foreach (Control child in ctrl.Controls)
                yield return new QuickHtml(child);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Control child in ctrl.Controls)
                yield return new QuickHtml(child);
        }
    }
}
