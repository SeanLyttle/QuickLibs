﻿using System;
using System.Text;
using System.Collections.Generic;


namespace QuickLibs.Utils
{

    /// <summary>
    /// Summary description for NewsFeeder
    /// </summary>
    public class TimeSpanNotation
    {
        private TimeSpan _ts;
        private Dictionary<int, string> dd = new Dictionary<int, string> {
            //Reverse order for quicker processing (Most dates will be more than 24hrs away)
            { 86400, "{D}{t}" }, //24 hours
            { 7200,  "{h} hours" }, //2 hours
            { 5300,  "about an hour and a half" },//<90 minutes
            { 3500,  "about an hour" }, //<60 minutes
            { 1700,  "about half an hour" }, //<30 minutes
            { 300,   "{m} minutes" }, //5 minutes
            { 170,   "a few minutes" }, //Just under 3 minutes
            { 100,   "a couple of minutes" }, //Just over a minute and a half
            { 40,    "about a minute" }, // Nearly a minute
            { 5,     "a few seconds" }, // Between 5 and 55...
            { 0,     "" } //Between 0 and 5
        };

        public TimeSpanNotation(TimeSpan newTimeSpan)
        {
            _ts = newTimeSpan;
            if (_ts.TotalSeconds > 0) //Corrects rounding arror for positive TimePans
                _ts = _ts.Add(new TimeSpan(0, 0, 1));
        }

        public TimeSpanNotation(DateTime newDateTime)
        {
            _ts = new TimeSpan(newDateTime.Ticks - DateTime.Now.Ticks);
            if (_ts.TotalSeconds > 0) //Corrects rounding arror for positive TimePans
                _ts = _ts.Add(new TimeSpan(0, 0, 1));
        }

        public TimeSpanNotation(DateTime StartDateTime, DateTime EndDateTime)
        {
            _ts = new TimeSpan(StartDateTime.Ticks - EndDateTime.Ticks);
            if (_ts.TotalSeconds > 0) //Corrects rounding arror for positive TimePans
                _ts = _ts.Add(new TimeSpan(0, 0, 1));
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool days)
        {
            StringBuilder sb = new StringBuilder();

            String str = "";
            DateTime d = new DateTime(_ts.Ticks + DateTime.Now.Ticks);

            if (!days && (_ts.Days < -7 || _ts.Days > 14))
                return d.ToString("dd MMMM") + " at " + d.ToString("HH:mm");

            if (DateTime.Now.Day == d.Day)
            {
                foreach (KeyValuePair<int, string> kvp in dd)
                {
                    str = kvp.Value;
                    if (Math.Abs(_ts.TotalSeconds) >= kvp.Key)
                        break;
                }
            }
            else
                str = "{D}{t}";

            if (String.IsNullOrEmpty(str))
                return "just now";

            str = str
                    .Replace("{m}", Math.Abs(_ts.Minutes).ToString())
                    .Replace("{h}", Math.Abs(_ts.Hours).ToString());


            if (_ts.TotalSeconds > 0) //Future
            {
                if (str.Contains("{D}"))
                {
                    if (_ts.TotalDays <= 1)
                        sb.Append("tomorrow");
                    else if (_ts.Days <= 7)
                        sb.Append(d.ToString("dddd"));
                    else if (_ts.Days <= 14)
                        sb.Append("next " + d.ToString("dddd"));
                    else
                        str = str.Replace("{D}", _ts.Days.ToString() + " days");
                }

                if (sb.Length == 0)
                {
                    sb.Append("in ");
                    sb.Append(str.Replace("{t}", ""));
                    sb.Append(str.EndsWith("s") ? "' time" : "'s time");
                }

                if (str.Contains("{t}"))
                {
                    sb.Append(" at ");
                    sb.Append(d.ToString("HH:mm"));
                }
            }
            else //Past
            {
                if (str.Contains("{D}"))
                {
                    if (_ts.TotalDays >= -1)
                        sb.Append("yesterday");
                    else if (_ts.Days >= -7)
                        sb.Append("last " + d.ToString("dddd"));
                    else
                        str = str.Replace("{D}", (_ts.Days * -1).ToString() + " days");
                }

                if (sb.Length == 0)
                {
                    sb.Append(str.Replace("{t}", ""));
                    sb.Append(" ago");
                }

                if (str.Contains("{t}"))
                {
                    sb.Append(" at ");
                    sb.Append(d.ToString("HH:mm"));
                }
            }

            return sb.ToString();
        }
    }
}
