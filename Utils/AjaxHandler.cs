﻿using QuickLibs.Javascript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AjaxHandler
/// </summary>

namespace QuickLibs.Utils
{
    /// <summary>
    /// Place this AjaxHandler at the top of your Page.Load script.  If the ActionParameter has a value, the
    /// AjaxHandler will interrupt the request.  If the value matches a name in the actions list the following action will be executed.
    /// If no matches are found the default action will be exectued.  The default action is the last 'odd' action in the action list (instead
    /// of a string action name). If no default action is defined AjaxHelper will respond to the client with a JSON exception.
    /// </summary>
    public class AjaxHandler
    {
        /// <summary>
        /// Default: 400 (BadRequest) Override the default HttpStatusCode returned when an exception is thrown.
        /// </summary>
        public static int ErrorStatusCode = 400;

        /// <summary>
        /// Place this AjaxHandler at the top of your Page.Load script.  If the ActionParameter has a value, the
        /// AjaxHandler will interrupt the request.  If the value matches a name in the actions list the following action will be executed.
        /// If no matches are found the default action will be exectued.  The default action is the last 'odd' action in the action list (instead
        /// of a string action name). If no default action is defined AjaxHelper will respond to the client with a JSON exception.
        /// </summary>
        /// <param name="action">The action name to execute OR request parameter containing the desired action name.</param>
        /// <param name="actions">Alternating list of (string) action names and (Action) action methods. Use the (Action) cast to reference existing methods.</param>
        /// <returns>Returns true if a valid Action is found.</returns>
        public static bool Do(string action, params object[] actions)
        {
            if (HttpContext.Current != null)
                return DoParam(action, actions);
            else
                return DoAction(action, actions);
        }

        /// <summary>
        /// Place this AjaxHandler at the top of your Page.Load script.  If the ActionParameter has a value, the
        /// AjaxHandler will interrupt the request.  If the value matches a name in the actions list the following action will be executed.
        /// If no matches are found the default action will be exectued.  The default action is the last 'odd' action in the action list (instead
        /// of a string action name). If no default action is defined AjaxHelper will respond to the client with a JSON exception.
        /// </summary>
        /// <param name="actionParameter">The Request parameter containing the desired action name.</param>
        /// <param name="actions">Alternating list of (string) action names and (Action) action methods. Use the (Action) cast to reference existing methods.</param>
        /// <returns>Returns true if a valid Action is found.</returns>
        public static bool DoParam(string actionParameter, params object[] actions)
        {
            try
            {
                if (HttpContext.Current != null)
                    return DoAction(HttpContext.Current.Request[actionParameter], actions);
                else
                    return DoAction(null, actions);
            }
            catch (InvalidOperationException exp)
            {
                throw new InvalidOperationException(actionParameter + " " + exp.Message);
            }
        }

        /// <summary>
        /// Place this AjaxHandler at the top of your Page.Load script.  If the ActionParameter has a value, the
        /// AjaxHandler will interrupt the request.  If the value matches a name in the actions list the following action will be executed.
        /// If no matches are found the default action will be exectued.  The default action is the last 'odd' action in the action list (instead
        /// of a string action name). If no default action is defined AjaxHelper will respond to the client with a JSON exception.
        /// </summary>
        /// <param name="action">The action name to execute.</param>
        /// <param name="actions">Alternating list of (string) action names and (Action) action methods. Use the (Action) cast to reference existing methods.</param>
        /// <returns>Returns true if a valid Action is found.</returns>
        public static bool DoAction(string action, params object[] actions)
        {
            try
            {
                //If action is empty - ignore request.
                if (!string.IsNullOrEmpty(action))
                {
                    int i = 0;
                    //Find the first matching action string value.
                    while (i < actions.Length - 1 && !actions[i].Equals(action))
                    {
                        if (!actions[i].GetType().Name.Equals("String"))
                            throw new ArgumentException("Invalid argument type in actions[" + i + "]. Expected: " + "".GetType().FullName + ". Found: " + actions[i].GetType().FullName);

                        i += 2;
                    }

                    //If found, index the action itself.
                    if (i < actions.Length && actions[i].Equals(action))
                        i++;

                    //If we're pointing at an action, execute it.
                    if (i < actions.Length)
                    {
                        if (!actions[i].GetType().Name.Equals("Action"))
                            throw new ArgumentException("Invalid argument type in actions[" + i + "]. Expected: " + typeof(Action).FullName + ". Found: " + actions[i].GetType().FullName);

                        //Invoke action - which should end the response
                        ((Action)actions[i]).Invoke();

                        //If response hasn't already ended. 
                        throw new NotImplementedException("'" + action + "' did not return a valid response.");
                    }
                    else //If not found AND no default action is defined, throw exception.
                        throw new InvalidOperationException("'" + action + "' not found.");
                }
            }
            catch (System.Threading.ThreadAbortException exp)
            {
                //Do Nothing - this is expected behaviour with AJAX responses.
            }
            catch (Exception exp)
            {
                new JSON(exp).DoResponse(ErrorStatusCode);
            }
            return false;
        }

    }
}