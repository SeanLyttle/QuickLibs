﻿using QuickLibs.Javascript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLibs.Utils
{
    /// <summary>
    /// The logging class provides a simple standard interface for logging
    /// </summary>
    public class Logging
    {
        /// <summary>
        /// Default: Red. Colour used to display error messages in the console.
        /// </summary>
        public ConsoleColor ErrorColor = ConsoleColor.Red;
        /// <summary>
        /// Default: Yellow. Colour used to display warning messages in the console.
        /// </summary>
        public ConsoleColor WarningColor = ConsoleColor.Yellow;
        /// <summary>
        /// Default: Blue. Colour used to display debug messages in the console.
        /// </summary>
        public ConsoleColor DebugColor = ConsoleColor.Blue;
        /// <summary>
        /// Default: Gray. Colour used to display information messages in the console.
        /// </summary>
        public ConsoleColor InfoColor = ConsoleColor.Gray;

        /// <summary>
        /// Default: False. True = All information messages will be output to the console. False = Only Error and Warning messages will be output.
        /// </summary>
        public bool VerboseOutput = false;
        /// <summary>
        /// Default: True. True = Log is returned immediately to the Console output. False = remain quiet.
        /// </summary>
        public bool OutputLog = true;
        /// <summary>
        /// Default: False. When True, Debug messages will be output to the console. (Regardless of the "OutputLog" value)
        /// </summary>
        public bool DebugMode = false;
        /// <summary>
        /// Default: null. Adds a prefix to the beginning of all log messages.
        /// </summary>
        public string Prefix = null;
        /// <summary>
        /// Default: null. Adds a suffix to the end of all log messages.
        /// </summary>
        public string Suffix = null;

        /// <summary>
        /// Internal LOG. An array of all log messages returned this session.
        /// </summary>
        public JSON LOG;

        /// <summary>
        /// Internal dynamic data.
        /// </summary>
        public JSON Data = new JSON();

        /// <summary>
        /// True = LOG will be recorded internally. False = Internal LOG will be discarded.
        /// </summary>
        public bool RecordLog
        {
            get {
                return LOG != null;
            }
            set {
                if (value && LOG == null)
                    LOG = new JSON();
                if (!value && LOG != null)
                    LOG = null;
            }
        }

        /// <summary>
        /// Maintains a cumulative count of Error messages.
        /// </summary>
        public int ErrorCount
        {
            get; protected set;
        }

        /// <summary>
        /// Maintains a cumulative count of Warning messages.
        /// </summary>
        public int WarningCount
        {
            get; protected set;
        }

        /// <summary>
        /// Maintains a cumulative count of Info messages.
        /// </summary>
        public int InfoCount
        {
            get; protected set;
        }

        /// <summary>
        /// Maintains a cumulative count of Debug messages.
        /// </summary>
        public int DebugCount
        {
            get; protected set;
        }

        /// <summary>
        /// Maintains a cumulative count of all messages.
        /// </summary>
        public int MessageCount
        {
            get {
                return ErrorCount + WarningCount + InfoCount + DebugCount;
            }
        }



        /// <summary>
        /// Creates an instance of the Logging Utility.
        /// </summary>
        public Logging()
        {
        }

        /// <summary>
        /// Creates an instance of the Logging Utility with a blank JSON Log.
        /// </summary>
        /// <param name="recordLog">Defalut: False. True = LOG will be recorded internally. False = Internal LOG will be discarded.</param>
        public Logging(bool recordLog)
        {
            RecordLog = recordLog;

        }

        /// <summary>
        /// Creates an instance of the Logging Utility with a blank JSON Log.
        /// </summary>
        /// <param name="recordLog">Defalut: False. True = LOG will be recorded internally. False = Internal LOG will be discarded.</param>
        /// <param name="outputLog">Default: True. True = Log is returned immediately to the Console output. False = remain quiet.</param>
        public Logging(bool recordLog, bool outputLog)
        {
            RecordLog = recordLog;
            OutputLog = outputLog;
        }

        /// <summary>
        /// Creates an instance of the Logging Utility using an existing JSON Log.
        /// </summary>
        /// <param name="Log">A JSON Array to store future log messages.</param>
        public Logging(JSON Log)
        {
            LOG = Log;
        }

        /// <summary>
        /// Creates an instance of the Logging Utility using an existing JSON Log.
        /// </summary>
        /// <param name="Log">A JSON Array to store future log messages.</param>
        /// <param name="outputLog">Default: True. True = Log is returned immediately to the Console output. False = remain quiet.</param>
        public Logging(JSON Log, bool outputLog)
        {
            LOG = Log;
            OutputLog = outputLog;
        }

        /// <summary>
        ///     Returns an information message to the log.
        /// </summary>
        /// <param name="Message">Message text to return to the log.</param>
        public void Info(string Message)
        {
            Log(-1, Message, LogType.Information);
        }

        /// <summary>
        ///     Returns an information message to the log.
        /// </summary>
        /// <param name="Message">Message text to return to the log.</param>
        /// <param name="Verbose">Force this message to use verbose logging.</param>
        public void Info(string Message, bool Verbose)
        {
            bool vo = VerboseOutput;
            VerboseOutput = Verbose;
            Log(-1, Message, LogType.Information);
            VerboseOutput = vo;
        }

        /// <summary>
        ///     Returns an information message to the log.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Message">Message text to return to the log.</param>
        public void Info(int Code, string Message)
        {
            Log(Code, Message, LogType.Information);
        }

        /// <summary>
        ///     Returns an error message to the log base on the given Exception.
        ///     If VerboseOutput=true, full exception detilas will be returned.
        ///     If VerboseOutput=false, the basic exp.Message will be returned.
        /// </summary>
        public void Info(Exception exp)
        {
            if (VerboseOutput)
                Log(-1, exp.ToString(), LogType.Information);
            else
                Log(-1, exp.Message, LogType.Information);
        }

        /// <summary>
        /// Returns a debug message to the console. Debug messages are never recorded.
        /// </summary>
        /// <param name="Message">Message text to return to the log.</param>
        public void Debug(string Message)
        {
            Log(-1, Message, LogType.Debug);
        }

        /// <summary>
        /// Returns a debug message to the console. Debug messages are never recorded.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Message">Message text to return to the log.</param>
        public void Debug(int Code, string Message)
        {
            Log(Code, Message, LogType.Debug);
        }

        /// <summary>
        ///     Returns a debug message to the console base on the given Exception. Debug messages are never recorded.
        ///     If VerboseOutput=true, full exception detilas will be returned.
        ///     If VerboseOutput=false, the basic exp.Message will be returned.
        /// </summary>
        public void Debug(Exception exp)
        {
            if (VerboseOutput)
                Log(-1, exp.ToString(), LogType.Debug);
            else
                Log(-1, exp.Message, LogType.Debug);
        }

        /// <summary>
        /// Returns a warning message to the log.
        /// </summary>
        /// <param name="Message">Message text to return to the log.</param>
        public void Warning(string Message)
        {
            Log(-1, Message, LogType.Warning);
        }

        /// <summary>
        /// Returns a warning message to the log.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Message">Message text to return to the log.</param>
        public void Warning(int Code, string Message)
        {
            Log(Code, Message, LogType.Warning);
        }

        /// <summary>
        ///     Returns a warning message to the log base on the given Exception.
        ///     If VerboseOutput=true, full exception detilas will be returned.
        ///     If VerboseOutput=false, the basic exp.Message will be returned.
        /// </summary>
        public void Warning(Exception exp)
        {
            if (VerboseOutput)
                Log(-1, exp.ToString(), LogType.Warning);
            else
                Log(-1, exp.Message, LogType.Warning);
        }

        /// <summary>
        ///     Returns an error message to the log with the given code.
        /// </summary>
        /// <param name="Message">Message text to return to the log.</param>
        public void Error(string Message)
        {
            Log(-1, Message, LogType.Error);
        }

        /// <summary>
        ///     Returns an error message to the log with the given code.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Message">Message text to return to the log.</param>
        public void Error(int Code, string Message)
        {
            Log(Code, Message, LogType.Error);
        }

        /// <summary>
        ///     Returns an error message to the log base on the given Exception.
        ///     If VerboseOutput=true, full exception detilas will be returned.
        ///     If VerboseOutput=false, the basic exp.Message will be returned.
        /// </summary>
        public void Error(Exception exp)
        {
            if (VerboseOutput)
                Log(-1, exp.ToString(), LogType.Error);
            else
                Log(-1, exp.Message, LogType.Error);
        }

        /// <summary>
        ///     Returns a message to the consolde log with the given code and type.
        /// </summary>
        /// <param name="Code">Error / Warning code.</param>
        /// <param name="Message">Message text to return to the log.</param>
        /// <param name="Type">INFORMATION, WARNING or ERROR. If unspecified: Defaults to INFORMATION when no code is given. Defaults to ERROR when code is supplied.</param>
        private void Log(int Code, string Message, LogType Type)
        {
            string msg;
            Console.BackgroundColor = ConsoleColor.Black;
            switch (Type)
            {
                case LogType.Error:
                    Console.ForegroundColor = ErrorColor;
                    msg = (Prefix + ": ERROR").TrimStart(':',' ');
                    ErrorCount++;
                    break;
                case LogType.Warning:
                    Console.ForegroundColor = WarningColor;
                    msg = (Prefix + ": WARNING").TrimStart(':', ' ');
                    WarningCount++;
                    break;
                case LogType.Debug:
                    Console.ForegroundColor = DebugColor;
                    msg = Prefix;
                    DebugCount++;
                    break;
                default:
                    Console.ForegroundColor = InfoColor;
                    msg = Prefix;
                    InfoCount++;
                    break;
            }

            if (Code != -1)
                msg = string.Concat(msg, " ", Code);

            msg = string.Concat(msg, ": ", Message, Suffix).TrimStart(':', ' ');

            if ((OutputLog && (Type != LogType.Information || VerboseOutput) && Type != LogType.Debug) || (Type == LogType.Debug && DebugMode))
                Console.WriteLine(msg);

            if ((JSON.IsDefined(LOG) && (Type != LogType.Information || VerboseOutput) && Type != LogType.Debug) || (Type == LogType.Debug && DebugMode))
                LOG.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + msg);

            Console.ResetColor();
        }

    }

    /// <summary>
    /// Log type enumerator.
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// Information.
        /// </summary>
        Information,
        /// <summary>
        /// Debug.
        /// </summary>
        Debug,
        /// <summary>
        /// Warning.
        /// </summary>
        Warning,
        /// <summary>
        /// Error.
        /// </summary>
        Error
    };
}
