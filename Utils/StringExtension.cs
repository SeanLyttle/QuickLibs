﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickLibs.Extensions.String
{
    /// <summary>
    /// A collection of string extensions to help with string manipulaiton.
    /// </summary>
    public static class StringExtension
    {
        #region Utilities
        /// <summary>
        /// Returns the number of words in this string.
        /// </summary>
        public static int WordCount(this string str)
        {
            return str.WordCount(new char[] { ' ' });
        }

        /// <summary>
        /// Returns the number of words in this string.
        /// </summary>
        /// <param name="wordBreaks">Default: [' ']. A list of characters that represent a break between words.</param>
        public static int WordCount(this string str, char[] wordBreaks)
        {
            if (string.IsNullOrEmpty(str))
                return 0;

            return str.Split(wordBreaks, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        /// <summary>
        /// Returns a new string in which all matches for the specified regular expression pattern are replaced with the another specified string.
        /// </summary>
        /// <param name="pattern">The regular expression pattern to match.</param>
        /// <param name="replacement">The string to replace all matches.</param>
        /// <param name="useRegularExpression">Use the input pattern as a regular expression?</param>
        public static string Replace(this string str, string pattern, string replacement, bool useRegularExpression)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (!useRegularExpression)
                return str.Replace(pattern, replacement);

            if (pattern == null)
                return str;

            Regex regex = new Regex(pattern);
            if (pattern == null || !regex.IsMatch(str))
                return str;

            return regex.Replace(str, replacement);
        }

        /// <summary>
        /// Returns a new string in which all matches for the specified regular expression are replaced with the another specified string.
        /// </summary>
        /// <param name="regex">The regular expression to match.</param>
        /// <param name="replacement">The string to replace all matches.</param>
        public static string Replace(this string str, Regex regex, string replacement)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (regex == null || !regex.IsMatch(str))
                return str;

            return regex.Replace(str, replacement);
        }

        /// <summary>
        /// Returns a new string in which all matches for the specified regular expression are replaced with the another specified string.
        /// </summary>
        /// <param name="regex">The regular expression to match.</param>
        /// <param name="replacement">The string to replace all matches.</param>
        /// <param name="count">The maximum number of times the replacement can occur.</param>
        public static string Replace(this string str, Regex regex, string replacement, int count)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (regex == null || !regex.IsMatch(str))
                return str;

            return regex.Replace(str, replacement, count);
        }

        /// <summary>
        /// Returns a new string in which all matches for the specified regular expression are replaced with the another specified string.
        /// </summary>
        /// <param name="regex">The regular expression to match.</param>
        /// <param name="replacement">The string to replace all matches.</param>
        /// <param name="count">The maximum number of times the replacement can occur.</param>
        /// <param name="startat">The character position where the search begins.</param>
        public static string Replace(this string str, Regex regex, string replacement, int count, int startat)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (regex == null || !regex.IsMatch(str))
                return str;

            return regex.Replace(str, replacement, count, startat);
        }
        #endregion

        #region Case
        /// <summary>
        /// Returns this string in sentence case. (First letter of every sentence is capitalised).
        /// </summary>
        public static string ToSentenceCase(this string str)
        {
            return str.ToSentenceCase(new char[] { '.', '?', '!' });
        }

        /// <summary>
        /// Returns this string in sentence case. (First letter of every sentence is capitalised).
        /// </summary>
        /// <param name="sentenceBreaks">Default: {'.', '?', '!'}. Custom array of sentence break characters.</param>
        public static string ToSentenceCase(this string str, char[] sentenceBreaks)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.IsBreakable())
                str = str.BreakWords();

            char[] chars = str.ToCharArray();

            string sb = Regex.Escape(new string(sentenceBreaks));
            Regex words = new Regex($@"(?:^|[{ sb }]\s)(\w+)", RegexOptions.Compiled);

            foreach (Match word in words.Matches(str))
            {
                int charIndex = word.Groups[1].Index;
                chars[charIndex] = chars[charIndex].ToUpper();
            }

            return new string(chars);
        }


        /// <summary>
        /// Returns this string in name case. (First letter of each name is capitalised, including some special cases).
        /// </summary>
        public static string ToNameCase(this string str)
        {
            return str.ToNameCase(new string[] { "Mc", "Mac", "O'" });
        }

        /// <summary>
        /// Returns this string in name case. (First letter of each name is capitalised, including some special cases).
        /// </summary>
        /// <param name="nameSpecials">Default: {"Mc", "Mac", "O'"}. Custom array of special name strings. </param>
        public static string ToNameCase(this string str, string[] nameSpecials)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            str = str.ToTitleCase();
            char[] chars = str.ToCharArray();

            int i = 0;
            while (i < str.Length - 1)
            {                
                int index = str.Length;
                foreach (string value in nameSpecials)
                {
                    int idx = str.IndexOf(value, i);
                    if (idx != -1)
                        index = Math.Min(index, idx + value.Length);
                }

                i = index;

                if(i < str.Length)
                    chars[i] = chars[i].ToUpper();
            }

            return new string(chars);
        }


        /// <summary>
        /// Returns this string in title case. (First letter of each word is capitalised).
        /// </summary>
        public static string ToTitleCase(this string str)
        {
            return str.ToTitleCase(3);
        }

        /// <summary>
        /// Returns this string in title case. (First letter of each word is capitalised).
        /// </summary>
        /// <param name="minLength">Default: 3. Only capitalize words of this or more characters.</param>
        public static string ToTitleCase(this string str, int minLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.IsBreakable())
                str = str.BreakWords();

            char[] chars = str.ToCharArray();

            Regex words = new Regex(@"\b\w+\b", RegexOptions.Compiled);

            foreach (Match word in words.Matches(str))
            {
                if (word.Value.Length >= minLength)
                {
                    int charIndex = word.Index;
                    chars[charIndex] = chars[charIndex].ToUpper();
                }
            }

            return new string(chars);            
        }


        /// <summary>
        /// Returns this string in camelCase. (First letter is lower case, then first letter of each subsequent word is capitalised and spaces removed).
        /// </summary>
        public static string ToCamelCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (!str.IsPascalCase())
                str = str.ToPascalCase();

            return str.First().ToLower() + str.Substring(1);
        }

        /// <summary>
        /// Returns this string in PascalCase. (First letter of each word is capitalised and spaces removed).
        /// </summary>
        public static string ToPascalCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.IsSnakeCase())
                str = str.BreakWords();

            if (str.IsCamelCase())
                return str.First().ToUpper() + str.Substring(1);

            return str.ToLower().ToTitleCase(0).Replace(" ", "");
        }

        /// <summary>
        /// Returns this string in snake_case. (All lowercase with words separated by underscores).
        /// </summary>
        public static string ToSnakeCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.IsCamelCase() || str.IsPascalCase())
                str = str.BreakWords();

            return str.ToLower().Replace(' ', '_');
        }

        /// <summary>
        /// Returns this PascalCase, camelCase or snake_case string as expanded string.
        /// </summary>
        public static string BreakWords(this string str)
        {
            Regex br = new Regex(@"\B[A-Z]");
            return br.Replace(str, " $&").Replace('_',' ');
        }


        /// <summary>
        /// Returns true if this string is camelCase.
        /// </summary>
        public static bool IsCamelCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            Regex camel = new Regex(@" ^[^A-Z\W][a-z0-9]*(?:[A-Z][a-z0-9]*)*$", RegexOptions.Compiled);
            return camel.IsMatch(str);
        }

        /// <summary>
        /// Returns true if this string is PascalCase.
        /// </summary>
        public static bool IsPascalCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            Regex camel = new Regex(@"^[^a-z\W][a-z0-9]*(?:[A-Z][a-z0-9]*)*$", RegexOptions.Compiled);
            return camel.IsMatch(str);
        }

        /// <summary>
        /// Returns true if this string is snake_case.
        /// </summary>
        public static bool IsSnakeCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            Regex snake = new Regex(@"^(?:[a-zA-Z0-9-]*_?)*$", RegexOptions.Compiled);
            return snake.IsMatch(str);
        }

        public static bool IsBreakable(this string str)
        {
            return str.IsPascalCase() || str.IsCamelCase() || str.IsSnakeCase();
        }
        #endregion

        #region Plurals
        /// <summary>
        /// Returns this string with an "s" appended dependant on whether or not the given value should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There are x report".s(x);
        /// This is a copy of the pl() extension.
        /// </summary>
        /// <param name="Value">Number of items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        public static string s(this string str, double Value)
        {
            return str.pl(Value, "", "s");
        }

        /// <summary>
        /// Returns this string with an "s" appended dependant on whether or not the given value should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There are x report".s(x);
        /// </summary>
        /// <param name="Value">Number of items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        public static string pl(this string str, double Value)
        {
            return str.pl(Value, "", "s");
        }

        /// <summary>
        /// Returns this string or the Plural string dependant on whether or not the given value should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There " + "is".Pl(x,"are") + " x report" + "".Pl(x,"s");
        /// </summary>
        /// <param name="Value">Number of items.</param>
        /// <param name="Plural">Expression for multiple items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        public static string pl(this string str, double Value, string Plural)
        {
            return "".pl(Value, str, Plural);
        }

        /// <summary>
        /// Returns this string with one of two strings appended dependant on whether or not the given value should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There ".Pl(x,"is","are") + " x report".Pl(x,"","s");
        /// </summary>
        /// <param name="Value">Number of items.</param>
        /// <param name="Singular">Expression for 1 item.</param>
        /// <param name="Plural">Expression for multiple items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        public static string pl(this string str, double Value, string Singular, string Plural)
        {
            return string.Concat(str, (Value == 1) ? Singular : Plural);
        }
        #endregion

        #region formatting
        /// <summary>
        /// Underlines the last line of text with the given Chars.
        /// </summary>
        public static string Underline(this string str)
        {
            return str.Underline("=", 0);
        }

        /// <summary>
        /// Underlines the last line of text with the given Chars.
        /// </summary>
        /// <param name="Chars">Repeating character pattern to use as the underline.</param>
        public static string Underline(this string str, string Chars)
        {
            return str.Underline(Chars, 0);
        }

        /// <summary>
        /// Underlines the last line of text with the given Chars.
        /// </summary>
        /// <param name="Overhang">Number of characters to overhang at each end.</param>
        public static string Underline(this string str, int Overhang)
        {
            return str.Underline("=", Overhang);
        }

        /// <summary>
        /// Underlines the last line of text with the given Chars.
        /// </summary>
        /// <param name="Chars">Repeating character pattern to use as the underline.</param>
        /// <param name="Overhang">Number of characters to overhang at each end.</param>
        public static string Underline(this string str, string Chars, int Overhang)
        {
            if (Chars.Length > 0)
            {
                string[] lines = str.Split('\n');
                int i = lines.Length - 1;
                string line = "";
                if (i >= 0 && lines[i].Length > 0)
                    line = lines[i];
                else if (--i >= 0 && lines[i].Length > 0)
                    line = lines[i];
                if (i == lines.Length - 1)
                    str += "\n";

                int Length = Math.Max(line.Length + Overhang * 2, 0);
                if (Length > line.Length)
                    str = str.Insert(str.LastIndexOf(line), new string(' ', Overhang));
                else if (Length < line.Length)
                    str += new string(' ', Math.Abs(Overhang));
                str += string.Concat(Enumerable.Repeat(Chars, (int)Math.Ceiling(1.0 * Length / Chars.Length))).Substring(0, Length);

                if (i != lines.Length - 1)
                    str += "\n";
            }
            return str;
        }
        #endregion
    }

    /// <summary>
    /// A collection of char extensions to help with string manipulaiton.
    /// </summary>
    public static class CharExtension {
        /// <summary>
        /// Returns this char in upper case.
        /// </summary>
        public static char ToUpper(this char chr)
        {
            return chr.ToString().ToUpper()[0];
        }
        /// <summary>
        /// Returns this char in lower case.
        /// </summary>
        public static char ToLower(this char chr)
        {
            return chr.ToString().ToLower()[0];
        }
    }
}
