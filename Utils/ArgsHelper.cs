﻿using QuickLibs.Javascript;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickLibs.Utils
{
    public class ArgsHelper : IEnumerable<string>
    {
        private string _help;

        /// <summary>
        /// Default: "?". Specify the argument used to call the help routine.
        /// </summary>
        public string helpArg = "?";

        /// <summary>
        /// Default: '/'. Specify the character used in the help file to prefix switches. (Display only - both '/' and '-' will always be accpted at runtime). 
        /// </summary>
        public char switchChar = '/';

        /// <summary>
        /// Creates a new instance of ArgsHelper which will help extract values from any commandline string array.
        /// </summary>
        public ArgsHelper()
        {
            args = new string[0];
            LoadHelp("args.json");
        }

        /// <summary>
        /// Creates a new instance of ArgsHelper which will help extract values from any commandline string array.
        /// </summary>
        /// <param name="args">A list of key-values or an args string[] array from the commandline parameters.</param>
        public ArgsHelper(params string[] args)
        {
            this.args = args;
            LoadHelp("args.json");
        }

        private void LoadHelp(string argsFile)
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            string resourceName = assembly.GetManifestResourceNames().FirstOrDefault(s => s.EndsWith(argsFile));

            if (!string.IsNullOrEmpty(resourceName))
            {
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                    using (StreamReader reader = new StreamReader(stream))
                        _help = reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Sends formated argument documentation to the console. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        /// <returns>Returns True if args contains "?"</returns>
        public bool CheckHelp() { return CheckHelp(GetConsoleWidth()); }

        /// <summary>
        /// Sends formated argument documentation to the console. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        /// <param name="wrapWidth">Default: Console.WindowWidth. Encourage word-wrap at this number of characters where possible.</param>
        /// <returns>Returns True if args contains helpArg</returns>
        public bool CheckHelp(int wrapWidth)
        {
            if (!GetBool(helpArg))
                return false;

            DoHelp(wrapWidth);
            return true;
        }

        /// <summary>
        /// Sends formated argument documentation to the console. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        public void DoHelp() { DoHelp(GetConsoleWidth()); }

        /// <summary>
        /// Sends formated argument documentation to the console. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        /// <param name="wrapWidth">Default: Console.WindowWidth. Encourage word-wrap at this number of characters where possible (Minimum: 30).</param>
        private void DoHelp(int wrapWidth)
        {
            if (wrapWidth <= 30) //Ignore wrapping.
                Console.Write(GetHelp());
            else
            {
                foreach (string paragraph in GetHelp().Replace("\r","").Split('\n'))
                {
                    int indent = new Regex(@"^ -\S*\s*(?: |\* )", RegexOptions.Compiled).Match(paragraph).Length;
                                    
                    string[] words = paragraph.Split(' ');
                    List<string> lines = words.Skip(1).Aggregate(words.Take(1).ToList(), (list, word) =>
                    {
                        if (list.Last().Length + word.Length >= wrapWidth)
                            list.Add(new string(' ', indent) + word);
                        else
                            list[list.Count - 1] += " " + word;
                        return list;
                    });

                    Console.Write(lines.Aggregate(new StringBuilder(), (sb, line) => {
                        return (line.Length == wrapWidth) ? sb.Append(line) : sb.AppendLine(line);
                    }));
                }
            }
        }

        /// <summary>
        /// Returns argument documentation in a string. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        public string GetHelp()
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(_help))
            {
                helpArg = helpArg.Trim('/', '-');
                
                string UsageString = string.Concat("Usage:\n", Assembly.GetEntryAssembly().ManifestModule.Name.Substring(0, Assembly.GetEntryAssembly().ManifestModule.Name.LastIndexOf('.')));
                JSON data = new JSON(_help);

                //Header
                if (data["ApplicationName"].IsString)
                {
                    sb.Append('=', data["ApplicationName"].String.Length + 2).AppendLine();
                    sb.AppendLine(" " + data["ApplicationName"].String);
                    sb.Append('=', data["ApplicationName"].String.Length + 2).AppendLine();
                    sb.AppendLine();
                }

                if (args.Length == 0 || args[0].TrimStart('-', '/').Equals(helpArg) || !GetBool(helpArg))
                {
                    //Main Description
                    if (data["Description"].IsString)
                        sb.AppendLine(data["Description"].String).AppendLine();

                    //Usage string and Arg Descriptions
                    if (data["Args"].IsDictionary)
                        sb.AppendLine(UsageString + GetUsage(data["Args"])).AppendLine();
                    else if (data["Args"].IsArray)
                        foreach (JSON args in data["Args"])
                            sb.AppendLine(UsageString + GetUsage(args)).AppendLine();

                    //Footer details.
                    if (data["Details"].IsString)
                        sb.AppendLine(data["Details"].String).AppendLine();
                }
                else
                {
                    //Specific Argument Details.
                    JSON arg = data.Find(args[0].TrimStart('-','/'));
                    if (arg["Details"].IsString)
                    {
                        sb.Append(UsageString);
                        GetUsageDef(sb, new JSON(arg), 0);
                        sb.AppendLine().AppendLine().AppendLine();

                        sb.AppendLine(" " + arg.Key);
                        sb.Append('=', arg.Key.Length + 2).AppendLine();
                        sb.AppendLine();

                        sb.AppendLine(arg["Details"].String).AppendLine();
                    }
                    else
                    {
                        sb.Append("The argument '").Append(args[0].TrimStart('-', '/')).AppendLine("' has no specific documentation.").AppendLine();
                    }
                }
            }
            return sb.ToString();
        }

        private string GetUsage(JSON args)
        {
            StringBuilder sb = new StringBuilder();            
            int indent = GetUsageDef(sb, args, helpArg.Length);
            sb.AppendLine().AppendLine();
            GetUsageDesc(sb, indent, args);

            sb.Append("  ").Append(switchChar).Append(helpArg).Append(" ");
            sb.Append(' ', indent - helpArg.Length + 1);
            sb.AppendLine("Display this information.");

            if (args.Find("Details").IsString)
                sb.AppendLine().AppendFormat("  * For more details, type \"{0} <argument> ?\"", Assembly.GetEntryAssembly().ManifestModule.Name.Substring(0, Assembly.GetEntryAssembly().ManifestModule.Name.LastIndexOf('.'))).AppendLine();

            return sb.ToString();
        }

        private int GetUsageDef(StringBuilder sb, JSON args, int indent, bool required = false)
        {

            foreach (JSON arg in args)
            {
                sb.Append(" ");
                if (arg["Required"].IsBoolean ? !arg["Required"].Boolean : !required)
                    sb.Append("[");

                if (!arg["Indexed"].Boolean)
                    sb.Append(switchChar);

                sb.Append(arg.Key);

                if (arg["Value"].IsString)
                {
                    if (arg["ValueOptional"].Boolean)
                        sb.Append("[");
                    sb.Append(":").Append(arg["Value"].String);
                    if (arg["ValueOptional"].Boolean)
                        sb.Append("]");
                }

                if (arg["Args"].IsDictionary) //Sub Args
                    indent = GetUsageDef(sb, arg["Args"], indent);
                else if (arg["Args"].IsArray) //Alt Args
                    foreach (JSON altArgs in arg["Args"])
                        indent = GetUsageDef(sb.Append(" |"), altArgs, indent, true);

                if (arg["Required"].IsBoolean ? !arg["Required"].Boolean : !required)
                    sb.Append("]");               

                indent = Math.Max(indent, arg.Key.Length);
            }
            return indent;
        }

        private string GetUsageDesc(StringBuilder sb, int indent, JSON args)
        {
            foreach (JSON arg in args)
            {
                sb.Append("  ");
                if (!arg["Indexed"].Boolean)
                    sb.Append(switchChar);
                sb.Append(arg.Key);
                sb.Append(' ', indent - arg.Key.Length + (!arg["Indexed"].Boolean ? 0 : 1));
                sb.Append(arg["Details"].IsString ? "* " : "  ");
                sb.Append(arg["Description"].SafeString.Replace("\n", "\n" + new string(' ', indent + (!arg["Indexed"].Boolean ? 5 : 6))));
                if (arg["Default"].IsNumber)
                    sb.Append(" (Default=").Append(arg["Default"].String).Append(")");
                else if (arg["Default"].IsString)
                    sb.Append(" (Default = \"").Append(arg["Default"].String).Append("\")");
                sb.AppendLine();

                if (arg["Args"].IsDictionary)
                    GetUsageDesc(sb, indent, arg["Args"]);
                else if (arg["Args"].IsArray)
                    foreach (JSON subArgs in arg["Args"])
                        GetUsageDesc(sb, indent, subArgs);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Sends argument data to the console. If the 'debug' argument is passed to the process.
        /// </summary>
        public void DoDebug() { DoDebug("debug", GetConsoleWidth()); }

        /// <summary>
        /// Sends argument data to the console. If the debugArg is passed to the process.
        /// </summary>
        /// <param name="debugArg">Default: "debug", overrides the default 'debug' argument.</param>
        public void DoDebug(string debugArg) { DoDebug(debugArg, GetConsoleWidth()); }

        /// <summary>
        /// Sends argument data to the console. If the 'debug' argument is passed to the process.
        /// </summary>
        /// <param name="wrapWidth">Default: Console.WindowWidth. Encourage word-wrap at this number of characters where possible (Minimum: 30).</param>
        public void DoDebug(int wrapWidth) { DoDebug("debug", wrapWidth); }

        /// <summary>
        /// Sends argument data to the console. If the debugArg is passed to the process.
        /// </summary>
        /// <param name="debugArg">Default: "debug", overrides the default 'debug' argument.</param>
        /// <param name="wrapWidth">Default: Console.WindowWidth. Encourage word-wrap at this number of characters where possible (Minimum: 30).</param>
        public void DoDebug(string debugArg, int wrapWidth)
        {
            if (GetBool(debugArg))
            {
                if (wrapWidth <= 30) //Ignore wrapping.
                    Console.Write(GetDebugInfo());
                else
                {
                    foreach (string paragraph in GetDebugInfo().Replace("\r", "").Split('\n'))
                    {
                        int indent = new Regex(@"^ -\S*\s*(?:= )", RegexOptions.Compiled).Match(paragraph).Length;

                        string[] words = paragraph.Split(' ');
                        List<string> lines = words.Skip(1).Aggregate(words.Take(1).ToList(), (list, word) =>
                        {
                            if (list.Last().Length + word.Length >= wrapWidth)
                                list.Add(new string(' ', indent) + word);
                            else
                                list[list.Count - 1] += " " + word;
                            return list;
                        });

                        Console.Write(lines.Aggregate(new StringBuilder(), (sb, line) =>
                        {
                            return (line.Length == wrapWidth) ? sb.Append(line) : sb.AppendLine(line);
                        }));
                    }
                }
            }
        }

        /// <summary>
        /// Returns argument data in a string. Documentation should be saved as "args.json" with the properties.BuildAction = "Embedded Resource".
        /// </summary>
        public string GetDebugInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" Debug Mode ");
            sb.AppendLine("============");

            sb.AppendLine();
            sb.AppendLine("Arguments:");

            int i = 1;
            foreach (string arg in args)
            {
                if(i < 10)
                    sb.Append(" ");
                sb.Append(i++.ToString());
                sb.Append(". ");
                sb.AppendLine(arg);            
            }
            if (i == 1)
                sb.AppendLine(" No Arguments.");


            sb.AppendLine();
            sb.AppendLine("Accepted Arguments:");

            if (!string.IsNullOrEmpty(_help))
            {
                JSON data = new JSON(_help);
                JSON dataArgs = new JSON();

                if (data["Args"].IsDictionary)
                {
                    foreach (JSON arg in data["Args"])
                        if (!dataArgs.Contains(arg.Key))
                            dataArgs.Add(arg.Key);
                }
                else if (data["Args"].IsArray)
                {
                    foreach (JSON args in data["Args"])
                        foreach (JSON arg in args)
                            if (!dataArgs.Contains(arg.Key))
                                dataArgs.Add(arg.Key);
                }

                dataArgs.Sort();

                foreach (JSON arg in dataArgs)
                {
                    string val = GetString(arg.String) ?? "Not given.";

                    if (dataArgs.Contains(val.TrimStart('/', '-')) || val.Trim() == "")
                        val = "True";

                    sb.Append(" -");
                    sb.Append(arg.String);
                    sb.Append(" = ");
                    sb.Append(val);
                    sb.AppendLine();
                }

            }
            return sb.ToString();
        }

        private int GetConsoleWidth()
        {
            try
            {
                return Console.WindowWidth;
            }
            catch (Exception exp)
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets or sets the current argument array.
        /// </summary>
        public string[] args { get; set; }

        /// <summary>
        /// Gets or sets the argument in the current array at the given index.
        /// </summary>
        public string this[int index] {
            get { return args[index]; }
            set { args[index] = value; }
        }

        /// <summary>
        /// Adds a single argument to the end of the current array. If the argument is missing the
        /// leading '/' or '-' character, it will be added automatically.
        /// </summary>
        public ArgsHelper Add(string arg)
        {
            arg = arg.Replace('/', '-').Trim();
            if (!arg.StartsWith("-"))
                arg = "-" + arg;

            string[] newArgs = new string[args.Length + 1];
            args.CopyTo(newArgs, 0);
            args = newArgs;
            args[args.Length - 1] = arg;

            return this;
        }

        /// <summary>
        /// Adds a key-value pair to the end of the current array. If the key is missing the
        /// leading '/' or '-' character, it will be added automatically. The value will not be changed.
        /// </summary>
        /// <param name="key">The string identifier used to retrieve the given value.</param>
        /// <param name="value">The string value associated with the key.</param>
        /// <returns></returns>
        public ArgsHelper Add(string key, string value)
        {
            key = key.Replace('/', '-').Trim();
            if (!key.StartsWith("-"))
                key = "-" + key;

            string[] newArgs = new string[args.Length + 2];
            args.CopyTo(newArgs, 0);
            args = newArgs;
            args[args.Length - 2] = key;
            args[args.Length - 1] = value;

            return this;
        }



        /// <summary>
        /// Returns true if the given key can be found in the current array.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public bool GetBool(string key)
        {
            return GetBool(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns true if the given key can be found in the current array.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public bool GetBool(string key, StringComparison comparisonType)
        {
            key = key.Replace('/', '-').Trim();
            if (!key.StartsWith("-"))
                key = "-" + key;

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i].Replace('/', '-');
                if (!arg.StartsWith("-"))
                    arg = "-" + arg;

                if (arg.Contains(":"))
                    arg = arg.Substring(0, arg.IndexOf(':'));

                if (arg.Equals(key, comparisonType))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the given index can be found in the current array.
        /// </summary>
        /// <param name="index">Zero-based index of item.</param>
        public bool GetBool(int index)
        {
            if (args.Length < index && index >= 0)
                return true;

            return false;
        }

        /// <summary>
        /// Returns the string value assosiated with the key, or null if the key does not reference a value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public string GetString(string key)
        {
            return GetString(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns the string value assosiated with the key, or null if the key does not reference a value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public string GetString(string key, StringComparison comparisonType)
        {
            key = key.Replace('/', '-').Trim();
            if (!key.StartsWith("-"))
                key = "-" + key;

            for(int i = 0; i < args.Length; i++)
            {
                string arg = args[i].Replace('/', '-');
                if (!arg.StartsWith("-"))
                    arg = "-" + arg;

                //Delimeted values
                if (arg.Contains(":"))
                {
                    string val = arg.Substring(arg.IndexOf(':') + 1);
                    arg = arg.Substring(0,arg.IndexOf(':'));
                    if (arg.Equals(key, comparisonType))
                        return val;
                }

                //Consecutive value
                if (arg.Equals(key, comparisonType) && i < args.Length - 1 && !args[i + 1].StartsWith("/") && !args[i + 1].StartsWith("-"))
                    return args[i + 1];
            }

            return null;
        }

        /// <summary>
        /// Returns the string value assosiated with the key or "True" if the key exists with no value, or null if the key does not reference a value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public string GetStringOrBool(string key)
        {
            return GetStringOrBool(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns the string value assosiated with the key or "True" if the key exists with no value, or null if the key does not reference a value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public string GetStringOrBool(string key, StringComparison comparisonType)
        {
            return GetString(key, comparisonType) ?? (GetBool(key, comparisonType) ? "True" : null);
        }

        /// <summary>
        /// Returns the string value at the given index, or null if the index does not reference a value.
        /// </summary>
        /// <param name="index">Zero-based index of item.</param>
        public string GetString(int index)
        {
            if (index < args.Length && index >= 0 && !args[index].StartsWith("/") && !args[index].StartsWith("-"))
                return args[index];

            return null;
        }

        /// <summary>
        /// Returns the integer value assosiated with the key, or 0 if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public int GetInt(string key)
        {
            return GetInt(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns the integer value assosiated with the key, or 0 if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public int GetInt(string key, StringComparison comparisonType)
        {
            int val;
            int.TryParse(GetString(key, comparisonType), out val);
            return val;
        }

        /// <summary>
        /// Returns the integer value at the given index, or 0 if the index does not reference a valid value.
        /// </summary>
        /// <param name="index">Zero-based index of item.</param>
        public int GetInt(int index)
        {
            int val;
            int.TryParse(GetString(index), out val);
            return val;
        }

        /// <summary>
        /// Returns the double precision value assosiated with the key, or 0.0 if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public double GetDouble(string key)
        {
            return GetDouble(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns the double precision value assosiated with the key, or 0.0 if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public double GetDouble(string key, StringComparison comparisonType)
        {
            double val;
            double.TryParse(GetString(key, comparisonType), out val);
            return val;
        }

        /// <summary>
        /// Returns the double precision value at the given index, or 0.0 if the index does not reference a valid value.
        /// </summary>
        /// <param name="index">Zero-based index of item.</param>
        public double GetDouble(int index)
        {
            double val;
            double.TryParse(GetString(index), out val);
            return val;
        }

        /// <summary>
        /// Returns the DateTime value assosiated with the key, or DateTime.MinValue if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        public DateTime GetDateTime(string key)
        {
            return GetDateTime(key, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Returns the DateTime value assosiated with the key, or DateTime.MinValue if the key does not reference a valid value.
        /// </summary>
        /// <param name="key">Key to find. A leading '/' or '-' may be used but is not necessary.</param>
        /// <param name="comparisonType">Default: CurrentCultureIgnoreCase. Comparison type used to find the key. Use this to force a Case Sensitive search.</param>
        public DateTime GetDateTime(string key, StringComparison comparisonType)
        {
            DateTime val;
            DateTime.TryParse(GetString(key, comparisonType), out val);
            return val;
        }

        /// <summary>
        /// Returns the DateTime value at the given index, or DateTime.MinValue if the key does not reference a valid value.
        /// </summary>
        /// <param name="index">Zero-based index of item.</param>
        public DateTime GetDateTime(int index)
        {
            DateTime val;
            DateTime.TryParse(GetString(index), out val);
            return val;
        }


        /// <summary>
        /// Returns an IEnumerator for the current array.
        /// </summary>
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            foreach (string arg in args)
                yield return arg;
        }

        /// <summary>
        /// Returns an IEnumerator for the current array.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (string arg in args)
                yield return arg;
        }
    }
}
