using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;

namespace QuickLibs.Data
{
    /// <summary>
    /// A simple class to handle database activity
    /// </summary>
    public class QuickDB : IEnumerable<QuickDB>
    {
        private DataSet dbData;
        private DataTable dbTable;
        private DataRow dbRow;
        private string dbValueKey;
        private IDbDataAdapter dataAdapter;
        private object LastResult;
        private Exception _LastException;
        private event EventHandler<string> _InfoMessage;
        private Dictionary<string, object> _ReturnValues = new Dictionary<string, object>();
        private ImportMode _ImportMode;
        private int _retries = 1;

        private static bool _throwExceptions = false;
        private static string _DefaultString = "";
        private static int _DefaultInt = 0;
        private static double _DefaultDouble = 0.0;
        private static bool _DefaultBool = false;
        private static DateTime _DefaultDateTime = DateTime.MinValue;
        private static string _DefaultEncryptionKey = null;

        #region Constructors

        private void PrepareConnection(string Connection)
        {
            try
            {
                string name = Connection;
                Connection = ConnectionString(Connection);

                if (string.IsNullOrWhiteSpace(Connection))
                {
                    Connection = ""; //Ensure connection is not null as it is used in the catch.
                    if (!string.IsNullOrEmpty(name))
                        throw new NullReferenceException("The given connetion name is missing from the configuration file: \"" + name + "\"");
                    else
                        throw new NullReferenceException("No connection string supplied.");
                }

                if (Connection.StartsWith("Driver=", StringComparison.CurrentCultureIgnoreCase))
                {
                    dataAdapter = new OdbcDataAdapter("", Connection);
                    ((OdbcDataAdapter)dataAdapter).SelectCommand.Connection.InfoMessage += odbcInfoMessage;
                }
                else if (Connection.StartsWith("Provider", StringComparison.CurrentCultureIgnoreCase))
                {
                    dataAdapter = new OleDbDataAdapter("", Connection);
                    ((OleDbDataAdapter)dataAdapter).SelectCommand.Connection.InfoMessage += oledbInfoMessage;
                }
                else
                {
                    dataAdapter = new SqlDataAdapter("", Connection);
                    ((SqlDataAdapter)dataAdapter).SelectCommand.Connection.InfoMessage += sqlInfoMessage;
                }
            }
            catch (Exception exp)
            {
                Connection = MaskConnectionString(Connection, "user", "password", "user id", "uid", "pwd");
                _LastException = new ArgumentException("Invalid Connection String: \"" + Connection + "\"", exp);
                if (_throwExceptions)
                    throw _LastException;
            }
        }

        /// <summary>
        /// Initiate with connection string ready to connect on demand.
        /// </summary>
        /// <param name="Connection">Name of connection string in config file.</param>
        public QuickDB(string Connection)
        {
            PrepareConnection(Connection);
        }

        /// <summary>
        /// Initiate with trusted connection string based on given parameters.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        public QuickDB(string Server, string Database)
        {
            PrepareConnection(ConnectionString(Server, Database));
        }

        /// <summary>
        /// Initiate with connection string based on given parameters and credentials.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        /// <param name="Username">SQL User username</param>
        /// <param name="Password">Password</param>
        public QuickDB(string Server, string Database, string Username, string Password)
        {
            PrepareConnection(ConnectionString(Server, Database, Username, Password));
        }

        /// <summary>
        /// Initiate with connection string based on given parameters and credentials.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        /// <param name="Username">SQL User username</param>
        /// <param name="Password">Password</param>
        /// <param name="PersistSecurity">Persist Security Info (required for Import();)</param>
        public QuickDB(string Server, string Database, string Username, string Password, bool PersistSecurity)
        {
            PrepareConnection(ConnectionString(Server, Database, Username, Password, PersistSecurity));
        }

        /// <summary>
        /// Provides quick access to the given DataSet.
        /// </summary>
        public QuickDB(DataSet InDataSet)
        {
            dbData = InDataSet;
        }

        /// <summary>
        /// Provides quick access to the given DataTable.
        /// </summary>
        public QuickDB(DataTable InDataTable)
        {
            dbTable = InDataTable;
        }

        /// <summary>
        /// Provides quick access to the given DataRow.
        /// </summary>
        public QuickDB(DataRow InDataRow)
        {
            dbRow = InDataRow;
        }

        /// <summary>
        /// Provides quick access to the given object.
        /// </summary>
        public QuickDB(DataRow InDataRow, string InValueKey)
        {
            dbRow = InDataRow;
            dbValueKey = InValueKey;
        }

        /// <summary>
        /// Creates and empty QuickDB object.
        /// </summary>
        public QuickDB()
        {
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~QuickDB()
        {
            dbData = null;
            dbTable = null;
            dbRow = null;
            dbValueKey = null;
            dataAdapter = null;
            LastResult = null;
            _LastException = null;
            _InfoMessage = null;
        }

        /// <summary>
        /// Returns the connection string of this QuckDB connection.
        /// </summary>
        /// <returns>Connection String (or "Off-line DataSet").</returns>
        public override string ToString()
        {
            if (dataAdapter != null)
                return dataAdapter.SelectCommand.Connection.ConnectionString;
            else if (dbData != null)
                return "DataSet[" + dbData.Tables.Count + "]";
            else if (dbTable != null)
                return "DataTable[" + dbTable.Rows.Count + "]";
            else if (dbValueKey != null)
                return "DataValue[\"" + dbValueKey + "\"]";
            else if (dbRow != null)
                return "DataRow[" + dbRow.Table.Columns.Count + "]";
            else
                return "Null DataSet"; 
        }

        #endregion

        #region Get Data

        private void odbcInfoMessage(object sender, OdbcInfoMessageEventArgs e)
        {
            _InfoMessage?.Invoke(sender, e.Message);
        }
        private void oledbInfoMessage(object sender, OleDbInfoMessageEventArgs e)
        {
            _InfoMessage?.Invoke(sender, e.Message);
        }
        private void sqlInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            _InfoMessage?.Invoke(sender, e.Message);
        }

        //GET OBJECT
        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// Or retrieve a parameter value by using the @ParameterName.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <returns>DBNull safe value from the first Row in the first Table in the current DataSet.</returns>
        public object GetValue(string Name, object ValIfNull)
        {
            return GetValue(Name, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe value from the indexed Row in the first Table current DataSet.</returns>
        public object GetValue(string Name, object ValIfNull, int RowNumber)
        {
            return GetValue(Name, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe value from the indexed Row in the indexed Table in the current DataSet.</returns>
        public object GetValue(string Name, object ValIfNull, int RowNumber, int TableNumber)
        {
            if (Name.StartsWith("@")) //Special behaviour for parameter names.
            {
                Name = Name.TrimStart('@');
                if (dataAdapter.SelectCommand.Parameters.Contains(Name))
                    return ((IDataParameter)dataAdapter.SelectCommand.Parameters[Name]).Value == DBNull.Value ? ValIfNull : ((IDataParameter)dataAdapter.SelectCommand.Parameters[Name]).Value;
                else if (_ReturnValues.ContainsKey(Name))
                    return _ReturnValues[Name] ?? ValIfNull;
                else
                    return ValIfNull;
            }
            else
            {
                object Val = null;

                if (dbValueKey != null)
                {
                    if (TableNumber != 0 || RowNumber != 0 || !string.IsNullOrEmpty(Name))
                    {
                        _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Rows[", RowNumber, "][\"", Name, "\"] does not exist or is out of range."));
                        if (_throwExceptions)
                            throw _LastException;
                    }
                    else
                        Val = dbRow[dbValueKey];
                }
                else if (dbRow != null)
                {
                    if (TableNumber != 0 || RowNumber != 0 || !dbRow.Table.Columns.Contains(Name))
                    {
                        _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Rows[", RowNumber, "][\"", Name, "\"] does not exist or is out of range."));
                        if (_throwExceptions)
                            throw _LastException;
                    }
                    else
                        Val = dbRow[Name];
                }
                else if (dbTable != null)
                {
                    if (TableNumber != 0 || RowNumber >= dbTable.Rows.Count || RowNumber < 0 || !dbTable.Columns.Contains(Name))
                    {
                        _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Rows[", RowNumber, "][\"", Name, "\"] does not exist or is out of range."));
                        if (_throwExceptions)
                            throw _LastException;
                    }
                    else
                        Val = dbTable.Rows[RowNumber][Name];
                }
                else
                {
                    if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || RowNumber >= dbData.Tables[TableNumber].Rows.Count || RowNumber < 0 || !dbData.Tables[TableNumber].Columns.Contains(Name))
                    {
                        _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Rows[", RowNumber, "][\"", Name, "\"] does not exist or is out of range."));
                        if (_throwExceptions)
                            throw _LastException;
                    }
                    else
                        Val = dbData.Tables[TableNumber].Rows[RowNumber][Name];
                }

                return LastResult = ((Val == null || Val == DBNull.Value) ? ValIfNull : Val);
            }
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <returns>DBNull safe value from the first Row in the first Table in the current DataSet.</returns>
        public object GetValue(object ValIfNull)
        {
            return GetValue(0, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <returns>DBNull safe value from the first Row in the first Table in the current DataSet.</returns>
        public object GetValue(int Index, object ValIfNull)
        {
            return GetValue(Index, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe value from the indexed Row in the first Table in the current DataSet.</returns>
        public object GetValue(int Index, object ValIfNull, int RowNumber)
        {
            return GetValue(Index, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a value from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe value from the indexed Row in the indexed Table in the current DataSet.</returns>
        public object GetValue(int Index, object ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = null;

            if (dbValueKey != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || Index != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    Val = dbRow[dbValueKey];
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    Val = dbRow[Index];
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || RowNumber >= dbTable.Rows.Count || RowNumber < 0 || Index >= dbTable.Columns.Count || (Index < 0))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    Val = dbTable.Rows[RowNumber][Index];
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || RowNumber >= dbData.Tables[TableNumber].Rows.Count || RowNumber < 0 || Index >= dbData.Tables[TableNumber].Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    Val = dbData.Tables[TableNumber].Rows[RowNumber][Index];
            }

            return LastResult = ((Val == null || Val == DBNull.Value) ? ValIfNull : Val);
        }


        //GET STRING
        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <returns>DBNull safe string from the first Row in the first Table in the current DataSet.</returns>
        public string GetString(string Name)
        {
            return GetString(Name, _DefaultString, 0, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <returns>DBNull safe string from the first Row in the first Table in the current DataSet.</returns>
        public string GetString(string Name, string ValIfNull)
        {
            return GetString(Name, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the first Table current DataSet.</returns>
        public string GetString(string Name, int RowNumber)
        {
            return GetString(Name, _DefaultString, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the first Table current DataSet.</returns>
        public string GetString(string Name, string ValIfNull, int RowNumber)
        {
            return GetString(Name, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the indexed Table in the current DataSet.</returns>
        public string GetString(string Name, int RowNumber, int TableNumber)
        {
            return GetString(Name, _DefaultString, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the indexed Table in the current DataSet.</returns>
        public string GetString(string Name, string ValIfNull, int RowNumber, int TableNumber)
        {
            object val = GetValue(Name, ValIfNull, RowNumber, TableNumber);
            if (val == null)
                return (string)val;
            else
                return val.ToString();
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <returns>DBNull safe string from the first Row in the first Table in the current DataSet.</returns>
        public string GetString()
        {
            return GetString(0, _DefaultString, 0, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>DBNull safe string from the first Row in the first Table in the current DataSet.</returns>
        public string GetString(int Index)
        {
            return GetString(Index, _DefaultString, 0, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <returns>DBNull safe string from the first Row in the first Table in the current DataSet.</returns>
        public string GetString(int Index, string ValIfNull)
        {
            return GetString(Index, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the first Table in the current DataSet.</returns>
        public string GetString(int Index, int RowNumber)
        {
            return GetString(Index, _DefaultString, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the first Table in the current DataSet.</returns>
        public string GetString(int Index, string ValIfNull, int RowNumber)
        {
            return GetString(Index, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the indexed Table in the current DataSet.</returns>
        public string GetString(int Index, int RowNumber, int TableNumber)
        {
            return GetString(Index, _DefaultString, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a string from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">String to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe string from the indexed Row in the indexed Table in the current DataSet.</returns>
        public string GetString(int Index, string ValIfNull, int RowNumber, int TableNumber)
        {
            object val = GetValue(Index, ValIfNull, RowNumber, TableNumber);
            if (val == null)
                return (string)val;
            else
                return val.ToString();
        }


        //GET INT
        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <returns>DBNull safe int from the first Row in the first Table in the current DataSet.</returns>
        public int GetInt(string Name)
        {
            return GetInt(Name, 0, 0, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the first Table current DataSet.</returns>
        public int GetInt(string Name, int RowNumber)
        {
            return GetInt(Name, RowNumber, 0, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the indexed Table in the current DataSet.</returns>
        public int GetInt(string Name, int RowNumber, int TableNumber)
        {
            return GetInt(Name, RowNumber, TableNumber, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">Int to return in case of a null or invalid response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the indexed Table in the current DataSet.</returns>
        public int GetInt(string Name, int RowNumber, int TableNumber, int ValIfNull)
        {
            object Val = GetValue(Name, ValIfNull, RowNumber, TableNumber);
            int x;
            if (!int.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <returns>DBNull safe int from the first Row in the first Table in the current DataSet.</returns>
        public int GetInt()
        {
            return GetInt(0, 0, 0, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>DBNull safe int from the first Row in the first Table in the current DataSet.</returns>
        public int GetInt(int Index)
        {
            return GetInt(Index, 0, 0, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the first Table in the current DataSet.</returns>
        public int GetInt(int Index, int RowNumber)
        {
            return GetInt(Index, RowNumber, 0, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the indexed Table in the current DataSet.</returns>
        public int GetInt(int Index, int RowNumber, int TableNumber)
        {
            return GetInt(Index, RowNumber, TableNumber, _DefaultInt);
        }

        /// <summary>
        /// Retreive a int from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Int to return in case of a null or invalid response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe int from the indexed Row in the indexed Table in the current DataSet.</returns>
        public int GetInt(int Index, int RowNumber, int TableNumber, int ValIfNull)
        {
            object Val = GetValue(Index, ValIfNull, RowNumber, TableNumber);
            int x;
            if (!int.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }


        //GET DOUBLE
        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <returns>DBNull safe Double from the first Row in the first Table in the current DataSet.</returns>
        public double GetDouble(string Name)
        {
            return GetDouble(Name, _DefaultDouble, 0, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <returns>DBNull safe Double from the first Row in the first Table in the current DataSet.</returns>
        public double GetDouble(string Name, double ValIfNull)
        {
            return GetDouble(Name, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the first Table current DataSet.</returns>
        public double GetDouble(string Name, int RowNumber)
        {
            return GetDouble(Name, _DefaultDouble, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the first Table current DataSet.</returns>
        public double GetDouble(string Name, double ValIfNull, int RowNumber)
        {
            return GetDouble(Name, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the indexed Table in the current DataSet.</returns>
        public double GetDouble(string Name, int RowNumber, int TableNumber)
        {
            return GetDouble(Name, _DefaultDouble, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the indexed Table in the current DataSet.</returns>
        public double GetDouble(string Name, double ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Name, ValIfNull, RowNumber, TableNumber);
            double x;
            if (!double.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <returns>DBNull safe Double from the first Row in the first Table in the current DataSet.</returns>
        public double GetDouble()
        {
            return GetDouble(0, _DefaultDouble, 0, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>DBNull safe Double from the first Row in the first Table in the current DataSet.</returns>
        public double GetDouble(int Index)
        {
            return GetDouble(Index, _DefaultDouble, 0, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <returns>DBNull safe Double from the first Row in the first Table in the current DataSet.</returns>
        public double GetDouble(int Index, double ValIfNull)
        {
            return GetDouble(Index, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the first Table in the current DataSet.</returns>
        public double GetDouble(int Index, int RowNumber)
        {
            return GetDouble(Index, _DefaultDouble, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the first Table in the current DataSet.</returns>
        public double GetDouble(int Index, double ValIfNull, int RowNumber)
        {
            return GetDouble(Index, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the indexed Table in the current DataSet.</returns>
        public double GetDouble(int Index, int RowNumber, int TableNumber)
        {
            return GetDouble(Index, _DefaultDouble, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a Double from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Double to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Double from the indexed Row in the indexed Table in the current DataSet.</returns>
        public double GetDouble(int Index, double ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Index, ValIfNull, RowNumber, TableNumber);
            double x;
            if (!double.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }


        //GET BOOL
        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <returns>DBNull safe Boolean from the first Row in the first Table in the current DataSet.</returns>
        public bool GetBool(string Name)
        {
            return GetBool(Name, _DefaultBool, 0, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <returns>DBNull safe Boolean from the first Row in the first Table in the current DataSet.</returns>
        public bool GetBool(string Name, bool ValIfNull)
        {
            return GetBool(Name, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the first Table current DataSet.</returns>
        public bool GetBool(string Name, int RowNumber)
        {
            return GetBool(Name, _DefaultBool, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the first Table current DataSet.</returns>
        public bool GetBool(string Name, bool ValIfNull, int RowNumber)
        {
            return GetBool(Name, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the indexed Table in the current DataSet.</returns>
        public bool GetBool(string Name, int RowNumber, int TableNumber)
        {
            return GetBool(Name, _DefaultBool, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the indexed Table in the current DataSet.</returns>
        public bool GetBool(string Name, bool ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Name, ValIfNull, RowNumber, TableNumber);
            bool x;
            if (!bool.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <returns>DBNull safe Boolean from the first Row in the first Table in the current DataSet.</returns>
        public bool GetBool()
        {
            return GetBool(0, _DefaultBool, 0, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the first Row in the first Table in the current DataSet.</returns>
        public bool GetBool(int Index)
        {
            return GetBool(Index, _DefaultBool, 0, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <returns>DBNull safe Boolean from the first Row in the first Table in the current DataSet.</returns>
        public bool GetBool(int Index, bool ValIfNull)
        {
            return GetBool(Index, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the first Table in the current DataSet.</returns>
        public bool GetBool(int Index, int RowNumber)
        {
            return GetBool(Index, _DefaultBool, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the first Table in the current DataSet.</returns>
        public bool GetBool(int Index, bool ValIfNull, int RowNumber)
        {
            return GetBool(Index, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the indexed Table in the current DataSet.</returns>
        public bool GetBool(int Index, int RowNumber, int TableNumber)
        {
            return GetBool(Index, _DefaultBool, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a Boolean from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">Boolean to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe Boolean from the indexed Row in the indexed Table in the current DataSet.</returns>
        public bool GetBool(int Index, bool ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Index, ValIfNull, RowNumber, TableNumber);
            bool x;
            if (!bool.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }


        //GET DATE
        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <returns>DBNull safe DateTime from the first Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(string Name)
        {
            return GetDateTime(Name, _DefaultDateTime, 0, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <returns>DBNull safe DateTime from the first Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(string Name, DateTime ValIfNull)
        {
            return GetDateTime(Name, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the first Table current DataSet.</returns>
        public DateTime GetDateTime(string Name, int RowNumber)
        {
            return GetDateTime(Name, _DefaultDateTime, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the first Table current DataSet.</returns>
        public DateTime GetDateTime(string Name, DateTime ValIfNull, int RowNumber)
        {
            return GetDateTime(Name, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the indexed Table in the current DataSet.</returns>
        public DateTime GetDateTime(string Name, int RowNumber, int TableNumber)
        {
            return GetDateTime(Name, _DefaultDateTime, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the indexed Table in the current DataSet.</returns>
        public DateTime GetDateTime(string Name, DateTime ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Name, ValIfNull, RowNumber, TableNumber);
            DateTime x;
            if (!DateTime.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <returns>DBNull safe DateTime from the first Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime()
        {
            return GetDateTime(0, _DefaultDateTime, 0, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the first Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index)
        {
            return GetDateTime(Index, _DefaultDateTime, 0, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <returns>DBNull safe DateTime from the first Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index, DateTime ValIfNull)
        {
            return GetDateTime(Index, ValIfNull, 0, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index, int RowNumber)
        {
            return GetDateTime(Index, _DefaultDateTime, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the first Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index, DateTime ValIfNull, int RowNumber)
        {
            return GetDateTime(Index, ValIfNull, RowNumber, 0);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the indexed Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index, int RowNumber, int TableNumber)
        {
            return GetDateTime(Index, _DefaultDateTime, RowNumber, TableNumber);
        }

        /// <summary>
        /// Retreive a DateTime from the current DataSet. DataSet can be populated via the
        /// QuickDB.Select() command or via the new QuickDB() constructor.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="ValIfNull">DateTime to return in case of a null response.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>DBNull safe DateTime from the indexed Row in the indexed Table in the current DataSet.</returns>
        public DateTime GetDateTime(int Index, DateTime ValIfNull, int RowNumber, int TableNumber)
        {
            object Val = GetValue(Index, ValIfNull, RowNumber, TableNumber);
            DateTime x;
            if (!DateTime.TryParse(Val.ToString(), out x))
                return ValIfNull;
            else
                return x;
        }



        //GET DATA
        /// <summary>
        /// Returns a DataRow from the current DataSet.
        /// </summary>
        /// <returns>The first Row from the first Table in the current DataSet.</returns>
        public DataRow GetRow()
        {
            return GetRow(0, 0);
        }

        /// <summary>
        /// Returns a DataRow from the current DataSet.
        /// </summary>
        /// <param name="RowNumber">Row Index (0 based)</param>
        /// <returns>The indexed Row from the first Table in the current DataSet.</returns>
        public DataRow GetRow(int RowNumber)
        {
            return GetRow(RowNumber, 0);
        }

        /// <summary>
        /// Returns a DataRow from the current DataSet.
        /// </summary>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>The indexed Row from the indexed Table in the current DataSet</returns>
        public DataRow GetRow(int RowNumber, int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || RowNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("RowNumber ", RowNumber, " in TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow;
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("RowNumber ", RowNumber, " in TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || RowNumber >= dbData.Tables[TableNumber].Rows.Count || RowNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("RowNumber ", RowNumber, " in TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable.Rows[RowNumber];
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || RowNumber >= dbData.Tables[TableNumber].Rows.Count || RowNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableNumber].Rows[RowNumber];
            }

            return null;
        }

        /// <summary>
        /// Returns a DataTable from the current DataSet.
        /// </summary>
        /// <returns>The indedxed Table from the current DataSet.</returns>
        public DataTable GetTable()
        {
            return GetTable(0);
        }

        /// <summary>
        /// Returns a DataTable from the current DataSet.
        /// </summary>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>The indedxed Table from the current DataSet.</returns>
        public DataTable GetTable(int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || dbRow.Table == null)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table;
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || dbRow.Table == null)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable;
            }
            else
            {
                if ((dbData == null) || (dbData.Tables.Count <= TableNumber) || (TableNumber < 0))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableNumber];
            }

            return null;
        }

        /// <summary>
        /// Returns a DataTable from the current DataSet.
        /// </summary>
        /// <param name="TableName">Table Name.</param>
        /// <returns>The named Table from the current DataSet.</returns>
        public DataTable GetTable(string TableName)
        {
            if (dbValueKey != null)
            {
                if (!string.IsNullOrEmpty(TableName) || dbRow.Table == null)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table;
            }
            else if (dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName) || dbRow.Table == null)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table;
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable;
            }
            else
            {
                if ((dbData == null) || !dbData.Tables.Contains(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableName];
            }

            return null;
        }

        /// <summary>
        ///     Returns the current DataSet.
        /// </summary>
        public DataSet GetDataSet()
        {
            if (dbRow != null && dbRow.Table != null && dbRow.Table.DataSet != null)
                return dbRow.Table.DataSet;
            else if (dbTable != null && dbTable.DataSet != null)
                return dbTable.DataSet;
            else
                return dbData;      
        }

        /// <summary>
        /// Returns the number of tables in the current DataSet. 
        /// </summary>
        /// <returns>Number of tables in the current DataSet.</returns>
        public int TableCount()
        {
            if (dbValueKey != null || dbRow != null || dbTable != null)
                return (int)(LastResult = 1);
            if (dbData == null)
                return (int)(LastResult = 0);
            else
                return (int)(LastResult = dbData.Tables.Count);
        }

        /// <summary>
        /// Returns the number of rows from the current DataSet. 
        /// </summary>
        /// <returns>Number of rows in the first Table of the current DataSet.</returns>
        public int RowCount()
        {
            return RowCount(0);
        }

        /// <summary>
        /// Returns the number of rows from the current DataSet.
        /// </summary>
        /// <param name="TableNumber">Table Index (0 based)</param>
        /// <returns>Number of rows in the indexed Table in the current DataSet</returns>
        public int RowCount(int TableNumber)
        {
            LastResult = 0;

            if (dbValueKey != null || dbRow != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = 1;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = dbTable.Rows.Count;
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("TableNumber ", TableNumber, " is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = dbData.Tables[TableNumber].Rows.Count;
            }

            return (int)LastResult;
        }

        /// <summary>
        /// Returns the number of rows from the current DataSet.
        /// </summary>
        /// <param name="TableName">Table Name.</param>
        /// <returns>Number of rows in the indexed Table in the current DataSet</returns>
        public int RowCount(string TableName)
        {
            LastResult = 0;

            if (dbValueKey != null || dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = 1;
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = dbTable.Rows.Count;
            }
            else
            {
                if (dbData == null || !dbData.Tables.Contains(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table \"", TableName, "\" does not exist."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    LastResult = dbData.Tables[TableName].Rows.Count;
            }

            return (int)LastResult;
        }



        ///// <summary>
        ///// Returns a DataRow from the current DataSet.
        ///// </summary>
        ///// <param name="RowNumber">Row Index (0 based).</param>
        ///// <param name="TableNumber">Table Index (0 based).</param>
        ///// <returns>The indexed Row from the indexed Table in the current DataSet</returns>
        //public QuickDB Row(int RowNumber, int TableNumber)
        //{
        //    return new QuickDB(GetRow(RowNumber, TableNumber));
        //}


        /// <summary>
        /// This function returns the aggregate sum of all the numeral values in the given column in the first table.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <returns>The aggregate sum of given column.</returns>
        public double GetSum(string Name)
        {
            return GetSum(Name, 0);
        }

        /// <summary>
        /// This function returns the aggregate sum of all the numeral values in the given column in the indexed table.
        /// </summary>
        /// <param name="Name">Column Name</param>
        /// <param name="TableNumber">Table Index (0 based)</param>
        /// <returns>The aggregate sum of given column.</returns>
        public double GetSum(string Name, int TableNumber)
        {
            if ((dbData == null) || (dbData.Tables.Count <= TableNumber) || (TableNumber < 0) || (!dbData.Tables[TableNumber].Columns.Contains(Name)))
            {
                _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber.ToString(), "].Columns[\"", Name, "\"] does not exist or is out of range."));
                if (_throwExceptions)
                    throw _LastException;
                return (int)(LastResult = -1);
            }
            else
            {
                double Sum = 0;
                foreach (DataRow dr in dbData.Tables[TableNumber].Rows)
                {
                    try
                    {
                        Sum += Convert.ToDouble(dr[Name]);
                    }
                    catch (Exception exp)
                    {
                        //do nothing - if object doesn't convert to double we don't want to add it to the sum anyway.
                    }
                }
                return (Double)(LastResult = Sum);
            }
        }

        /// <summary>
        /// Returns the ColumnName of the given Column Index in the first table.
        /// </summary>
        /// <returns>The Column Name of the indexed column in the first Table of the current DataSet.</returns>
        public string GetColumnName()
        {
            return GetColumnName(0, 0);
        }

        /// <summary>
        /// Returns the ColumnName of the given Column Index in the first table.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <returns>The Column Name of the indexed column in the first Table of the current DataSet.</returns>
        public string GetColumnName(int Index)
        {
            return GetColumnName(Index, 0);
        }

        /// <summary>
        /// Returns the ColumnName of the given Column Index and Table.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>The Column Name of the indexed column in the indexed Table of the current DataSet.</returns>
        public string GetColumnName(int Index, int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || Index != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbValueKey;
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || Index < 0 || Index >= dbRow.Table.Columns.Count)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table.Columns[Index].ColumnName;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || Index < 0 || Index >= dbTable.Columns.Count)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable.Columns[Index].ColumnName;
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || Index >= dbData.Tables[TableNumber].Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableNumber].Columns[Index].ColumnName;
            }

            return "";
        }

        /// <summary>
        /// Returns the ColumnName of the given Column Index and Table Index.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableName">Table Name.</param>
        /// <returns>The Column Name of the indexed column in the named Table of the current DataSet.</returns>
        public string GetColumnName(int Index, string TableName)
        {
            if (dbValueKey != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index != 0)
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                else
                    return dbValueKey;
            }
            else if (dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbRow.Table.Columns[Index].ColumnName;
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index >= dbTable.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable.Columns[Index].ColumnName;
            }
            else
            {
                if (dbData == null || !dbData.Tables.Contains(TableName) || Index >= dbData.Tables[TableName].Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableName].Columns[Index].ColumnName;
            }
            
            return "";
        }

        /// <summary>
        /// Returns the TableName of the given Table Index.
        /// </summary>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>The Table Name of the indexed Table in the current DataSet.</returns>
        public string GetTableName(int TableNumber)
        {
            if (dbValueKey != null || dbRow != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable.TableName;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbTable.TableName;
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    return dbData.Tables[TableNumber].TableName;
            }

            return "";
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        public string ToCsv()
        {
            return ToCsv(",", "\"", "yyyy-MM-dd HH:mm:ss", Environment.NewLine, true);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="HeaderRow">Default: true. When true, the first row returned will contain the column names.</param>
        /// <returns></returns>
        public string ToCsv(bool HeaderRow)
        {
            return ToCsv(",", "\"", "yyyy-MM-dd HH:mm:ss", Environment.NewLine, HeaderRow);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter)
        {
            return ToCsv(ColumnDelimiter, "\"", "yyyy-MM-dd HH:mm:ss", Environment.NewLine, true);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="HeaderRow">Default: true. When true, the first row returned will contain the column names.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, bool HeaderRow)
        {
            return ToCsv(ColumnDelimiter, "\"", "yyyy-MM-dd HH:mm:ss", Environment.NewLine, HeaderRow);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier)
        {
            return ToCsv(ColumnDelimiter, TextQualifier, "yyyy-MM-dd HH:mm:ss", Environment.NewLine, true);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <param name="HeaderRow">Default: true. When true, the first row returned will contain the column names.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier, bool HeaderRow)
        {
            return ToCsv(ColumnDelimiter, TextQualifier, "yyyy-MM-dd HH:mm:ss", Environment.NewLine, HeaderRow);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <param name="DateFormat">Default: "yyyy-MM-dd HH:mm:ss". Used to specify how DateTime objects will be formatted.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier, string DateFormat)
        {
            return ToCsv(ColumnDelimiter, TextQualifier, DateFormat, Environment.NewLine, true);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <param name="DateFormat">Default: "yyyy-MM-dd HH:mm:ss". Used to specify how DateTime objects will be formatted.</param>
        /// <param name="HeaderRow">Default: true. When true, the first row returned will contain the column names.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier, string DateFormat, bool HeaderRow)
        {
            return ToCsv(ColumnDelimiter, TextQualifier, DateFormat, Environment.NewLine, HeaderRow);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <param name="DateFormat">Default: "yyyy-MM-dd HH:mm:ss". Used to specify how DateTime objects will be formatted.</param>
        /// <param name="RecordDelimiter">Default: Environment.NewLine. Used to specigy the delimeter to use between records.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier, string DateFormat, string RecordDelimiter)
        {
            return ToCsv(ColumnDelimiter, TextQualifier, DateFormat, RecordDelimiter, true);
        }

        /// <summary>
        /// Returns the contents of the first table as a delimited string.
        /// </summary>
        /// <param name="ColumnDelimiter">Default: ",". Used to specify the delimeter to use between values.</param>
        /// <param name="TextQualifier">Default: "\"". Used to specify the text qualifer to use on non numeric fields.</param>
        /// <param name="DateFormat">Default: "yyyy-MM-dd HH:mm:ss". Used to specify how DateTime objects will be formatted.</param>
        /// <param name="RecordDelimiter">Default: Environment.NewLine. Used to specigy the delimeter to use between records.</param>
        /// <param name="HeaderRow">Default: true. When true, the first row returned will contain the column names.</param>
        /// <returns></returns>
        public string ToCsv(string ColumnDelimiter, string TextQualifier, string DateFormat, string RecordDelimiter, bool HeaderRow)
        {
            StringBuilder sb = new StringBuilder();
            if (dbValueKey != null)
            {
                string strVal = GetString();
                bool isInt = GetInt().ToString().Equals(strVal);
                bool isDbl = GetDouble().ToString().Equals(strVal);
                bool isBol = GetBool().ToString().Equals(strVal);

                DateTime datVal = GetDateTime();
                if (datVal.ToString().Equals(strVal))
                    strVal = datVal.ToString(DateFormat);

                if (!(isBol || isInt || isDbl))
                    sb.Append(TextQualifier);

                sb.Append(strVal);

                if (!(isBol || isInt || isDbl))
                    sb.Append(TextQualifier);

                return sb.ToString();
            }
            else if (dbRow != null)
            {
                foreach (QuickDB val in this)
                {
                    sb.Append(val.ToCsv(ColumnDelimiter, TextQualifier, RecordDelimiter, DateFormat, HeaderRow));
                    sb.Append(ColumnDelimiter);
                }

                return sb.ToString(0, sb.Length - ColumnDelimiter.Length);
            }
            else if (dbTable != null)
            {
                if (HeaderRow)
                {
                    foreach (DataColumn col in dbTable.Columns)
                    {
                        sb.Append(TextQualifier);
                        sb.Append(col.ColumnName);
                        sb.Append(TextQualifier);
                        sb.Append(ColumnDelimiter);
                    }
                    sb.Remove(sb.Length - ColumnDelimiter.Length, ColumnDelimiter.Length);
                    sb.Append(RecordDelimiter);
                }

                foreach (QuickDB row in this)
                {
                    sb.Append(row.ToCsv(ColumnDelimiter, TextQualifier, RecordDelimiter, DateFormat, HeaderRow));
                    sb.Append(RecordDelimiter);
                }

                return sb.ToString(0, sb.Length - RecordDelimiter.Length);
            }
            else if (dbData != null && dbData.Tables.Count > 0)
            {
                return this[0].ToCsv(ColumnDelimiter, TextQualifier, RecordDelimiter, DateFormat, HeaderRow);
            }

            return "";
        }


        /// <summary>
        /// Fetches the schemas from the current database for the given table names so that they can be populated and imported using the Import() function.
        /// SELECT permissions are required on both the target table and INFORMATION_SCHEMA.COLUMNS.  On error, no schema will be loaded for any table.
        /// </summary>
        /// <param name="TableNames"></param>
        public void GetSchema(params string[] TableNames)
        {
            //Build SQL string.
            string SQL = "";
            foreach (string Table in TableNames)
            {
                if (!string.IsNullOrEmpty(Table))
                {
                    Stack<string> s = new Stack<string>(NormalizeTableName(Table).Trim('[',']').Replace("].[", "�").Replace("]..[","��").Split(new char[] { '�' }, StringSplitOptions.None));
                    string tbl = "'" + s.Pop() + "'";
                    string sch = s.Count > 0 ? ("'" + s.Pop() + "'").Replace("''","TABLE_SCHEMA") : "TABLE_SCHEMA";
                    string dbo = s.Count > 0 ? "[" + s.Pop() + "]." : "";

                    SQL += "SELECT * FROM " + NormalizeTableName(Table) + " WHERE 0=1;";
                    SQL += "SELECT CHARACTER_MAXIMUM_LENGTH FROM " + dbo + "INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = " + sch + " AND TABLE_NAME = " + tbl + " ORDER BY ORDINAL_POSITION;";
                }
            }

            try
            {
                Select(SQL);
            }
            catch
            {
                //No need to do anything here.
            }

            if (_LastException == null)
            {
                //Rename tables and assign column lenghts.
                //Odd tables contain empty data tables.
                //Even tables contain table schema.
                int i = 0;
                foreach (string Table in TableNames)
                {
                    if (!string.IsNullOrEmpty(Table))
                    {
                        int c = 0;
                        foreach (DataColumn col in dbData.Tables[i].Columns)
                            col.MaxLength = GetInt(0, c++, i + 1, -1);

                        SetTableName(Table, i++);

                        dbData.Tables.RemoveAt(i);
                    }
                }
            }
            else
            {
                _LastException = new OperationCanceledException("Unable to fetch table schema.", _LastException);
                if (_throwExceptions)
                    throw _LastException;
            }
        }

        /// <summary>
        /// Fetches the OleDbSchemaTable and stores it in the current DataTable. (Only valid for OleDb connections).
        /// </summary>
        public void GetSchema()
        {
            if (dataAdapter.GetType().Name.Equals("OleDbDataAdapter"))
            {
                dbData = null;
                dbRow = null;
                dbValueKey = null;
                try
                {
                    dataAdapter.SelectCommand.Connection.Open();
                    dbTable = ((OleDbDataAdapter)dataAdapter).SelectCommand.Connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                }
                catch (Exception exp)
                {
                    _LastException = exp; //Connection falure. //Syntax error or Procedure not found.
                    if (_throwExceptions)
                        throw _LastException;
                }
                finally
                {
                    dataAdapter.SelectCommand.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Tests to see if the given table name exists in the current SQL instance.
        /// </summary>
        /// <param name="TableName">Database.Schema.TabletName</param>
        /// <returns></returns>
        public bool TableExists(string TableName)
        {
            return ObjectExists(TableName, "U");
        }

        /// <summary>
        /// Tests to see if the given object name exists in the current SQL instance.
        /// </summary>
        /// <param name="ObjectName">Database.Schema.ObjectName</param>
        /// <returns></returns>
        public bool ObjectExists(string ObjectName)
        {
            return ObjectExists(ObjectName, null);
        }

        /// <summary>
        /// Tests to see if the given object name exists in the current SQL instance.
        /// </summary>
        /// <param name="ObjectName">Database.Schema.ObjectName</param>
        /// <param name="ObjectType">SQL Server system.object type.</param>
        /// <returns></returns>
        public bool ObjectExists(string ObjectName, string ObjectType)
        {
            string ConnectionString = string.Empty;
            if (dataAdapter != null)
                ConnectionString = dataAdapter.SelectCommand.Connection.ConnectionString;

            if (!string.IsNullOrEmpty(ConnectionString))
            {
                try
                {
                    QuickDB qDB = new QuickDB(ConnectionString);
                    qDB.SetParameter("@ObjectName", ObjectName);
                    qDB.SetParameter("@ObjectType", ObjectType, !string.IsNullOrEmpty(ObjectType));
                    qDB.Select("SELECT OBJECT_ID(@ObjectName" + (string.IsNullOrEmpty(ObjectType) ? ");" : ",@ObjectType);"));
                    qDB.Throw();

                    return qDB.GetInt() > 0;
                }
                catch (Exception exp)
                {
                    _LastException = exp;
                    if (_throwExceptions)
                        throw _LastException;
                }
            }
            else
            {
                _LastException = new Exception("Unable to check objects in an off-line DataSet.");
                if (_throwExceptions)
                    throw _LastException;
            }

            return false;
        }


        #endregion

        #region Set Data

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Name">Column Name.</param>
        /// <param name="ValIfNull">Value to return in case of a null response.</param>
        public QuickDB SetValue(object Value, string Name)
        {
            return SetValue(Value, Name, 0, 0);
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Name">Column Name.</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        public QuickDB SetValue(object Value, string Name, int RowNumber)
        {
            return SetValue(Value, Name, RowNumber, 0);
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        public QuickDB SetValue(object Value, string Name, int RowNumber, int TableNumber)
        {
            if (Value == null)
                Value = DBNull.Value;

            if (dbValueKey != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || (!string.IsNullOrEmpty(Name) && !dbValueKey.Equals(Name)))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow[dbValueKey] = Value;
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    if (!dbRow.Table.Columns.Contains(Name))
                        dbRow.Table.Columns.Add(Name);

                    //Set cell value
                    SetValue(dbRow, Name, Value);
                }
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException("Cannot set value. At Table level, TableNumber must be 0.");
                    if (_throwExceptions)
                        throw _LastException;
                }
                else if (RowNumber < 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    if (!dbTable.Columns.Contains(Name))
                        dbTable.Columns.Add(Name);

                    //Get DataRow or create if null
                    while (dbTable.Rows.Count <= RowNumber)
                        dbTable.Rows.Add(dbTable.NewRow());

                    //Set cell value
                    SetValue(dbTable.Rows[RowNumber], Name, Value);
                }
            }
            else
            {
                if (TableNumber < 0 || RowNumber < 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Get DataTable or create if null
                    while (TableNumber >= dbData.Tables.Count)
                        dbData.Tables.Add();

                    //Create column if it doesn't already exist
                    if (!dbData.Tables[TableNumber].Columns.Contains(Name))
                        dbData.Tables[TableNumber].Columns.Add(Name);

                    //Get DataRow or create if null
                    while (RowNumber >= dbData.Tables[TableNumber].Rows.Count)
                        dbData.Tables[TableNumber].Rows.Add(dbData.Tables[TableNumber].NewRow());

                    //Set cell value
                    SetValue(dbData.Tables[TableNumber].Rows[RowNumber], Name, Value);
                }
            }
            return this;
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Name">Column Name</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableName">Table Name</param>
        public QuickDB SetValue(object Value, string Name, int RowNumber, string TableName)
        {
            if (Value == null)
                Value = DBNull.Value;

            if (dbValueKey != null)
            {
                if (!string.IsNullOrEmpty(TableName) || RowNumber != 0 || (!string.IsNullOrEmpty(Name) && dbValueKey.Equals(Name)))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    SetValue(dbRow, Name, Value);
            }
            else if (dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName) || RowNumber != 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    if (!dbRow.Table.Columns.Contains(Name))
                        dbRow.Table.Columns.Add(Name);

                    //Set cell value
                    SetValue(dbRow, Name, Value);
                }
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName) || RowNumber < 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    if (!dbTable.Columns.Contains(Name))
                        dbTable.Columns.Add(Name);

                    //Get DataRow or create if null
                    while (dbTable.Rows.Count <= RowNumber)
                        dbTable.Rows.Add(dbTable.NewRow());

                    //Set cell value
                    SetValue(dbTable.Rows[RowNumber], Name, Value);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(TableName) || RowNumber < 0 || string.IsNullOrEmpty(Name))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][\"", Name, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Get DataTable or create if null
                    if (!dbData.Tables.Contains(TableName))
                        dbData.Tables.Add(TableName);

                    //Create column if it doesn't already exist
                    if (!dbData.Tables[TableName].Columns.Contains(Name))
                        dbData.Tables[TableName].Columns.Add(Name);

                    //Get DataRow or create if null
                    while (RowNumber >= dbData.Tables[TableName].Rows.Count)
                        dbData.Tables[TableName].Rows.Add(dbData.Tables[TableName].NewRow());

                    //Set cell value
                    SetValue(dbData.Tables[TableName].Rows[RowNumber], Name, Value);
                }
            }
            return this;
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        public QuickDB SetValue(object Value)
        {
            return SetValue(Value, 0, 0, 0);
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Index">Column Index (0 based).</param>
        public QuickDB SetValue(object Value, int Index)
        {
            return SetValue(Value, Index, 0, 0);
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        public QuickDB SetValue(object Value, int Index, int RowNumber)
        {
            return SetValue(Value, Index, RowNumber, 0);
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        public QuickDB SetValue(object Value, int Index, int RowNumber, int TableNumber)
        {
            if (Value == null)
                Value = DBNull.Value;

            if (dbValueKey != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || Index != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow[dbValueKey] = Value;
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || RowNumber != 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    //Set cell value
                    SetValue(dbRow, Index, Value);
                }
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || RowNumber < 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    while (Index >= dbTable.Columns.Count)
                        dbTable.Columns.Add();

                    //Get DataRow or create if null
                    while (dbTable.Rows.Count <= RowNumber)
                        dbTable.Rows.Add(dbTable.NewRow());

                    //Set cell value
                    SetValue(dbTable.Rows[RowNumber], Index, Value);
                }
            }
            else
            {
                if (TableNumber < 0 || RowNumber < 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Get DataTable or create if null
                    while (TableNumber >= dbData.Tables.Count)
                        dbData.Tables.Add();

                    //Create column if it doesn't already exist
                    while (Index >= dbData.Tables[TableNumber].Columns.Count)
                        dbData.Tables[TableNumber].Columns.Add();

                    //Get DataRow or create if null
                    while (RowNumber >= dbData.Tables[TableNumber].Rows.Count)
                        dbData.Tables[TableNumber].Rows.Add(dbData.Tables[TableNumber].NewRow());

                    //Set cell value
                    SetValue(dbData.Tables[TableNumber].Rows[RowNumber], Index, Value);
                }
            }
            return this;
        }

        /// <summary>
        /// Sets a value in the current DataSet.
        /// </summary>
        /// <param name="Value">Value to be placed in the DataSet.</param>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="RowNumber">Row Index (0 based).</param>
        /// <param name="TableName">Table Name</param>
        public QuickDB SetValue(object Value, int Index, int RowNumber, string TableName)
        {
            if (Value == null)
                Value = DBNull.Value;

            if (dbValueKey != null)
            {
                if (!string.IsNullOrEmpty(TableName) || RowNumber != 0 || Index != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow[dbValueKey] = Value;
            }
            else if (dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName) || RowNumber != 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    //Set cell value
                    SetValue(dbRow, Index, Value);
                }
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName))
                {
                    _LastException = new IndexOutOfRangeException("Cannot set value. At Table level, TableName must be \"\".");
                    if (_throwExceptions)
                        throw _LastException;
                }
                else if (RowNumber < 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create column if it doesn't already exist
                    while (Index >= dbTable.Columns.Count)
                        dbTable.Columns.Add();

                    //Get DataRow or create if null
                    while (dbTable.Rows.Count <= RowNumber)
                        dbTable.Rows.Add(dbTable.NewRow());

                    //Set cell value
                    SetValue(dbTable.Rows[RowNumber], Index, Value);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(TableName) || RowNumber < 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Row[", RowNumber, "][", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Get DataTable or create if null
                    if (!dbData.Tables.Contains(TableName))
                        dbData.Tables.Add(TableName);

                    //Create column if it doesn't already exist
                    while (Index >= dbData.Tables[TableName].Columns.Count)
                        dbData.Tables[TableName].Columns.Add();

                    //Get DataRow or create if null
                    while (RowNumber >= dbData.Tables[TableName].Rows.Count)
                        dbData.Tables[TableName].Rows.Add(dbData.Tables[TableName].NewRow());

                    //Set cell value
                    SetValue(dbData.Tables[TableName].Rows[RowNumber], Index, Value);
                }
            }
            return this;
        }

        private void SetValue(DataRow row, string col, object val)
        {
            DataColumn def = row.Table.Columns[col];
            if (def.DataType == typeof(string) && val != null)
            {
                if (def.MaxLength > 0 && val.ToString().Length > def.MaxLength)
                {
                    _LastException = new InvalidCastException("Imported value '" + val +"' is too long. (Max field length of column '" + col + "' is " + def.MaxLength + ")");
                    if (_throwExceptions)
                        throw _LastException;

                    val = val.ToString().Substring(0, def.MaxLength);
                }   
            }

            row[col] = val;
        }

        private void SetValue(DataRow row, int idx, object val)
        {
            DataColumn def = row.Table.Columns[idx];
            if (def.DataType == typeof(string) && val != null)
            {
                if (def.MaxLength > 0 && val.ToString().Length > def.MaxLength)
                {
                    _LastException = new InvalidCastException("Imported value '" + val + "' is too long. (Max field length of column number " + idx + " is " + def.MaxLength + ")");
                    if (_throwExceptions)
                        throw _LastException;

                    val = val.ToString().Substring(0, def.MaxLength);
                }
            }

            row[idx] = val;
        }


        /// <summary>
        /// Adds a new table to the current DataSet.
        /// </summary>
        /// <returns>Returns the new table in a QuickDB object.</returns>
        public QuickDB AddTable()
        {
            return AddTable(null);
        }

        /// <summary>
        /// Adds a new table to the current DataSet.  If the given TableName already exists, this will return the existing table instead.
        /// </summary>
        /// <param name="TableName">Name to assign to the new table.</param>
        /// <returns>Returns the new table in a QuickDB object.</returns>
        public QuickDB AddTable(string TableName)
        {
            //Create table if it doesn't already exist.
            if (dbValueKey != null && dbRow.Table != null && dbRow.Table.DataSet != null)
            {
                if (!dbRow.Table.DataSet.Tables.Contains(TableName))
                    dbRow.Table.DataSet.Tables.Add(TableName);
                return new QuickDB(dbRow.Table.DataSet.Tables[TableName]);
            }
            else if (dbRow != null && dbRow.Table != null && dbRow.Table.DataSet != null)
            {
                if (!dbRow.Table.DataSet.Tables.Contains(TableName))
                    dbRow.Table.DataSet.Tables.Add(TableName);
                return new QuickDB(dbRow.Table.DataSet.Tables[TableName]);
            }
            else if (dbTable != null)
            {
                if (!dbTable.DataSet.Tables.Contains(TableName))
                    dbTable.DataSet.Tables.Add(TableName);
                return new QuickDB(dbTable.DataSet.Tables[TableName]);
            }
            else
            {
                //Create DataSet if null
                if (dbData == null)
                    dbData = new DataSet();

                if (!dbData.Tables.Contains(TableName))
                    dbData.Tables.Add(TableName);

                return this[TableName];
            }
        }

        /// <summary>
        /// Adds a new row to the given table.
        /// </summary>
        /// <returns>Returns the new row in a QuickDB object.</returns>
        public QuickDB AddRow()
        {
            return AddRow(0);
        }

        /// <summary>
        /// Adds a new row to the given table.
        /// </summary>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>Returns the new row in a QuickDB object.</returns>
        public QuickDB AddRow(int TableNumber)
        {
            if (dbValueKey != null && dbRow.Table != null)
            {
                dbRow.Table.Rows.Add(dbRow.Table.NewRow());
                return new QuickDB(dbRow.Table.Rows[dbRow.Table.Rows.Count - 1]);
            }
            else if (dbRow != null && dbRow.Table != null)
            {
                dbRow.Table.Rows.Add(dbRow.Table.NewRow());
                return new QuickDB(dbRow.Table.Rows[dbRow.Table.Rows.Count - 1]);
            }
            else if (dbTable != null)
            {
                dbTable.Rows.Add(dbTable.NewRow());
                return this[dbTable.Rows.Count - 1];
            }
            else
            {
                if (TableNumber < 0 || TableNumber >= dbData.Tables.Count)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    dbData.Tables[TableNumber].Rows.Add(dbData.Tables[TableNumber].NewRow());
                    return this[TableNumber][dbData.Tables[TableNumber].Rows.Count - 1];
                }
            }
            return null;
        }

        /// <summary>
        /// Adds a new row to the given table.
        /// </summary>
        /// <param name="TableName">Table Name.</param>
        /// <returns>Returns the new row in a QuickDB object.</returns>
        public QuickDB AddRow(string TableName)
        {
            if (dbValueKey != null && dbRow.Table != null)
            {
                dbRow.Table.Rows.Add(dbRow.Table.NewRow());
                return new QuickDB(dbRow.Table.Rows[dbRow.Table.Rows.Count - 1]);
            }
            else if (dbRow != null && dbRow.Table != null)
            {
                dbRow.Table.Rows.Add(dbRow.Table.NewRow());
                return new QuickDB(dbRow.Table.Rows[dbRow.Table.Rows.Count - 1]);
            }
            else if (dbTable != null)
            {
                dbTable.Rows.Add(dbTable.NewRow());
                return this[dbTable.Rows.Count - 1];
            }
            else
            {
                if (!dbData.Tables.Contains(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    dbData.Tables[TableName].Rows.Add(dbData.Tables[TableName].NewRow());
                    return this[TableName][dbData.Tables[TableName].Rows.Count - 1];
                }
            }
            return null;
        }


        ///// <summary>
        ///// Sets the contents of a DataRow. The format must match the destination table.
        ///// </summary>
        ///// <param name="dbRow">A row of table content.</param>
        ///// <param name="RowNumber">Row Index (0 based)</param>
        //public void SetRow(DataRow NewRow, int RowNumber)
        //{
        //    SetRow(NewRow, RowNumber, 0);
        //}

        ///// <summary>
        ///// Sets the contents of a DataRow. The format must match the destination table.
        ///// </summary>
        ///// <param name="dbRow">A row of table content.</param>
        ///// <param name="RowNumber">Row Index (0 based).</param>
        ///// <param name="TableNumber">Table Index (0 based).</param>
        //public void SetRow(DataRow NewRow, int RowNumber, int TableNumber)
        //{
        //    DataTable dbTable;

        //    //Create DataSet if null
        //    if (dbData == null)
        //        dbData = new DataSet();

        //    //Get DataTable or create if null
        //    if ((dbData.Tables.Count <= TableNumber) || (TableNumber < 0))
        //    {
        //        dbTable = dbData.Tables.Add();
        //        //Copy column structure from NewRow.
        //        foreach (DataColumn col in NewRow.Table.Columns)
        //            dbTable.Columns.Add(col);
        //    }
        //    else
        //        dbTable = dbData.Tables[TableNumber];

        //    //Add / replace DataRow
        //    try
        //    {
        //        if ((dbTable.Rows.Count <= RowNumber) || (RowNumber < 0))
        //            dbTable.Rows.Add(NewRow);
        //        else
        //            dbTable.Rows[RowNumber].ItemArray = (object[])NewRow.ItemArray.Clone();
        //    }
        //    catch (Exception exp)
        //    {
        //        _LastException = exp;
        //        if (_throwExceptions)
        //            throw _LastException;
        //    }
        //}

        ///// <summary>
        ///// Sets the contents of a DataRow. The format must match the destination table.
        ///// </summary>
        ///// <param name="dbRow">A row of table content.</param>
        ///// <param name="RowNumber">Row Index (0 based).</param>
        ///// <param name="TableNumber">Table Name.</param>
        //public void SetRow(DataRow NewRow, int RowNumber, string TableName)
        //{
        //    DataTable dbTable;

        //    //Create DataSet if null
        //    if (dbData == null)
        //        dbData = new DataSet();

        //    //Get DataTable or create if null
        //    if (dbData.Tables.Contains(TableName))
        //    {
        //        dbTable = dbData.Tables.Add();
        //        dbTable.TableName = TableName;
        //        //Copy column structure from NewRow.
        //        foreach (DataColumn col in NewRow.Table.Columns)
        //            dbTable.Columns.Add(col);
        //    }
        //    else
        //        dbTable = dbData.Tables[TableName];

        //    //Add / replace DataRow
        //    try
        //    {
        //        if ((dbTable.Rows.Count <= RowNumber) || (RowNumber < 0))
        //            dbTable.Rows.Add(NewRow);
        //        else
        //            dbTable.Rows[RowNumber].ItemArray = (object[])NewRow.ItemArray.Clone();
        //    }
        //    catch (Exception exp)
        //    {
        //        _LastException = exp;
        //        if (_throwExceptions)
        //            throw _LastException;
        //    }
        //}

        ///// <summary>
        ///// Sets the contents of a DataTable.
        ///// </summary>
        //public void SetTable(DataTable NewTable)
        //{
        //    SetTable(NewTable, 0);
        //}

        ///// <summary>
        ///// Sets the contents of a DataTable.
        ///// </summary>
        ///// <param name="NewTable">Table of Content.</param>
        ///// <param name="TableNumber">Table Index (0 based).</param>
        //public void SetTable(DataTable NewTable, int TableNumber)
        //{
        //    //Create DataSet if null
        //    if (dbData == null)
        //        dbData = new DataSet();

        //    //Get DataTable or create if null
        //    if ((dbData.Tables.Count <= TableNumber) || (TableNumber < 0))
        //        dbData.Tables.Add(NewTable);
        //    else
        //    {
        //        dbData.Tables.RemoveAt(TableNumber);
        //        dbData.Tables.Add(NewTable);
        //    }
        //}

        ///// <summary>
        ///// Sets the contents of a DataTable.
        ///// </summary>
        ///// <param name="NewTable>Table of Content.</param>
        ///// <param name="TableNumber">Table Index (0 based).</param>
        //public void SetTable(DataTable NewTable, string TableName)
        //{
        //    //Create DataSet if null
        //    if (dbData == null)
        //        dbData = new DataSet();

        //    NewTable.TableName = TableName; //Just in case it's different.

        //    //Get DataTable or create if null
        //    if (dbData.Tables.Contains(TableName))
        //        dbData.Tables.Add(NewTable);
        //    else
        //    {
        //        dbData.Tables.Remove(TableName);
        //        dbData.Tables.Add(NewTable);
        //    }
        //}


        /// <summary>
        /// Sets the column names of the first table, creating new columns if necessary.
        /// </summary>
        public void SetColumnNames(params string[] ColumnNames)
        {
            int i = 0;
            foreach (string Name in ColumnNames)
                if (!string.IsNullOrEmpty(Name))
                    SetColumnName(Name, i++);
        }

        /// <summary>
        /// Sets the ColumnName of the given Column Index in the first table, creating a new column if necessary.
        /// </summary>
        /// <param name="Name">New Column Name.</param>
        /// <param name="Index">Column Index (0 based).</param>
        public void SetColumnName(string Name, int Index)
        {
            SetColumnName(Name, Index, 0);
        }

        /// <summary>
        /// Sets the ColumnName of the given Column Index and Table Index, creating a new column if necessary.
        /// </summary>
        /// <param name="Name">New Column Name.</param>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        public void SetColumnName(string Name, int Index, int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    dbRow.Table.Columns[Index].ColumnName = Name;
                }
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    dbRow.Table.Columns[Index].ColumnName = Name;
                }
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    while (Index >= dbTable.Columns.Count)
                        dbTable.Columns.Add();

                    dbTable.Columns[Index].ColumnName = Name;
                }
            }
            else
            {
                if (Index < 0 || TableNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Create table if null
                    while (TableNumber >= dbData.Tables.Count)
                        dbData.Tables.Add();

                    //Create column if it doesn't already exist
                    while (Index >= dbData.Tables[TableNumber].Columns.Count)
                        dbData.Tables[TableNumber].Columns.Add();

                    dbData.Tables[TableNumber].Columns[Index].ColumnName = Name;
                }
            }
        }

        /// <summary>
        /// Sets the ColumnName of the given Column Index and Table Index, creating a new column if necessary.
        /// </summary>
        /// <param name="Name">New Column Name.</param>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableName">Table Name.</param>
        public void SetColumnName(string Name, int Index, string TableName)
        {
            if (dbValueKey != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    dbRow.Table.Columns[Index].ColumnName = Name;
                }
            }
            else if (dbRow != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index < 0)
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                else
                {
                    while (Index >= dbRow.Table.Columns.Count)
                        dbRow.Table.Columns.Add();

                    dbRow.Table.Columns[Index].ColumnName = Name;
                }
            }
            else if (dbTable != null)
            {
                if (!string.IsNullOrEmpty(TableName) || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    while (Index >= dbTable.Columns.Count)
                        dbTable.Columns.Add();

                    dbTable.Columns[Index].ColumnName = Name;
                }
            }
            else
            {
                if (Index < 0 || string.IsNullOrEmpty(TableName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Columns[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Create table if null
                    if (!dbData.Tables.Contains(TableName))
                        dbData.Tables.Add(TableName);

                    //Create column if it doesn't already exist
                    while (Index >= dbData.Tables[TableName].Columns.Count)
                        dbData.Tables[TableName].Columns.Add();

                    dbData.Tables[TableName].Columns[Index].ColumnName = Name;
                }
            }
        }

        /// <summary>
        /// Sets the table names of the current DataSet, creating new tables if necessary.
        /// </summary>
        public void SetTableNames(params string[] TableNames)
        {
            int i = 0;
            foreach (string Name in TableNames)
                if (!string.IsNullOrEmpty(Name))
                    SetTableName(Name, i++);
        }

        /// <summary>
        /// Sets the TableName of the first Table, creating it if necessary.
        /// </summary>
        /// <param name="Name">New Table Name.</param>
        /// <returns>The Table Name of the indexed Table in the current DataSet.</returns>
        public void SetTableName(string Name)
        {
            SetTableName(Name, 0);
        }

        /// <summary>
        /// Sets the TableName of the given Table Index, creating a new table if necessary.
        /// </summary>
        /// <param name="Name">New Table Name.</param>
        /// <param name="TableNumber">Table Index (0 based).</param>
        /// <returns>The Table Name of the indexed Table in the current DataSet.</returns>
        public void SetTableName(string Name, int TableNumber)
        {
            if (dbValueKey != null || dbRow != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.TableName = Name;
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbTable.TableName = Name;
            }
            else
            {
                if (TableNumber < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                {
                    //Create DataSet if null
                    if (dbData == null)
                        dbData = new DataSet();

                    //Get DataTable or create if null
                    while (TableNumber >= dbData.Tables.Count)
                        dbData.Tables.Add();

                    dbData.Tables[TableNumber].TableName = Name;
                }
            }
        }


        /// <summary>
        /// Removes a column from the first table.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        public void RemoveColumn(int Index)
        {
            RemoveColumn(Index, 0);
        }
        
        /// <summary>
        /// Removes a column from the first table.
        /// </summary>
        /// <param name="ColumnName">Column Name.</param>
        public void RemoveColumn(string ColumnName)
        {
            RemoveColumn(ColumnName, 0);
        }

        /// <summary>
        /// Removes a column from the given table.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableNumber">Table Number (0 based).</param>
        public void RemoveColumn(int Index, int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.RemoveAt(Index);
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.RemoveAt(Index);
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || Index >= dbTable.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbTable.Columns.RemoveAt(Index);
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || Index >= dbData.Tables[TableNumber].Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbData.Tables[TableNumber].Columns.RemoveAt(Index);
            }
        }

        /// <summary>
        /// Removes a column from the given table.
        /// </summary>
        /// <param name="ColumnName">Column Name.</param>
        /// <param name="TableNumber">Table Number (0 based).</param>
        public void RemoveColumn(string ColumnName, int TableNumber)
        {
            if (dbValueKey != null)
            {
                if (TableNumber != 0 || !dbRow.Table.Columns.Contains(ColumnName))
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[\"", ColumnName, "\"] is out of range."));
                else
                    dbRow.Table.Columns.Remove(ColumnName);
            }
            else if (dbRow != null)
            {
                if (TableNumber != 0 || !dbRow.Table.Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.Remove(ColumnName);
            }
            else if (dbTable != null)
            {
                if (TableNumber != 0 || !dbTable.Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbTable.Columns.Remove(ColumnName);
            }
            else
            {
                if (dbData == null || TableNumber >= dbData.Tables.Count || TableNumber < 0 || !dbData.Tables[TableNumber].Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[", TableNumber, "].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbData.Tables[TableNumber].Columns.Remove(ColumnName);
            }
        }

        /// <summary>
        /// Removes a column from the given table.
        /// </summary>
        /// <param name="Index">Column Index (0 based).</param>
        /// <param name="TableName">Table Name.</param>
        public void RemoveColumn(int Index, string TableName)
        {
            if (dbValueKey != null)
            {
                if (string.IsNullOrEmpty(TableName) || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.RemoveAt(Index);
            }
            else if (dbRow != null)
            {
                if (string.IsNullOrEmpty(TableName) || Index >= dbRow.Table.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.RemoveAt(Index);
            }
            else if (dbTable != null)
            {
                if (string.IsNullOrEmpty(TableName) || Index >= dbTable.Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbTable.Columns.RemoveAt(Index);
            }
            else
            {
                if (dbData == null || !dbData.Tables.Contains(TableName) || Index >= dbData.Tables[TableName].Columns.Count || Index < 0)
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[", Index, "] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbData.Tables[TableName].Columns.RemoveAt(Index);
            }
        }

        /// <summary>
        /// Removes a column from the given table.
        /// </summary>
        /// <param name="ColumnName">Column Name.</param>
        /// <param name="TableName">Table Name.</param>
        public void RemoveColumn(string ColumnName, string TableName)
        {
            if (dbValueKey != null)
            {
                if (string.IsNullOrEmpty(TableName) || !dbRow.Table.Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.Remove(ColumnName);
            }
            else if (dbRow != null)
            {
                if (string.IsNullOrEmpty(TableName) || !dbRow.Table.Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbRow.Table.Columns.Remove(ColumnName);
            }
            else if (dbTable != null)
            {
                if (string.IsNullOrEmpty(TableName) || !dbTable.Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbTable.Columns.Remove(ColumnName);
            }
            else
            {
                if (dbData == null || !dbData.Tables.Contains(TableName) || !dbData.Tables[TableName].Columns.Contains(ColumnName))
                {
                    _LastException = new IndexOutOfRangeException(string.Concat("Table[\"", TableName, "\"].Column[\"", ColumnName, "\"] is out of range."));
                    if (_throwExceptions)
                        throw _LastException;
                }
                else
                    dbData.Tables[TableName].Columns.Remove(ColumnName);
            }
        }

        #endregion

        #region SQL Execution

        /// <summary>
        /// Sets the number of times QuickDB will retry a connection to an Azure DB for the SELECT command.
        /// This can be used to continuously test
        /// Default: 1.
        /// </summary>
        public QuickDB Retry(int retries) {
            _retries = Math.Max(1, retries);
            return this;
        }

        /// <summary>
        /// Execute a SQL Select statement and output the results in the form of a DataSet.
        /// This DataSet can be accessed via the GetValue() commands, or copied into another DataSet object
        /// directly from this command.
        /// </summary>
        /// <param name="SqlStatement">SQL Select Statement</param>
        /// <returns>Response data from the database.</returns>
        public DataSet Select(string SqlStatement)
        {
            if (SqlStatement.ToUpper().Contains(" "))
                return Select(SqlStatement, CmdType.Text);
            else
                return Select(SqlStatement, CmdType.StoredProcedure);
        }

        /// <summary>
        /// Execute a SQL Select statement and output the results in the form of a DataSet. This DataSet
        /// can be accessed via the QuickDB.GetValue() commands, or copied into another DataSet object
        /// directly from this command.
        /// </summary>
        /// <param name="SqlStatement">SQL Select Statement</param>
        /// </summary>
        /// <param name="SqlStatement">SQL Select Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure. Default depends on SQL Statement (Starts with SELECT)</param>
        /// <returns>Response data from the database.</returns>
        public DataSet Select(string SqlStatement, CmdType SqlCommandType)
        {
            if (dataAdapter != null)
            {
                dbTable = null;
                dbRow = null;
                dbValueKey = null;
                dbData = new DataSet();
                dataAdapter.SelectCommand.CommandText = SqlStatement;
                dataAdapter.SelectCommand.CommandType = (CommandType)SqlCommandType;
                int retry = _retries;
                try
                {
                    //Catch and retry Azure DB's sleeping connection.
                    while (retry-- > 0)
                    {
                        try
                        {
                            dataAdapter.Fill(dbData);
                            retry = 0;
                        }
                        catch (SqlException exp)
                        {
                            if (!exp.Message.Contains("Please retry the connection later.") || retry == 0)
                                throw exp;
                        }
                    }
                }
                catch (Exception exp)
                {
                    _LastException = exp;
                    if (_throwExceptions)
                        throw _LastException;
                }

                GetReturnValues(dataAdapter.SelectCommand.Parameters);
                dataAdapter.SelectCommand.Parameters.Clear();
            }
            else
                return dbData; //Allows "off-line" use of the dataset features.
            return dbData.Copy();
        }

        /// <summary>
        /// Add or edit a parameter value for the Select command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        [Obsolete("Please use QuickDB.SetParameter instead.", false)]
        public void SetSelectParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Execute a SQL Insert statement and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Insert Statement, or Stored Procedure name.</param>
        /// <returns>Number of rows affected.</returns>
        public int Insert(string SqlStatement)
        {
            if (SqlStatement.ToUpper().Contains(" "))
                return Insert(SqlStatement, CmdType.Text);
            else
                return Insert(SqlStatement, CmdType.StoredProcedure);
        }

        /// <summary>
        /// Execute a SQL Insert statement and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Insert Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure. Default depends on SQL Statement (Starts with INSERT)</param>
        /// <returns>Number of rows affected.</returns>
        public int Insert(string SqlStatement, CmdType SqlCommandType)
        {
            if (dataAdapter != null)
            {
                dataAdapter.SelectCommand.CommandText = SqlStatement;
                dataAdapter.SelectCommand.CommandType = (CommandType)SqlCommandType;
                LastResult = ExecuteCommand(dataAdapter.SelectCommand);
                dataAdapter.SelectCommand.Parameters.Clear();
            }
            else
                return -1;

            return (Int32)LastResult;
        }

        /// <summary>
        /// Add or edit a parameter value for the Insert command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        [Obsolete("Please use QuickDB.SetParameter instead.", false)]
        public void SetInsertParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Execute a SQL Update statement and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Update Statement, or Stored Procedure name.</param>
        /// <returns>Number of rows affected.</returns>
        public int Update(string SqlStatement)
        {
            if (SqlStatement.ToUpper().Contains(" "))
                return Update(SqlStatement, CmdType.Text);
            else
                return Update(SqlStatement, CmdType.StoredProcedure);
        }

        /// <summary>
        /// Execute a SQL Update statement and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Update Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure. Default depends on SQL Statement (Starts with UPDATE)</param>
        /// <returns>Number of rows affected.</returns>
        public int Update(string SqlStatement, CmdType SqlCommandType)
        {
            if (dataAdapter != null)
            {
                dataAdapter.SelectCommand.CommandText = SqlStatement;
                dataAdapter.SelectCommand.CommandType = (CommandType)SqlCommandType;
                LastResult = ExecuteCommand(dataAdapter.SelectCommand);
                dataAdapter.SelectCommand.Parameters.Clear();
            }
            else
                return -1;

            return (Int32)LastResult;
        }

        /// <summary>
        /// Add or edit a parameter value for the Update command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        [Obsolete("Please use QuickDB.SetParameter instead.", false)]
        public void SetUpdateParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Execute a SQL Delete statement and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Delete Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure.</param>
        /// <returns>Number of rows affected.</returns>
        public int Delete(string SqlStatement)
        {
            if (SqlStatement.ToUpper().Contains(" "))
                return Delete(SqlStatement, CmdType.Text);
            else
                return Delete(SqlStatement, CmdType.StoredProcedure);
        }

        /// <summary>
        /// Execute a SQL Insert Delete and output the number of rows affected.
        /// Return Value -100 = Connection error, -10 = Syntax error or Stored procedure not found.
        /// </summary>
        /// <param name="SqlStatement">SQL Delete Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure. Default depends on SQL Statement (Starts with DELETE)</param>
        /// <returns>Number of rows affected.</returns>
        public int Delete(string SqlStatement, CmdType SqlCommandType)
        {
            if (dataAdapter != null)
            {
                dataAdapter.SelectCommand.CommandText = SqlStatement;
                dataAdapter.SelectCommand.CommandType = (CommandType)SqlCommandType;
                LastResult = ExecuteCommand(dataAdapter.SelectCommand);
                dataAdapter.SelectCommand.Parameters.Clear();
            }
            else
                return -1;

            return (int)LastResult;
        }

        /// <summary>
        /// Add or edit a parameter value for the Delete command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        [Obsolete("Please use QuickDB.SetParameter instead.", false)]
        public void SetDeleteParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Execute a generic SQL statement with no output.
        /// (N.B. This actually utilises the Update Command)
        /// </summary>
        /// <param name="SqlStatement">SQL Statement, or Stored Procedure name.</param>
        public void Execute(string SqlStatement)
        {
            if (SqlStatement.ToUpper().Contains(" "))
                Execute(SqlStatement, CmdType.Text);
            else
                Execute(SqlStatement, CmdType.StoredProcedure);
        }

        /// <summary>
        /// Execute a generic SQL statement with no output.
        /// (N.B. This actually utilises the Update Command)
        /// </summary>
        /// <param name="SqlStatement">SQL Statement, or Stored Procedure name.</param>
        /// <param name="SqlCommandType">Command Type - Text or Stored Procedure. Default depends on SQL Statement (Starts with UPDATE)</param>
        public void Execute(string SqlStatement, CmdType SqlCommandType)
        {
            if (dataAdapter != null)
            {
                dataAdapter.SelectCommand.CommandText = SqlStatement;
                dataAdapter.SelectCommand.CommandType = (CommandType)SqlCommandType;
                ExecuteCommand(dataAdapter.SelectCommand);
                dataAdapter.SelectCommand.Parameters.Clear();
            }
        }

        /// <summary>
        /// Executes an SQL script file.
        /// </summary>
        /// <param name="url">A url or filePath to the script file.</param>
        public void ExecuteScript(string url)
        {
            try
            {
                if (HttpContext.Current != null && File.Exists(HttpContext.Current.Server.MapPath(url))) //If url points to a path on this server.
                    Execute(File.ReadAllText(HttpContext.Current.Server.MapPath(url)));
                else if (File.Exists(url)) //If url is local file path.
                    Execute(File.ReadAllText(url));
                else
                    throw new FileNotFoundException("File not found.");
            }
            catch (Exception exp)
            {
                _LastException = exp;
                if (_throwExceptions)
                    throw exp;
            }
        }

        /// <summary>
        /// Add or edit a parameter value for the Execute command.
        /// (N.B. This actually utilises the Update Command)
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        [Obsolete("Please use QuickDB.SetParameter instead.", false)]
        public void SetExecuteParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Add or edit a parameter value for the next command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        /// <param name="OnlyIf">Parameter will only be set if this expression is true.</param>
        public void SetParameter(string Name, object Value, bool OnlyIf)
        {
            if (OnlyIf)
                SetParameter(Name, Value);
        }

        /// <summary>
        /// Add or edit a parameter value for the next command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        public void SetParameter(string Name, object Value)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, ParamDir.Input);
        }

        /// <summary>
        /// Add or edit a parameter value for the next command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        /// <param name="OnlyIf">Parameter will only be set if this expression is true.</param>
        /// <param name="Direction">Default: Input.  Allows you to configure the direction of this parameter. Use GetValue("@ParamName") to read the value of an Output parameter.</param>
        public void SetParameter(string Name, object Value, bool OnlyIf, ParamDir Direction)
        {
            if (OnlyIf)
                SetParameter(Name, Value, Direction);
        }

        /// <summary>
        /// Add or edit a parameter value for the next command.
        /// </summary>
        /// <param name="Name">Parameter Name</param>
        /// <param name="Value">Parameter Value</param>
        /// <param name="Direction">Default: Input.  Allows you to configure the direction of this parameter. Use GetValue("@ParamName") to read the value of an Output parameter.</param>
        public void SetParameter(string Name, object Value, ParamDir Direction)
        {
            if (dataAdapter != null)
                SetParameter(dataAdapter.SelectCommand, Name, Value, Direction);
        }

        private void SetParameter(IDbCommand Command, string Name, object Value, ParamDir direction)
        {
            Name = Name.TrimStart('@');

            if (Value == null)
                Value = DBNull.Value;

            IDataParameter param = null;
            switch (Command.GetType().Name) {
                case "SqlCommand":
                    param = new SqlParameter(Name, Value);
                    break;
                case "OdbcCommand":
                    param = new OdbcParameter(Name, Value);
                    break;
                case "OleDbCommand":
                    param = new OleDbParameter(Name, Value);
                    break;
                default:
                    _LastException = new Exception("Unable to set parameter for off-line DataSet.");
                    if (_throwExceptions)
                        throw _LastException;
                    break;
            }

            switch (direction)
            {
                case ParamDir.Input:
                default:
                    param.Direction = ParameterDirection.Input;
                    break;
                case ParamDir.Output:
                    param.Direction = ParameterDirection.Output;
                    break;
                case ParamDir.InputOutput:
                    param.Direction = ParameterDirection.InputOutput;
                    break;
                case ParamDir.RetrunValue:
                    param.Direction = ParameterDirection.ReturnValue;
                    break;
            }


            if (param != null)
            {
                if (Command.Parameters.IndexOf(Name) == -1)
                    Command.Parameters.Add(param);
                else
                    Command.Parameters[Name] = param;
            }
        }

        //private void SetParameter(OdbcCommand Command, string Name, object Value)
        //{
        //    if (Value == null)
        //        Value = DBNull.Value;

        //    if (Command.Parameters.IndexOf(Name) == -1)
        //        Command.Parameters.Add(new OdbcParameter(Name, Value));
        //    else
        //        Command.Parameters[Name].Value = Value;
        //}

        private int ExecuteCommand(IDbCommand Command)
        {
            int NumRows = -1;
            try
            {
                Command.Connection.Open();
                NumRows = Command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                _LastException = exp; //Connection falure. //Syntax error or Procedure not found.
                if (_throwExceptions)
                    throw _LastException;
            }
            finally
            {
                Command.Connection.Close();
            }

            GetReturnValues(Command.Parameters);
            return NumRows;
        }

        private void GetReturnValues(IDataParameterCollection parameters)
        {
            foreach (IDbDataParameter param in parameters)
            {
                if (param.Direction != ParameterDirection.Input)
                {
                    if (_ReturnValues.ContainsKey(param.ParameterName.TrimStart('@')))
                        _ReturnValues[param.ParameterName.TrimStart('@')] = param.Value == DBNull.Value ? null : param.Value;
                    else
                        _ReturnValues.Add(param.ParameterName.TrimStart('@'), param.Value == DBNull.Value ? null : param.Value);
                }
            }
        }



        /// <summary>
        /// Copies all the rows in all the DataTables int SQL tables, then clears the DataTable ready for more records.
        /// NOTE: All tables MUST have a table name matching the SQL table name. Use "SetTableName()" to ensure table names are set up correctly.
        /// </summary>
        public void Import()
        {
            foreach (DataTable Table in dbData.Tables)
                Import(Table.TableName, Table);
        }

        /// <summary>
        /// Copies all the rows in the relevant DataTable into SQL table, then clears the DataTable ready for more records.
        /// If a table exists within the DataSet with the same name as the destination SQL Table Name, that table will be imported.
        /// Otherwise the first table in the dataset will be imported.
        /// Column names in both tables must match.
        /// </summary>
        /// <param name="SQLTableName">Destination SQL Table Name.</param>
        public void Import(string SQLTableName)
        {
            if (dbData != null && dbData.Tables.Count > 0)
            {
                if (dbData.Tables.Contains(SQLTableName))
                    Import(SQLTableName, dbData.Tables[SQLTableName]);
                else
                    Import(SQLTableName, dbData.Tables[0]);
            }
        }

        /// <summary>
        /// Copies all the rows in the given DataTable into SQL table, then clears the DataTable ready for more records.
        /// Column names in both tables must match.
        /// </summary>
        /// <param name="SQLTableName">Destination SQL Table Name.</param>
        /// <param name="TableNumber">Source Table Number to import (0 based).</param>
        public void Import(string SQLTableName, int TableNumber)
        {
            if (dbData != null && dbData.Tables.Count > TableNumber && TableNumber >= 0)
                Import(SQLTableName, dbData.Tables[TableNumber]);
        }

        /// <summary>
        /// Copies all the rows in the given DataTable into SQL table, then clears the DataTable ready for more records.
        /// Column names in both tables must match.
        /// </summary>
        /// <param name="SQLTableName">Destination SQL Table Name.</param>
        /// <param name="TableName">Source Table Name.</param>
        public void Import(string SQLTableName, string TableName)
        {
            if (dbData != null && dbData.Tables.Contains(TableName))
                Import(SQLTableName, dbData.Tables[TableName]);
        }

        /// <summary>
        /// Copies all the rows in the given DataTable into SQL table, then clears the DataTable ready for more records.
        /// Column names in both tables must match.
        /// </summary>
        /// <param name="SQLTableName">Destination SQL Table Name.</param>
        /// <param name="dbTable">Source DataTable to import.</param>
        public void Import(string SQLTableName, DataTable dbTable)
        {
            try
            {
                ClearLastException();
                bool TableExists = this.TableExists(SQLTableName);
                if (TableExists && _ImportMode == ImportMode.Truncate)
                {
                    Execute("TRUNCATE TABLE " + NormalizeTableName(SQLTableName) + ";");
                }
                else if (!TableExists && _ImportMode == ImportMode.CreateAndAppend)
                {
                    CreateTable(SQLTableName, dbTable);
                    TableExists = true;
                }
                else if (_ImportMode == ImportMode.DropAndCreate)
                {
                    if (TableExists)
                        Execute("DROP TABLE " + NormalizeTableName(SQLTableName) + ";");

                    CreateTable(SQLTableName, dbTable);
                    TableExists = true;
                }

                //If there was a permission issue manipulating the table, abort import.
                if (_LastException != null)
                    throw _LastException;

                if (TableExists)
                {
                    string ConnectionString = string.Empty;
                    if (dataAdapter != null)
                        ConnectionString = dataAdapter.SelectCommand.Connection.ConnectionString;

                    if (!string.IsNullOrEmpty(ConnectionString))
                    {
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in dbTable.Columns)
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                            bulkCopy.BulkCopyTimeout = Timeout;
                            bulkCopy.DestinationTableName = NormalizeTableName(SQLTableName);
                            bulkCopy.WriteToServer(dbTable);
                        }
                        dbTable.Rows.Clear();
                    }
                    else
                    {
                        _LastException = new Exception("Unable to import data with an off-line DataSet.");
                        if (_throwExceptions)
                            throw _LastException;
                    }
                }
                else
                {
                    _LastException = new Exception("Unable to import data. SQL table '" + SQLTableName + "' does not exist.");
                    if (_throwExceptions)
                        throw _LastException;
                }
            }
            catch (Exception exp)
            {
                _LastException = new Exception("Unable to import data into SQL table '" + SQLTableName + "'.", exp);
                if (_throwExceptions)
                    throw _LastException;
            }
        }

        /// <summary>
        /// Generates and executes the script required to create the given table in the current database.
        /// </summary>
        /// <param name="SQLTableName">Destination SQL Table Name.</param>
        /// <param name="dbTable">Source DataTable containing table definition.</param>
        public string CreateTable(string SQLTableName, DataTable dbTable)
        {
            string SQL = "CREATE TABLE " + NormalizeTableName(SQLTableName) + " (";
            try
            {
                for (int i = 0; i < dbTable.Columns.Count; i++)
                {
                    SQL += "\n [" + dbTable.Columns[i].ColumnName + "] ";
                    string columnType = dbTable.Columns[i].DataType.ToString();
                    string dataType = (string)dbTable.Columns[i].ExtendedProperties["DataType"];
                    if (!string.IsNullOrEmpty(dataType))
                        SQL += " " + dataType;
                    else
                    {
                        switch (columnType)
                        {
                            case "System.Int32":
                                SQL += " INT";
                                break;
                            case "System.Int64":
                                SQL += " BIGINT";
                                break;
                            case "System.Int16":
                                SQL += " SMALLINT";
                                break;
                            case "System.Byte":
                                SQL += " TINYINT";
                                break;
                            case "System.Decimal":
                                SQL += " DECIMAL(18, 6)";
                                break;
                            case "System.DateTime":
                                SQL += " DATETIME2(7)";
                                break;
                            case "System.String":
                            default:
                                SQL += string.Format(" NVARCHAR({0})", dbTable.Columns[i].MaxLength == -1 ? "MAX" : dbTable.Columns[i].MaxLength.ToString());
                                break;
                        }
                    }
                    if (dbTable.Columns[i].AutoIncrement)
                        SQL += " IDENTITY(" + dbTable.Columns[i].AutoIncrementSeed.ToString() + "," + dbTable.Columns[i].AutoIncrementStep.ToString() + ")";
                    if (!dbTable.Columns[i].AllowDBNull)
                        SQL += " NOT NULL";
                    SQL += ",";
                }

                SQL = SQL.TrimEnd(',') + "\n)";
                Execute(SQL);
            }
            catch (Exception exp)
            {
                _LastException = new Exception("Unable to creat table '" + SQLTableName + "'.", exp);
                if (_throwExceptions)
                    throw _LastException;
            }

            return SQL;
        }

        /// <summary>
        /// Changes the current database.
        /// </summary>
        public QuickDB Use(string DatabaseName) {
            string ConnectionString = dataAdapter.SelectCommand.Connection.ConnectionString;
            if (ConnectionString.StartsWith("Driver=", StringComparison.CurrentCultureIgnoreCase))
            {
                _LastException = new InvalidOperationException("Cannot change database of an ODBC connection.");
                if (_LastException != null)
                    throw _LastException;
            }
            else if (ConnectionString.StartsWith("Provider", StringComparison.CurrentCultureIgnoreCase))
            {
                _LastException = new InvalidOperationException("Cannot change database of an OLEDB connection.");
                if (_LastException != null)
                    throw _LastException;
            }
            else
            {
                SqlConnectionStringBuilder cs = new SqlConnectionStringBuilder(ConnectionString);
                cs.InitialCatalog = DatabaseName;
                dataAdapter.SelectCommand.Connection.ConnectionString = cs.ConnectionString;
            }
            return this;
        }


        #endregion

        #region Interrigation

        /// <summary>
        /// Returns the current DataSet.
        /// </summary>
        public DataSet DataSet
        {
            get { return dbData; }
        }
        /// <summary>
        /// Returns the current DataTable.
        /// </summary>
        public DataTable Table
        {
            get { return dbTable; }
        }
        /// <summary>
        /// Returns the current DataRow.
        /// </summary>
        public DataRow Row
        {
            get { return dbRow; }
        }
        /// <summary>
        /// Returns the current DataValue.
        /// </summary>
        public object Value
        {
            get { return dbRow[dbValueKey]; }
        }

        /// <summary>
        /// Gets the Table or Row in this DataSet or Table at the given index as a QuickDB.
        /// </summary>
        public QuickDB this[int Index]
        {
            get
            {
                if (dbValueKey != null)
                    return this;
                else if (dbRow != null)
                    return new QuickDB(dbRow, dbRow.Table.Columns[ RelIndex(Index, dbRow.Table.Columns.Count, "Column") ].ColumnName);
                else if (dbTable != null)
                    return new QuickDB(dbTable.Rows[ RelIndex(Index, dbTable.Rows.Count, "Row") ]);
                else if (dbData != null)
                    return new QuickDB(dbData.Tables[ RelIndex(Index, dbData.Tables.Count, "Table") ]);
                else
                    throw new IndexOutOfRangeException("DataSet is null.");
            }
        }

        private int RelIndex(int index, int length, string type)
        {
            if (index >= length || Math.Abs(index) > length || length == 0) //Index out of range.
                throw new IndexOutOfRangeException(type + "[" + index + "] is out of range.");
            else if (index < 0)
                return (index + length) % length;

            return index;
        }

        /// <summary>
        /// Gets the Table or Value in this DataSet or Row at the given TableName or ColumnName as a QuickDB.
        /// </summary>
        public QuickDB this[string Name]
        {
            get
            {
                if (dbValueKey != null)
                    return this;
                else if (dbRow != null)
                    return new QuickDB(dbRow, dbRow.Table.Columns[Name].ColumnName);
                else if (dbTable != null)
                    throw new ArgumentOutOfRangeException("Cannot access rows by name. Please use GetValue() instead.");
                else if (dbData != null)
                    return new QuickDB(dbData.Tables[Name]);
                else
                    throw new IndexOutOfRangeException("DataSet is null.");
            }
        }

        /// <summary>
        /// Returns the previous Table, Row or Value in its container.
        /// </summary>
        public QuickDB Prev
        {
            get
            {
                if (dbValueKey != null)
                {
                    int i = dbRow.Table.Columns.IndexOf(dbValueKey) - 1;
                    if (i >= 0)
                        return new QuickDB(dbRow, dbRow.Table.Columns[i].ColumnName);
                }
                else if (dbRow != null)
                {
                    int i = dbRow.Table.Rows.IndexOf(dbRow) - 1;
                    if (i >= 0)
                        return new QuickDB(dbRow.Table.Rows[i]);
                }
                else if (dbTable != null)
                {
                    int i = dbTable.DataSet.Tables.IndexOf(dbTable) - 1;
                    if (i >= 0)
                        return new QuickDB(dbTable.DataSet.Tables[i]);
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the next Table, Row or Value in its container.
        /// </summary>
        public QuickDB Next
        {
            get
            {
                if (dbValueKey != null)
                {
                    int i = dbRow.Table.Columns.IndexOf(dbValueKey) + 1;
                    if (i < dbRow.Table.Columns.Count)
                        return new QuickDB(dbRow, dbRow.Table.Columns[i].ColumnName);
                }
                else if (dbRow != null)
                {
                    int i = dbRow.Table.Rows.IndexOf(dbRow) + 1;
                    if (i < dbRow.Table.Rows.Count)
                        return new QuickDB(dbRow.Table.Rows[i]);
                }
                else if (dbTable != null)
                {
                    int i = dbTable.DataSet.Tables.IndexOf(dbTable) + 1;
                    if (i < dbTable.DataSet.Tables.Count)
                        return new QuickDB(dbTable.DataSet.Tables[i]);
                }
                return null;
            }
        }

        /// <summary>
        /// Returns this item's parent container.
        /// </summary>
        public QuickDB Parent
        {
            get
            {
                if (dbValueKey != null)
                    return new QuickDB(dbRow);
                else if (dbRow != null)
                    return new QuickDB(dbRow.Table);
                else if (dbTable != null)
                    return new QuickDB(dbTable.DataSet);
                return this;
            }
        }

        /// <summary>
        /// Returns this item's first child.
        /// </summary>
        public QuickDB FirstChild
        {
            get
            {
                if (dbRow != null)
                    return this[0];
                else if (dbTable != null)
                    return this[0];
                else if (dbData != null)
                    return this[0];
                return null;
            }
        }

        /// <summary>
        /// Returns this item's last child.
        /// </summary>
        public QuickDB LastChild
        {
            get
            {
                if (dbRow != null)
                    return this[dbRow.Table.Columns.Count - 1];
                else if (dbTable != null)
                    return this[dbTable.Rows.Count - 1];
                else if (dbData != null)
                    return this[dbData.Tables.Count - 1];
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the current database for this connection.
        /// </summary>
        public string DatabaseName
        {
            get {
                return dataAdapter.SelectCommand.Connection.Database;
            }
            set {
                Use(value);
            }
        }


        /// <summary>
        /// Returns an IEnumerator for the JSON.
        /// </summary>
        IEnumerator<QuickDB> IEnumerable<QuickDB>.GetEnumerator()
        {
            if (dbValueKey != null)
                yield return this;
            else if (dbRow != null)
                foreach (DataColumn col in dbRow.Table.Columns)
                    yield return new QuickDB(dbRow, col.ColumnName);
            else if (dbTable != null)
                foreach (DataRow row in dbTable.Rows)
                    yield return new QuickDB(row);
            else if (dbData != null)
                foreach (DataTable table in dbData.Tables)
                    yield return new QuickDB(table);
        }

        /// <summary>
        /// Returns an IEnumerator for the JSON.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            if (dbValueKey != null)
                yield return this;
            else if (dbRow != null)
                foreach (DataColumn col in dbRow.Table.Columns)
                    yield return new QuickDB(dbRow, col.ColumnName);
            else if (dbTable != null)
                foreach (DataRow row in dbTable.Rows)
                    yield return new QuickDB(row);
            else if (dbData != null)
                foreach (DataTable table in dbData.Tables)
                    yield return new QuickDB(table);
        }




        /// <summary>
        /// Returns the current DataTableCollection or null if it doesn't exist.
        /// </summary>
        public DataTableCollection Tables
            {
                get {
                    if (dbData != null)
                        return dbData.Tables;
                    else if (dbTable != null && dbTable.DataSet != null)
                        return dbTable.DataSet.Tables;
                    else if (dbRow != null && dbRow.Table != null && dbRow.Table.DataSet != null)
                        return dbRow.Table.DataSet.Tables;
                    else
                        return null;
                }
            }

        /// <summary>
        /// Returns the DataRowCollection in the first table or null if it doesn't exist.
        /// </summary>
        public DataRowCollection Rows
        {
            get
            {
                if (dbData != null && dbData.Tables.Count > 0)
                    return dbData.Tables[0].Rows;
                else if (dbTable != null)
                    return dbTable.Rows;
                else if (dbRow != null && dbRow.Table != null)
                    return dbRow.Table.Rows;
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns the DataColumnCollection of the first table or null if it doesn't exist.
        /// </summary>
        public DataColumnCollection Columns
        {
            get
            {
                if (dbData != null && dbData.Tables.Count > 0)
                    return dbData.Tables[0].Columns;
                else if (dbTable != null)
                    return dbTable.Columns;
                else if (dbRow != null && dbRow.Table != null)
                    return dbRow.Table.Columns;
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns the latest exception.
        /// </summary>
        public Exception LastException
        {
            get { return _LastException; }
        }

        /// <summary>
        /// Gets or Sets the wait time in seconds before terminating an attempt to execute a command and generating an error. Default: 30s.
        /// </summary>
        public int Timeout
        {
            get
            {
                if (dataAdapter != null)
                    return dataAdapter.SelectCommand.CommandTimeout;
                else
                    return 0;
            }
            set
            {
                if (dataAdapter != null)
                    dataAdapter.SelectCommand.CommandTimeout = value;
            }
        }

        /// <summary>
        /// Gets or Sets the current import mode.
        /// </summary>
        public ImportMode ImportMode
        {
            get
            {
                return _ImportMode;
            }
            set
            {
                _ImportMode = value;
            }
        }

        /// <summary>
        /// This event handler will pass a string message every time a PRINT command is executed.
        /// </summary>
        public event EventHandler<string> InfoMessage
        {
            add
            {
                _InfoMessage += value;
            }
            remove
            {
                _InfoMessage -= value;
            }
        }

        /// <summary>
        /// Returns an optional "s" dependant on whether or not the LastResult should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There are x report" + pl()
        /// </summary>
        /// <returns>An "s" if plural.</returns>
        public string pl()
        {
            return pl("", "s");
        }

        /// <summary>
        /// Returns one of two strings dependant on whether or not the LastResult should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There " + pl("is","are") + " x report" + pl("","s")
        /// </summary>
        /// <param name="Singular">Expression for 1 item.</param>
        /// <param name="Plural">Expression for multiple items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        public string pl(string Singular, string Plural)
        {
            try
            {
                return pl(Convert.ToDouble(LastResult), Singular, Plural);
            }
            catch (Exception exp)
            {
                return Plural;
            }
        }

        /// <summary>
        /// If this QuickDB HasErrors, this will throw the LastException. Otherwise, do nothing.
        /// </summary>
        public void Throw()
        {
            if (_LastException != null)
                throw _LastException;
        }

        /// <summary>
        /// Set the default behaviour of this QuickDB when errors are encountered.
        /// </summary>
        /// <param name="ThrowExceptions">Default: False. When True, exceptions will be thrown immediately. Otherwise errors are surpressed.</param>
        /// <returns>This QuickDB.</returns>
        public QuickDB Throw(bool ThrowExceptions)
        {
            _throwExceptions = ThrowExceptions;
            return this;
        }

        /// <summary>
        /// Did the last operation cause an exception.
        /// </summary>
        /// <returns>True if there was an exception.</returns>
        public bool HasErrors()
        {
            return (_LastException != null);
        }

        /// <summary>
        /// Did the last operation return any records.
        /// </summary>
        /// <returns>True if there was an exception.</returns>
        public bool HasData()
        {
            return (_LastException == null && RowCount(0) > 0);
        }

        /// <summary>
        /// Did the last operation return any records in the given table.
        /// </summary>
        /// <param name="TableNumber">Table Index (0 based)</param>
        /// <returns>True if there was an exception.</returns>
        public bool HasData(int TableNumber)
        {
            return (_LastException == null && RowCount(TableNumber) > 0);
        }

        /// <summary>
        /// Clears the last exception from memory.
        /// </summary>
        public void ClearLastException()
        {
            _LastException = null;
        }

        #endregion

        #region Static functions

        /// <summary>
        /// Gets or sets the default string to be used in case of null values. (Initially: "")
        /// </summary>
        public static string DefaultString
        {
            get { return _DefaultString; }
            set { _DefaultString = value; }
        }
        /// <summary>
        /// Gets or sets the default int to be used in case of null values. (Initially: 0)
        /// </summary>
        public static int DefaultInt
        {
            get { return _DefaultInt; }
            set { _DefaultInt = value; }
        }
        /// <summary>
        /// Gets or sets the default double to be used in case of null values. (Initially: 0.0)
        /// </summary>
        public static double DefaultDouble
        {
            get { return _DefaultDouble; }
            set { _DefaultDouble = value; }
        }
        /// <summary>
        /// Gets or sets the default boolean to be used in case of null values. (Initially: false)
        /// </summary>
        public static bool DefaultBool
        {
            get { return _DefaultBool; }
            set { _DefaultBool = value; }
        }
        /// <summary>
        /// Gets or sets the default date time to be used in case of null values. (Initially: DateTime.MinValue)
        /// </summary>
        public static DateTime DefaultDateTime
        {
            get { return _DefaultDateTime; }
            set { _DefaultDateTime = value; }
        }
        /// <summary>
        /// Gets or sets the current Encryption Key. If null, this will return AppSettings["EncryptionKey"].
        /// </summary>
        public static string EncryptionKey
        {
            get { return  _DefaultEncryptionKey ?? ConfigurationManager.AppSettings["QuickLibs.Cryptography.EncryptionKey"]; }
            set { _DefaultEncryptionKey = value; }
        }

        /// <summary>
        /// Returns the given connection string with the password removed, if present.
        /// <param name="mask">Default: "[HIDDEN]". String to replace hidden elments.</param>
        /// </summary>
        public static string MaskConnectionString(string connectionString, string mask = null)
        {
            return MaskConnectionString(connectionString, mask, "password", "pwd");
        }

        /// <summary>
        /// Returns the given connection string with each of the maskItems removed, if present. E.g. QuickDB.MaskConnectionString(connStr, "user", "password")
        /// </summary>
        /// <param name="mask">Default: "[HIDDEN]". String to replace hidden elments.</param>
        /// <param name="maskItems">List of connection string items to mask.</param>
        public static string MaskConnectionString(string connectionString, string mask, params string[] maskItems)
        {
            if (string.IsNullOrEmpty(connectionString) || maskItems.Length == 0)
                return connectionString;

            foreach (string item in maskItems)
            {
                int index = connectionString.IndexOf(item, StringComparison.CurrentCultureIgnoreCase);

                if (index > 0)
                {
                    index = connectionString.IndexOf("=", index) + 1;
                    int length = connectionString.IndexOf(";", index) - index;
                    if (length < 0)
                        length = connectionString.Length - index;

                    connectionString = connectionString.Remove(index, length).Insert(index, mask ?? "[HIDDEN]");
                }
            }

            return connectionString;
        }



        /// <summary>
        /// Returns a connection string from the configuration file, or formats the given connection string, replacing encoded or encrpyted values.
        /// </summary>
        /// <param name="Connection">Full connection string OR connection name in the config file.</param>
        public static string ConnectionString(string Connection)
        {
            //If Empty return non-null string.
            if(string.IsNullOrWhiteSpace(Connection))
                return "";

            //If Connection looks like a connections string
            if (!Connection.Contains(";"))
            {
                if (ConfigurationManager.ConnectionStrings[Connection] != null)
                    Connection = ConfigurationManager.ConnectionStrings[Connection].ConnectionString;
                else
                    Connection = "";
            }

            bool EncodedPassword = Connection.Contains("EncodedPassword=");
            bool EncryptedPassword = Connection.Contains("EncryptedPassword=");

            if (EncodedPassword  || EncryptedPassword)
                Connection = Connection.Replace("EncodedPassword", "Password").Replace("EncryptedPassword", "Password");

            if (Connection.StartsWith("Driver=", StringComparison.CurrentCultureIgnoreCase))
            {
                OdbcConnectionStringBuilder cs = new OdbcConnectionStringBuilder(Connection);
                if (EncodedPassword)
                    cs["Pwd"] = Decode(cs["Password"].ToString()) ?? "";
                else if (EncryptedPassword)
                    cs["Pwd"] = Decrypt(cs["Password"].ToString(), EncryptionKey ?? cs["Uid"].ToString()) ?? "";
                cs.Remove("Password");

                Connection = cs.ConnectionString;
            }
            else if (Connection.StartsWith("Provider", StringComparison.CurrentCultureIgnoreCase))
            {
                OleDbConnectionStringBuilder cs = new OleDbConnectionStringBuilder(Connection);
                if (EncodedPassword)
                    cs["Password"] = Decode(cs["Password"].ToString()) ?? "";
                else if (EncryptedPassword)
                    cs["Password"] = Decrypt(cs["Password"].ToString(), EncryptionKey ?? cs["User Id"].ToString()) ?? "";

                Connection = cs.ConnectionString;
            }
            else
            {
                SqlConnectionStringBuilder cs = new SqlConnectionStringBuilder(Connection);
                if (EncodedPassword)
                    cs.Password = Decode(cs.Password) ?? "";
                else if (EncryptedPassword)
                    cs.Password = Decrypt(cs.Password, EncryptionKey ?? cs.UserID) ?? "";

                if (!string.IsNullOrEmpty(cs.Password))
                    cs.PersistSecurityInfo = true;

                Connection = cs.ConnectionString;
            }
            return Connection;
        }

        /// <summary>
        /// Returns a SQL Server connection string based on a trusted connection.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        public static string ConnectionString(string Server, string Database)
        {
            return string.Concat("Server=", Server, ";Database=", Database, ";Trusted_Connection=True;");
        }

        /// <summary>
        /// Returns a SQL Server connection string based on given parameters and credentials.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        /// <param name="Username">SQL User username</param>
        /// <param name="Password">Password</param>
        public static string ConnectionString(string Server, string Database, string Username, string Password)
        {
            return string.Concat("Server=", Server, ";Database=", Database, ";User Id=", Username, ";Password=", Password, ";");
        }

        /// <summary>
        /// Returns a SQL Server connection string based on given parameters and credentials.
        /// </summary>
        /// <param name="Server">SQL Server name / instance</param>
        /// <param name="Database">SQL Database name</param>
        /// <param name="Username">SQL User username</param>
        /// <param name="Password">Password</param>
        /// <param name="PersistSecurity">Persist Security Info (required for Import();)</param>
        public static string ConnectionString(string Server, string Database, string Username, string Password, bool PersistSecurity)
        {
            return string.Concat("Server=", Server, ";Database=", Database, ";User Id=", Username, ";Password=", Password, ";Persist Security Info=", PersistSecurity, ";");
        }

        /// <summary>
        /// Returns the given multi-part TableName normalised into [DB].[schema].[table] format regardless of the input format.
        /// </summary>
        /// <param name="TableName">Full table name path e.g. DB.schema.table  OR  [DB].[schema].[table]</param>
        public static string NormalizeTableName(string TableName)
        {
            return NormalizeObjectName(DenormalizeObjectName(TableName));
        }

        /// <summary>
        /// Returns the given multi-part object name as an array of strings.
        /// </summary>
        /// <param name="ObjectName">Full object name path. eg. DB.schema.table  OR [DB].[schema].[table]</param>
        public static string[] DenormalizeObjectName(string ObjectName)
        {
            StringBuilder sb = new StringBuilder();
            List<string> parts = new List<string>();
            int i = 0;
            bool open = false;
            while (i < ObjectName.Length)
            {
                char chr = ObjectName[i++];
                if (open) //Inside brackets
                {
                    switch (chr)
                    {
                        case ']':
                            if (i < ObjectName.Length && ObjectName[i] == ']') //If escaped, copy the character into the part
                            {
                                sb.Append("]");
                                i++;
                            }
                            else //If not escaped, close the 'bracket'.
                                open = false;
                            break;
                        default:
                            sb.Append(chr);
                            break;
                    }
                }
                else //No brackets
                {
                    switch (chr)
                    {
                        case '.':
                            parts.Add(sb.ToString());
                            sb.Clear();
                            break;
                        case '[':
                            if (sb.Length == 0) //Only open brackets if at the start of a name.
                                open = true;
                            else
                                sb.Append(chr);
                            break;
                        default:
                            sb.Append(chr);
                            break;
                    }
                }
            }
            parts.Add(sb.ToString());

            return parts.ToArray();
        }

        /// <summary>
        /// Returns the given ObjectName parts normalised into [ObjectName] format regardless of the input format. Multiple ObjectNames will be joined with a "."  eg. [DB].[Schema].[Table]
        /// </summary>
        /// <param name="ObjectNameParts">List of ObjectName parts, with or without square brackets.</param>
        public static string NormalizeObjectName(params string[] ObjectNameParts)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string part in ObjectNameParts)
            {
                if (!string.IsNullOrEmpty(part))
                {
                    if (part.StartsWith("[") && part.EndsWith("]"))
                        sb.Append(part);
                    else
                        sb.Append('[')
                          .Append(part.Replace("]", "]]")) //Escape any other close brackets.
                          .Append(']');
                }
                sb.Append('.');
            }
            sb.Length = sb.Length - 1; //Remove last '.'
            return sb.ToString();
        }

        /// <summary>
        /// Returns one of two strings dependant on whether or not the given value should be refered to as plural.
        /// eg. "There are x report(s)" becomes "There " + pl(x,"is","are") + " x report" + pl(x,"","s")
        /// </summary>
        /// <param name="Value">Number of items.</param>
        /// <param name="Singular">Expression for 1 item.</param>
        /// <param name="Plural">Expression for multiple items.</param>
        /// <returns>Singular or Plural phrase.</returns>
        [Obsolete("Please use the string.pl() extensions in QuickLibs.Utils instead.", false)]
        private static string pl(double Value, string Singular, string Plural)
        {
            return (Value == 1) ? Singular : Plural;
        }

        /// <summary>
        ///   Generates a SHA1 hash string from the given input string.
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <returns></returns>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.GetHash instead.", false)]
        private static string GetHashLegacy(string str)
        {
            return GetHashLegacy(str, ""); //Default salt was "MoSaiC"
        }

        /// <summary>
        ///   Generates a SHA1 hash string from the given input.
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <param name="salt">Salt to further obscure the hash.</param>
        /// <returns>SHA1 hash (with salt)</returns>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.GetHashSHA1 instead.", false)]
        private static string GetHashLegacy(string str, string salt)
        {
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            sha1.ComputeHash(Encoding.ASCII.GetBytes(str + salt));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in sha1.Hash)
                sb.Append(b.ToString("x2"));
            return sb.ToString().ToUpper();
        }

        /// <summary>
        ///   Generates a hash-based message authentication code (HMAC) hash string from the given input.
        /// </summary>
        /// <param name="data">This is the data to be authenticated.</param>
        /// <param name="key">This is the secret key to authenticate the hash.</param>
        /// <returns>SHA1 hash (with salt)</returns>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.GetHash instead.", false)]
        private static string GetHash(string data, string key)
        {
            Encoding enc = Encoding.UTF8;
            HMACSHA256 hmac = new HMACSHA256(enc.GetBytes(key));
            hmac.ComputeHash(enc.GetBytes(data));

            return Convert.ToBase64String(hmac.Hash);
        }


        /// <summary>
        ///     Encodes the given text as a Base64String and trims the trailing padding characters.
        /// </summary>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Encode instead.", false)]
        private static string Encode(string plainText)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText)).Trim('=');
        }

        /// <summary>
        ///     Decodes the given text from a Base64String. Trailing padding characters are not required.
        /// </summary>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Decode instead.", false)]
        private static string Decode(string encodedText)
        {
            if(encodedText.Length % 4 > 0)
                encodedText += new string('=', 4 - encodedText.Length % 4);
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedText));
        }


        /// <summary>
        /// Converts plain text into an encrypted string that can be decrypted again later.
        /// </summary>
        /// <param name="plainText">Original text to be encrypted.</param>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Encrypt instead.", false)]
        private static string Encrypt(string plainText)
        {
                return Encrypt(plainText, EncryptionKey);
        }

        /// <summary>
        /// Converts plain text into an encrypted string that can be decrypted again later.
        /// </summary>
        /// <param name="plainText">Original text to be encrypted.</param>
        /// <param name="password">Default: ConfigurationManager.AppSettings["EncryptionKey"] or User ID if null. The same key must be used to decrypt the encrypted text.</param>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Encrypt instead.", false)]
        private static string Encrypt(string plainText, string password)
        {
            string cipherText = "";
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            byte[] saltStringBytes = RandomBytes(32);
            byte[] ivStringBytes = RandomBytes(32);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            using (Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(password, saltStringBytes, 1000))
            {
                var keyBytes = key.GetBytes(256 / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new System.IO.MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                cipherText = Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }

            return cipherText.Replace('=', '*');
        }


        /// <summary>
        /// Converts an encrypted cipher back into plain text using the given key.
        /// </summary>
        /// <param name="cipherText">Encrypted text.</param>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Decrypt instead.", false)]
        private static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, EncryptionKey);
        }

        /// <summary>
        /// Converts an encrypted cipher back into plain text using the given key.
        /// </summary>
        /// <param name="cipherText">Encrypted text.</param>
        /// <param name="password">Default: ConfigurationManager.AppSettings["EncryptionKey"] or User ID if null. This key must match the key used to encrypt the text.</param>
        [Obsolete("Please use QuickLibs.Cryptography.Crypto.Decrypt instead.", false)]
        private static string Decrypt(string cipherText, string password)
        {
            string plainText = cipherText;
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            byte[] cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText.Replace('*','='));
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            byte[] saltStringBytes = cipherTextBytesWithSaltAndIv.Take(256 / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            byte[] ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(256 / 8).Take(256 / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            byte[] cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((256 / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((256 / 8) * 2)).ToArray();

            try
            {
                using (Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(password, saltStringBytes, 1000))
                {
                    byte[] keyBytes = key.GetBytes(256 / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.BlockSize = 256;
                        symmetricKey.Mode = CipherMode.CBC;
                        symmetricKey.Padding = PaddingMode.PKCS7;
                        using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                        {
                            using (var memoryStream = new System.IO.MemoryStream(cipherTextBytes))
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                //throw new InvalidOperationException("Decryption failed: Invalid password.", exp);
                plainText = null;
            }

            return plainText;
        }

        private static byte[] RandomBytes(int bytes)
        {
            var randomBytes = new byte[bytes]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }
        #endregion
    }

    /// <summary>
    /// Command Type.
    /// </summary>
    public enum CmdType
    {
        /// <summary>
        /// Stored Procedure
        /// </summary>
        StoredProcedure = CommandType.StoredProcedure,
        /// <summary>
        /// Text
        /// </summary>
        Text = CommandType.Text,
        /// <summary>
        /// Table Direct
        /// </summary>
        TableDirect = CommandType.TableDirect
    }

    /// <summary>
    /// Parameter Direction.
    /// </summary>
    public enum ParamDir {
        /// <summary>
        /// Input
        /// </summary>
        Input = ParameterDirection.Input,
        /// <summary>
        /// Output
        /// </summary>
        Output = ParameterDirection.Output,
        /// <summary>
        /// Input / Output
        /// </summary>
        InputOutput = ParameterDirection.InputOutput,
        /// <summary>
        /// Return Value
        /// </summary>
        RetrunValue = ParameterDirection.ReturnValue
    }

    /// <summary>
    /// Import Mode
    /// </summary>
    public enum ImportMode {
        /// <summary>
        /// Append records to the end of the existing table. Uses existing table schema.
        /// </summary>
        Append,
        /// <summary>
        /// Truncates the table, then appends records. Uses existing table schema.
        /// </summary>
        Truncate,
        /// <summary>
        /// Drop the existing table and create a new one using the source schema. Then append records.
        /// </summary>
        DropAndCreate,
        /// <summary>
        /// Create a new table if it doesn't already exist using the source schema. Then append records, using the existing schema if it already existed.
        /// </summary>
        CreateAndAppend
    }
}