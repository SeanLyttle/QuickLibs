<?xml version="1.0"?>
<doc>
    <assembly>
        <name>QuickLibs.Cryptography.Crypto</name>
    </assembly>
    <members>
        <member name="T:QuickLibs.Cryptography.Crypto">
            <summary>
            A simple cryptographic library for secure RNG.
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.Crypto.EncryptionKey">
            <summary>
            Encryption Key used by the Encrypt / Decrypt methods. Default: AppSettings["QuickLibs.Cryptography.EncryptionKey"] OR a SHA1 hash of AppDomain.CurrentDomain.FriendlyName if AppSetting is null.
            </summary>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHashSHA1(System.String,System.String)">
            <summary>
            LEGACY: Generates a SHA1 hash string from the given input and salt.
            </summary>
            <param name="str">String to hash</param>
            <param name="salt">Salt to further obscure the hash.</param>
            <returns>SHA1 hash of salt + str.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHashSHA512(System.String,System.String)">
            <summary>
              Generates a SHA512 hash string from the given input and salt.
            </summary>
            <param name="str">String to hash</param>
            <param name="salt">Salt to further obscure the hash.</param>
            <returns>SHA512 hash of salt + str.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHashHMAC(System.String,System.String)">
            <summary>
              Generates a hash-based message authentication code (HMAC) hash string from the given input.
            </summary>
            <param name="data">This is the data to be authenticated.</param>
            <param name="key">This is the secret key to authenticate the hash.</param>
            <returns>HMACSHA512 hash in Base64 encoding.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHashHMAC256(System.String,System.String)">
            <summary>
            LEGACY: Generates a hash-based message authentication code (HMAC) hash string from the given input.
            </summary>
            <param name="data">This is the data to be authenticated.</param>
            <param name="key">This is the secret key to authenticate the hash.</param>
            <returns>HMACSHA256 hash in Base64 encoding.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHash(System.String)">
            <summary>
              Generates a SHA512 hash string from the given input string with no salt. Shorthand for GetHashSHA512(str, "");
            </summary>
            <param name="str">String to hash</param>
            <returns>SHA512 hash of str.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHash(System.String,System.String,System.Int32)">
            <summary>
              Generates a SHA512 hash string from the given input string with no salt. Shorthand for GetHashSHA512(str, "");
            </summary>
            <param name="str">String to hash</param>
            <param name="iterations">Number of times to repeat the hash</param>
            <returns>SHA512 hash of str.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetHash(System.String,System.String)">
            <summary>
              Generates a hash-based message authentication code (HMAC) hash string from the given input. Shorthand for GetHashHMAC(data, key);
            </summary>
            <param name="data">This is the data to be authenticated.</param>
            <param name="key">This is the secret key to authenticate the hash.</param>
            <returns>HMACSHA256 hash in Base64 encoding.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Encode(System.String)">
            <summary>
                Encodes the given text as a Base64String and trims the trailing padding characters.
            </summary>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Decode(System.String)">
            <summary>
                Decodes the given text from a Base64String. Trailing padding characters are not required.
            </summary>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Encrypt(System.String)">
            <summary>
            Converts plain text into an encrypted string that can be decrypted again later.
            </summary>
            <param name="plainText">Original text to be encrypted.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Encrypt(System.String,System.String)">
            <summary>
            Converts plain text into an encrypted string that can be decrypted again later.
            </summary>
            <param name="plainText">Original text to be encrypted.</param>
            <param name="cipherKey">Default: EncryptionKey. The same key must be used to decrypt the encrypted text.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Decrypt(System.String)">
            <summary>
            Converts an encrypted cipher back into plain text using the default key stored in ConfigurationManager.AppSettings["EncryptionKey"].
            </summary>
            <param name="cipherText">Encrypted text.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.Decrypt(System.String,System.String)">
            <summary>
            Converts an encrypted cipher back into plain text using the given key.
            </summary>
            <param name="cipherText">Encrypted text.</param>
            <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GeneratePassword(System.Int32)">
            <summary>
            Returns a string with the given number of random AlphaNumeric characters.
            </summary>
            <param name="Length">Number of characters to return.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GeneratePassword(System.Int32,QuickLibs.Cryptography.CharTypes)">
            <summary>
            Returns a string with the given number of random characters from the given preset of CharTypes.
            </summary>
            <param name="Length">Number of characters to return.</param>
            <param name="Flags">Default: CharTypes.AlphaNumeric. A set of CharTypes that can be used in the generation. Separate multiple CharTypes with "|".</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GeneratePassword(System.Int32,System.String)">
            <summary>
            Returns a string with the given number of random characters from the given selection available.
            </summary>
            <param name="Length">Number of characters to return.</param>
            <param name="AvailableChars">Only characters that appear in this string will be used in the generation.</param>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetPassword(System.String)">
            <summary>
            Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
            The PasswordAppSetting must contain "Password" and will be manipulated to "EncodedPassword" and "EncryptedPassword" respectively.
            E.g. "API.Password" will fill "API.EncodedPassword" and "API.EncryptedPassword"  (Case sensitive).
            </summary>
            <param name="PasswordAppSetting">AppSettings key for the Plain-text Password.</param>
            <returns>Password in PlainText, or NULL.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetPassword(System.String,System.String)">
            <summary>
            Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
            The PasswordAppSetting must contain "Password" and will be manipulated to "EncodedPassword" and "EncryptedPassword" respectively.
            E.g. "API.Password" will fill "API.EncodedPassword" and "API.EncryptedPassword"  (Case sensitive).
            </summary>
            <param name="PasswordAppSetting">AppSettings key for the Plain-text Password.</param>
            <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
            <returns>Password in PlainText, or NULL.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetPassword(System.String,System.String,System.String)">
            <summary>
            Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
            It's expected that only one of the three parameters will contain a value.  If all three are populated, the actual values will be taken from AppSettings with the given keys.
            E.g. AppSettings[Password]
            </summary>
            <param name="Password">Plain-text password OR AppSettings key.</param>
            <param name="EncodedPassword">Encode password OR AppSettings key.</param>
            <param name="EncryptedPassword">Encrypted password OR AppSettings key.</param>
            <returns>Password in PlainText, or NULL.</returns>
        </member>
        <member name="M:QuickLibs.Cryptography.Crypto.GetPassword(System.String,System.String,System.String,System.String)">
            <summary>
            Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
            It's expected that only one of the three parameters will contain a value.  If all three are populated, the actual values will be taken from AppSettings with the given keys.
            E.g. AppSettings[Password]
            </summary>
            <param name="Password">Plain-text password OR AppSettings key.</param>
            <param name="EncodedPassword">Encode password OR AppSettings key.</param>
            <param name="EncryptedPassword">Encrypted password OR AppSettings key.</param>
            <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
            <returns>Password in PlainText, or NULL.</returns>
        </member>
        <member name="T:QuickLibs.Cryptography.CharTypes">
            <summary>
            Character Types.
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Numerals">
            <summary>
            "0123456789"
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.UCase">
            <summary>
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.LCase">
            <summary>
            "abcdefghijklmnopqrstuvwxyz"
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Symbols">
            <summary>
            "!#$%(),.-;@[]_{}£~"
            </summary>
        </member>
        <!-- Badly formed XML comment ignored for member "F:QuickLibs.Cryptography.CharTypes.Specials" -->
        <member name="F:QuickLibs.Cryptography.CharTypes.Exotics">
            <summary>
            "§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"  (Includes white space)
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Hex">
            <summary>
            Limits the UpperCase and LowerCase to their first 6 characters.
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.UCaseHex">
            <summary>
            =Numerals | UCase | Hex
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.LCaseHex">
            <summary>
            =Numerals | LCase | Hex
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.AlphaNumeric">
            <summary>
            =Numerals | UCase | LCase
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Symbolic">
            <summary>
            =Symbols | Specials
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Obscure">
            <summary>
            =Numerals | UCase | LCase | Symbols (Easily typed characters)
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.Complex">
            <summary>
            =Numerals | UCase | LCase | Symbols | Specials
            </summary>
        </member>
        <member name="F:QuickLibs.Cryptography.CharTypes.ALL">
            <summary>
            All ASCII characters.
            </summary>
        </member>
    </members>
</doc>
