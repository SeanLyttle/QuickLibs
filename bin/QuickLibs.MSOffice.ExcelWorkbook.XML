<?xml version="1.0"?>
<doc>
    <assembly>
        <name>QuickLibs.MSOffice.ExcelWorkbook</name>
    </assembly>
    <members>
        <member name="T:QuickLibs.MSOffice.ExcelWorkbook">
            <summary>
            Generates a MS Office Excel (XLSX) file from a JSON definition.
            </summary>
        </member>
        <member name="F:QuickLibs.MSOffice.ExcelWorkbook.xlWorkbook">
            <summary>
            Reference to the inner ClosedXML.XLWorkbook.
            </summary>
        </member>
        <member name="F:QuickLibs.MSOffice.ExcelWorkbook.Log">
            <summary>
            Messages generated during generation of this Excel workbook will be logged to the consolie by default and not recorded.
            Use this interface to enable, disable and read from the loging output (e.g. Log.OutputLogging, Log.RecordLogging, Log.LOG).
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor">
            <summary>
            Creates an empty Excel Workbook.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(QuickLibs.Javascript.JSON)">
            <summary>
            Creates an Excel Workbook based on the given JSON WorkbookDefinition.
            </summary>
            <param name="WorkbookDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.Data.DataSet)">
            <summary>
            Creates an Excel Workbook based on the given DataSet.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.Data.DataTable)">
            <summary>
            Creates an Excel Workbook based on the given DataTable.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.String,System.Boolean)">
            <summary>
            Creates an Excel Workbook and loads the contents of the given Excel document.
            </summary>
            <param name="filePath">Location of the Excel spreadsheet.</param>
            <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.String,System.String,System.Boolean)">
            <summary>
            Creates and Excel Workbook and loads the contents of the given password protected Excel document.
            </summary>
            <param name="filePath">Location of the password protected Excel spreadsheet.</param>
            <param name="password">Plaintext password to decrypt the file.</param>
            <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.IO.FileStream)">
            <summary>
            Creates an Excel Workbook and loads the contents of the given Excel document.
            </summary>
            <param name="fileStream">The File Stream of an Excel file.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.#ctor(System.IO.FileStream,System.String)">
            <summary>
            Creates and Excel Workbook and loads the contents of the given password protected Excel document.
            </summary>
            <param name="fileStream">The File Stream of a password protected Excel file.</param>
            <param name="password">Plaintext password to decrypt the file.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Finalize">
            <summary>
            Destructor.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Save">
            <summary>
            Saves changes to the current Excel file.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Save(System.String)">
            <summary>
            Saves the current Excel file to the given filepath.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Save(System.IO.Stream)">
            <summary>
            Saves the current Excel file to the given fileStream.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Load(System.String,System.Boolean)">
            <summary>
            Loads an Excel file from the given filepath.
            </summary>
            <param name="filePath">Location of the Excel spreadsheet.</param>
            <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Load(System.String,System.String,System.Boolean)">
            <summary>
            Loads an Excel file from the given filepath.
            </summary>
            <param name="filePath">Location of the password protected Excel file.</param>
            <param name="password">Plaintext password to decrypt the file.</param>
            <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Load(System.IO.FileStream)">
            <summary>
            Loads an Excel file from the given file stream.
            </summary>
            <param name="fileStream">The File Stream of an Excel file.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.Load(System.IO.FileStream,System.String)">
            <summary>
            Loads an Excel file from the given file stream.
            </summary>
            <param name="fileStream">The File Stream of an Excel file.</param>
            <param name="password">Plaintext password to decrypt the file.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.ToMemoryStream">
            <summary>
            Returns the current XL file as a MemoryStream.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.GenerateXLSX(QuickLibs.Javascript.JSON)">
            <summary>
                Generates an XLSX file from the given JSON Workbook definition OR JSON DataSet or DataTable.
            </summary>
            <param name="WorkbookDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.AddSheet(QuickLibs.Javascript.JSON)">
            <summary>
            Generates a new sheet based on the given JSON Sheet definition.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.AddTable(QuickLibs.Javascript.JSON)">
            <summary>
            Generates a new sheet based on the given JSON data table. Use the JSON(DataTable dataTable) constructor to generate a JSON data table.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.PopulateCells(ClosedXML.Excel.IXLCell,System.String)">
            <summary>
            Inserts data into a worksheet, starting at the specified cell, from a string ('\t' will move right to the next cell, '\n' will move down to the next row).
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.PopulateCells(ClosedXML.Excel.IXLRange,System.String)">
            <summary>
            Inserts the same data into each cell in the given range ('\n' and '\t' will still change cells which will cause some data to be overridden!)
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.PopulateCells(ClosedXML.Excel.IXLCell,QuickLibs.Javascript.JSON,System.Int32,System.String)">
            <summary>
            Inserts data into a worksheet, starting at the specified cell, from a JSON table.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.ApplyRangeFormatting(QuickLibs.Javascript.JSON,ClosedXML.Excel.IXLStyle)">
            <summary>
            Applys formatting to the given cell range based on the given JSON definition.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.FixRelativeFormula(System.String,System.Int32,ClosedXML.Excel.IXLWorksheet)">
            <summary>
            Finds relative cell references in the given formula and shifts them down, based on previous entries.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.BorderStyleFromString(System.String)">
            <summary>
            Returns a valid border style based on the given string. (Default: None)
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.IsColor(System.String)">
            <summary>
            Returns true if the given string can be matched againsta a color.
            </summary>
        </member>
        <member name="M:QuickLibs.MSOffice.ExcelWorkbook.GetColor(System.String)">
            <summary>
            Returns a Color based on the given string ("Color Tint": where Color = any color string and positive Tint = brightness%  and negative Tint = darkness%)
            </summary>
        </member>
    </members>
</doc>
