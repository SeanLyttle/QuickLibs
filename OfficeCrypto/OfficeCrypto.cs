﻿/******************************************************************************* 
 *  _                      _     _ _ _         
 * | |   _   _  __ _ _   _(_) __| (_) |_ _   _ 
 * | |  | | | |/ _` | | | | |/ _` | | __| | | |
 * | |__| |_| | (_| | |_| | | (_| | | |_| |_| |
 * |_____\__, |\__, |\__,_|_|\__,_|_|\__|\__, |
 *       |___/    |_|                    |___/ 
 * 
 *  Decrypytion library for Office Open XML files
 *  API Version: 2008-12-12
 *  Generated: Wed Dec 12 12:33:00 GMT 2008 
 * 
 * ***************************************************************************** 
 *  Copyright Lyquidity Solutions Limited 2008
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with permission LGPL2 license. 
 *  You may obtain a copy of the License at: 
 *  
 *  http://creativecommons.org/licenses/by-sa/3.0/
 *
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 * Algorithms in this code file are based on the MS-OFFCRYPT.PDF provided by
 * Microsoft as part of its Open Specification Promise (OSP) program and which 
 * is available here:
 * 
 * http://msdn.microsoft.com/en-us/library/cc313071.aspx
 * 
 */

/*
 * Modified by Danilo Mirkovic, Oct 2009
 * - works with NPOI (for OLE Compound File access)
 * - added a few methods for convenience (e.g. EncryptToFile, EncryptToStream)
 * - addapted by Sean Lyttle to include Agile Encryption. (see https://consolidata.atlassian.net/wiki/spaces/KNB/pages/214106113/Decrypting+password+protected+Office+Documents for reference links)
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO.Packaging;
using System.IO;
using System.Xml;
using System.Security.Cryptography;
using System.Linq;

namespace QuickLibs.MSOffice.Crypto
{
    /// <summary>
    /// This class tries to implement the algorithms documents in MS-OFFCRYPTO 2.3.4.7-2.3.4.9.
    /// Usage: OfficeCryptoTest.OfficeCrypto.OfficePasswordHash();
    /// 
    /// It is intended to be used with .NET 3.0 or later and it is assumes the WindowsBase 
    /// assembly is referenced by the project to include the System.IO.Packaging namespace.
    /// </summary>
    /// <remarks>
    /// -------------------------------------------------------------------------
    ///
    /// This is the content of the EncryptionInfo stream created by Excel 2007
    /// when saving a password protected xlsb file.  The password used is:
    /// "password"
    ///
    /// 00000000 03 00 02 00                                                         Version
    /// 00000000             24 00 00 00                                             Flags (fCryptoAPI + fAES)
    /// 00000000                         A4 00 00 00                                 Header length
    /// 00000000                                     24 00 00 00                     Flags (again)
    /// 00000010 00 00 00 00                                                         Size extra
    /// 00000010             0E 66 00 00                                             Alg ID 0x0000660E = 128-bit AES,0x0000660F  192-bit AES, 0x00006610  256-bit AES
    /// 00000010                         04 80 00 00                                 Alg hash ID 0x00008004 SHA1
    /// 00000010                                     80 00 00 00                     Key size AES = 0x00000080, 0x000000C0, 0x00000100  128, 192 or 256-bit 
    /// 00000020 18 00 00 00                                                         Provider type 0x00000018 AES
    /// 00000020             A0 C7 DC 02 00 00 00 00                                 Reserved
    /// 00000020                                     4D 00 69 00             M?i?    CSP Name
    /// 00000030 63 00 72 00 6F 00 73 00 6F 00 66 00 74 00 20 00 c?r?o?s?o?f?t? ?
    /// 00000040 45 00 6E 00 68 00 61 00 6E 00 63 00 65 00 64 00 E?n?h?a?n?c?e?d?
    /// 00000050 20 00 52 00 53 00 41 00 20 00 61 00 6E 00 64 00  ?R?S?A? ?a?n?d?
    /// 00000060 20 00 41 00 45 00 53 00 20 00 43 00 72 00 79 00  ?A?E?S? ?C?r?y?
    /// 00000070 70 00 74 00 6F 00 67 00 72 00 61 00 70 00 68 00 p?t?o?g?r?a?p?h?
    /// 00000080 69 00 63 00 20 00 50 00 72 00 6F 00 76 00 69 00 i?c? ?P?r?o?v?i?
    /// 00000090 64 00 65 00 72 00 20 00 28 00 50 00 72 00 6F 00 d?e?r? ?(?P?r?o?
    /// 000000A0 74 00 6F 00 74 00 79 00 70 00 65 00 29 00 00 00 t?o?t?y?p?e?)
    /// 
    /// 000000B0 10 00 00 00                                                         Key size
    /// 000000B0             90 AC 68 0E 76 F9 43 2B 8D 13 B7 1D                     Salt
    /// 000000C0 B7 C0 FC 0D                                                 
    /// 000000C0             43 8B 34 B2 C6 0A A1 E1 0C 40 81 CE                     Encrypted verifier
    /// 000000D0 83 78 F4 7A                                    
    /// 000000D0             14 00 00 00                                             Hash length
    /// 000000D0                         48 BF F0 D6 C1 54 5C 40                     EncryptedVerifierHash
    /// 000000E0 FE 7D 59 0F 8A D7 10 B4 C5 60 F7 73 99 2F 3C 8F 
    /// 000000F0 2C F5 6F AB 3E FB 0A D5                        
    ///
    /// -------------------------------------------------------------------------
    /// </remarks>
    public class OfficeCrypto
    {
        #region The entry points

        /// <summary>
        /// Static test function exposed by the class
        /// </summary>
        public static Package OfficePasswordHash(string filename, string password)
        {
            OfficeCrypto officeCrypto = new OfficeCrypto();

            try
            {
                Package package = officeCrypto.DecryptToPackage(filename, password);
                //Console.WriteLine("Package decrypted and opened");
                return package;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }

            return null;
        }

        public static bool EncryptPackageFile(string filename, string password, out byte[] encryptionInfo, out byte[] encryptedPackage)
        {
            OfficeCrypto officeCrypto = new OfficeCrypto();

            encryptionInfo = null;
            encryptedPackage = null;

            try
            {
                //TODO: Replace //Console.WriteLine (not // Console.WriteLine) with Log.Info(), throughout;
                officeCrypto.EncryptPackage(filename, password, out encryptionInfo, out encryptedPackage);
                //Console.WriteLine("Package encrypted");
                officeCrypto.TestEncrytion(password, encryptionInfo, encryptedPackage);
                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                throw ex;
            }

            return false;
        }

        #endregion

        #region Everything else

        #region Prolog

        #region Enumerations

        [Flags]
        enum EncryptionFlags
        {
            None        = 0,
            Reserved1    = 1,    // MUST be 0, and MUST be ignored.
            Reserved2    = 2,    // MUST be 0, and MUST be ignored.
            fCryptoAPI    = 4,    // A flag that specifies whether CryptoAPI RC4 or [ECMA-376] encryption is used. MUST be 1 unless fExternal is 1. If fExternal is 1, MUST be 0. 
            fDocProps    = 8,    // MUST be 0 if document properties are encrypted. Encryption of document properties is specified in section 2.3.5.4. 
            fExternal    = 16,    // If extensible encryption is used, MUST be 1. If this field is 1, all other fields in this structure MUST be 0. 
            fAES        = 32,    // If the protected content is an [ECMA-376] document, MUST be 1. If the fAES bit is 1, the fCryptoAPI bit MUST also be 1.
            fAgile      = 64
        }

        enum AlgId
        {
            ByFlags    = 0x00,
            RC4        = 0x00006801,
            AES128    = 0x0000660E,
            AES192    = 0x0000660F, 
            AES256    = 0x00006610
        }

        enum AlgHashId
        {
            Any        = 0x00,
            RC4        = 0x00008000,
            SHA1    = 0x00008004,
            SHA512  = 0x00008008
        }

        enum ProviderType
        {
            Any = 0x00000000,
            RC4 = 0x00000001,
            AES = 0x00000018
        }

        #endregion

        #region Constants

        internal const string csEncryptionInfoStreamName = "EncryptionInfo";
        internal const string csEncryptedPackageStreamName = "EncryptedPackage";

        internal const string csNotStorage = "The file is not an OLE Storage document.";
        internal const string csNoEntry = "The file does not contain an entry called {0}.";
        internal const string csNoStream = "The file does not contain a stream called {0}.";
        internal const string csNotStream = "The entry in the file called {0} is not a stream.";
        internal const string csPasswordInvalid = "The password is not valid";
        internal const string csPasswordVerificationFailed = "Password verification failed";
        internal const string csPasswordVerificationSuceeded = "Password verification suceeded";

        // These block keys are defined in the specification document
        static byte[] encryptedVerifierHashInputBlockKey = new byte[8] { 0xfe, 0xa7, 0xd2, 0x76, 0x3b, 0x4b, 0x9e, 0x79 };
        static byte[] encryptedVerifierHashValueBlockKey = new byte[8] { 0xd7, 0xaa, 0x0f, 0x6d, 0x30, 0x61, 0x34, 0x4e };
        static byte[] encryptedKeyValueBlockKey = new byte[8] { 0x14, 0x6e, 0x0b, 0xe7, 0xab, 0xac, 0xd0, 0xd6 };
        static byte[] encryptedDataIntegritySaltBlockKey = new byte[] { 0x5f, 0xb2, 0xad, 0x01, 0x0c, 0xb9, 0xe1, 0xf6 };
        static byte[] encryptedDataIntegrityHmacValueBlocKkey = new byte[] { 0xa0, 0x67, 0x7f, 0x02, 0xb2, 0x2c, 0x84, 0x33 };

        #endregion

        #region Class variables

        ushort versionMajor = 0;
        ushort versionMinor = 0;
        EncryptionFlags encryptionFlags = EncryptionFlags.fCryptoAPI | EncryptionFlags.fAES;

        // Encryption header
        uint            sizeExtra     = 0;
        // Standard Encryption
        AlgId           algId         = AlgId.AES128;
        AlgHashId       algHashId     = AlgHashId.SHA1;
        int             keySize       = 0x80; // AES = 0x00000080, 0x000000C0, 0x00000100  128, 192 or 256-bit 
        ProviderType    providerType  = ProviderType.AES;
        string          CSPName       = "";
        //Agile Encryption
        int             spinCount     = 50000; // Default to the Standard spin count


        // Encryption verifier
        int    saltSize              = 0x10;  // Default
        byte[] salt                  = null;
        byte[] encryptedVerifier     = null;
        int    verifierHashSize      = 0x14;
        byte[] encryptedVerifierHash = null;
               
        byte[] encryptedPackage      = null;

        public bool UseCryptoAPI     = false;
        public bool VerifyIntegrity  = false;
        public bool VerifyPassword   = false;

        #endregion

        #region Constructor

        public OfficeCrypto()
        {
            // Check the SHA1 and AES functions work
            // if (!TestSHA1()) return;
            // if (!TestAES()) return;

            // byte[] input = new byte[0x10];
            // byte[] result = AESCryptoAPIEncrypt(input, new byte[0x10]);
            // Console.WriteLine(result.Length);
        }

        #endregion

        #endregion

        #region To package

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The name of the storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public Package DecryptToPackage(string filename, string password)
        {
            return CreatePackage(DecryptToArray(filename, password));
        }

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">A stream of a storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public Package DecryptToPackage(byte[] contents, string password)
        {
            return CreatePackage(DecryptToArray(contents, password));
        }

        /// <summary>
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public Package DecryptToPackage(OleStorage stgRoot, string password)
        {
            return CreatePackage(DecryptToArray(stgRoot, password));
        }

        #endregion

        #region To MemoryStream

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The name of the storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.MemoryStream instance</returns>
        public MemoryStream DecryptToStream(string filename, string password)
        {
            return CreateStream(DecryptToArray(filename, password));
        }

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">A stream of a storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.MemoryStream instance</returns>
        public MemoryStream DecryptToStream(byte[] contents, string password)
        {
            return CreateStream(DecryptToArray(contents, password));
        }

        /// <summary>
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.MemoryStream instance</returns>
        public MemoryStream DecryptToStream(OleStorage stgRoot, string password)
        {
            return CreateStream(DecryptToArray(stgRoot, password));
        }

        #endregion

        #region To Byte array

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The name of the storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public byte[] DecryptToArray(string filename, string password)
        {
            //Console.WriteLine("Open the storage");            
            OleStorage stgRoot = new OleStorage(filename);

            return DecryptToArray(stgRoot, password);
        }

        /// <summary>
        /// Validates and opens the storage containing the encrypted package.
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="filename">The name of the storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public byte[] DecryptToArray(byte[] contents, string password)
        {
            //Console.WriteLine("Open the storage");
            OleStorage stgRoot = new OleStorage(contents);

            return DecryptToArray(stgRoot, password);
        }

        /// <summary>
        /// Reads the encryption information and encrypted package
        /// Parses the encryption information
        /// Generates a decrypytion key and validates it against the password
        /// Decrypts the encrypted package and creates and Packaging.Package.
        /// </summary>
        /// <param name="stgRoot">The storage file containing the encrypted package</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <returns>System.IO.Packaging.Package instance</returns>
        public byte[] DecryptToArray(OleStorage stgRoot, string password)
        {
            // TODO: check if the EncryptedPackage exists (if it's a valid doc)
            encryptedPackage = stgRoot.ReadStream(csEncryptedPackageStreamName);
            
            // TODO: check if the EncryptionInfo exists (if it's a valid doc)
            byte[] encryptionInfo = stgRoot.ReadStream(csEncryptionInfoStreamName);

            // Delegate the rest to this common function
            return DecryptInternalAgile(password, encryptionInfo, encryptedPackage);
        }

        #endregion


        #region Encrypt to File

        /// <summary>
        /// Encryptes the package to a file, using the given password. 
        /// </summary>
        /// <param name="packageContents">Plaintext contents of the package.</param>
        /// <param name="password">Password to use to encrypt.</param>
        /// <param name="encryptedFilename">Name of the encrypted file to save</param>
        public void EncryptToFile(byte[] packageContents, string password, string encryptedFilename)
        {
            byte[] encryptionInfo;
            byte[] encryptedPackage;
            EncryptPackage(packageContents, password, out encryptionInfo, out encryptedPackage);

            OleStorage storage = new OleStorage();
            storage.WriteStream(csEncryptionInfoStreamName, encryptionInfo);
            storage.WriteStream(csEncryptedPackageStreamName, encryptedPackage);
            storage.Save(encryptedFilename);
        }

        /// <summary>
        /// Encryptes the package to a stream, using the given password. 
        /// </summary>
        /// <param name="packageContents">Plaintext contents of the package.</param>
        /// <param name="password">Password to use to encrypt.</param>
        /// <param name="encryptedFilename">Name of the encrypted stream write to</param>
        public void EncryptToStream(byte[] packageContents, string password, Stream encryptedStream)
        {
            byte[] encryptionInfo;
            byte[] encryptedPackage;
            EncryptPackage(packageContents, password, out encryptionInfo, out encryptedPackage);

            OleStorage storage = new OleStorage();
            storage.WriteStream(csEncryptionInfoStreamName, encryptionInfo);
            storage.WriteStream(csEncryptedPackageStreamName, encryptedPackage);
            storage.Save(encryptedStream);
        }


        #endregion

        #region Public methods

        /// <summary>
        /// Encrypts a package (zip) file using a supplied password and returns 
        /// an array to create an encryption information stream and a byte array 
        /// of the encrypted package.
        /// </summary>
        /// <param name="filename">The package (zip) file to be encrypted</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <param name="encryptionInfo">An array of bytes containing the encrption info</param>
        /// <param name="encryptedPackage">The encrpyted package</param>
        /// <returns></returns>
        public void EncryptPackage(string filename, string password, out byte[] encryptionInfo, out byte[] encryptedPackage)
        {
            if (!File.Exists(filename)) throw new ArgumentException("Package file does not exist");

            // Grab the package contents and encrypt
            byte[] packageContents = null;
            using(System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
            {
                packageContents = new byte[fs.Length];
                fs.Read(packageContents, 0, packageContents.Length);
            }

            EncryptPackage(packageContents, password, out encryptionInfo, out encryptedPackage);
        }

        /// <summary>
        /// Encrypts a package (zip) file using a supplied password and returns 
        /// an array to create an encryption information stream and a byte array 
        /// of the encrypted package.
        /// </summary>
        /// <param name="packageContents">The package (zip) file to be encrypted</param>
        /// <param name="password">The password to decrypt the package</param>
        /// <param name="encryptionInfo">An array of bytes containing the encrption info</param>
        /// <param name="encryptedPackage">The encrpyted package</param>
        /// <returns></returns>
        public void EncryptPackage(byte[] packageContents, string password, out byte[] encryptionInfo, out byte[] encryptedPackage)
        {
            versionMajor = 3;
            versionMinor = 2;

            // Console.WriteLine(password);

            // this.algId = AlgId.AES256;
            // this.keySize = 0x100;

            // Generate a salt
            System.Security.Cryptography.RijndaelManaged aes = new System.Security.Cryptography.RijndaelManaged();
            saltSize = 0x10;
            byte[] tempSalt = SHA1Hash(aes.IV);
            aes = null;
            this.verifierHashSize = tempSalt.Length;

            salt = new byte[saltSize];
            Array.Copy(tempSalt, salt, saltSize);
            
            // Generate a key from salt and password
            byte[] key = GeneratePasswordHashUsingSHA1(password);

            CreateVerifier(key);

            int originalLength = packageContents.Length;

            // Pad the array to the nearest 16 byte boundary
            int remainder = packageContents.Length % 0x10;
            if (remainder != 0)
            {
                byte[] tempContents = new byte[packageContents.Length + 0x10 - remainder];
                Array.Copy(packageContents, tempContents, packageContents.Length);
                packageContents = tempContents;
            }

            byte[] encryptionResult = AESEncrypt(packageContents, key);

            // Need to prepend the original package size as a Int64 (8 byte) field
            encryptedPackage = new byte[encryptionResult.Length +  8];
            // MUST record the original length here
            Array.Copy(BitConverter.GetBytes((long)originalLength), encryptedPackage, 8);  
            Array.Copy(encryptionResult, 0, encryptedPackage, 8, encryptionResult.Length);

            byte[] encryptionHeader = null;

            // Generate the encryption header structure
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                System.IO.BinaryWriter br = new System.IO.BinaryWriter(ms);
                br.Write((int)this.encryptionFlags);
                br.Write((int)this.sizeExtra);
                br.Write((int)this.algId);
                br.Write((int)this.algHashId);
                br.Write((int)this.keySize);
                br.Write((int)this.providerType);
                br.Write(new byte[] { 0xA0, 0xC7, 0xDC, 0x02, 0x00, 0x00, 0x00, 0x00 } );
                br.Write(System.Text.UnicodeEncoding.Unicode.GetBytes("Microsoft Enhanced RSA and AES Cryptographic Provider (Prototype)\0"));

                ms.Flush();
                encryptionHeader = ms.ToArray();
            }

            byte[] encryptionVerifier = null;

            // Generate the encryption header structure
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                System.IO.BinaryWriter br = new System.IO.BinaryWriter(ms);
                br.Write((int)salt.Length);
                br.Write(this.salt);
                br.Write(this.encryptedVerifier);
                br.Write(this.verifierHashSize); // Hash length
                br.Write(this.encryptedVerifierHash);

                ms.Flush();
                encryptionVerifier = ms.ToArray();
            }

            // Now generate the encryption info structure
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                System.IO.BinaryWriter br = new System.IO.BinaryWriter(ms);
                br.Write(versionMajor);
                br.Write(versionMinor);
                br.Write((int)this.encryptionFlags);
                br.Write((int)encryptionHeader.Length);
                br.Write(encryptionHeader);
                br.Write(encryptionVerifier);

                ms.Flush();
                encryptionInfo = ms.ToArray();
            }

            // Console.WriteLine( Lyquidity.OLEStorage.Utilities.FormatData(encryptionInfo, 128, 0) );
        }

        public void TestEncrytion(string password, byte[] encryptionInfo, byte[] encryptedPackage)
        {
            byte[] array = DecryptInternalAgile(password, encryptionInfo, encryptedPackage);
        }

        #endregion

        #region Private functions

        #region 2.3.4.7 & 2.3.4.9 algorithm implementation methods

        private void CreateVerifier(byte[] key)
        {
            // Much of the commmentary in this function is taken from 2.3.3
            // The EncryptionVerifier structure MUST be set using the following process: 

            // 1)    Random data are generated and written into the Salt field. 
            // 2)    The encryption key is derived from the password and salt, as specified in section 
            //        2.3.4.7 or 2.3.5.2, with block number 0.

            //        This is passed in as parameter key

            // 3)    Generate 16 bytes of additional random data as the Verifier.

            System.Security.Cryptography.RijndaelManaged aes = new System.Security.Cryptography.RijndaelManaged();
            byte[] verifier = aes.IV;
            aes = null;

            // Console.WriteLine("Verifier");
            // Console.WriteLine( Lyquidity.OLEStorage.Utilities.FormatData(verifier, 32, 0) );
            // Console.WriteLine();

            // 4)    Results of step 3 are encrypted and written into the EncryptedVerifier field.

            encryptedVerifier = AESEncrypt( verifier, key);

            // Console.WriteLine("encryptedVerifier");
            // Console.WriteLine( Lyquidity.OLEStorage.Utilities.FormatData(encryptedVerifier, 32, 0) );
            // Console.WriteLine();

            // 5)    For the hashing algorithm chosen, obtain the size of the hash data and write this value 
            //        into the VerifierHashSize field.
 
            // Not applicable right now

            // 6)    Obtain the hashing algorithm output using an input of data generated in step 3. 

            byte[] verifierHash = SHA1Hash( verifier );
            // Console.WriteLine("verifierHash");
            // Console.WriteLine( Lyquidity.OLEStorage.Utilities.FormatData(verifierHash, 32, 0) );
            // Console.WriteLine();

            // 7)    Encrypt the hashing algorithm output from step 6 using the encryption algorithm 
            //        chosen, and write the output into the EncryptedVerifierHash field.

            // First pad to 32 bytes
            byte[] tempHash = new byte[0x20];
            Array.Copy(verifierHash, tempHash, verifierHash.Length);
            verifierHash = tempHash;

            encryptedVerifierHash = AESEncrypt( verifierHash, key );

            // Console.WriteLine("encryptedVerifierHash");
            // Console.WriteLine( Lyquidity.OLEStorage.Utilities.FormatData(encryptedVerifierHash, 32, 0) );
            // Console.WriteLine();
        }

        /// <summary>
        /// Implements the password verifier process
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True if the password is a match</returns>
        private bool PasswordVerifier(byte[] key)
        {
            // Decrypt the encrypted verifier...
            byte[] decryptedVerifier = AESDecrypt( encryptedVerifier, key);

            // Truncate
            byte[] data = new byte[16];
            Array.Copy(decryptedVerifier, data, data.Length);
            decryptedVerifier = data;

            // ... and hash
            byte[] decryptedVerifierHash = AESDecrypt( encryptedVerifierHash, key );

            // Hash the decrypted verifier (2.3.4.9)
            byte[] checkHash = SHA1Hash(decryptedVerifier);

            // Check the 
            for (int i = 0; i < checkHash.Length; i++)
            {
                if (decryptedVerifierHash[i] != checkHash[i])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Implements (tries to) the hash key generation algorithm in section 2.3.4.7
        /// The 
        /// </summary>
        /// <param name="password">The password used to decode the stream</param>
        /// <returns>The derived encryption key byte array</returns>
        private byte[] GeneratePasswordHashUsingSHA1(string password)
        {
            byte[] hashBuf = null;

            try
            {
                // H(0) = H(salt, password);
                hashBuf = SHA1Hash(salt, password);

                for (int i = 0; i < 50000; i++)
                {
                    // Generate each hash in turn
                    // H(n) = H(i, H(n-1))
                    hashBuf = SHA1Hash(i, hashBuf);
                }

                // Finally, append "block" (0) to H(n)
                hashBuf = SHA1Hash(hashBuf, 0);

                // The algorithm in this 'DeriveKey' function is the bit that's not clear from the documentation
                byte[] key = DeriveKey(hashBuf);

                // Should handle the case of longer key lengths as shown in 2.3.4.9
                // Grab the key length bytes of the final hash as the encrypytion key
                byte[] final = new byte[keySize/8];
                Array.Copy(key, final, final.Length);

                return final;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        #endregion

        #region Standard Encryption
        internal byte[] DecryptInternalStandard(string password, byte[] encryptionInfo, byte[] encryptedPackage)
        {
            #region Parse the encryption info data

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(encryptionInfo))
            {
                System.IO.BinaryReader reader = new System.IO.BinaryReader(ms);

                versionMajor = reader.ReadUInt16();
                versionMinor = reader.ReadUInt16();

                encryptionFlags = (EncryptionFlags)reader.ReadUInt32();
                if (encryptionFlags == EncryptionFlags.fExternal)
                    throw new Exception("An external cryptographic provider is not supported");

                //string test = ASCIIEncoding.ASCII.GetString(reader.ReadBytes((int)reader.BaseStream.Length - (int)reader.BaseStream.Position));

                // Encryption header
                uint headerLength        = reader.ReadUInt32(); 
                int skipFlags            = reader.ReadInt32(); headerLength -= 4;
                sizeExtra                = reader.ReadUInt32(); headerLength -= 4;
                algId                    = (AlgId)reader.ReadUInt32(); headerLength -= 4;
                algHashId                = (AlgHashId)reader.ReadUInt32(); headerLength -= 4;
                keySize                    = reader.ReadInt32(); headerLength -= 4;
                providerType            = (ProviderType)reader.ReadUInt32(); headerLength -= 4;
                                          reader.ReadUInt32(); headerLength -= 4; // Reserved 1
                                          reader.ReadUInt32(); headerLength -= 4; // Reserved 2
                CSPName                    = System.Text.UnicodeEncoding.Unicode.GetString(reader.ReadBytes((int)headerLength));

                // Encryption verifier
                saltSize                = reader.ReadInt32(); 
                salt                    = reader.ReadBytes(saltSize);
                encryptedVerifier        = reader.ReadBytes(0x10);
                verifierHashSize        = reader.ReadInt32(); 
                encryptedVerifierHash    = reader.ReadBytes(providerType == ProviderType.RC4 ? 0x14 : 0x20);
            }

            #endregion

            #region Encryption key generation

            //Console.WriteLine("Encryption key generation");
            byte[] encryptionKey = GeneratePasswordHashUsingSHA1(password);
            if (encryptionKey == null) return null;

            #endregion

            #region Password verification

            //Console.WriteLine("Password verification");
            if (PasswordVerifier(encryptionKey))
            {
                //Console.WriteLine("Password verification succeeded");
            } 
            else
            {
                //Console.WriteLine("Password verification failed");
                throw new InvalidPasswordException("The password is not valid");
            }

            #endregion

            #region Decrypt

            // First 8 bytes hold the size of the stream
            long length = BitConverter.ToInt64(encryptedPackage, 0);

            // Decrypt the stream using the generated and validated key
            //Console.WriteLine("Decrypt the stream using the generated and validated key");
            encryptedPackage = AESDecrypt(encryptedPackage, 8, encryptedPackage.Length-8, encryptionKey);

            // !! IMPORTANT !! Make sure the final array is the correct size
            // Failure to do this will cause an error when the decrypted stream
            // is opened by the System.IO.Packaging.Package.Open() method.

            byte[] result = encryptedPackage;

            if (encryptedPackage.Length > length)
            {
                result = new byte[length];
                Array.Copy(encryptedPackage, result, result.Length);
            }

            //using (System.IO.FileStream fs = new System.IO.FileStream(@"c:\x.zip", System.IO.FileMode.Create))
            //{
            //    fs.Write(result, 0, result.Length);
            //}

            return result;

            #endregion

        }

        private System.IO.MemoryStream CreateStream(byte[] decryptedPackage)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            ms.Write(decryptedPackage, 0, decryptedPackage.Length);
            ms.Flush();
            ms.Position = 0;

            return ms;
        }

        private Package CreatePackage(byte[] decryptedPackage)
        {
            using (MemoryStream ms = CreateStream(decryptedPackage))
            {
                return Package.Open(ms, FileMode.Open, FileAccess.ReadWrite);
            }

            // What happens if we later try writing to a closed stream?
        }

        #region SHA1/AES test functions

        /// <summary>
        /// This method tests that the AES implementation works as it should given
        /// a key and some data to encrypt/decrypt.
        /// </summary>
        /// <returns>
        /// True if the test confirms the AES implementation generations 
        /// the expected values
        /// </returns>
        private bool TestAES()
        {
            // These test ciphers are available in this Wikipedia article:
            // http://en.wikipedia.org/wiki/Advanced_Encryption_Standard#C.23_.2F.NET
            byte[] key = { 0x00, 0x01, 0x02, 0x03, 0x05, 0x06, 0x07, 0x08, 0x0a, 0x0b, 0x0c, 0x0d, 0x0f, 0x10, 0x11, 0x12 };
            byte[] data = { 0x50, 0x68, 0x12, 0xa4, 0x5f, 0x08, 0xc8, 0x89, 0xb9, 0x7f, 0x59, 0x80, 0x03, 0x8b, 0x83, 0x59 };
            byte[] expected = { 0xD8, 0xF5, 0x32, 0x53, 0x82, 0x89, 0xEF, 0x7D, 0x06, 0xB5, 0x06, 0xA4, 0xFD, 0x5B, 0xE9, 0xC9 };

            byte[] result = AESEncrypt(data, key);

            //if (result.Length != expected.Length) return false;

            //for (int i = 0; i < result.Length; i++)
            //{
            //    if (result[i] != expected[i]) return false;
            //}

            result = AESDecrypt(result, key);
            //if (result.Length != data.Length) return false;

            //for (int i = 0; i < result.Length; i++)
            //{
            //    if (result[i] != data[i]) return false;
            //}

            return true;
        }

        /// <summary>
        /// Tests the SHA1 implementation to confirm it generates the expected
        /// hash when given a known string as input.
        /// </summary>
        /// <returns></returns>
        private bool TestSHA1()
        {
            // This example text and resulting SHA1 hash is from the Wikipedia article:
            // http://en.wikipedia.org/wiki/SHA-1#Example_hashes
            string test = "The quick brown fox jumps over the lazy dog";
            byte[] expected = { 0x2f, 0xd4, 0xe1, 0xc6,
                                0x7a, 0x2d, 0x28, 0xfc,
                                0xed, 0x84, 0x9e, 0xe1,
                                0xbb, 0x76, 0xe7, 0x39,
                                0x1b, 0x93, 0xeb, 0x12 };

            byte[] result = SHA1Hash(System.Text.ASCIIEncoding.ASCII.GetBytes(test));

            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] != expected[i])
                    return false;
            }

            return true;
        }

        #endregion

        #region Derive key

        private byte[] DeriveKey(byte[] hashValue)
        {
            // And one more hash to derive the key
            byte[] derivedKey = new byte[64];

            // This is step 4a in 2.3.4.7 of MS_OFFCRYPT version 1.0
            // and is required even though the notes say it should be 
            // used only when the encryption algorithm key > hash length.
            for (int i = 0; i < derivedKey.Length; i++)
                derivedKey[i] = (byte)(i < hashValue.Length ? 0x36 ^ hashValue[i] : 0x36);

            byte[] X1 = SHA1Hash(derivedKey);

            if (verifierHashSize > keySize / 8)
                return X1;

            for (int i = 0; i < derivedKey.Length; i++)
                derivedKey[i] = (byte)(i < hashValue.Length ? 0x5C ^ hashValue[i] : 0x5C);

            byte[] X2 = SHA1Hash(derivedKey);

            byte[] X3 = new byte[X1.Length + X2.Length];

            Array.Copy(X1, 0, X3, 0, X1.Length);
            Array.Copy(X1, 0, X3, X1.Length, X2.Length);

            return X3;
        }

        #endregion

        #region SHA1 functions

        private byte[] SHA1Hash(byte[] salt, string password)
        {
            return SHA1Hash(HashPassword(salt, password));
        }

        private byte[] HashPassword(byte[] salt, string password)
        {
            // Use a unicode form of the password
            byte[] passwordBuf = System.Text.UnicodeEncoding.Unicode.GetBytes(password);
            byte[] inputBuf = new byte[salt.Length + passwordBuf.Length];
            Array.Copy(salt, inputBuf, salt.Length);
            Array.Copy(passwordBuf, 0, inputBuf, salt.Length, passwordBuf.Length);

            return inputBuf;
        }

        private byte[] SHA1Hash(int iterator, byte[] hashBuf)
        {
            // Create an input buffer for the hash.  This will be 4 bytes larger than 
            // the hash to accommodate the unsigned int iterator value.
            byte[] inputBuf = new byte[0x14 + 0x04];

            // Create a byte array of the integer and put at the front of the input buffer
            // 1.3.6 says that little-endian byte ordering is expected

            // Copy the iterator bytes into the hash input buffer
            Array.Copy(System.BitConverter.GetBytes(iterator), inputBuf, 4);

            // 'append' the previously generated hash to the input buffer
            Array.Copy(hashBuf, 0, inputBuf, 4, hashBuf.Length);

            return SHA1Hash(inputBuf);
        }

        private byte[] SHA1Hash(byte[] hashBuf, int block)
        {
            // Create an input buffer for the hash.  This will be 4 bytes larger than 
            // the hash to accommodate the unsigned int iterator value.
            byte[] inputBuf = new byte[0x14 + 0x04];

            Array.Copy(hashBuf, inputBuf, hashBuf.Length);
            Array.Copy(System.BitConverter.GetBytes(block), 0, inputBuf, hashBuf.Length, 4);

            return SHA1Hash(inputBuf);
        }

        private byte[] SHA1Hash(byte[] hashBuf, byte[] block0)
        {
            // Create an input buffer for the hash.  This will be 4 bytes larger than 
            // the hash to accommodate the unsigned int iterator value.
            byte[] inputBuf = new byte[hashBuf.Length + block0.Length];

            Array.Copy(hashBuf, inputBuf, hashBuf.Length);
            Array.Copy(block0, 0, inputBuf, hashBuf.Length, block0.Length);

            return SHA1Hash(inputBuf);
        }

        private byte[] SHA1Hash(byte[] inputBuffer)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1CryptoServiceProvider.Create();
            return sha1.ComputeHash(inputBuffer);
        }

        #endregion

        #region AES functions

        private byte[] AESDecrypt(byte[] data, byte[] key)
        {
            return AESDecrypt(data, 0, data.Length, key);
        }

        private byte[] AESDecrypt(byte[] data, int index, int count, byte[] key)
        {
            byte[] decryptedBytes = null;

            //  Create uninitialized Rijndael encryption object.
            System.Security.Cryptography.RijndaelManaged symmetricKey = new System.Security.Cryptography.RijndaelManaged();

            // It is required that the encryption mode is Electronic Codebook (ECB) 
            // see MS-OFFCRYPTO v1.0 2.3.4.7 pp 39.
            symmetricKey.Mode = System.Security.Cryptography.CipherMode.ECB;
            symmetricKey.Padding = System.Security.Cryptography.PaddingMode.None;
            symmetricKey.KeySize = keySize;
            // symmetricKey.IV = null; // new byte[16];
            // symmetricKey.Key = key;

            //  Generate decryptor from the existing key bytes and initialization 
            //  vector. Key size will be defined based on the number of the key 
            //  bytes.
            System.Security.Cryptography.ICryptoTransform decryptor;
            decryptor = symmetricKey.CreateDecryptor(key, null);

            //  Define memory stream which will be used to hold encrypted data.
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(data, index, count))
            {
                //  Define memory stream which will be used to hold encrypted data.
                using (System.Security.Cryptography.CryptoStream cryptoStream
                        = new System.Security.Cryptography.CryptoStream(memoryStream, decryptor, System.Security.Cryptography.CryptoStreamMode.Read))
                {
                    //  Since at this point we don't know what the size of decrypted data
                    //  will be, allocate the buffer long enough to hold ciphertext;
                    //  plaintext is never longer than ciphertext.
                    decryptedBytes = new byte[data.Length];
                    int decryptedByteCount = cryptoStream.Read(decryptedBytes, 0, decryptedBytes.Length);

                    return decryptedBytes;
                }
            }
        }

        private byte[] AESEncrypt(byte[] data, byte[] key)
        {
            byte[] cipherTextBytes = null;

            //  Create uninitialized Rijndael encryption object.
            System.Security.Cryptography.RijndaelManaged symmetricKey = new System.Security.Cryptography.RijndaelManaged();

            // It is required that the encryption mode is Electronic Codebook (ECB) 
            // see MS-OFFCRYPTO v1.0 2.3.4.7 pp 39.
            symmetricKey.Mode = System.Security.Cryptography.CipherMode.ECB;
            symmetricKey.Padding = System.Security.Cryptography.PaddingMode.None; // System.Security.Cryptography.PaddingMode.None;
            symmetricKey.KeySize = this.keySize;
            // symmetricKey.Key = key;
            // symmetricKey.IV = new byte[16];

            // Generate encryptor from the existing key bytes and initialization vector. 
            // Key size will be defined based on the number of the key bytes.
            System.Security.Cryptography.ICryptoTransform encryptor = symmetricKey.CreateEncryptor(key, null);

            //  Define memory stream which will be used to hold encrypted data.
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                //  Define cryptographic stream (always use Write mode for encryption).
                using (System.Security.Cryptography.CryptoStream cryptoStream
                        = new System.Security.Cryptography.CryptoStream(memoryStream, encryptor, System.Security.Cryptography.CryptoStreamMode.Write))
                {
                    //  Start encrypting.
                    cryptoStream.Write(data, 0, data.Length);

                    //  Finish encrypting.
                    cryptoStream.FlushFinalBlock();
                }

                //  Convert our encrypted data from a memory stream into a byte array.
                cipherTextBytes = memoryStream.ToArray();
                return cipherTextBytes;
            }
        }

        #endregion

        #endregion

        #region Agile Encryption

        internal byte[] DecryptInternalAgile(string password, byte[] encryptionInfo, byte[] encryptedPackage)
        {
            #region Parse the encryption info data

            XmlDocument XmlEncryptionDescriptor = new XmlDocument();
            EncryptionFlags encryptionFlags;

            using (MemoryStream ms = new MemoryStream(encryptionInfo))
            {
                BinaryReader reader = new BinaryReader(ms);

                versionMajor = reader.ReadUInt16();
                versionMinor = reader.ReadUInt16();
                encryptionFlags = (EncryptionFlags)reader.ReadUInt32();

                if (versionMajor != 0x04 && versionMinor != 0x04)
                {
                    throw new Exception("This document is encrypted using an encryption method not covered in this example");
                }

                XmlEncryptionDescriptor.LoadXml(Encoding.ASCII.GetString(reader.ReadBytes((int)reader.BaseStream.Length - (int)reader.BaseStream.Position)));
            }

            //If NOT Agile encryption try standard encryption instead.
            if (encryptionFlags != EncryptionFlags.fAgile)
                return DecryptInternalStandard(password, encryptionInfo, encryptedPackage);

            // Only a few bits of information are really needed
            string kdSaltValue = "";
            int kdBlockSize = 16;
            int kdHashSize = 20;

            int pkeBlockSize = 16;
            int pkeKeyBits = 128;

            string pkeEncryptedKeyValue = "";
            string pkeCipherAlgorithm = "AES";
            string pkeCipherChaining = "ChainingModeCBC";
            string pkeHashAlgorithm = "SHA1";
            string pkeSaltValue = "";
            string kdHashAlgorithm = "SHA1";

            string encryptedDataIntegritySaltValue = "";
            string encryptedDataIntegrityHashValue = "";
            string encryptedVerifierHashValue = "";
            string encryptedVerifierHashInput = "";
            int pkeHashSize = 20;

            try
            {

                XmlElement KeyData = (XmlElement)XmlEncryptionDescriptor.GetElementsByTagName("keyData")[0];

                int kdSaltSize = KeyData.Attributes["saltSize"].ToInt(0);
                kdBlockSize = KeyData.Attributes["blockSize"].ToInt(0);
                int kdKeyBits = KeyData.Attributes["keyBits"].ToInt(0);
                kdHashSize = KeyData.Attributes["hashSize"].ToInt(0);

                string kdCipherAlgorithm = KeyData.Attributes["cipherAlgorithm"].ValueOrDefault("AES");
                string kdCipherChaining = KeyData.Attributes["cipherChaining"].ValueOrDefault("ChainingModeCBC");
                kdHashAlgorithm = KeyData.Attributes["hashAlgorithm"].ValueOrDefault("SHA1");
                kdSaltValue = KeyData.Attributes["saltValue"].ValueOrDefault("");

                XmlElement DataIntegrity = (XmlElement)XmlEncryptionDescriptor.GetElementsByTagName("dataIntegrity")[0];

                string encryptedHmacKey = DataIntegrity.Attributes["encryptedHmacKey"].ValueOrDefault("");
                string encryptedHmacValue = DataIntegrity.Attributes["encryptedHmacValue"].ValueOrDefault("");

                encryptedDataIntegritySaltValue = DataIntegrity.Attributes["encryptedSaltValue"].ValueOrDefault("");
                encryptedDataIntegrityHashValue = DataIntegrity.Attributes["encryptedHashValue"].ValueOrDefault("");

                encryptedDataIntegritySaltValue = encryptedHmacKey;
                encryptedDataIntegrityHashValue = encryptedHmacValue;

                XmlElement EncryptedKey = (XmlElement)XmlEncryptionDescriptor.GetElementsByTagName("keyEncryptor")[0].FirstChild;

                this.spinCount = EncryptedKey.Attributes["spinCount"].ToInt(0);
                int pkeSaltSize = EncryptedKey.Attributes["saltSize"].ToInt(0);
                pkeBlockSize = EncryptedKey.Attributes["blockSize"].ToInt(0);
                pkeKeyBits = EncryptedKey.Attributes["keyBits"].ToInt(0);
                pkeHashSize = EncryptedKey.Attributes["hashSize"].ToInt(0);

                pkeCipherAlgorithm = EncryptedKey.Attributes["cipherAlgorithm"].ValueOrDefault("AES");
                pkeCipherChaining = EncryptedKey.Attributes["cipherChaining"].ValueOrDefault("ChainingModeCBC");
                pkeHashAlgorithm = EncryptedKey.Attributes["hashAlgorithm"].ValueOrDefault("SHA1");
                pkeSaltValue = EncryptedKey.Attributes["saltValue"].ValueOrDefault("");

                encryptedVerifierHashInput = EncryptedKey.Attributes["encryptedVerifierHashInput"].ValueOrDefault("");
                encryptedVerifierHashValue = EncryptedKey.Attributes["encryptedVerifierHashValue"].ValueOrDefault("");
                pkeEncryptedKeyValue = EncryptedKey.Attributes["encryptedKeyValue"].ValueOrDefault("");

            }
            catch (Exception ex)
            {
                //Console.WriteLine(string.Format("An error occured parsing the EncryptionInfo.  The problem is: {0}", ex.Message));
                throw ex;
            }

            #endregion

            try
            {
                #region Create the hash and symmetric algorithm

                HashAlgorithm hashAlg = null;

                switch (pkeHashAlgorithm)
                {
                    case CryptoAPI_SHA1.HashAlgorithmName:
                        hashAlg = UseCryptoAPI ? CryptoAPI_SHA1.Create() : HashAlgorithm.Create(pkeHashAlgorithm);
                        break;

                    default:
                        hashAlg = HashAlgorithm.Create(pkeHashAlgorithm);
                        break;
                }

                byte[] pkeSaltBytes = Convert.FromBase64String(pkeSaltValue);

                SymmetricAlgorithm symmetricKey = null;

                switch (pkeCipherAlgorithm)
                {
                    case "RC2":
                        symmetricKey = new RC2CryptoServiceProvider();
                        break;

                    case "RC4":
                        throw new Exception("RC2 must not be used");

                    case "DES":
                        symmetricKey = new DESCryptoServiceProvider();
                        break;

                    case "DESX":
                        throw new Exception("DESX is not a supported algorithm");

                    case "3DES":
                        symmetricKey = new TripleDESCryptoServiceProvider();
                        break;

                    case "3DES_112":
                        symmetricKey = new TripleDESCryptoServiceProvider();
                        break;

                    case "AES":
                    default:
                        symmetricKey = new AesCryptoServiceProvider();
                        break;

                }

                if (symmetricKey == null) throw new Exception(string.Format("An symmetic key algorithm of {0} could not be created.", pkeCipherAlgorithm));

                switch (pkeCipherChaining)
                {
                    case "ChainingModeCFB":
                        symmetricKey.Mode = CipherMode.CFB;
                        break;

                    case "ChainingModeCBC":
                    default:
                        symmetricKey.Mode = CipherMode.CBC;
                        break;
                }

                symmetricKey.BlockSize = pkeBlockSize << 3;
                symmetricKey.KeySize = pkeKeyBits;
                symmetricKey.Padding = PaddingMode.Zeros;

                #endregion

                #region Now do the work

                var decryptedByteCount = 0;
                var stmLength = (int)BitConverter.ToUInt64(encryptedPackage, 0);
                var keyDataSaltBytes = Convert.FromBase64String(kdSaltValue);

                #region Verifier

                /*

				These are the words from 2.3.4.13 encryptedVerifierHashValue and the code show how to implement
				the opposite (decryption) of these.

				2) Generate an encryption key as specified in section 2.3.4.11, using the user-supplied password, 
				the binary byte array used to create the saltvalue attribute, and a block_key byte array
				consisting of the following bytes 0xd7, 0xaa, 0x0f, 0x6d, 0x30, 0x61, 0x34, 0x4e.

				3) Encrypt the hash value obtained in step 1 using the binary form of the saltValue attribute as
				an initialization vector as specified in section 2.3.4.12. If hashSize is not an integral multiple
				of blockSize bytes, pad the hash value with 0x00 to an integral multiple of blockSize bytes.

				4) Base64 encode the result of step 3.

				*/
                if (VerifyPassword)
                {
                    byte[] encryptedVerifierHashInputBytes = Convert.FromBase64String(encryptedVerifierHashInput);
                    var verifierInputKey = GenerateAgileEncryptionKey(hashAlg, pkeSaltBytes, System.Text.UnicodeEncoding.Unicode.GetBytes(password), encryptedVerifierHashInputBlockKey, pkeKeyBits >> 3);
                    var decryptedVerifierHashInputBytes = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, verifierInputKey, pkeSaltBytes, encryptedVerifierHashInputBytes, out decryptedByteCount);

                    var hash = hashAlg.ComputeHash(decryptedVerifierHashInputBytes);

                    byte[] encryptedVerifierHashBytes = Convert.FromBase64String(encryptedVerifierHashValue);
                    var verifierHashKey = GenerateAgileEncryptionKey(hashAlg, pkeSaltBytes, System.Text.UnicodeEncoding.Unicode.GetBytes(password), encryptedVerifierHashValueBlockKey, pkeKeyBits >> 3);
                    var decryptedVerifierHashBytes = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, verifierHashKey, pkeSaltBytes, encryptedVerifierHashBytes, out decryptedByteCount);

                    bool passwordVerificationMatch = decryptedVerifierHashBytes.Take(pkeHashSize).Select((b, i) => hashAlg.ComputeHash(decryptedVerifierHashInputBytes)[i] == b).All(b => b);
                    if (!passwordVerificationMatch)
                    {
                        //Console.WriteLine(csPasswordVerificationFailed);
                        throw new InvalidPasswordException(csPasswordInvalid);
                    }

                    //Console.WriteLine(csPasswordVerificationSuceeded);
                }

                #endregion

                #region Generate encryption key (2.3.4.11)

                var hashBuf = GenerateAgileEncryptionKey(hashAlg, pkeSaltBytes, System.Text.UnicodeEncoding.Unicode.GetBytes(password), encryptedKeyValueBlockKey, pkeKeyBits >> 3);
                var decryptedKeyValue = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, hashBuf, pkeSaltBytes, Convert.FromBase64String(pkeEncryptedKeyValue), out decryptedByteCount);

                #endregion

                #region Data Integrity (2.3.4.14)

                if (VerifyIntegrity)
                {
                    var encryptedDataIntegritySaltBytes = Convert.FromBase64String(encryptedDataIntegritySaltValue);
                    var encryptedDataIntegrityHashBytes = Convert.FromBase64String(encryptedDataIntegrityHashValue);

                    // Decrypt the salt value
                    var ivDataIntegritySalt = HashAppend(hashAlg, keyDataSaltBytes, encryptedDataIntegritySaltBlockKey);
                    CorrectHashSize(ref ivDataIntegritySalt, kdBlockSize, 0x00);
                    var decryptedDataIntegritySaltValue = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, decryptedKeyValue, ivDataIntegritySalt, encryptedDataIntegritySaltBytes, out decryptedByteCount);

                    // Use the salt to hash the encrypted document
                    KeyedHashAlgorithm keyDataHashAlg = KeyedHashAlgorithm.Create("HMAC" + kdHashAlgorithm);
                    keyDataHashAlg.Key = decryptedDataIntegritySaltValue.Take(kdHashSize).ToArray();
                    var dataIntegrityHash = keyDataHashAlg.ComputeHash(encryptedPackage);

                    // Decrypt the hash generated by the encryptor
                    var ivDataIntegrityHash = HashAppend(hashAlg, keyDataSaltBytes, encryptedDataIntegrityHmacValueBlocKkey);
                    CorrectHashSize(ref ivDataIntegrityHash, kdBlockSize, 0x00);
                    var decryptedDataIntegrityHmacValue = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, decryptedKeyValue, ivDataIntegrityHash, encryptedDataIntegrityHashBytes, out decryptedByteCount);

                    // dataIntegrityHash should equal the decryptedDataIntegrityHmacValue
                    bool dataIntegrityMatch = decryptedDataIntegrityHmacValue.Take(kdHashSize).Select((b, i) => dataIntegrityHash[i] == b).All(b => b);
                    if (!dataIntegrityMatch)
                    {
                        //Console.WriteLine("Encrypted package is invalid");
                        return null;
                    }

                    //Console.WriteLine("Encrypted package is valid");
                }

                #endregion

                #region Decrypt

                // 2.3.4.15 Decrypt the document
                var packageBytes = encryptedPackage.Skip(8).Window(4096)
                    .Select((segment, index) =>
                    {
                        // Create an IV. The technique is documented here 2.3.4.10.
                        var iv = HashAppend(hashAlg, keyDataSaltBytes, BitConverter.GetBytes((int)index));
                        CorrectHashSize(ref iv, kdBlockSize, 0x36);

                        var decrypted = DecryptUsingSymmetricKeyAlgorithm(symmetricKey, decryptedKeyValue, iv, segment, out decryptedByteCount);

                        return decrypted;
                    }).SelectMany(segment => segment).Take(stmLength).ToArray();

                #endregion

                return packageBytes;

                #endregion

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                throw ex;
            }
        }


		byte[] GenerateAgileEncryptionKey(HashAlgorithm hashAlg, byte[] salt, byte[] password, byte[] blockKey, int hashSize)
		{
			// H(0) = H(salt, password);
			var hashBuf = HashAppend(hashAlg, salt, password);
			var hashBufWorking = new byte[hashBuf.Length+4];

			for (int i = 0; i < this.spinCount; i++)
			{
				// Generate each hash in turn
				// H(n) = H(i, H(n-1))
				// hashBuf = HashAppend(hashAlg, System.BitConverter.GetBytes(i), hashBuf);
				hashBuf = hashAlg.ComputeHash(System.BitConverter.GetBytes(i).Concat(hashBuf).ToArray());

				// Uncomment to use a static hash buffer
				//Array.Copy(System.BitConverter.GetBytes(i), hashBufWorking, 4);
				//Array.Copy(hashBuf, 0, hashBufWorking, 4, hashBuf.Length);

				//hashBuf = hashAlg.ComputeHash(hashBufWorking);

			}

			// Finally, append "block" (0) to H(n)
			hashBuf = HashAppend(hashAlg, hashBuf, blockKey);

			// Fix up the size per the spec
			CorrectHashSize(ref hashBuf, hashSize, 0x36);

			return hashBuf;
		}

		void CorrectHashSize(ref byte[] hashBuf, int size, byte padding)
		{
			// Fix up the size per the spec
			if (hashBuf.Length < size)
			{
				hashBuf = hashBuf.Concat(Enumerable.Repeat(padding, size - hashBuf.Length)).ToArray();
			}
			else if (hashBuf.Length > size)
			{
				hashBuf = hashBuf.Take(size).ToArray();
			}
		}

		#region Hash function

		/// <summary>
		/// Generates a hash using an arbitrary algorithm and appending the value to the hash
		/// </summary>
		/// <param name="alg">The algorithm to use (eg. SHA1)</param>
		/// <param name="size">The size of the hash buffer to create</param>
		/// <param name="hashBuf">The buffer to hash with</param>
		/// <param name="block">The iterator to hash with</param>
		/// <returns>A new hash buffer to use</returns>
		private byte[] HashAppend(HashAlgorithm alg, byte[] hashBuf, byte[] block)
		{
			// Create an input buffer for the hash.  This will be block.Length bytes larger than 
			// the hash to accommodate the unsigned int iterator value.
			return alg.ComputeHash(hashBuf.Concat(block).ToArray());
		}

		#endregion

		#region Cryptographic functions For agile

		private byte[] DecryptUsingSymmetricKeyAlgorithm(SymmetricAlgorithm symmetricKey, byte[] keyBytes, byte[] ivBytes, byte[] sourceBytes, out int decryptedByteCount)
		{
			//  Generate encryptor from the existing key bytes and initialization 
			//  vector. Key size will be defined based on the number of the key 
			//  bytes.
			symmetricKey.Padding = PaddingMode.Zeros;

			ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, ivBytes);
			byte[] decryptedBytes = null;

			//  Define memory stream which will be used to hold encrypted data.
			using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(sourceBytes, 0, sourceBytes.Length))
			{
				//  Define memory stream which will be used to hold encrypted data.
				using (System.Security.Cryptography.CryptoStream cryptoStream
						= new System.Security.Cryptography.CryptoStream(memoryStream, decryptor, System.Security.Cryptography.CryptoStreamMode.Read))
				{
					//  Since at this point we don't know what the size of decrypted data
					//  will be, allocate the buffer long enough to hold ciphertext;
					//  plaintext is never longer than ciphertext.
					decryptedBytes = new byte[sourceBytes.Length];
					decryptedByteCount = cryptoStream.Read(decryptedBytes, 0, decryptedBytes.Length);

					return decryptedBytes;
				}
			}
		}

        #endregion

        #endregion

        #endregion

        #endregion
    }

    #region Agile Extension methods

    public static class ExtensionMethods
    {
        public static string ValueOrDefault(this XmlAttribute attr, string defaultValue)
        {
            if (attr == null) return defaultValue;
            return attr.Value;
        }

        public static int ToInt(this XmlAttribute attr, int defaultValue)
        {
            if (attr == null) return defaultValue;
            int result = defaultValue;
            int.TryParse(attr.Value, out result);
            return result;
        }

    }

    /// <summary>
    /// More extension methods for enumerables
    /// </summary>
    public static class Chunk
    {
        /// <summary>
        /// Turns a stream into an IEnumerable<byte>
        /// </summary>
        /// <param name="stream">The stream to wrap</param>
        /// <returns>The stream as an enumeration</returns>
        public static IEnumerable<byte> ToEnumerable(this System.IO.Stream stream)
        {
            while (true)
            {
                var result = stream.ReadByte();
                if (result >= 0)
                {
                    yield return (byte)result;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Split an IEnumerable<T> into an IEnumerable<T[]>
        /// </summary>
        /// <typeparam name="T">Element type of the array</typeparam>
        /// <param name="source">The IEnumerable to operate on</param>
        /// <param name="chunk">The window size</param>
        /// <param name="skip">The number of elements to skip before chunking</param>
        /// <returns>The enumeration</returns>
        /// <remarks>
        /// For some reason using Skip() prevents the chunking mechanism.
        /// So the method allows the caller to skip n elements by updating
        /// the iterator
        /// </remarks>
        public static IEnumerable<T[]> Window<T>(this IEnumerable<T> source, int chunk)
        {
            var readonce = new Ratchet<T>(source);

            while (true)
            {
                var result = readonce.Take(chunk).ToArray();

                if (result.Any())
                {
                    yield return result;
                }
                else
                {
                    readonce.DisposeEnumerator();
                    break;
                }
            }
        }

        private class HideIEnumerator<T> : IEnumerator<T>
        {
            IEnumerator<T> ienumerator = null;

            public HideIEnumerator(IEnumerator<T> ienumerator)
            {
                this.ienumerator = ienumerator;
            }

            public T Current
            {
                get
                {
                    if (this.ienumerator == null) new NotImplementedException();
                    return this.ienumerator.Current;
                }
            }

            public void Dispose()
            {
                // Do nothing - that's the purpose of this implementation
            }

            internal void DisposeEnumerator()
            {
                this.ienumerator.Dispose();
            }

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    if (this.ienumerator == null) new NotImplementedException();
                    return this.ienumerator.Current;
                }
            }

            public bool MoveNext()
            {
                if (this.ienumerator == null) new NotImplementedException();
                return this.ienumerator.MoveNext();
            }

            public void Reset()
            {
                if (this.ienumerator == null) new NotImplementedException();
                this.ienumerator.Reset();
            }
        }

        private class Ratchet<T> : IEnumerable<T>
        {
            private readonly HideIEnumerator<T> ienumerator;

            public Ratchet(IEnumerable<T> source)
            {
                this.ienumerator = new HideIEnumerator<T>(source.GetEnumerator());
            }

            public IEnumerator<T> GetEnumerator()
            {
                return ienumerator;
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return ienumerator;
            }

            public void DisposeEnumerator()
            {
                if (this.ienumerator == null) return;
                this.ienumerator.DisposeEnumerator();
            }
        }
    }

    #endregion

    #region Crypto API

    public class ProvName
    {
        public const string MS_DEF_PROV = "Microsoft Base Cryptographic Provider v1.0";
        public const string MS_ENHANCED_PROV = "Microsoft Enhanced Cryptographic Provider v1.0";
        public const string MS_STRONG_PROV = "Microsoft Strong Cryptographic Provider";
        public const string MS_DEF_RSA_SIG_PROV = "Microsoft RSA Signature Cryptographic Provider";
        public const string MS_DEF_RSA_SCHANNEL_PROV = "Microsoft RSA SChannel Cryptographic Provider";
        public const string MS_DEF_DSS_PROV = "Microsoft Base DSS Cryptographic Provider";
        public const string MS_DEF_DSS_DH_PROV = "Microsoft Base DSS and Diffie-Hellman Cryptographic Provider";
        public const string MS_ENH_DSS_DH_PROV = "Microsoft Enhanced DSS and Diffie-Hellman Cryptographic Provider";
        public const string MS_DEF_DH_SCHANNEL_PROV = "Microsoft DH SChannel Cryptographic Provider";
        public const string MS_SCARD_PROV = "Microsoft Base Smart Card Crypto Provider";
        public const string MS_ENH_RSA_AES_PROV = "Microsoft Enhanced RSA and AES Cryptographic Provider";
    }

    public enum ProvType : uint
    {
        NONE = 0,
        RSA_FULL = 1,
        RSA_SIG = 2,
        DSS = 3,
        FORTEZZA = 4,
        MS_EXCHANGE = 5,
        SSL = 6,
        RSA_SCHANNEL = 12,
        DSS_DH = 13,
        EC_ECDSA_SIG = 14,
        EC_ECNRA_SIG = 15,
        EC_ECDSA_FULL = 16,
        EC_ECNRA_FULL = 17,
        DH_SCHANNEL = 18,
        SPYRUS_LYNKS = 20,
        RNG = 21,
        INTEL_SEC = 22,
        REPLACE_OWF = 23,
        RSA_AES = 24,
    }

    [Flags]
    public enum ContextFlag : uint
    {
        // dwFlags definitions for CryptAcquireContext
        NONE = 0,
        VERIFYCONTEXT = 0xF0000000,
        NEWKEYSET = 0x00000008,
        DELETEKEYSET = 0x00000010,
        MACHINE_KEYSET = 0x00000020,
        SILENT = 0x00000040,
    }

    public enum HashParam : uint
    {
        ALGID = 0x0001,  // Hash algorithm
        HASHVAL = 0x0002,  // Hash value
        HASHSIZE = 0x0004,  // Hash value size
        HMAC_INFO = 0x0005,  // information for creating an HMAC
        TLS1PRF_LABEL = 0x0006,  // label for TLS1 PRF
        TLS1PRF_SEED = 0x0007,  // seed for TLS1 PRF
    }

    public class AlgSid
    {
        // Generic sub-ids
        public const uint ANY = (0);
        // Some RSA sub-ids
        public const uint RSA_ANY = 0;
        public const uint RSA_PKCS = 1;
        public const uint RSA_MSATWORK = 2;
        public const uint RSA_ENTRUST = 3;
        public const uint RSA_PGP = 4;
        // Some DSS sub-ids
        public const uint DSS_ANY = 0;
        public const uint DSS_PKCS = 1;
        public const uint DSS_DMS = 2;
        // Block cipher sub ids
        // DES sub_ids
        public const uint DES = 1;
        public const uint TRIP_DES = 3;
        public const uint DESX = 4;
        public const uint IDEA = 5;
        public const uint CAST = 6;
        public const uint SAFERSK64 = 7;
        public const uint SAFERSK128 = 8;
        public const uint TRIP_DES_112 = 9;
        public const uint CYLINK_MEK = 12;
        public const uint RC5 = 13;
        public const uint AES_128 = 14;
        public const uint AES_192 = 15;
        public const uint AES_256 = 16;
        public const uint AES = 17;
        // Fortezza sub-ids
        public const uint SKIPJACK = 10;
        public const uint TEK = 11;
        // RC2 sub-ids
        public const uint RC2 = 2;
        // Stream cipher sub-ids
        public const uint RC4 = 1;
        public const uint SEAL = 2;
        // Diffie-Hellman sub-ids
        public const uint DH_SANDF = 1;
        public const uint DH_EPHEM = 2;
        public const uint AGREED_KEY_ANY = 3;
        public const uint KEA = 4;
        // Hash sub ids
        public const uint MD2 = 1;
        public const uint MD4 = 2;
        public const uint MD5 = 3;
        public const uint SHA = 4;
        public const uint SHA1 = 4;
        public const uint MAC = 5;
        public const uint RIPEMD = 6;
        public const uint RIPEMD160 = 7;
        public const uint SSL3SHAMD5 = 8;
        public const uint HMAC = 9;
        public const uint TLS1PRF = 10;
        public const uint HASH_REPLACE_OWF = 11;
        public const uint SHA_256 = 12;
        public const uint SHA_384 = 13;
        public const uint SHA_512 = 14;
        // secure channel sub ids
        public const uint SSL3_MASTER = 1;
        public const uint SCHANNEL_MASTER_HASH = 2;
        public const uint SCHANNEL_MAC_KEY = 3;
        public const uint PCT1_MASTER = 4;
        public const uint SSL2_MASTER = 5;
        public const uint TLS1_MASTER = 6;
        public const uint SCHANNEL_ENC_KEY = 7;
        // Our silly example sub-id
        public const uint EXAMPLE = 80;
    }

    public enum AlgClass : uint
    {
        // Algorithm classes
        ANY = (0),
        SIGNATURE = (1 << 13),
        MSG_ENCRYPT = (2 << 13),
        DATA_ENCRYPT = (3 << 13),
        HASH = (4 << 13),
        KEY_EXCHANGE = (5 << 13),
        ALL = (7 << 13),
    }

    public enum AlgType : uint
    {
        // Algorithm types
        ANY = (0),
        DSS = (1 << 9),
        RSA = (2 << 9),
        BLOCK = (3 << 9),
        STREAM = (4 << 9),
        DH = (5 << 9),
        SECURECHANNEL = (6 << 9),
    }

    public enum CalgHash : uint
    {
        MD2 = (AlgClass.HASH | AlgType.ANY | AlgSid.MD2),
        MD4 = (AlgClass.HASH | AlgType.ANY | AlgSid.MD4),
        MD5 = (AlgClass.HASH | AlgType.ANY | AlgSid.MD5), //32771
        SHA = (AlgClass.HASH | AlgType.ANY | AlgSid.SHA),
        SHA1 = (AlgClass.HASH | AlgType.ANY | AlgSid.SHA1),
        MAC = (AlgClass.HASH | AlgType.ANY | AlgSid.MAC),
        SSL3_SHAMD5 = (AlgClass.HASH | AlgType.ANY | AlgSid.SSL3SHAMD5),
        HMAC = (AlgClass.HASH | AlgType.ANY | AlgSid.HMAC),
        TLS1PRF = (AlgClass.HASH | AlgType.ANY | AlgSid.TLS1PRF),
        HASH_REPLACE_OWF = (AlgClass.HASH | AlgType.ANY | AlgSid.HASH_REPLACE_OWF),
        SHA_256 = (AlgClass.HASH | AlgType.ANY | AlgSid.SHA_256),
        SHA_384 = (AlgClass.HASH | AlgType.ANY | AlgSid.SHA_384),
        SHA_512 = (AlgClass.HASH | AlgType.ANY | AlgSid.SHA_512),
    }

    public class CryptoAPI
    {
        [DllImport("advapi32.dll", EntryPoint = "CryptAcquireContext")]
        public static extern bool CryptAcquireContext(out IntPtr hProv,
             string pszContainer, string pszProvider,
             uint dwProvType, uint dwFlags);

        [DllImport("advapi32.dll", EntryPoint = "CryptCreateHash")]
        public static extern bool CryptCreateHash(IntPtr hProv,
             uint Algid, IntPtr hKey, uint dwFlags, out IntPtr phHash);

        [DllImport("advapi32.dll", EntryPoint = "CryptDestroyHash")]
        public static extern bool CryptDestroyHash(IntPtr hHash);

        [DllImport("advapi32.dll", EntryPoint = "CryptHashData")]
        public static extern bool CryptHashData(IntPtr hHash,
             byte[] pbData, int dwDataLen, uint dwFlags);

        [DllImport("advapi32.dll", EntryPoint = "CryptGetHashParam", SetLastError = true)]
        public static extern bool CryptGetHashParam(IntPtr hHash,
             uint dwParam, byte[] pbData, ref uint pdwDataLen, uint dwFlags);

        [DllImport("advapi32.dll", EntryPoint = "CryptReleaseContext")]
        public static extern bool CryptReleaseContext(IntPtr hProv, uint dwFlags);
        public static IntPtr AcquireContext()
        {
            return AcquireContext("MD5Container", ProvName.MS_ENHANCED_PROV,
                ProvType.RSA_FULL, ContextFlag.NONE);
        }
        public static IntPtr AcquireContext(string container)
        {
            return AcquireContext(container, ProvName.MS_ENHANCED_PROV,
                ProvType.RSA_FULL, ContextFlag.NONE);
        }
        public static IntPtr AcquireContext(ProvType provType)
        {
            return AcquireContext(null, null, provType, ContextFlag.NONE);
        }
        public static IntPtr AcquireContext(string provName, ProvType provType)
        {
            return AcquireContext(null, provName, provType, ContextFlag.NONE);
        }
        public static IntPtr AcquireContext(string provName,
             ProvType provType, ContextFlag conFlag)
        {
            return AcquireContext(null, provName, provType, conFlag);
        }
        public static IntPtr AcquireContext(string conName,
             string provName, ProvType provType)
        {
            return AcquireContext(conName, provName, provType, ContextFlag.NONE);
        }
        public static IntPtr AcquireContext(string conName,
           string provName, ProvType provType, ContextFlag conFlag)
        {
            IntPtr hProv;
            bool retVal = CryptoAPI.CryptAcquireContext(out hProv, conName,
                provName, (uint)provType, (uint)conFlag);
            if (!retVal) //try creating a new key container
            {
                retVal = CryptoAPI.CryptAcquireContext(out hProv, conName, provName,
                   (uint)provType, (uint)ContextFlag.NEWKEYSET);
            }
            if (hProv == IntPtr.Zero)
                throw new Exception("System.Security.Cryptography");
            return hProv;
        }
    }

    #endregion

    #region CryptoAPI_SHA1

    public sealed class CryptoAPI_SHA1 : HashAlgorithm
    {
        public const string HashAlgorithmName = "SHA1";

        public CryptoAPI_SHA1()
        {
            HashSizeValue = 160;
            m_Prov = CryptoAPI.AcquireContext(ProvType.RSA_FULL);

            Initialize();
            m_Disposed = false;
        }

        public override void Initialize()
        {
            if (m_Disposed)
                throw new ObjectDisposedException(this.GetType().FullName);
            if (m_Hash != IntPtr.Zero)
            {
                CryptoAPI.CryptDestroyHash(m_Hash);
            }
            bool retVal = CryptoAPI.CryptCreateHash(m_Prov, (uint)CalgHash.SHA1, IntPtr.Zero, 0, out m_Hash);

        }

        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            if (m_Disposed)
                throw new ObjectDisposedException(this.GetType().FullName);
            byte[] copy = (byte[])array.Clone();
            bool retVal = false;
            retVal = CryptoAPI.CryptHashData(m_Hash, copy, copy.Length, 0);
        }

        protected override byte[] HashFinal()
        {
            if (m_Disposed)
                throw new ObjectDisposedException(this.GetType().FullName);
            byte[] data = new byte[0];
            uint dataLen = 0;
            uint flags = 0;
            //size
            bool retVal = CryptoAPI.CryptGetHashParam(m_Hash, (uint)HashParam.HASHVAL,
                    data, ref dataLen, flags);
            if (234 == System.Runtime.InteropServices.Marshal.GetLastWin32Error())//MORE_DATA = 234,
            {
                //data
                data = new byte[dataLen];
                retVal = CryptoAPI.CryptGetHashParam(m_Hash, (uint)HashParam.HASHVAL,
                       data, ref dataLen, flags);
            }
            return data;
        }

        protected override void Dispose(bool disposing)
        {
            if (!m_Disposed)
            {
                if (m_Hash != IntPtr.Zero)
                {
                    bool retVal = CryptoAPI.CryptDestroyHash(m_Hash);
                    m_Hash = IntPtr.Zero;
                }
                if (m_Prov != IntPtr.Zero)
                {
                    CryptoAPI.CryptReleaseContext(m_Prov, 0);
                    m_Prov = IntPtr.Zero;
                }
                try
                {
                    GC.SuppressFinalize(this);
                }
                catch { }
                m_Disposed = true;
            }
        }
        ~CryptoAPI_SHA1()
        {
            Clear();
        }
        private IntPtr m_Hash = IntPtr.Zero;
        private bool m_Disposed;
        private IntPtr m_Prov = IntPtr.Zero;

        // Create a new instance of the "SHA1" class.
        public new static CryptoAPI_SHA1 Create()
        {
            return (CryptoAPI_SHA1)(new CryptoAPI_SHA1());
        }
    }

    #endregion
}
