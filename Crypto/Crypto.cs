﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuickLibs.Cryptography
{
    /// <summary>
    /// A simple cryptographic library for secure RNG.
    /// </summary>
    public class Crypto
    {
        /// <summary>
        /// Encryption Key used by the Encrypt / Decrypt methods. Default: AppSettings["QuickLibs.Cryptography.EncryptionKey"] OR a SHA1 hash of AppDomain.CurrentDomain.FriendlyName if AppSetting is null.
        /// </summary>
        public static string EncryptionKey = ConfigurationManager.AppSettings["QuickLibs.Cryptography.EncryptionKey"] ?? GetHash(AppDomain.CurrentDomain.FriendlyName);

        /// <summary>
        /// LEGACY: Generates a SHA1 hash string from the given input and salt.
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <param name="salt">Salt to further obscure the hash.</param>
        /// <returns>SHA1 hash of salt + str.</returns>
        [Obsolete("This function is included for legacy purposes. SHA1 is not considered secure. Please use GetHash instead.", false)]
        public static string GetHashSHA1(string str, string salt)
        {
            SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
            sha.ComputeHash(Encoding.ASCII.GetBytes($"{salt}{str}"));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in sha.Hash)
                sb.Append(b.ToString("x2"));
            return sb.ToString().ToUpper();
        }

        /// <summary>
        ///   Generates a SHA512 hash string from the given input and salt.
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <param name="salt">Salt to further obscure the hash.</param>
        /// <returns>SHA512 hash of salt + str.</returns>
        public static string GetHashSHA512(string str, string salt)
        {
            SHA512CryptoServiceProvider sha = new SHA512CryptoServiceProvider();
            sha.ComputeHash(Encoding.ASCII.GetBytes($"{salt}{str}"));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in sha.Hash)
                sb.Append(b.ToString("x2"));
            return sb.ToString().ToUpper();
        }

        /// <summary>
        ///   Generates a hash-based message authentication code (HMAC) hash string from the given input.
        /// </summary>
        /// <param name="data">This is the data to be authenticated.</param>
        /// <param name="key">This is the secret key to authenticate the hash.</param>
        /// <returns>HMACSHA512 hash in Base64 encoding.</returns>
        public static string GetHashHMAC(string data, string key)
        {
            Encoding enc = Encoding.UTF8;
            HMACSHA512 hmac = new HMACSHA512(enc.GetBytes(key));
            hmac.ComputeHash(enc.GetBytes(data));

            return Convert.ToBase64String(hmac.Hash);
        }

        /// <summary>
        /// LEGACY: Generates a hash-based message authentication code (HMAC) hash string from the given input.
        /// </summary>
        /// <param name="data">This is the data to be authenticated.</param>
        /// <param name="key">This is the secret key to authenticate the hash.</param>
        /// <returns>HMACSHA256 hash in Base64 encoding.</returns>
        [Obsolete("This function is included for legacy purposes. If you do not require HMAC256, please use GetHashHMAC for the best available algorithm.", false)]
        public static string GetHashHMAC256(string data, string key)
        {
            Encoding enc = Encoding.UTF8;
            HMACSHA256 hmac = new HMACSHA256(enc.GetBytes(key));
            hmac.ComputeHash(enc.GetBytes(data));

            return Convert.ToBase64String(hmac.Hash);
        }

        /// <summary>
        ///   Generates a SHA512 hash string from the given input string with no salt. Shorthand for GetHashSHA512(str, "");
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <returns>SHA512 hash of str.</returns>
        public static string GetHash(string str) { return GetHashSHA512(str, ""); }

        /// <summary>
        ///   Generates a SHA512 hash string from the given input string with no salt. Shorthand for GetHashSHA512(str, "");
        /// </summary>
        /// <param name="str">String to hash</param>
        /// <param name="iterations">Number of times to repeat the hash</param>
        /// <returns>SHA512 hash of str.</returns>
        public static string GetHash(string str, string salt, int iterations = 1) {
            for (int i = 0; i < iterations; i++)
                str = GetHashSHA512(str,salt);
            return str;
        }

        /// <summary>
        ///   Generates a hash-based message authentication code (HMAC) hash string from the given input. Shorthand for GetHashHMAC(data, key);
        /// </summary>
        /// <param name="data">This is the data to be authenticated.</param>
        /// <param name="key">This is the secret key to authenticate the hash.</param>
        /// <returns>HMACSHA256 hash in Base64 encoding.</returns>
        public static string GetHash(string data, string key) { return GetHashHMAC(data, key); }
        

        /// <summary>
        ///     Encodes the given text as a Base64String and trims the trailing padding characters.
        /// </summary>
        public static string Encode(string plainText)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText)).Trim('=');
        }

        /// <summary>
        ///     Decodes the given text from a Base64String. Trailing padding characters are not required.
        /// </summary>
        public static string Decode(string encodedText)
        {
            if (encodedText.Length % 4 > 0)
                encodedText += new string('=', 4 - encodedText.Length % 4);
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedText));
        }


        /// <summary>
        /// Converts plain text into an encrypted string that can be decrypted again later.
        /// </summary>
        /// <param name="plainText">Original text to be encrypted.</param>
        public static string Encrypt(string plainText)
        {
                return Encrypt(plainText, EncryptionKey);
        }

        /// <summary>
        /// Converts plain text into an encrypted string that can be decrypted again later.
        /// </summary>
        /// <param name="plainText">Original text to be encrypted.</param>
        /// <param name="cipherKey">Default: EncryptionKey. The same key must be used to decrypt the encrypted text.</param>
        public static string Encrypt(string plainText, string cipherKey)
        {
            string cipherText = "";
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            byte[] saltStringBytes = RandomBytes(32);
            byte[] ivStringBytes = RandomBytes(32);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            using (Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(cipherKey ?? EncryptionKey, saltStringBytes, 1000))
            {
                var keyBytes = key.GetBytes(256 / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new System.IO.MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                cipherText = Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }

            return cipherText.Replace('=', '*');
        }


        /// <summary>
        /// Converts an encrypted cipher back into plain text using the default key stored in ConfigurationManager.AppSettings["EncryptionKey"].
        /// </summary>
        /// <param name="cipherText">Encrypted text.</param>
        public static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, EncryptionKey);
        }

        /// <summary>
        /// Converts an encrypted cipher back into plain text using the given key.
        /// </summary>
        /// <param name="cipherText">Encrypted text.</param>
        /// <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
        public static string Decrypt(string cipherText, string cipherKey)
        {
            string plainText = cipherText;
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            byte[] cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText.Replace('*', '='));
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            byte[] saltStringBytes = cipherTextBytesWithSaltAndIv.Take(256 / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            byte[] ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(256 / 8).Take(256 / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            byte[] cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((256 / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((256 / 8) * 2)).ToArray();

            try
            {
                using (Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(cipherKey ?? EncryptionKey, saltStringBytes, 1000))
                {
                    byte[] keyBytes = key.GetBytes(256 / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.BlockSize = 256;
                        symmetricKey.Mode = CipherMode.CBC;
                        symmetricKey.Padding = PaddingMode.PKCS7;
                        using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                        {
                            using (var memoryStream = new System.IO.MemoryStream(cipherTextBytes))
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                //throw new InvalidOperationException("Decryption failed: Invalid password.", exp);
                plainText = null;
            }

            return plainText;
        }

        private static byte[] RandomBytes(int bytes)
        {
            var randomBytes = new byte[bytes]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

        /// <summary>
        /// Returns a string with the given number of random AlphaNumeric characters.
        /// </summary>
        /// <param name="Length">Number of characters to return.</param>
        public static string GeneratePassword(int Length) { return GeneratePassword(Length, CharTypes.AlphaNumeric); }

        /// <summary>
        /// Returns a string with the given number of random characters from the given preset of CharTypes.
        /// </summary>
        /// <param name="Length">Number of characters to return.</param>
        /// <param name="Flags">Default: CharTypes.AlphaNumeric. A set of CharTypes that can be used in the generation. Separate multiple CharTypes with "|".</param>
        public static string GeneratePassword(int Length, CharTypes Flags)
        {
            string chars =
                ((Flags & (CharTypes.Numerals))  > 0 ? "0123456789" : "") +
                ((Flags & (CharTypes.UCase))     > 0 ? "ABCDEF" + ((Flags & CharTypes.Hex) == 0 ? "GHIJKLMNOPQRSTUVWXYZ" : "") : "") +
                ((Flags & (CharTypes.LCase))     > 0 ? "abcdef" + ((Flags & CharTypes.Hex) == 0 ? "ghijklmnopqrstuvwxyz" : "") : "") +
                ((Flags & (CharTypes.Symbols))   > 0 ? "!#$%(),.-;@[]_{}£~" : "") +
                ((Flags & (CharTypes.Specials))  > 0 ? "\"&+*':/<=>?\\^_`|¦€" : "") +
                ((Flags & (CharTypes.Exotics))   > 0 ? "§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ" : "");

            return GeneratePassword(Length, chars);
        }

        /// <summary>
        /// Returns a string with the given number of random characters from the given selection available.
        /// </summary>
        /// <param name="Length">Number of characters to return.</param>
        /// <param name="AvailableChars">Only characters that appear in this string will be used in the generation.</param>
        public static string GeneratePassword(int Length, string AvailableChars)
        {
            if (string.IsNullOrEmpty(AvailableChars))
                return null;

            StringBuilder sb = new StringBuilder(Length);
            byte[] rnd = RandomBytes(Length);
            int charIndex = 0;
            for (int i = 0; i < Length; i++)
            {
                charIndex = (charIndex + rnd[i]) % AvailableChars.Length;
                sb.Append(AvailableChars[charIndex]);
            }
            return sb.ToString();
        }


        /// <summary>
        /// Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
        /// The PasswordAppSetting must contain "Password" and will be manipulated to "EncodedPassword" and "EncryptedPassword" respectively.
        /// E.g. "API.Password" will fill "API.EncodedPassword" and "API.EncryptedPassword"  (Case sensitive).
        /// </summary>
        /// <param name="PasswordAppSetting">AppSettings key for the Plain-text Password.</param>
        /// <returns>Password in PlainText, or NULL.</returns>
        public static string GetPassword(string PasswordAppSetting)
        {
            if (!string.IsNullOrEmpty(PasswordAppSetting))
            {
                return GetPassword(
                    ConfigurationManager.AppSettings[PasswordAppSetting],
                    ConfigurationManager.AppSettings[PasswordAppSetting.Replace("Password", "EncodedPassword")],
                    ConfigurationManager.AppSettings[PasswordAppSetting.Replace("Password", "EncryptedPassword")]
                );
            }
            return null;
        }

        /// <summary>
        /// Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
        /// The PasswordAppSetting must contain "Password" and will be manipulated to "EncodedPassword" and "EncryptedPassword" respectively.
        /// E.g. "API.Password" will fill "API.EncodedPassword" and "API.EncryptedPassword"  (Case sensitive).
        /// </summary>
        /// <param name="PasswordAppSetting">AppSettings key for the Plain-text Password.</param>
        /// <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
        /// <returns>Password in PlainText, or NULL.</returns>
        public static string GetPassword(string PasswordAppSetting, string cipherKey)
        {
            if (!string.IsNullOrEmpty(PasswordAppSetting))
            {
                return GetPassword(
                    ConfigurationManager.AppSettings[PasswordAppSetting],
                    ConfigurationManager.AppSettings[PasswordAppSetting.Replace("Password", "EncodedPassword")],
                    ConfigurationManager.AppSettings[PasswordAppSetting.Replace("Password", "EncryptedPassword")],
                    cipherKey
                );
            }
            return null;
        }

        /// <summary>
        /// Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
        /// It's expected that only one of the three parameters will contain a value.  If all three are populated, the actual values will be taken from AppSettings with the given keys.
        /// E.g. AppSettings[Password]
        /// </summary>
        /// <param name="Password">Plain-text password OR AppSettings key.</param>
        /// <param name="EncodedPassword">Encode password OR AppSettings key.</param>
        /// <param name="EncryptedPassword">Encrypted password OR AppSettings key.</param>
        /// <returns>Password in PlainText, or NULL.</returns>
        public static string GetPassword(string Password, string EncodedPassword, string EncryptedPassword)
        {
            return GetPassword(Password, EncodedPassword, EncryptedPassword, null);
        }

        /// <summary>
        /// Returns a Plain Text password from the first non-NULL parameter. If EncodedPassword populated, it will be decoded first.  If EncryptedPassword is populated it will be decrypted first.
        /// It's expected that only one of the three parameters will contain a value.  If all three are populated, the actual values will be taken from AppSettings with the given keys.
        /// E.g. AppSettings[Password]
        /// </summary>
        /// <param name="Password">Plain-text password OR AppSettings key.</param>
        /// <param name="EncodedPassword">Encode password OR AppSettings key.</param>
        /// <param name="EncryptedPassword">Encrypted password OR AppSettings key.</param>
        /// <param name="cipherKey">Default: EncryptionKey. This key must match the key used to encrypt the text.</param>
        /// <returns>Password in PlainText, or NULL.</returns>
        public static string GetPassword(string Password, string EncodedPassword, string EncryptedPassword, string cipherKey)
        {
            if (!string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(EncodedPassword) && !string.IsNullOrEmpty(EncryptedPassword))
            {
                Password = ConfigurationManager.AppSettings[Password];
                EncodedPassword = ConfigurationManager.AppSettings[EncodedPassword];
                EncryptedPassword = ConfigurationManager.AppSettings[EncryptedPassword];
            }

            if (!string.IsNullOrEmpty(Password))
                return Password;

            if (!string.IsNullOrEmpty(EncodedPassword))
                return Decode(EncodedPassword);

            if (!string.IsNullOrEmpty(EncryptedPassword))
                return Decrypt(EncryptedPassword, cipherKey);

            return null;
        }
    }

    /// <summary>
    /// Character Types.
    /// </summary>
    [Flags]
    public enum CharTypes
    {
        /// <summary>
        /// "0123456789"
        /// </summary>
        Numerals = 0x01,
        /// <summary>
        /// "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        /// </summary>
        UCase = 0x02,
        /// <summary>
        /// "abcdefghijklmnopqrstuvwxyz"
        /// </summary>
        LCase = 0x04,
        /// <summary>
        /// "!#$%(),.-;@[]_{}£~"
        /// </summary>
        Symbols = 0x08,
        /// <summary>
        /// "\"&+*':/$lt;=&gt;?\\^_`|¦€" : ""
        /// </summary>
        Specials = 0x10,
        /// <summary>
        /// "§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"  (Includes white space)
        /// </summary>
        Exotics = 0x20,
        /// <summary>
        /// Limits the UpperCase and LowerCase to their first 6 characters.
        /// </summary>
        Hex = 0x80,

        /// <summary>
        /// =Numerals | UCase | Hex
        /// </summary>
        UCaseHex = 0x83,
        /// <summary>
        /// =Numerals | LCase | Hex
        /// </summary>
        LCaseHex = 0x85,
        /// <summary>
        /// =Numerals | UCase | LCase
        /// </summary>
        AlphaNumeric = 0x07,
        /// <summary>
        /// =Symbols | Specials
        /// </summary>
        Symbolic = 0x18,
        /// <summary>
        /// =Numerals | UCase | LCase | Symbols (Easily typed characters)
        /// </summary>
        Obscure = 0x0F,
        /// <summary>
        /// =Numerals | UCase | LCase | Symbols | Specials
        /// </summary>
        Complex = 0x1F,
        /// <summary>
        /// All ASCII characters.
        /// </summary>
        ALL = 0x7F
    }
}
