﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Data;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using System.Xml;
using System.ComponentModel;
using System.Linq;

namespace QuickLibs.Javascript
{
    /// <summary>
    /// Represents an object encoded in JSON. Can be either a dictionary 
    /// mapping strings to other objects, an array of objects, or a single 
    /// object, which represents a scalar.
    /// </summary>
    public class JSON : IEnumerable<JSON>
    {
        protected string _stringData;
        protected JSON[] _arrayData;
        protected Dictionary<string, JSON> _dictData;
        protected List<string> _keys;
        //protected bool _quoted;  //TODO: Record quoted state on deserialization.

        protected int _index = -1;
        protected string _key = null;
        protected JSON _parent;

        protected int _internalIndex = 0;
        protected bool _dynamicAllocation = false;
        protected string _keyIfNull = "null";
        protected Exception _exp;

        #region CONSTRUCTORS

        /// <summary>
        ///     Creates an empty JSON
        /// </summary>
        public JSON()
        { }

        /// <summary>
        ///     Creates a JSON from a serialized JSONstring OR QueryString
        /// </summary>
        public JSON(string s)
        {
            JSONDeserialize(s, true, this, out _arrayData, out _dictData, out _stringData);
            if (_dictData != null)
                _keys = new List<string>(_dictData.Keys);
        }

        /// <summary>
        ///     Creates a JSON from a serialized JSONstring OR QueryString.
        /// </summary>
        /// <param name="s">Raw string OR Serialized JSON string OR QueryString.</param>
        /// <param name="Deserialize">Default: true, Set to false to use Raw string</param>
        public JSON(string s, bool Deserialize)
        {
            JSONDeserialize(s, Deserialize, this, out _arrayData, out _dictData, out _stringData);
            if (_dictData != null)
                _keys = new List<string>(_dictData.Keys);
        }

        /// <summary>
        ///     Creates a JSON Dictionary with a single JSON entry.
        /// </summary>
        public JSON(string key, JSON value)
        {
            key = key ?? _keyIfNull;
            _dictData = new Dictionary<string, JSON>();
            _dictData.Add(key, value.SetKey(key).SetParent(this));
            _keys = new List<string>();
            _keys.Add(key);
        }

        /// <summary>
        ///     Creates a JSON Dictionary with a single String entry.
        /// </summary>
        public JSON(string key, string value)
        {
            key = key ?? _keyIfNull;
            _dictData = new Dictionary<string, JSON>();
            _dictData.Add(key, new JSON(value, false).SetKey(key).SetParent(this));
            _keys = new List<string>();
            _keys.Add(key);
        }

        /// <summary>
        ///     Creates an Integer based JSON
        /// </summary>
        public JSON(int i)
        {
            _stringData = i.ToString();
        }

        /// <summary>
        ///     Creates an Integer based JSON
        /// </summary>
        public JSON(long l)
        {
            _stringData = l.ToString();
        }

        /// <summary>
        ///     Creates a Boolean based JSON
        /// </summary>
        public JSON(bool b)
        {
            _stringData = b.ToString().ToLower();
        }

        /// <summary>
        ///     Creates an Array based JSON from an existing JSON Array
        /// </summary>
        public JSON(JSON[] a)
        {
            if (a != null)
            {
                _arrayData = new JSON[a.Length];
                for (int i = 0; i < _arrayData.Length; i++)
                    _arrayData[i] = a[i].SetIndex(i).SetParent(this);
            }
        }

        /// <summary>
        ///     Creates an Array based JSON from an existing string Array.
        /// </summary>
        public JSON(string[] a)
        {
            if (a != null)
            {
                _arrayData = new JSON[a.Length];
                for (int i = 0; i < a.Length; i++)
                    _arrayData[i] = new JSON(a[i], false).SetIndex(i).SetParent(this);
            }
        }

        /// <summary>
        ///     Creates an Array based JSON from an existing string Array.
        /// </summary>
        /// <param name="s">A String Array containing raw strings OR Serialized JSON strings OR QueryStrings OR a combination of the three.</param>
        /// <param name="Deserialize">Default: false, Set to true to use Deserialize strings</param>
        public JSON(string[] a, bool Deserialize)
        {
            if (a != null)
            {
                _arrayData = new JSON[a.Length];
                for (int i = 0; i < a.Length; i++)
                    _arrayData[i] = new JSON(a[i], Deserialize).SetIndex(i).SetParent(this);
            }
        }

        /// <summary>
        ///     Creates an Array based JSON from an existing Array of serializable objects.
        /// </summary>
        public JSON(object[] a)
        {
            if (a != null)
            {
                _arrayData = new JSON[a.Length];
                for (int i = 0; i < a.Length; i++)
                    _arrayData[i] = new JSON(a[i]).SetIndex(i).SetParent(this);
            }
        }

        /// <summary>
        ///     Creates a Dictionary based JSON
        /// </summary>
        public JSON(Dictionary<string, JSON> d)
        {
            if (d != null)
            {
                _dictData = new Dictionary<string, JSON>();
                _keys = new List<string>();
                foreach (KeyValuePair<string, JSON> kv in d)
                {
                    _dictData.Add(kv.Key, kv.Value.SetKey(kv.Key).SetParent(this));
                    _keys.Add(kv.Key);
                }
            }
        }

        /// <summary>
        ///     Creates an Array based JSON with one JSON entry.
        /// </summary>
        public JSON(JSON j)
        {
            _arrayData = new JSON[1];
            _arrayData[0] = (j ?? new JSON()).SetIndex(0).SetParent(this);
        }

        /// <summary>
        ///     Creates a Date based JSON
        /// </summary>
        public JSON(DateTime oDate)
        {
            if (oDate != null)
            {
                if (oDate.Equals(oDate - oDate.TimeOfDay))
                    _stringData = oDate.ToString("yyyy-MM-dd");
                else if (DateTime.MinValue.Equals(oDate - oDate.TimeOfDay))
                    _stringData = oDate.ToString("HH:mm:ss");
                else
                    _stringData = oDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        /// <summary>
        ///     Creates a TimeSpan based JSON
        /// </summary>
        public JSON(TimeSpan oTimeSpan)
        {
            if(oTimeSpan != null)
                _stringData = oTimeSpan.ToString("hh\\:mm\\:ss");
        }

        /// <summary>
        ///     Creates a DataRow based JSON.
        /// </summary>
        public JSON(DataRow dataRow)
        {
            Copy(FromDataRow(dataRow));
        }

        /// <summary>
        ///     Creates a DataTable based JSON.
        /// </summary>
        public JSON(DataTable dataTable)
        {
            Copy(FromDataTable(dataTable));
        }

        /// <summary>
        ///     Creates a DataSet based JSON.
        /// </summary>
        public JSON(DataSet dataSet)
        {
            Copy(FromDataSet(dataSet));
        }

        ///// <summary>
        ///// Creates a QuickDB based JSON.
        ///// </summary>
        //public JSON(QuickDB qDB)
        //{
        //    if (qDB.DataSet != null)
        //        Copy(FromDataSet(qDB.DataSet));
        //    else if (qDB.Table != null)
        //        Copy(FromDataTable(qDB.Table));
        //    else if (qDB.Row != null)
        //        Copy(FromDataRow(qDB.Row));
        //    else if (qDB.Value != null)
        //        Copy(new JSON(qDB.Value));
        //}

        /// <summary>
        ///     Creates a serializable object based JSON (Object.ToString() is used to record the data.)
        /// </summary>
        public JSON(object o)
        {
            if (o != null)
            {
                if (o.GetType().Equals(typeof(DateTime)))
                    Clone(new JSON((DateTime)o));
                else
                {
                    JSONDeserialize(o.ToString(), false, this, out _arrayData, out _dictData, out _stringData);
                    if (_dictData != null)
                        _keys = new List<string>(_dictData.Keys);
                }
            }
        }

        /// <summary>
        /// Creates a JSON representation of the given exception. error = Exception, stack = Full stack trace. error.message = Exception message. error.innerError = Inner exception. (error.innerError.message = Inner exception message).
        /// </summary>
        public JSON(Exception e)
        {
            if (e != null)
            {
                this.Add("error", 
                    new JSON("message", e.Message)
                        .Add("type", e.GetType().ToString())
                );

                if (e is WebException)
                {
                    WebException we = (WebException)e;
                    JSON r = new JSON();
                    if (we.Response != null)
                    {
                        try
                        {
                            using (WebResponse response = we.Response)
                                using (Stream str = response.GetResponseStream())
                                    using (StreamReader reader = new StreamReader(str))
                                        r = new JSON(reader.ReadToEnd());
                        }
                        catch
                        {
                            //Don't cry if we can't read the response.
                        }
                    }

                    this.Add("response", r);

                    //If response is an exception - embed it in the orignal exception as an inner exception.
                    if (r.IsException)
                        e = new WebException(e.Message, r.Exception, we.Status, null);
                }

                this["error"]._exp = e;

                if (!string.IsNullOrEmpty(e.HelpLink))
                    this["error"].Add("help", e.HelpLink);

                if (!string.IsNullOrEmpty(e.Source))
                    this["error"].Add("source", e.Source);

                if (e.Data.Count > 0)
                {
                    this["error"].Add("data", new JSON());
                    foreach (string key in e.Data.Keys)
                        this["error"]["data"].Add(key, new JSON(e.Data[key]));
                }

                if (e.InnerException != null)
                {
                    JSON inner = new JSON(e.InnerException);
                    this["error"].Add("innerError", inner["error"]);
                    if (!inner["response"].IsNull)
                        this["error"]["innerError"].Add("response", inner["response"]);
                }

                this.Add("stack", e.ToString());                    
            }
        }

        /// <summary>
        /// LEGACY: Creates a JSON representation of the given exception. error.message = Exception message. error.type = exception type. error.cause = Inner exception. (error.cause.message = Inner exception message).
        /// </summary>
        /// <param name="legacy">Default: false. When true, exceptions well be encoded in the legacy format { error: { message:, type:, cause:, details: }}</param>
        [Obsolete("This legacy format will be removed in future updates.  Please update your code at your earliest convenience.", true)]
        public JSON(Exception e, bool legacy)
        {
            if (e != null)
            {
                this.Add("error",
                    new JSON("message", e.Message)
                        .Add("type", e.GetType().ToString())
                        .Add("cause", new JSON(e.InnerException, true)["error"])
                        .Add("details", e.ToString())
                );
                this["error"]._exp = e;
            }
        }

        ///// <summary>
        /////     Destructor
        ///// </summary>
        //~JSON()
        //{
        //    _stringData = null;
        //    _arrayData = null;
        //    _dictData = null;
        //    _keys = null;
        //    _internalIndex = 0;
        //    _index = 0;
        //    _key = null;
        //    _parent = null;
        //    _dynamicAllocation = false;
        //    _keyIfNull = null;
        //    _exp = null;
        //}

        #endregion

        #region Deserialization

        protected static void JSONDeserialize(string s, bool Deserialize, JSON parent, out JSON[] oArrayData, out Dictionary<string, JSON> oDictData, out string oStringData)
        {
            oArrayData = null;
            oDictData = null;
            oStringData = null;

            if (s == null || s.Trim().Equals("null", StringComparison.CurrentCultureIgnoreCase))
            {
                // null string input results in an empty JSON.
            }
            else if (Deserialize && (s.Trim().StartsWith("{") || s.Trim().StartsWith("[") || s.Trim().StartsWith("\"")))
            {
                int index = 0;
                string buffer = "";
                List<JSON> JSONL = new List<JSON>();

                while (index < s.Length)
                {
                    switch (s[index])
                    {
                        case '[':
                            oArrayData = JSONGetArray(ref s, ref index, parent);
                            buffer = "";
                            break;
                        case '{':
                            oDictData = JSONGetDictionary(ref s, ref index, parent);
                            buffer = "";
                            break;
                        case '\"':
                            oStringData = JSONGetString(ref s, ref index);
                            buffer = "";
                            break;
                        case '\n':
                            if (oArrayData != null)
                                JSONL.Add(new JSON(oArrayData).SetIndex(JSONL.Count).SetParent(parent));
                            else if (oDictData != null)
                                JSONL.Add(new JSON(oDictData).SetIndex(JSONL.Count).SetParent(parent));
                            else if (oStringData != null)
                                JSONL.Add(new JSON(oStringData, false).SetIndex(JSONL.Count).SetParent(parent));
                            oArrayData = null;
                            oDictData = null;
                            oStringData = null;
                            buffer = "";
                            index++;
                            break;
                        default:
                            buffer += s[index++];
                            break;
                    }
                }

                //JSON Lines file.
                if (JSONL.Count == 1)
                {
                    //Only one line - revert to normal JSON.
                    oArrayData = JSONL[0].Array;
                    oDictData = JSONL[0].Dictionary;
                    oStringData = JSONL[0].String;
                }
                if (JSONL.Count > 1)
                {
                    //Multiple Lines: Each line contains a valid JSON
                    if (oArrayData != null)
                        JSONL.Add(new JSON(oArrayData).SetIndex(JSONL.Count));
                    else if (oDictData != null)
                        JSONL.Add(new JSON(oDictData).SetIndex(JSONL.Count));
                    else if (oStringData != null)
                        JSONL.Add(new JSON(oStringData, false).SetIndex(JSONL.Count));
                    oDictData = null;
                    oStringData = null;

                    oArrayData = JSONL.ToArray();
                }
                //Other data types
                else if ((oArrayData == null) && (oDictData == null) && (oStringData == null) && (!buffer.Trim().ToLower().Equals("null")))
                    oStringData = buffer.Trim();
            }
            else if (Deserialize && Regex.IsMatch(s.Trim(), "(?:(?:[^=\\s]+=[^=\\s]+)+|(?:[?](?:[^\\s]*)?)+)$")) //Crude check that s could match a Query String or at least key=value pairs.
            {
                oDictData = new Dictionary<string, JSON>();

                if (s.Contains("?"))
                    s = s.Split('?')[1];

                foreach (string kv in s.Split('&'))
                {
                    if (kv.Contains("="))
                    {
                        if (oDictData.ContainsKey(kv.Split('=')[0]))
                            oDictData[kv.Split('=')[0]].String += $",{ kv.Split('=')[1] }";
                        else
                            oDictData.Add(kv.Split('=')[0], new JSON(kv.Split('=')[1], false).SetKey(kv.Split('=')[0]).SetParent(parent));
                    }
                    else
                        oDictData.Add(kv, new JSON(true).SetKey(kv).SetParent(parent));
                }
            }
            //else if (Deserialize && Regex.IsMatch(s.Trim(), "^[^\\s]+,[^\\s]+$")) //will match any comma separated list with no spaces. for JSON shorthand.
            //{
            //    int index = 0;
            //    string buffer = "";

            //    while (index < s.Length)
            //    {
            //        switch (s[index])
            //        {
            //            case '(':
            //                oArrayData = JSONGetArray(ref s, ref index);
            //                buffer = "";
            //                break;
            //            case '.':
            //                oDictData = JSONGetDictionary(ref s, ref index);
            //                buffer = "";
            //                break;
            //            default:
            //                buffer += s[index++];
            //                break;
            //        }
            //    }

            //    if ((oArrayData == null) && (oDictData == null) && (oStringData == null))
            //        oStringData = buffer.Trim();
            //}
            else //if not deserialize  OR  string does not contain key characters. 
                oStringData = s;
        }

        private static string JSONGetString(ref string s, ref int index)
        {
            StringBuilder sb = new StringBuilder();
            index++;
            try
            {
                while ((index < s.Length) && !s[index].Equals('\"'))
                {
                    switch (s[index])
                    {
                        case '\\':
                            switch (Char.ToLower(s[++index]))
                            {
                                case 'b': sb.Append('\b'); index++; break;
                                case 'f': sb.Append('\f'); index++; break;
                                case 'n': sb.Append('\n'); index++; break;
                                case 'r': sb.Append('\r'); index++; break;
                                case 't': sb.Append('\t'); index++; break;
                                case 'u':
                                    sb.Append((char)int.Parse(s.Substring(++index, 4), NumberStyles.HexNumber));
                                    index += 4;
                                    break;
                                default:
                                    sb.Append(s[index++]);
                                    break;
                            }
                            break;
                        default:
                            sb.Append(s[index++]);
                            break;
                    }
                }
            }
            catch
            {
                sb = new StringBuilder("Error parsing JSON string at character " + index.ToString() + " ('" + s[index] + "')");
            }

            index++;
            return sb.ToString();
        }

        private static JSON[] JSONGetArray(ref string s, ref int index, JSON parent)
        {
            List<JSON> objArr = new List<JSON>();
            index++;
            string buffer = "";

            while ((index < s.Length) && !s[index].Equals(']'))
            {
                switch (s[index])
                {
                    case '[':
                        objArr.Add(new JSON(JSONGetArray(ref s, ref index, parent)).SetIndex(objArr.Count).SetParent(parent));
                        buffer = "";
                        break;
                    case '{':
                        objArr.Add(new JSON(JSONGetDictionary(ref s, ref index, parent)).SetIndex(objArr.Count).SetParent(parent));
                        buffer = "";
                        break;
                    case '\"':
                        objArr.Add(new JSON(JSONGetString(ref s, ref index), false).SetIndex(objArr.Count).SetParent(parent));
                        buffer = "";
                        break;
                    case ',':
                        index++;
                        if (!string.IsNullOrEmpty(buffer.Trim()))
                            objArr.Add((buffer.Trim().ToLower().Equals("null") ? new JSON() : new JSON(buffer.Trim()) ).SetIndex(objArr.Count).SetParent(parent));
                        buffer = "";
                        break;
                    default:
                        buffer += s[index++];
                        break;
                }
            }

            if (!string.IsNullOrEmpty(buffer.Trim()))
                objArr.Add((buffer.Trim().ToLower().Equals("null") ? new JSON() : new JSON(buffer.Trim()) ).SetIndex(objArr.Count).SetParent(parent));

            index++;
            return objArr.ToArray();
        }

        private static Dictionary<string, JSON> JSONGetDictionary(ref string s, ref int index, JSON parent)
        {
            Dictionary<string, JSON> objDic = new Dictionary<string, JSON>();
            index++;
            string Key = null;
            string buffer = "";

            while ((index < s.Length) && !s[index].Equals('}'))
            {
                if (Key == null)
                {
                    switch (s[index])
                    {
                        case '\"':
                            Key = JSONGetString(ref s, ref index);
                            buffer = "";
                            break;
                        case ':':
                            index++;
                            Key = buffer.Trim();
                            buffer = "";
                            break;
                        case ',':
                            index++;
                            buffer = "";
                            break;
                        default:
                            buffer += s[index++];
                            break;
                    }
                }
                else
                {
                    JSON value = null;
                    switch (s[index])
                    {
                        case '[':
                            value = new JSON(JSONGetArray(ref s, ref index, parent));
                            break;
                        case '{':
                            value = new JSON(JSONGetDictionary(ref s, ref index, parent));
                            break;
                        case '\"':
                            value = new JSON(JSONGetString(ref s, ref index), false);
                            break;
                        case ':':
                            index++;
                            buffer = "";
                            break;
                        case ',':
                            index++;
                            if (!string.IsNullOrEmpty(buffer.Trim()))
                                value = (buffer.Trim().ToLower().Equals("null") ? new JSON() : new JSON(buffer.Trim()));
                            break;
                        default:
                            buffer += s[index++];
                            break;
                    }

                    //If value has been assigned.
                    if (!object.ReferenceEquals(value, null))
                    {
                        if (objDic.ContainsKey(Key))
                            objDic[Key].Concat(value.SetKey(Key));
                        else
                            objDic.Add(Key, value.SetKey(Key).SetParent(parent));
                        Key = null;
                        buffer = "";
                    }
                }
            }

            if (Key != null && !string.IsNullOrEmpty(buffer.Trim()))
            {
                JSON value = (buffer.Trim().ToLower().Equals("null") ? new JSON() : new JSON(buffer.Trim()));
                if (objDic.ContainsKey(Key))
                    objDic[Key].Concat(value.SetKey(Key));
                else
                    objDic.Add(Key, value.SetKey(Key).SetParent(parent));
            }

            index++;
            return objDic;
        }
        #endregion

        #region DataType Analysis
        /// <summary>
        /// Returns true if this JSON represents a dictionary.
        /// </summary>
        public bool IsDictionary
        {
            get
            {
                return _dictData != null;
            }
        }

        /// <summary>
        /// Returns true if this JSON represents an array.
        /// </summary>
        public bool IsArray
        {
            get
            {
                return _arrayData != null;
            }
        }

        /// <summary>
        /// Returns true if this JSON represents a string value. 
        /// </summary>
        public bool IsString
        {
            get
            {
                return _stringData != null;
            }
        }

        /// <summary>
        /// Returns true if this JSON represents an integer value.
        /// </summary>
        public bool IsInteger
        {
            get
            {
                Int64 tmp;
                return Int64.TryParse(_stringData, out tmp);
            }
        }

        /// <summary>
        /// Returns true if this JSON represents an integer or double value.
        /// </summary>
        public bool IsNumber
        {
            get
            {
                Double tmp;
                if (!string.IsNullOrEmpty(_stringData) && new Regex(@"^-?(\d{1,3})*(\,\d{3})*(\.\d+)?((e|E)(\+|-)?\d+)?$").IsMatch(_stringData))//Double.TryPass ignores commas in numbers - this checks the number is potentially valid.
                    return Double.TryParse(_stringData, out tmp);
                else
                    return false;
            }
        }

        /// <summary>
        /// Returns true if this JSON represents a boolean value.
        /// </summary>
        public bool IsBoolean
        {
            get
            {
                bool tmp;
                return bool.TryParse(_stringData, out tmp);
            }
        }

        /// <summary>
        /// Returns true if this JSON represents a DateTime object.
        /// </summary>
        public bool IsDateTime
        {
            get
            {
                DateTime tmp;
                return DateTime.TryParse(_stringData, out tmp);
            }
        }

        /// <summary>
        /// Returns true if this JSON represents a javascript function. (A string that starts with "function(" and ends with "}").
        /// </summary>
        public bool IsFunction
        {
            get
            {
                if (string.IsNullOrEmpty(_stringData))
                    return false;
                return Regex.IsMatch(_stringData, "^\\s*function\\s*\\(.*\\)\\s*{.*}\\s*$");
            }
        }

        /// <summary>
        /// Returns true if this JSON represents an Exception.
        /// </summary>
        public bool IsException
        {
            get
            {
                if (_exp != null)
                    return true;

                if (_dictData != null && (_dictData.ContainsKey("error") //QuickLibs exception.
                    || (_dictData.ContainsKey("ClassName") && _dictData.ContainsKey("Message") && _dictData.ContainsKey("StackTraceString")) //Newsonsoft exception.
                    || (_dictData.ContainsKey("ExceptionMessage") && _dictData.ContainsKey("ExceptionType")) ) //Microsoft JSON exception.
                )
                    return true;

                return false;
            }
        }

        /// <summary>
        ///     Returns true if this JSON is null.
        /// </summary>
        public bool IsNull
        {
            get
            {
                return (_arrayData == null && _dictData == null && _stringData == null);
            }
        }

        /// <summary>
        ///     Returns true if this JSON is empty or null.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return (this.Length == 0);
            }
        }

        /// <summary>
        ///     Returns true if internal index iteration is out of range.
        /// </summary>
        public bool IsOutOfRange
        {
            get
            {
                return ((_internalIndex >= this.Length) || (_internalIndex < 0));
            }
        }

        /// <summary>
        ///     Returns true if internal index iteration is pointing to last item.
        /// </summary>
        protected bool IsEndOfRange
        {
            get
            {
                return (_internalIndex == this.Length - 1);
            }
        }

        /// <summary>
        ///   (Is Child out of Range) Returns true if the JSON at the internal Index is out of range
        ///   Useful for nested auto iterations eg While(!a.IsOutOfRange) { a.[a.IsCOOR][true].String = "foo"; }
        /// </summary>
        public bool IsCOOR
        {
            get
            {
                return this[_internalIndex].IsEndOfRange;
            }
        }

        /// <summary>
        ///    Returns True if the given value is found in this JSON as an array value, a dictionary key, or string.
        /// </summary>
        /// <param name="value">The string value to search for.</param>
        public bool Contains(string value)
        {
            return this.Contains(value, false, StringComparison.CurrentCulture);
        }

        /// <summary>
        ///    Returns True if the given value is found in this JSON as an array value, a dictionary key, or string.
        /// </summary>
        /// <param name="value">The string value to search for.</param>
        /// <param name="recursive">Default: false, true = recursively search through sub JSONs.</param>
        public bool Contains(string value, bool recursive)
        {
            return this.Contains(value, recursive, StringComparison.CurrentCulture);
        }

        /// <summary>
        ///    Returns True if the given value is found in this JSON as an array value, a dictionary key, or string.
        /// </summary>
        /// <param name="value">The string value to search for.</param>
        /// <param name="comparisonType">Default: StringComparison.CurrentCulture, default is case sensitive.  Use an alternative comparisonType for case insensitive search.</param>
        public bool Contains(string value, StringComparison comparisonType)
        {
            return this.Contains(value, false, comparisonType);
        }
        
        /// <summary>
        ///    Returns True if the given value is found in this JSON as an array value, a dictionary key, or string.
        /// </summary>
        /// <param name="value">The string value to search for.</param>
        /// <param name="recursive">Default: false, true = recursively search through sub JSONs.</param>
        /// <param name="comparisonType">Default: StringComparison.CurrentCulture, default is case sensitive.  Use an alternative comparisonType for case insensitive search.</param>
        public bool Contains(string value, bool recursive, StringComparison comparisonType)
        {
            if (this.IsArray)
            {
                foreach (JSON j in _arrayData)
                    if (j.Contains(value, recursive, comparisonType))
                        return true;
                return false;
            }
            else if (this.IsDictionary)
            {
                foreach (KeyValuePair<String, JSON> kvp in _dictData)
                    if (kvp.Key.Equals(value, comparisonType) || (recursive && kvp.Value.Contains(value, recursive, comparisonType)))
                        return true;
                return false;
            }
            else if (this.IsString)
                return _stringData.Equals(value, comparisonType);
            else
                return false;
        }

        /// <summary>
        /// If this JSON HasErrors, this will throw the error as an exception. Otherwise, do nothing.
        /// </summary>
        public void Throw()
        {
            if(this.IsException)
                throw Exception;
        }

        /// <summary>
        /// Returns true if this JSON contains an "error" message.
        /// </summary>
        public bool HasErrors()
        {
            return this.Contains("error");
        }

        /// <summary>
        /// Returns true if this JSON contains an "error" message.
        /// </summary>
        /// <param name="recursive">Default: false, true = recursively search through sub JSONs.</param>
        public bool HasErrors(bool recursive)
        {
            return this.Contains("error", recursive);
        }

        /// <summary>
        /// Returns trus if this JSON contains data that is not an error message.
        /// </summary>
        public bool HasData()
        {
            return !this.IsException && this.Length > 0;
        }


        #endregion

        #region Public Members

        protected JSON SetParent(JSON parent)
        {
            _parent = parent;
            return this;
        }

        protected JSON SetKey(string key)
        {
            _key = key;
            return this;
        }

        protected JSON SetIndex(int index)
        {
            _index = index;
            return this;
        }

        /// <summary>
        /// Gets the parent of this JSON. If this JSON is at the top level this will return null.
        /// </summary>
        public JSON Parent
        {
            get
            {
                return _parent;
            }
        }

        /// <summary>
        ///   Gets the Key pair of this JSON if it is contained within a parent Dictionary.
        /// </summary>
        public string Key
        {
            get
            {
                return _key;
            }
        }

        /// <summary>
        ///   Gets the index of this JSON if it is contained within a parent Array or Dictionary.
        /// </summary>
        public int Index
        {
            get
            {
                return _index;
            }
        }

        /// <summary>
        ///   Gets or Sets the internal Index used for automatic iteration
        /// </summary>
        public int InternalIndex
        {
            get
            {
                return _internalIndex;
            }
            set
            {
                _internalIndex = value;
            }
        }

        /// <summary>
        ///   Default: False.  When true, 'out of range' elements will be created when referenced.
        /// </summary>
        public bool DynamicAllocation
        {
            get
            {
                return _dynamicAllocation;
            }
            set
            {
                _dynamicAllocation = value;
            }
        }

        /// <summary>
        /// Returns this JSON as an Exception
        /// </summary>
        public Exception Exception
        {
            get
            {
                if(_exp != null)
                    return _exp;
                if (this["error"].IsDictionary)
                    return this["error"].Exception;
                if (this.IsException)
                    return this.ToException();
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets or Sets the contents of the dictionary at key as a JSON
        /// </summary>
        public JSON this[string key, bool DynamicAllocation]
        {
            get
            {
                key = key ?? _keyIfNull;

                if (this.IsDictionary && _dictData.ContainsKey(key))
                    return _dictData[key];
                else if (DynamicAllocation)
                    return this[key] = new JSON().SetKey(key).SetParent(this).SetDynamic(DynamicAllocation);
                else
                    return new JSON().SetKey(key).SetParent(this);
            }
            set
            {
                key = key ?? _keyIfNull;

                if (this.IsDictionary)
                {
                    if (_dictData.ContainsKey(key))
                        _dictData[key] = value.SetKey(key).SetParent(this);
                    else
                    {
                        _dictData.Add(key, value.SetKey(key).SetParent(this));
                        _keys.Add(key);
                    }
                }
                else if (this.IsNull)
                {
                    _dictData = new Dictionary<string, JSON>();
                    _dictData.Add(key, value.SetKey(key).SetParent(this));
                    _keys = new List<string>();
                    _keys.Add(key);
                }
                else
                    throw new JSONException("Cannot assign a key value pair to a non-dictionary JSON object. [\"" + key + "\"]");
            }
        }

        /// <summary>
        /// Gets or Sets the contents of the dictionary at key as a JSON
        /// </summary>
        public JSON this[string key]
        {
            get
            {
                return this[key, _dynamicAllocation];
            }
            set
            {
                this[key, _dynamicAllocation] = value;
            }
        }

        /// <summary>
        /// Gets or Sets the contents of the Array or Dictionary at index as a JSON.
        /// </summary>
        public JSON this[int index, bool DynamicAllocation]
        {
            get
            {
                if (index >= this.Length || Math.Abs(index) > this.Length || this.Length == 0) //Index out of range.
                {
                    //return new JSON();
                }
                else if (index < 0)
                    index = (index + this.Length) % this.Length;

                if (this.IsArray && (index < _arrayData.Length) && (index >= 0))
                    return _arrayData[index];
                else if (this.IsDictionary && (index < _dictData.Keys.Count) && (index >= 0))
                    //return _dictData[new List<string>(_dictData.Keys)[index]]; //TODO: _keys (Done?)
                    return _dictData[_keys[index]];
                else if (this.IsString && index == 0)
                    return this;
                else if (DynamicAllocation)
                    return this[index] = new JSON().SetIndex(index).SetParent(this).SetDynamic(DynamicAllocation);
                else
                    return new JSON().SetIndex(index).SetParent(this);
            }
            set
            {
                if (this.IsArray)
                {
                    if (index < 0)// && Math.Abs(index) < _arrayData.Length)
                        index = (index + _arrayData.Length) % _arrayData.Length; // -1 = last item.

                    if (index >= _arrayData.Length)
                    {
                        JSON[] newArray = new JSON[index + 1];
                        _arrayData.CopyTo(newArray, 0);
                        for (int i = _arrayData.Length; i < index; i++)
                            newArray[i] = new JSON().SetIndex(i).SetParent(this);
                        newArray[index] = value.SetIndex(index).SetParent(this);
                        _arrayData = newArray;
                    }
                    else if (index < 0)
                        throw new JSONException("Cannot assign value to Array. Index out of range. [" + index + "]");
                    else
                    {
                        _arrayData[index] = value.SetIndex(index).SetParent(this);
                    }
                }
                else if (this.IsDictionary)
                {
                    if (index >= this.Length || Math.Abs(index) > this.Length || this.Length == 0) //Index out of range.
                        throw new JSONException("Cannot assign value to Dictionary. Index out of range. [" + index + "]");
                    else if (index < 0)
                        index = (index + this.Length) % this.Length;

                    //string key = new List<string>(_dictData.Keys)[index]; //TODO: _keys (Done?)
                    string key = _keys[index];
                    _dictData[key] = value.SetKey(key).SetParent(this);
                }
                else
                {
                    if (index >= -1)
                    {
                        index = Math.Abs(index); //-1 = last element. (Which will be 1 in this case)

                        _arrayData = new JSON[index + 1];
                        _arrayData[0] = new JSON(_stringData).SetIndex(0).SetParent(this); //_stringData may be overwritten if assigning to index 0.
                        _arrayData[index] = value.SetIndex(index).SetParent(this);
                        _stringData = null;
                    }
                    else
                        throw new JSONException("Cannot assign value to Array. Index out of range.");
                }
            }
        }

        /// <summary>
        /// Gets or Sets the contents of the Array or Dictionary at index as a JSON.
        /// </summary>
        public JSON this[int index]
        {
            get
            {
                return this[index, _dynamicAllocation];
            }
            set
            {
                this[index, _dynamicAllocation] = value;
            }
        }

        /// <summary>
        /// Gets or Sets the next JSON in an Array or Dictionary.
        /// </summary>
        /// <param name="inc">true: Increment pointer after peek, false: Leave pointer at current index.</param>
        public JSON this[bool inc]
        {
            get
            {
                if (!this.IsOutOfRange)
                {
                    if (inc)
                        return this[_internalIndex++];
                    else
                        return this[_internalIndex];
                }
                else
                    return new JSON().SetParent(this);
            }
            set
            {
                if (inc)
                    this[_internalIndex] = value.SetIndex(_internalIndex++).SetParent(this);
                else
                    this[_internalIndex] = value.SetIndex(_internalIndex).SetParent(this);
            }
        }

        /// <summary>
        /// Returns this JSON as a dictionary
        /// </summary>
        public Dictionary<string, JSON> Dictionary
        {
            get
            {
                return _dictData;
            }
        }

        /// <summary>
        /// Returns this JSON as an array
        /// </summary>
        public JSON[] Array
        {
            get
            {
                return _arrayData;
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a string
        /// </summary>
        public string String
        {
            get
            {
                return _stringData;
            }
            set
            {
                this.Clear();
                _stringData = value;
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a safe (non-null) string
        /// </summary>
        public string SafeString
        {
            get
            {
                return _stringData ?? "";
            }
            set
            {
                this.String = value;
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as an integer
        /// </summary>
        public long Integer
        {
            get
            {
                long tmp;
                long.TryParse(_stringData, out tmp);
                return tmp;
            }
            set
            {
                this.String = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a Double precision numumber
        /// </summary>
        public double Number
        {
            get
            {
                double tmp = 0;
                if (!string.IsNullOrEmpty(_stringData) && new Regex(@"^-?(\d{1,3})*(\,\d{3})*(\.\d+)?((e|E)(\+|-)?\d+)?$").IsMatch(_stringData))//Double.TryPass ignores commas in numbers - this checks the number is potentially valid.
                    double.TryParse(_stringData, out tmp);
                return tmp;
            }
            set
            {
                this.String = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a boolean
        /// </summary>
        public bool Boolean
        {
            get
            {
                if (this.IsNumber)
                    return !this.Number.Equals(0);

                bool tmp;
                bool.TryParse(_stringData, out tmp);
                return tmp;
            }
            set
            {
                _arrayData = null;
                _dictData = null;
                _keys = null;
                _stringData = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a DateTime
        /// </summary>
        public DateTime DateTime
        {
            get
            {
                DateTime tmp;
                if (DateTime.TryParse(_stringData, out tmp))
                    return tmp;
                else
                    return DateTime.MinValue;
            }
            set
            {
                this.String = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a DateTime containing only the Date
        /// </summary>
        public DateTime Date
        {
            get
            {
                DateTime tmp;
                if (DateTime.TryParse(_stringData, out tmp))
                    return DateTime.Parse(tmp.ToShortDateString());
                else
                    return DateTime.MinValue;
            }
            set
            {
                this.String = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets this JSON as a DateTime containing only the Time
        /// </summary>
        public DateTime Time
        {
            get
            {
                DateTime tmp;
                if (DateTime.TryParse(_stringData, out tmp))
                    return DateTime.Parse(tmp.ToShortTimeString());
                else
                    return DateTime.MinValue;
            }
            set
            {
                this.String = value.ToString();
            }
        }

        /// <summary>
        /// Gets or Sets the length of the data contained within
        /// </summary>
        public int Length
        {
            get
            {
                if (this.IsArray)
                    return _arrayData.Length;
                else if (this.IsDictionary)
                    return _dictData.Count;
                else if (this.IsString)
                    return 1;
                else
                    return 0;
            }
            set
            {
                if (this.IsArray)
                {
                    if (value >= 0)
                    {
                        JSON[] newArray = new JSON[value];
                        for (int i = 0; (i < newArray.Length) && (i < _arrayData.Length); i++)
                            newArray[i] = _arrayData[i];
                        _arrayData = newArray;
                    }
                    else
                        throw new JSONException("Length must be positive.");
                }
                else
                    throw new JSONException("Cannot extend or truncate a non-array JSON.");
            }
        }

        /// <summary>
        /// Gets or Sets the default key to use in a dictionary reference if the given key is null.
        /// If this value is null, a System.ArgumentNullException will be thrown when trying to reference a dictionary entry with a null key.
        /// </summary>
        public string KeyIfNull
        {
            get
            {
                return _keyIfNull;
            }
            set
            {
                _keyIfNull = value;
            }
        }

        /// <summary>
        /// Returns this JSON and all sub JSONs as a generic object type which can be cast to a specific type.
        /// </summary>
        public object ToObject()
        {
            if (this.IsArray)
            {
                object[] arr = new object[this.Length];
                for (int i = 0; i < this.Length; i++)
                    arr[i] = this[i].ToObject();
                return arr;
            }
            else if (this.IsDictionary)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                foreach (JSON j in this)
                    dict.Add(j.Key, j.ToObject());
                return dict;
            }
            else if (this.IsNull)
                return null;
            else if (this.IsBoolean)
                return this.Boolean;
            if (this.IsInteger)
                return this.Integer;
            if (this.IsNumber)
                return this.Number;
            if (this.IsDateTime)
                return this.DateTime;
            else
                return this._stringData;
        }

        /// <summary>
        /// Returns this JSON as an object of the given type, or null if incompatible.
        /// This process is compatible with Arrays (e.g. int?[]) and Dictionaries with a key type of string. (e.g. Dictionary&lt;string, int?&gt;)
        /// </summary>
        public object ToObject(Type T)
        {
            try
            {
                if (T.Name == "Dictionary`2" && this.IsDictionary) //&& T.GetGenericArguments()[0] == typeof(string)
                {
                    Type dictType = typeof(Dictionary<,>).MakeGenericType(T.GetGenericArguments());
                    IDictionary dict = (IDictionary)Activator.CreateInstance(dictType);
                    foreach (JSON j in this)
                        dict.Add(
                            new JSON(j.Key, false).ToObject(T.GetGenericArguments()[0]), //key
                            j.ToObject(T.GetGenericArguments()[1]) //value
                        );
                    return dict;
                }
                else if (T.IsArray && (this.IsArray || this.IsDictionary))
                {
                    //Cast all elements into an object array
                    object[] objArr = new object[this.Length];
                    for (int i = 0; i < this.Length; i++)
                        objArr[i] = this[i].ToObject(T.GetElementType());

                    //Convert object array into an array of the given type.
                    System.Array outArr = System.Array.CreateInstance(T.GetElementType(), objArr.Length);
                    System.Array.Copy(objArr, outArr, objArr.Length);

                    return outArr;
                }
                else
                    return TypeDescriptor.GetConverter(T).ConvertFromString(_stringData);
            }
            catch (Exception exp)
            {
                if (T.IsValueType)
                    return Activator.CreateInstance(T);

                return null;
            }
        }

        /// <summary>
        /// Returns this JSON in the given type, or the given type's default value if incompatible.
        /// This process is compatible with Arrays (e.g. int?[]) and Dictionaries with a key type of string. (e.g. Dictionary&lt;string, int?&gt;)
        /// </summary>
        public T ToObject<T>()
        {
            try
            {
                object val = ToObject(typeof(T));
                return (T)val;// TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(val);
            }
            catch (Exception exp)
            {
                return default;
            }
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string formatted for easy reading. Alternative access to JSON.ToString(3)
        /// </summary>
        public string ToJSON()
        {
            return this.ToJSON("", false);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string formatted for easy reading wrapped in the given callback function.
        /// </summary>
        /// <param name="callback">Wrap the JSON string in a callback function by this name, passing the JSON as a parameter. (Necessary for cross domain AJAX calls.)</param>
        public string ToJSON(string callback)
        {
            return this.ToJSON(callback, false);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string formatted for easy reading or minified as requested.
        /// </summary>
        /// <param name="minified">True: JSON string will be returned with whitespace removed. False: (default) Returned JSON string will be formatted for easy reading.</param>
        public string ToJSON(bool minified)
        {
            return this.ToJSON("", minified);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string formatted for easy reading or minified as requested wrapped in the given callback function.
        /// </summary>
        /// <param name="callback">Wrap the JSON string in a callback function by this name, passing the JSON as a parameter. (Necessary for cross domain AJAX calls.)</param>
        /// <param name="minified">True: JSON string will be returned with whitespace removed. False: (default) Returned JSON string will be formatted for easy reading.</param>
        public string ToJSON(string callback, bool minified)
        {
            string JSON = minified ? this.ToString(false, true, 0, 0) : this.ToString(false, true, 3, 0);  //no white space : formatted JSON string.

            if (string.IsNullOrEmpty(callback))
                return JSON;
            else
                return $"{callback}({ JSON });";
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string.
        /// </summary>
        public override string ToString()
        {
            return this.ToString(false, true, 0, 0);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string.
        /// </summary>
        /// <param name="QuoteValues">Default: False, Set to override Int and Bool behaviour and output them as Quoted strings.</param>
        private string ToString(bool QuoteValues)
        {
            return this.ToString(QuoteValues, true, 0, 0);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string.
        /// </summary>
        /// <param name="Indent">Default: 0, Number of characters to indent formatted JSON (0 outputs a minified JSON).</param>
        private string ToString(int Indent)
        {
            return this.ToString(false, true, Indent, 0);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string.
        /// </summary>
        /// <param name="QuoteValues">Default: False, Set to override Int and Bool behaviour and output them as Quoted strings.</param>
        /// <param name="QuoteKeys">Default: True, Clear to override Dictionary Keys and output them without Quoted strings.</param>
        private string ToString(bool QuoteValues, bool QuoteKeys)
        {
            return this.ToString(QuoteValues, QuoteKeys, 0, 0);
        }

        /// <summary>
        ///     Returns the JSON as a Serialized JSON string.
        /// </summary>
        /// <param name="QuoteValues">Default: False, Set to override Int and Bool behaviour and output them as Quoted strings.</param>
        /// <param name="QuoteKeys">Default: True, Clear to override Dictionary Keys and output them without Quoted strings.</param>
        /// <param name="Indent">Number of characters to indent formatted JSON (0 outputs a minified JSON).</param>
        private string ToString(bool QuoteValues, bool QuoteKeys, int Indent)
        {
            return this.ToString(QuoteValues, QuoteKeys, Indent, 0);
        }

        private string ToString(bool QuoteValues, bool QuoteKeys, int indent, int level)
        {
            StringBuilder sb = new StringBuilder();

            if (this.IsArray)
            {
                sb.Append("[");
                foreach (JSON o in _arrayData)
                {
                    if (indent > 0)
                        sb.AppendLine(sb.Length > 1 ? "," : "");
                    else
                        sb.Append(sb.Length > 1 ? "," : "");

                    sb.Append(' ', (level + 1) * indent);

                    sb.Append(o.ToString(QuoteValues, QuoteKeys, indent, level + 1));
                }
                if (indent > 0)
                    sb.AppendLine();
                sb.Append(' ', level * indent);
                sb.Append("]");
            }
            else if (this.IsDictionary)
            {
                sb.Append("{");
                foreach (string k in _keys)
                {
                    if (indent > 0)
                        sb.AppendLine(sb.Length > 2 ? "," : "");
                    else
                        sb.Append(sb.Length > 2 ? "," : "");

                    sb.Append(' ', (level + 1) * indent);

                    sb.Append(QuoteKeys ? "\"" : "");
                    sb.Append(k);
                    sb.Append(QuoteKeys ? "\"" : "");

                    sb.Append(":");

                    sb.Append(_dictData[k].ToString(QuoteValues, QuoteKeys, indent, level + 1));
                }
                if (indent > 0)
                    sb.AppendLine();
                sb.Append(' ', level * indent);
                sb.Append("}");
            }
            else if (this.IsNull)
                sb.Append("null"); //sb.Append((level == 0) ? "{}" : "null");
            else
            {
                if (this.IsBoolean)
                    _stringData = _stringData.ToLower();
                bool QuoteIt = QuoteValues || !(this.IsBoolean || this.IsNumber || this.IsFunction) || (_stringData.StartsWith("0") && !_stringData.StartsWith("0.") && _stringData != "0") || _stringData.Contains(',');
                sb.Append(QuoteIt ? "\"" : "");
                sb.Append(ExpandEscapeCharacters(_stringData));
                sb.Append(QuoteIt ? "\"" : "");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON as a Base64 encoded serialised string.
        /// </summary>
        public string ToEncoded()
        {
            return Cryptography.Crypto.Encode(this.ToString());
        }

        /// <summary>
        /// Returns the JSON as an encrypted serialised string.
        /// </summary>
        public string ToEncrypted()
        {
            return Cryptography.Crypto.Encrypt(this.ToString());
        }

        /// <summary>
        /// Returns the JSON as an encrypted serialised string.
        /// </summary>
        public string ToEncrypted(string Password)
        {
            return Cryptography.Crypto.Encrypt(this.ToString(), Password);
        }

        private string ExpandEscapeCharacters(string s)
        {
            return s
                .Replace(@"\", @"\\")
                .Replace("\"", @"\""")
                .Replace("\b", @"\b")
                .Replace("\f", @"\f")
                .Replace("\n", @"\n")
                .Replace("\r", @"\r")
                .Replace("\t", @"\t");
        }

        /// <summary>
        ///     Returns the JSON object as a querystring.
        /// </summary>
        public string ToQueryString()
        {
            return this.ToQueryString("?");
        }

        /// <summary>
        ///     Returns the JSON object as a querystring.
        /// </summary>
        /// <param name="prepend">String to prepend at the beginning of the string (Default: "?").</param>
        public string ToQueryString(string prepend)
        {
            StringBuilder sb = new StringBuilder(prepend);

            if (this.IsArray)
            {
                foreach (JSON o in _arrayData)
                {
                    sb.Append(o.ToQueryString(""));
                    sb.Append("&");
                }
            }
            else if (this.IsDictionary)
            {
                //foreach (KeyValuePair<string, JSON> kv in _dictData) //TODO: _keys (Done?)
                foreach (string k in _keys)
                {
                    sb.Append(k);
                    sb.Append(_dictData[k].ToQueryString("="));
                    sb.Append("&");
                }
            }
            else
            {
                sb.Append(_stringData);
                sb.Append("&"); //any character will do, this will be removed immediatly!
            }

            sb.Remove(sb.Length - 1, 1); //Remove final &

            if (sb.Length == prepend.Length)
                return "";
            else
                return sb.ToString();
        }

        /// <summary>
        ///     Returns the JSON object as an XML string.
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            if (this.IsDictionary && (this.Length == 1))
                return this.ToXmlNode().OuterXml;
            else
                return this.ToXmlNode().InnerXml;
        }

        /// <summary>
        ///     Returns the current JSON as an XmlNode. If this JSON is a dictionary with one entry, it will be used as
        ///     the root. Otherwise this and all sub JSONs will be created as child nodes of a new root node.
        /// </summary>
        public XmlNode ToXmlNode()
        {
            XmlDocument d = new XmlDocument();

            if (this.IsDictionary && (this.Length == 1))
            {
                //XmlNode node = d.CreateElement(new List<String>(_dictData.Keys)[0]); //TODO: _keys (Done?)
                XmlNode node = d.CreateElement(_keys[0]);
                _dictData[node.Name].ToXmlNode(node);
                return node;
            }
            else
            {
                XmlNode node = d.CreateElement("root");
                this.ToXmlNode(node);
                return node;
            }
        }

        private void ToXmlNode(XmlNode root)
        {
            XmlDocument d = root.OwnerDocument;

            if (this.IsString || this.IsNull)
                root.AppendChild(d.CreateTextNode(this.String));
            else if (this.IsArray)
            {
                // A two dimentional array should all append to the same root node
                // - proper notation should see alternating Arrays and Dictionarys if this is not the desired behaviour.
                foreach (JSON j in _arrayData)
                    j.ToXmlNode(root);
            }
            else
            {
                //foreach (KeyValuePair<string, JSON> kv in _dictData) //TODO: _keys (Done?)
                foreach (string k in _keys)
                {
                    //XmlNode node = d.CreateElement(kv.Key);
                    XmlNode node = d.CreateElement(k);
                    //if (kv.Value.Contains("_attributes"))
                    if (_dictData[k].Contains("_attributes"))
                    {
                        //foreach (KeyValuePair<string, JSON> attr in kv.Value["_attributes"].Dictionary)
                        foreach (string attr in _dictData[k]["_attributes"]._keys)
                        {
                            //XmlAttribute a = d.CreateAttribute(attr.Key);
                            XmlAttribute a = d.CreateAttribute(attr);
                            //a.Value = attr.Value.SafeString;
                            a.Value = _dictData[k]["_attributes"][attr].SafeString;
                            node.Attributes.Append(a);
                        }
                        //kv.Value["_value"].ToXmlNode(node);
                        _dictData[k]["_value"].ToXmlNode(node);
                    }
                    else
                        //kv.Value.ToXmlNode(node);
                        _dictData[k].ToXmlNode(node);

                    root.AppendChild(node);
                }
            }
        }

        /// <summary>
        ///     Returns a new Exception from the current JSON. Use json.Exception to return the original Exception.
        /// </summary>
        public Exception ToException()
        {
            if (this["error"].IsDictionary)
                return this["error"].ToException();
            else if ("error".Equals(this.Key) || "innerError".Equals(this.Key))
            {
                Exception e;
                if (this["innerError"].IsNull)
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["type"].SafeString) ?? typeof(System.Exception), this["message"].String);
                else
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["type"].SafeString) ?? typeof(System.Exception), this["message"].String, this["innerError"].ToException());

                if (this["help"].IsString)
                    e.HelpLink = this["help"].String;

                if (this["source"].IsString)
                    e.Source = this["source"].String;

                foreach (JSON datum in this["data"])
                    e.Data.Add(datum.Key, datum.String);

                return e;
            }
            //Else if Newsonsoft.Json serialised exception.
            else if (this["ClassName"].IsString && this["Message"].IsString && this["StackTraceString"].IsString)
            {
                Exception e;
                if (this["InnerException"].IsNull)
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["ClassName"].SafeString) ?? typeof(System.Exception), this["Message"].String);
                else
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["ClassName"].SafeString) ?? typeof(System.Exception), this["Message"].String, this["InnerException"].ToException());

                if (this["HelpURL"].IsString)
                    e.HelpLink = this["HelpURL"].String;

                if (this["Source"].IsString)
                    e.Source = this["Source"].String;

                foreach (JSON datum in this["Data"])
                    e.Data.Add(datum.Key, datum.String);

                return e;
            }
            //Else if Microsoft JSON Exception.
            else if (this["ExceptionMessage"].IsString && this["ExceptionType"].IsString)
            {
                Exception e;
                if (this["InnerException"].IsNull)
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["ExceptionType"].SafeString) ?? typeof(System.Exception), this["ExceptionMessage"].String);
                else
                    e = (Exception)Activator.CreateInstance(Type.GetType(this["ExceptionType"].SafeString) ?? typeof(System.Exception), this["ExceptionMessage"].String, this["InnerException"].ToException());

                if (this["HelpURL"].IsString)
                    e.HelpLink = this["HelpURL"].String;

                if (this["Source"].IsString)
                    e.Source = this["Source"].String;

                foreach (JSON datum in this["Data"])
                    e.Data.Add(datum.Key, datum.String);

                return e;
            }

            return new Exception();
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        /// </summary>
        public void DoResponse(int statusCode = (int)HttpStatusCode.OK)
        {
            if(HttpContext.Current != null)
                DoResponse(HttpContext.Current.Request.QueryString["callback"], "application/json", false, true, statusCode);
            else
                DoResponse("", "application/json", false, true, statusCode);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="minified">Default: False = return formatted string. True = return string with whitespace removed.</param>
        public void DoResponse(bool minified)
        {
            if (HttpContext.Current != null)
                DoResponse(HttpContext.Current.Request.QueryString["callback"], "application/json", minified, true);
            else
                DoResponse("", "application/json", minified, true);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="callback">Wrap the JSON in a callback function. (Default: QueryString["callback"])</param>
        public void DoResponse(string callback)
        {
            DoResponse(callback, "application/json", false, true);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="callback">Wrap the JSON in a callback function. (Default: QueryString["callback"])</param>
        /// <param name="minified">Default: False = return formatted string. True = return string with whitespace removed.</param>
        public void DoResponse(string callback, bool minified)
        {
            DoResponse(callback, "application/json", minified, true);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="callback">Wrap the JSON in a callback function. (Default: QueryString["callback"])</param>
        /// <param name="contentType">eg. "text/javascript" (Default: "applicaiton/json")</param>
        public void DoResponse(string callback, string contentType)
        {
            DoResponse(callback, contentType, false, true);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="callback">Wrap the JSON in a callback function. (Default: QueryString["callback"])</param>
        /// <param name="ContentType">eg. "text/javascript" (Default: "applicaiton/json")</param>
        /// <param name="minified">Default: False = return formatted string. True = return string with whitespace removed.</param>
        public void DoResponse(string callback, string ContentType, bool minified)
        {
            DoResponse(callback, ContentType, minified, true);
        }

        /// <summary>
        ///     Sends the current JSON back to the client via the current HttpResponse.
        ///     If completeResponse = True, the ASP.NET ThreadAbortException will be thrown. Either ignore it by ensuring DoResponse() is not inside a try{} OR explicitly handle the execepiton silently.
        /// </summary>
        /// <param name="callback">Wrap the JSON in a callback function. (Default: QueryString["callback"])</param>
        /// <param name="contentType">eg. "text/javascript" (Default: "applicaiton/json")</param>
        /// <param name="minified">Default: False = return formatted string. True = return string with whitespace removed.</param>
        /// <param name="completeResponse">Default: True = Send only the JSON and end the Response. False = Append the JSON and leave the Response open.</param>
        /// <param name="statusCode">Default: (int)HttpStatusCode.OK (200). HTTP Status Code to send in the response.</param>
        public void DoResponse(string callback, string contentType, bool minified, bool completeResponse, int statusCode = (int)HttpStatusCode.OK)
        {
            if (HttpContext.Current != null)
            {
                if (completeResponse)
                    HttpContext.Current.Response.Clear();

                HttpContext.Current.Response.ContentType = contentType;
                HttpContext.Current.Response.StatusCode = statusCode;
                HttpContext.Current.Response.Write(this.ToJSON(callback, minified));

                if (completeResponse)
                {
                    //The following doesn't throw an exception, but allows code execution to continue after this response.
                    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    
                    //The following ends execution but throws an exception, which should be ignored.
                    HttpContext.Current.Response.End();
                }
            }
            else
            {
                Console.WriteLine(this.ToJSON(callback, minified));
            }
        }

        /// <summary>
        ///    Returns the zero based index of the given value within this JSON. If it can't be found this will return -1.
        ///    If updateInternalIndex is set then after the value is found the Internal index will point to it. If it is not found the index will not change.
        /// </summary>
        /// <param name="value">value/key/substring to search for.</param>
        public int IndexOf(string value)
        {
            return IndexOf(value, 0, false);
        }

        /// <summary>
        ///    Returns the zero based index of the given value within this JSON. If it can't be found this will return -1.
        ///    If updateInternalIndex is set then after the value is found the Internal index will point to it. If it is not found the index will not change.
        /// </summary>
        /// <param name="value">value/key/substring to search for.</param>
        /// <param name="startIndex">Default: 0, Zero based index at wich to start the search.</param>
        public int IndexOf(string value, int startIndex)
        {
            return IndexOf(value, startIndex, false);
        }

        /// <summary>
        ///    Returns the zero based index of the given value within this JSON. If it can't be found this will return -1.
        ///    If updateInternalIndex is set then after the value is found the Internal index will point to it. If it is not found the index will not change.
        /// </summary>
        /// <param name="value">value/key/substring to search for.</param>
        /// <param name="updateInternalIndex">Default: false, Set to update the internal index if the value is found.</param>
        public int IndexOf(string value, bool updateInternalIndex)
        {
            return IndexOf(value, 0, updateInternalIndex);
        }

        /// <summary>
        ///    Returns the zero based index of the given value within this JSON. If it can't be found this will return -1.
        ///    If updateInternalIndex is set then after the value is found the Internal index will point to it. If it is not found the index will not change.
        /// </summary>
        /// <param name="value">value/key/substring to search for.</param>
        /// <param name="startIndex">Default: 0, Zero based index at wich to start the search.</param>
        /// <param name="updateInternalIndex">Default: false, Set to update the internal index if the value is found.</param>
        public int IndexOf(string value, int startIndex, bool updateInternalIndex)
        {
            if (this.IsArray)
            {
                for (int i = startIndex; i < _arrayData.Length; i++)
                {
                    if (_arrayData[i].IsString && _arrayData[i].String.Equals(value))
                    {
                        if (updateInternalIndex)
                            _internalIndex = i;
                        return i;
                    }
                }
                return -1;
            }
            else if (this.IsDictionary)
            {
                //int i = new List<String>(_dictData.Keys).IndexOf(value, startIndex); //TODO: _keys (Done?)
                int i = _keys.IndexOf(value, startIndex);
                if (updateInternalIndex && (i >= 0))
                    _internalIndex = i;
                return i;
            }
            else if (this.IsString)
            {
                int i = _stringData.IndexOf(value, startIndex);
                if (updateInternalIndex && (i >= 0))
                    _internalIndex = i;
                return i;
            }
            else
                return -1;
        }

        /// <summary>
        /// Saves the current JSON into a text file with the given name.
        /// </summary>
        /// <param name="filename">Target filename including relative path and extension.</param>
        public JSON Save(string filename)
        {
            try
            {
                if(HttpContext.Current != null)
                    File.WriteAllText(HttpContext.Current.Server.MapPath(filename), this.ToJSON());
                else
                    File.WriteAllText(filename, this.ToJSON());
            }
            catch (Exception e)
            {
                throw new JSONException($"Couldn't save JSON '{ filename }'", e);
            }

            return this;
        }

        #endregion

        #region JSON Magic

        /// <summary>
        /// Dynamically appends a scalar value to the end of the current Array.
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(string value)
        {
            return Add(new JSON(value, false));
        }

        /// <summary>
        /// Dynamically appends a JSON object to the end of the current JSON.
        /// </summary>
        /// <param name="value">JSON to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(JSON value)
        {
            if (this.IsArray || this.IsString || (this.Length == 0))
                this[this.Length] = value;
            else //If Dictionary, wrap it in a new array.
            {
                this._arrayData = new JSON[2];
                _arrayData[0] = new JSON(_dictData);
                _arrayData[1] = value;
                _dictData = null;
                _keys = null;
            }
            //else
            //    throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

            return this;
        }

        /// <summary>
        /// Dynamically appends a key and scalar value pair to the end of the current Dictionary.
        /// </summary>
        /// <param name="key">Key to append</param>
        /// <param name="value">Value to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(string key, string value)
        {
            return Add(key, new JSON(value, false));
        }

        /// <summary>
        /// Dynamically appends a key and JSON value pair to the end of the current Dictionary.
        /// </summary>
        /// <param name="key">Key to append</param>
        /// <param name="value">Value to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(string key, JSON value)
        {
            if (this.IsDictionary || (this.Length == 0))
                if (Contains(key))
                    this[key].Add(value);
                else
                    this[key] = value;
            else
                throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

            return this;
        }

        /// <summary>
        /// Dynamically appends a key and scalar value pair to the end of a Dictionary within this dictionary according to the given delimited path.
        /// </summary>
        /// <param name="path">Delimited path to the key to append</param>
        /// <param name="value">Value to append</param>
        /// <param name="Delimiter">Delimiting character allows creation of a JSON structure in one step. eg. '/' </param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(string path, string value, char Delimiter)
        {
            if (this.IsDictionary || (this.Length == 0))
            {
                if (!path.Contains(Delimiter.ToString()))
                    return this.Add(path, value);
                else
                {
                    if (this.Contains(path.Split(Delimiter)[0]))
                        this[path.Split(Delimiter)[0]].Add(path.Substring(path.IndexOf(Delimiter) + 1), value, Delimiter);
                    else
                        this.Add(path.Split(Delimiter)[0], new JSON().Add(path.Substring(path.IndexOf(Delimiter) + 1), value, Delimiter));
                }
            }
            else
                throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

            return this;
        }

        /// <summary>
        /// Dynamically appends a key and JSON value pair to the end of a Dictionary within this dictionary according to the given delimited path.
        /// </summary>
        /// <param name="path">Delimited path to the key to append</param>
        /// <param name="value">Value to append</param>
        /// <param name="Delimiter">Delimiting character allows creation of a JSON structure in one step. eg. '/' </param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Add(string path, JSON value, char Delimiter)
        {
            if (this.IsDictionary || (this.Length == 0))
            {
                if (!path.Contains(Delimiter.ToString()))
                    return this.Add(path, value);
                else
                {
                    if (this.Contains(path.Split(Delimiter)[0]))
                        this[path.Split(Delimiter)[0]].Add(path.Substring(path.IndexOf(Delimiter) + 1), value, Delimiter);
                    else
                        this.Add(path.Split(Delimiter)[0], new JSON().Add(path.Substring(path.IndexOf(Delimiter) + 1), value, Delimiter));
                }
            }
            else
                throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

            return this;
        }

        /// <summary>
        /// Dynamically joins 1 or more JSON objects to the end of the current JSON.
        /// (Duplicate dictionary keys will overide the existing entry).
        /// </summary>
        /// <param name="args">JSONs to concat</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Concat(params JSON[] args)
        {
            if (this.IsArray)
            {
                int originalLength = _arrayData.Length;
                foreach (JSON arg in args)
                    if (!IsNullOrEmpty(arg))
                    {
                        if (arg.IsArray)
                        {
                            JSON[] newArray = new JSON[_arrayData.Length + arg.Array.Length];
                            _arrayData.CopyTo(newArray, 0);
                            arg.Array.CopyTo(newArray, _arrayData.Length);
                            _arrayData = newArray;
                        }
                        else
                            this.Add(arg);
                    }

                for (int i = originalLength; i < _arrayData.Length; i++)
                    _arrayData[i].SetIndex(i).SetParent(this);
            }
            else if (this.IsDictionary)
            {
                foreach (JSON arg in args)
                    if (!IsNullOrEmpty(arg))
                    {
                        if (arg.IsDictionary)
                        {
                            foreach (JSON j in arg)
                                this[j.Key, true] = j;
                        }
                        else
                            throw new JSONException("Cannot mix array and dictionary concatinations.");
                    }
            }
            else if (this.IsNull)
            {
                if (args[0].IsDictionary)
                {
                    _dictData = new Dictionary<string, JSON>();
                    _keys = new List<string>();
                    this.Concat(args);
                }
                else
                {
                    _arrayData = new JSON[0];
                    this.Concat(args);
                }
            }
            else
            {
                _arrayData = new JSON[1];
                _arrayData[0] = new JSON(_stringData, false).SetIndex(0).SetParent(this);
                _stringData = null;
                this.Concat(args);
            }
            return this;
        }

        ///// <summary>
        ///// Dynamically appends a scalar value to the end of the current Array.
        ///// </summary>
        ///// <param name="value">Value to append</param>
        ///// <returns>This JSON. Enables a "Chain" to be used. </returns>
        //public JSON Insert(string value)
        //{
        //    if (this.IsArray || this.IsString || (this.Length == 0))
        //        this[this.Length] = new JSON(value, false);
        //    else
        //        throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

        //    return this;
        //}

        ///// <summary>
        ///// Dynamically appends a JSON object to the end of the current Array.
        ///// </summary>
        ///// <param name="value">JSON to append</param>
        ///// <returns>This JSON. Enables a "Chain" to be used. </returns>
        //public JSON Insert(JSON value)
        //{
        //    if (this.IsArray || this.IsString || (this.Length == 0))
        //        this[this.Length] = value;
        //    else
        //        throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

        //    return this;
        //}

        ///// <summary>
        ///// Dynamically appends a key and scalar value pair to the end of the current Dictionary.
        ///// </summary>
        ///// <param name="key">Key to append</param>
        ///// <param name="value">Value to append</param>
        ///// <returns>This JSON. Enables a "Chain" to be used. </returns>
        //public JSON Insert(string key, string value)
        //{
        //    if (this.IsDictionary || (this.Length == 0))
        //        this[key] = new JSON(value, false).SetKey(key);
        //    else
        //        throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

        //    return this;
        //}

        ///// <summary>
        ///// Dynamically inserts a key and JSON value pair into the current Dictionary.
        ///// </summary>
        ///// <param name="key">Key to append</param>
        ///// <param name="value">Value to append</param>
        ///// <returns>This JSON. Enables a "Chain" to be used. </returns>
        //public JSON Insert(int index, string key, JSON value)
        //{
        //    if (this.IsDictionary || (this.Length == 0))
        //        this[key] = value.SetKey(key);
        //    else
        //        throw new JSONException("Cannot mix array and dictionary 'Add Chains'.");

        //    return this;
        //}

        /// <summary>
        /// Dynamically removes a key/value/substring from this JSON.
        /// </summary>
        /// <param name="value">Value or Key to be removed</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Remove(string value)
        {
            if (this.IsArray)
            {
                int found = 0;
                for (int i = 0; i < this.Length; i++)
                {
                    if (_arrayData[i].String.Equals(value))
                        found++;
                    else if ((found > 0) && (i - found >= 0))
                        _arrayData[i - found] = _arrayData[i].SetIndex(i - found);
                }
                this.Length = this.Length - found;
            }
            else if (this.IsDictionary)
            {
                if (this._dictData.ContainsKey(value))
                {
                    this._dictData.Remove(value);
                    this._keys.Remove(value);
                }
            }
            else if (this.IsString)
                this._stringData.Replace(value, "");

            return this;
        }

        /// <summary>
        /// Dynamically removes given JSON from this JSON.
        /// </summary>
        /// <param name="value">JSON to be removed</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Remove(JSON value)
        {
            if (this.IsArray)
            {
                int found = 0;
                for (int i = 0; i < this.Length; i++)
                {
                    if (_arrayData[i].Equals(value))
                        found++;
                    else if ((found > 0) && (i - found >= 0))
                        _arrayData[i - found] = _arrayData[i].SetIndex(i - found);
                }
                this.Length = this.Length - found;
            }
            else if (this.IsDictionary)
            {
                if (this._dictData.ContainsKey(value.Key))
                {
                    this._dictData.Remove(value.Key);
                    this._keys.Remove(value.Key);
                }
            }
            else
                this._stringData.Replace(value.SafeString, "");

            return this;
        }
 
        /// <summary>
        /// Dynamically remove the given JSON structure from this JSON.
        /// </summary>
        /// <param name="values">Array of Values/Keys, or Dictionary structure to be removed</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON RemoveEach(JSON values)
        {
            if (values.IsArray)
            {
                foreach (JSON j in values)
                    Remove(j);
            }
            else if (values.IsDictionary)
            {
                foreach (JSON j in values)
                    if (this.IsDictionary && _dictData.ContainsKey(j.Key))
                        this[j.Key].Remove(j);
            }
            else if (values.IsString)
                Remove(values.String);

            return this;
        }

        /// <summary>
        ///     Dynamically filters for a list of keys/values/substrings found in this JSON.
        /// </summary>
        /// <param name="value">Comma seperated list of keys/values/substrings to be retained.</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Filter(string value)
        {
            value = Regex.Replace(value, @"(?<!\\),", "¦").Replace(@"\,", ","); //Allow escape of comma.
            string[] filters = value.Split('¦');

            if (this.IsArray)
            {
                int removed = 0;
                for (int i = 0; i < this.Length; i++)
                {
                    bool found = false;
                    foreach (string filter in filters)
                    {
                        if (filter.Equals(_arrayData[i].String, StringComparison.CurrentCultureIgnoreCase))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        removed++;
                    else if ((removed > 0) && (i - removed >= 0))
                        _arrayData[i - removed] = _arrayData[i].SetIndex(i - removed);
                }
                this.Length = this.Length - removed;
            }
            else if (this.IsDictionary)
            {
                List<string> keysToRemove = new List<string>();

                foreach (KeyValuePair<string, JSON> kv in _dictData)
                {
                    bool found = false;

                    foreach (string filter in filters)
                    {
                        if (filter.Equals(kv.Key, StringComparison.CurrentCultureIgnoreCase))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        keysToRemove.Add(kv.Key);
                }

                foreach (string key in keysToRemove)
                {
                    this._dictData.Remove(key);
                    this._keys.Remove(key);
                }
            }
            else if (this.IsString)
            {
                MatchCollection matches = Regex.Matches(this._stringData, value, RegexOptions.IgnoreCase);
                this._stringData = "";

                foreach (Match match in matches)
                    this._stringData += match.ToString();
            }

            return this;
        }

        /// <summary>
        ///     Dynamically filters for a list of keys/values/substrings found in this JSON.
        /// </summary>
        /// <param name="filter">JSON dictionary/list of keys/values/substrings to be retained.</param>
        /// <returns>This filtered JSON. Enables a "Chain" to be used. </returns>
        public JSON Filter(JSON filters)
        {
            if (this.IsArray)
            {
                int removed = 0;
                for(int i = 0; i < this.Length; i++)
                {
                    bool found = false;
                    if (filters.IsArray)
                    {
                        foreach (JSON j in filters)
                        {
                            if (_arrayData[i].SafeString.Equals(j.String))
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    else if(filters.IsDictionary)
                    {
                        //no specail behaviour.
                    }
                    else if (_arrayData[i].Equals(filters))
                    {
                        found = true;
                    }

                    if (!found && !_arrayData[i].Filter(filters).IsNull) //Perform sub-item filter
                        found = true;

                    //Remove items not found
                    if (!found)
                        removed++;
                    else if ((removed > 0) && (i - removed >= 0))
                        _arrayData[i - removed] = _arrayData[i].SetIndex(i - removed);
                }

            }
            else if (this.IsDictionary)
            {
                List<string> keysToRemove = new List<string>();

                foreach (KeyValuePair<string, JSON> kv in _dictData)
                {
                    bool found = false;

                    if (filters.IsArray)
                    {
                        foreach (JSON j in filters)
                        {
                            if (kv.Key.Equals(j.String))
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    else if (filters.IsDictionary)
                    {
                        foreach (JSON j in filters)
                        {
                            if (kv.Key.Equals(j.Key))
                            {
                                found = true;
                                kv.Value.Filter(j); //Perform sub-item filter.
                                break;
                            }
                        }
                    }
                    else if (kv.Key.Equals(filters.String))
                    {
                        found = true;
                    }

                    //Remove items not found
                    if (!found)
                        keysToRemove.Add(kv.Key);
                }

                foreach (string key in keysToRemove)
                {
                    _dictData.Remove(key);
                    _keys.Remove(key);
                }
            }
            else if (this.IsString)
            {

                if (filters.IsArray)
                {
                    foreach (JSON j in filters)
                    {
                        MatchCollection matches = Regex.Matches(_stringData, j.String);

                        _stringData = "";
                        foreach (Match match in matches)
                            _stringData += match.ToString();
                    }

                }
                else if (filters.IsDictionary)
                {
                    foreach (JSON j in filters)
                    {
                        MatchCollection matches = Regex.Matches(_stringData, j.Key);

                        _stringData = "";
                        foreach (Match match in matches)
                            _stringData += match.ToString();
                    }
                }
                else
                {
                    MatchCollection matches = Regex.Matches(_stringData, filters.SafeString);

                    _stringData = "";
                    foreach (Match match in matches)
                        _stringData += match.ToString();
                }
            }

            return this;
        }

        /// <summary>
        ///    Returns the first instance of a JSON with the given dictionary key within this JSON and all sub JSONs.
        /// </summary>
        public JSON Find(string key)
        {
            if (this.IsArray)
            {
                foreach (JSON j in _arrayData)
                {
                    JSON json = j.Find(key);
                    if (!json.IsNull)
                        return json;
                }
            }
            else if (this.IsDictionary)
            {
                if (_dictData.ContainsKey(key))
                    return _dictData[key];
                else
                {
                    //foreach (KeyValuePair<string, JSON> kv in _dictData) //TODO: _keys (Done?)
                    foreach (string k in _keys)
                    {
                        //JSON json = kv.Value.Find(key);
                        JSON json = _dictData[k].Find(key);
                        if (!json.IsNull)
                            return json;
                    }
                }
            }

            return new JSON();
        }

        /// <summary>
        ///    Returns the instance of a JSON at the given smart path within this JSON.
        /// </summary>
        /// <param name="path">The route of keys or indexes to the desired JSON. e.g. "FirstKey.SubKey.0"</param>
        /// <param name="Delimiter">Default: '.' Delimiting character that separates keys and indexes in the path.</param>
        /// <returns></returns>
        public JSON FindAt(string path)
        {
            return FindAt(path, '.');
        }

        /// <summary>
        ///    Returns the instance of a JSON at the given smart path within this JSON.
        /// </summary>
        public JSON FindAt(string path, char Delimiter)
        {
            int i = 0;
            if (!path.Contains(Delimiter.ToString()))
            {
                if (this.IsDictionary && this.Contains(path))
                    return this[path];
                else if (int.TryParse(path, out i))
                    return this[i];
            }
            else
            {
                string key = path.Split(Delimiter)[0];
                if (this.IsDictionary && this.Contains(key))
                    return this[key].FindAt(path.Substring(key.Length + 1), Delimiter);
                else if (int.TryParse(key, out i))
                    return this[i].FindAt(path.Substring(key.Length + 1), Delimiter);
            }
            //(else)
            return new JSON();
        }

        /// <summary>
        /// Dynamically clears all data from this JSON object.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Clear()
        {
            //Clear all content.
            _stringData = null;
            _arrayData = null;
            _dictData = null;
            _keys = null;
            _internalIndex = 0;
            _exp = null;

            //No need to reset this JSON's properties
            //  _dynamicAllocation
            //  _keyIfNull

            //Parental properties should not be altered as this JSON may already have a parent.
            //  _index
            //  _key
            //  _parent

            return this;
        }

        /// <summary>
        ///    Replaces contents of this JSON with a low level copy of j.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Copy(JSON j)
        {
            if (j.IsArray)
            {
                _stringData = null;
                _dictData = null;
                _keys = null;
                _arrayData = new JSON[j._arrayData.Length];
                for (int i = 0; i < j.Length; i++)
                    _arrayData[i] = new JSON().Copy(j[i]).SetIndex(i).SetParent(this);
            }
            else if (j.IsDictionary)
            {
                this._stringData = null;
                this._arrayData = null;
                this._dictData = new Dictionary<string, JSON>();
                foreach (KeyValuePair<string, JSON> kv in j.Dictionary)
                    this._dictData.Add(kv.Key, new JSON().Copy(kv.Value).SetKey(kv.Key));

                this._keys = new List<string>();
                foreach (string key in j._keys)
                    _keys.Add(key);
            }
            else
            {
                this._arrayData = null;
                this._dictData = null;
                this._keys = null;
                this._stringData = j.String;
            }

            //Copy properties.
            this._internalIndex = j._internalIndex;
            this._dynamicAllocation = j._dynamicAllocation;
            this._keyIfNull = j._keyIfNull;
            this._exp = j._exp;

            //Parental properties should not be altered as this JSON may already have a parent.
            //  _index
            //  _key
            //  _parent

            return this;
        }

        /// <summary>
        ///     Returns a low level copy of this JSON as a new JSON.
        /// </summary>
        public JSON Copy()
        {
            return new JSON().Copy(this);
        }

        /// <summary>
        ///    Replaces contents of this JSON with a high level copy of j. Updates to elements in j will update this JSON too. Any child elements will adopt this JSON as their new parent.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Clone(JSON j)
        {
            _stringData = j._stringData;
            _arrayData = j._arrayData;
            _dictData = j._dictData;
            _keys = j._keys;
            _internalIndex = j._internalIndex;
            _dynamicAllocation = j._dynamicAllocation;
            _keyIfNull = j._keyIfNull;
            if (_arrayData != null)
                foreach (JSON json in _arrayData)
                    json.SetParent(this);
            else if (_dictData != null)
                foreach (KeyValuePair<string, JSON> kvp in _dictData)
                    kvp.Value.SetParent(this);

            //Parental properties should not be altered as this JSON may already have a parent.
            //  _index
            //  _key
            //  _parent

            return this;
        }

        /// <summary>
        ///    Provides stack behaviour. Pushes the given scalar value onto the end of this JSON array. Eqivellent behaviour to "Add".  (Array based JSONs only) 
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Push(string value)
        {
            if (this.IsArray || this.IsString || (this.Length == 0))
                this[this.Length] = new JSON(value, false).SetIndex(this.Length).SetParent(this);
            else
                throw new JSONException("Cannot push a value into a non-array based JSON.");

            return this;
        }

        /// <summary>
        ///    Provides stack behaviour. Pushes the given scalar value onto the end of this JSON array. Eqivellent behaviour to "Add".  (Array based JSONs only)
        /// </summary>
        /// <param name="value">JSON to append</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Push(JSON value)
        {
            if (this.IsArray || this.IsString || (this.Length == 0))
                this[this.Length] = value.SetIndex(this.Length).SetParent(this);
            else
                throw new JSONException("Cannot push a value into a non-array based JSON.");

            return this;
        }

        /// <summary>
        ///    Provides stack behaviour. Peeks at the JSON at the top of the current stack.  (Array based JSONs only)
        /// </summary>
        /// <returns>The JSON at the top of the stack.</returns>
        public JSON Peek()
        {
            if (this.Length > 0 && !this.IsDictionary)
                return this[Length - 1];
            else
                return new JSON();
        }

        /// <summary>
        ///    Provides stack behaviour. Pops the JSON at off the top of the current stack.  (Array based JSONs only)
        /// </summary>
        /// <returns>The JSON that was at the top of the stack.</returns>
        public JSON Pop()
        {
            JSON j = new JSON();
            if (this.Length > 0 && !this.IsDictionary)
            {
                j.Copy(this[Length - 1]);
                this.Length = this.Length - 1;
            }

            return j;
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Sort()
        {
            return Sort(null, '.', false);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <param name="Path">The smart path of the sub Dictionary to sort by. e.g. "SortKey" or "Data.0.SortKey"</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Sort(string Path)
        {
            return Sort(Path, '.', false);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <param name="Path">The smart path of the sub Dictionary to sort by. e.g. "SortKey" or "Data.0.SortKey"</param>
        /// <param name="Delimiter">Default: '.'  Delimiting character that separates keys and indexes in the Smart Path. </param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Sort(string Path, char Delimiter)
        {
            return Sort(Path, Delimiter, false);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON SortDesc()
        {
            return Sort(null, '.', true);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <param name="Path">The smart path of the sub Dictionary to sort by. e.g. "SortKey" or "Data.0.SortKey"</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON SortDesc(string Path)
        {
            return Sort(Path, '.', true);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <param name="Path">The smart path of the sub Dictionary to sort by. e.g. "SortKey" or "Data.0.SortKey"</param>
        /// <param name="Delimiter">Default: '.'  Delimiting character that separates keys and indexes in the Smart Path. </param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON SortDesc(string Path, char Delimiter)
        {
            return Sort(Path, Delimiter, true);
        }

        /// <summary>
        ///     Sorts the contents of the current Array based or Dictionary based JSON into ascending order.
        ///     If a Smart Path parameter is given, it will sort the Array based on the contents of each sub JSON at the given Path.
        /// </summary>
        /// <param name="Path">The smart path of the sub Dictionary to sort by. e.g. "SortKey" or "Data.0.SortKey"</param>
        /// <param name="Delimiter">Default: '.'  Delimiting character that separates keys and indexes in the Smart Path. </param>
        /// <param name="Desc">Default: False = Ascending order. True = Descending order.</param>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Sort(string Path, char Delimiter, bool Desc)
        {
            int pass = this.Length - 1;
            bool done = false;
            while (pass > 0 && !done)
            {
                done = true;
                for (int i = 0; i < pass; i++)
                {
                    if (string.IsNullOrEmpty(Path))
                    {
                        JSON item = this[i];
                        JSON next = this[i + 1];
                        if (this.IsDictionary)
                        {
                            item = new JSON(item.Key);
                            next = new JSON(next.Key);
                        }

                        if ((item.IsNull || (item > next && !Desc) || (item < next && Desc)) && !next.IsNull) //Sort, with nulls always at the end.
                        {
                            this.Swap(i, i + 1);
                            done = false;
                        }
                    }
                    else
                    {
                        JSON item = this[i].FindAt(Path, Delimiter);
                        JSON next = this[i + 1].FindAt(Path, Delimiter);
                        if ((item.IsNull || (item > next && !Desc) || (item < next && Desc)) && !next.IsNull) //Sort, with nulls always at the end.
                        {
                            this.Swap(i, i + 1);
                            done = false;
                        }
                    }
                }
                pass--;
            }

            return this;
        }

        /// <summary>
        ///     Swaps position the JSONs stored at each of the given indexes.
        /// </summary>
        /// <returns>This JSON. Enables a "Chain" to be used. </returns>
        public JSON Swap(int indexA, int indexB)
        {
            if ((!this[indexA].IsNull || !this[indexB].IsNull) && this[indexA] != this[indexB])
            {
                if (this.IsArray)
                {
                    JSON tmp = this[indexA];
                    this[indexA] = this[indexB].SetIndex(indexA);
                    this[indexB] = tmp.SetIndex(indexB);
                }
                if (this.IsDictionary)
                {
                    string tmp = _keys[indexA];
                    _keys[indexA] = _keys[indexB];
                    _keys[indexB] = tmp;
                }
            }
            return this;
        }

        /// <summary>
        ///     Fetches the JSON with the highest value. If a key is supplied it will return the JSON containing the highest value with the given key.
        /// </summary>
        /// <returns>Returns the first JSON with the higest value.</returns>
        public JSON GetMax() { return GetMax(""); }

        /// <summary>
        ///     Fetches the JSON with the highest value. If a key is supplied it will return the JSON containing the highest value with the given key.
        /// </summary>
        /// <param name="key">The containing key to compare.</param>
        /// <returns>Returns the first JSON with the higest value.</returns>
        public JSON GetMax(string key)
        {
            JSON max = this[0];
            if (this.IsArray)
            {
                foreach (JSON j in this)
                    if (String.IsNullOrEmpty(key))
                    {
                        if (j > max)
                            max = j;
                    }
                    else
                    {
                        if (j[key] > max[key])
                            max = j;
                    }

                return max;
            }
            else
                return this;
        }

        /// <summary>
        ///     Fetches the JSON with the smallest value. If a key is supplied it will return the JSON containing the smallest value with the given key.
        /// </summary>
        /// <returns>Returns the first JSON with the smallest value.</returns>
        public JSON GetMin() { return GetMin(""); }

        /// <summary>
        ///     Fetches the JSON with the smallest value. If a key is supplied it will return the JSON containing the smallest value with the given key.
        /// </summary>
        /// <param name="key">The containing key to compare.</param>
        /// <returns>Returns the first JSON with the smallest value.</returns>
        public JSON GetMin(string key)
        {
            JSON min = this[0];
            if (this.IsArray)
            {
                foreach (JSON j in this)
                    if (String.IsNullOrEmpty(key))
                    {
                        if (j < min)
                            min = j;
                    }
                    else
                    {
                        if (j[key] < min[key])
                            min = j;
                    }

                return min;
            }
            else
                return this;
        }

        /// <summary>
        ///     Turns DynamicAllocation on.
        /// </summary>
        /// <returns>This JSON.</returns>
        public JSON SetDynamic()
        {
            _dynamicAllocation = true;
            return this;
        }

        /// <summary>
        ///     Turns DynamicAllocation on or off.
        /// </summary>
        /// <param name="dynamicAllocation">The desired state of DynamicAllocation.</param>
        /// <returns>This JSON.</returns>
        public JSON SetDynamic(bool dynamicAllocation)
        {
            _dynamicAllocation = dynamicAllocation;
            return this;
        }

        /// <summary>
        ///     <para>Pivots the current Array of Dictionaries around a given primaryKey.</para>
        ///     <para>This only works on an Array of Dictionaries.</para>
        /// </summary>
        /// <param name="primaryKey">A key in the sub Dictionaries whose value will become the Key for the pivoted JSON.</param>
        /// <returns>This JSON pivoted around the primaryKey</returns>
        public JSON Pivot(string primaryKey)
        {
            return Pivot(primaryKey, "", false);
        }

        /// <summary>
        ///     <para>Pivots the current Array of Dictionaries around a given primaryKey.</para>
        ///     <para>This only works on an Array of Dictionaries.</para>
        /// </summary>
        /// <param name="primaryKey">A key in the sub Dictionaries whose value will become the Key for the pivoted JSON.</param>
        /// <param name="append">True=Duplicate primaryKeys will be appended to an Array. False=Duplicated primaryKeys will replace the existing JSON (Default).</param>    
        /// <returns>This JSON pivoted around the primaryKey</returns>
        public JSON Pivot(string primaryKey, bool append)
        {
            return Pivot(primaryKey, "", append);
        }

        /// <summary>
        ///     <para>Pivots the current Array of Dictionaries around a given primaryKey.</para>
        ///     <para>This only works on an Array of Dictionaries.</para>
        /// </summary>
        /// <param name="primaryKey">A key in the sub Dictionaries whose value will become the Key for the pivoted JSON.</param>
        /// <param name="valueKey">A key in the sub Dictionaries whose value will become the Value for the pivoted JSON. If ommitted, the entire sub Dictionary (without the primaryKey) is used.</param>
        /// <returns>This JSON pivoted around the primaryKey</returns>
        public JSON Pivot(string primaryKey, string valueKey)
        {
            return Pivot(primaryKey, valueKey, false);
        }

        /// <summary>
        ///     <para>Pivots the current Array of Dictionaries around a given primaryKey.</para>
        ///     <para>This only works on an Array of Dictionaries.</para>
        /// </summary>
        /// <param name="primaryKey">A key in the sub Dictionaries whose value will become the Key for the pivoted JSON.</param>
        /// <param name="valueKey">A key in the sub Dictionaries whose value will become the Value for the pivoted JSON. If ommitted, the entire sub Dictionary (without the primaryKey) is used.</param>
        /// <param name="append">Default: False. True = Duplicate primaryKeys will be appended to an Array. False = Duplicated primaryKeys will replace the existing JSON.</param>    
        /// <returns>This JSON pivoted around the primaryKey</returns>
        public JSON Pivot(string primaryKey, string valueKey, bool append)
        {
            bool DA = _dynamicAllocation;
            _dynamicAllocation = true;
            if (this.Length > 0)
            {
                JSON json = new JSON().SetDynamic();

                int other = 1;

                while (!this.IsOutOfRange)
                {
                    if (!this[false].IsDictionary)
                        throw new JSONException("Unable to pivot on primaryKey. This only works on an Array of Dictionaries.");

                    string newKey = this[false][primaryKey].String ?? $"_other{ other++ }";

                    if (append)
                        if (string.IsNullOrEmpty(valueKey))
                            json[newKey].Add(this[true].Remove(primaryKey));
                        else
                            json[newKey].Add(this[true][valueKey]);
                    else
                        if (string.IsNullOrEmpty(valueKey))
                        json[newKey] = this[true].Remove(primaryKey);
                    else
                        json[newKey] = this[true][valueKey];
                }

                this.Copy(json);
            }

            _dynamicAllocation = DA;
            return this;            
        }

        /// <summary>
        ///     <para>Pivots the current Array or Dictionary based JSON around its sub JSONs. e.g. a 3x4 Array will become a 4x3 array.</para>
        ///     <para>Each sub JSON must be of the same type.</para>
        /// </summary>
        /// <returns>This JSON after pivot.</returns>
        public JSON Pivot()
        {
            _dynamicAllocation = true;
            if (this.Length > 0)
            {
                JSON json = new JSON().SetDynamic();

                if (this.IsArray)
                {
                    if (this[0].IsArray)
                    {
                        while (!this.IsOutOfRange)
                        {
                            if (!this[false].IsArray)
                                throw new JSONException("Unable to pivot a JSON of varying types.");

                            json[this[false]._internalIndex][_internalIndex] = this[IsCOOR][true];
                        }
                        this.Copy(json);
                    }
                    else if (this[0].IsDictionary)
                    {
                        while (!this.IsOutOfRange)
                        {
                            if (!this[false].IsDictionary)
                                throw new JSONException("Unable to pivot a JSON of varying types.");

                            json[this[false][false]._key][_internalIndex] = this[IsCOOR][true];
                        }
                        this.Copy(json);
                    }
                }
                else if (this.IsDictionary)
                {
                    if (this[0].IsArray)
                    {
                        while (!this.IsOutOfRange)
                        {
                            if (!this[false].IsArray)
                                throw new JSONException("Unable to pivot a JSON of varying types.");

                            json[this[false]._internalIndex][this[false]._key] = this[IsCOOR][true];
                        }
                        this.Copy(json);
                    }
                    else if (this[0].IsDictionary)
                    {
                        while (!this.IsOutOfRange)
                        {
                            if (!this[false].IsDictionary)
                                throw new JSONException("Unable to pivot a JSON of varying types.");

                            json[this[false][false]._key][this[false]._key] = this[IsCOOR][true];
                        }
                        this.Copy(json);
                    }
                }
            }

            _dynamicAllocation = false;
            return this;
        }

        #endregion

        #region JSON Operators

        /// <summary>
        /// Returns true if j > k.
        /// </summary>
        public static bool operator >(JSON j, JSON k)
        {
            if (object.ReferenceEquals(j, null) || j.IsNull)
                return false;
            else if (object.ReferenceEquals(k, null) || k.IsNull)
                return true;
            else if (!j.IsString && !k.IsString)
                return j.Length > k.Length; //Operator
            else if (j.IsNumber || k.IsNumber)
                return j.Number > k.Number; //Operator
            else if (j.IsDateTime && k.IsDateTime)
                return j.DateTime > k.DateTime; //Operator
            else if (j.IsBoolean && k.IsBoolean)
                return j.Boolean && !k.Boolean; //psudo operator
            else if (j.IsString && k.IsString)
            {
                int i = 0;
                while (i < j.String.Length && i < k.String.Length && j.String[i] == k.String[i])
                    i++;

                if (i < j.String.Length && i < k.String.Length)
                    return j.String[i] > k.String[i]; //Operator
                else
                    return j.String.Length > k.String.Length; //Operator
            }
            else
                return false;
        }

        /// <summary>
        /// Returns true if j < k
        /// </summary>
        public static bool operator <(JSON j, JSON k)
        {
            if (object.ReferenceEquals(j, null) || j.IsNull)
                return false;
            else if (object.ReferenceEquals(k, null) || k.IsNull)
                return true;
            else if (!j.IsString && !k.IsString)
                return j.Length < k.Length; //Operator
            else if (j.IsNumber || k.IsNumber)
                return j.Number < k.Number; //Operator
            else if (j.IsDateTime && k.IsDateTime)
                return j.DateTime < k.DateTime; //Operator
            else if (j.IsBoolean && k.IsBoolean)
                return !j.Boolean && k.Boolean; //psudo operator
            else if (j.IsString && k.IsString)
            {
                int i = 0;
                while (i < j.String.Length && i < k.String.Length && j.String[i] == k.String[i])
                    i++;

                if (i < j.String.Length && i < k.String.Length)
                    return j.String[i] < k.String[i]; //Operator
                else
                    return j.String.Length < k.String.Length; //Operator
            }
            else
                return false;
        }

        /// <summary>
        /// Returns true if j >= k.
        /// </summary>
        public static bool operator >=(JSON j, JSON k)
        {
            if (object.ReferenceEquals(j, null) || j.IsNull)
                return (object.ReferenceEquals(k, null) || k.IsNull);
            else if (object.ReferenceEquals(k, null) || k.IsNull)
                return true;
            else if (!j.IsString && !k.IsString)
                return j.Length >= k.Length; //Operator
            else if (j.IsNumber || k.IsNumber)
                return j.Number >= k.Number; //Operator
            else if (j.IsDateTime && k.IsDateTime)
                return j.DateTime >= k.DateTime; //Operator
            else if (j.IsBoolean && k.IsBoolean)
                return !k.Boolean; //psudo operator
            else if (j.IsString && k.IsString)
            {
                int i = 0;
                while (i < j.String.Length && i < k.String.Length && j.String[i] == k.String[i])
                    i++;

                if (i < j.String.Length && i < k.String.Length)
                    return j.String[i] >= k.String[i]; //Operator
                else
                    return j.String.Length >= k.String.Length; //Operator
            }
            else
                return false;
        }

        /// <summary>
        /// Returns true if j <= k.
        /// </summary>
        public static bool operator <=(JSON j, JSON k)
        {
            if (object.ReferenceEquals(j, null) || j.IsNull)
                return (object.ReferenceEquals(k, null) || k.IsNull);
            else if (object.ReferenceEquals(k, null) || k.IsNull)
                return true;
            else if (!j.IsString && !k.IsString)
                return j.Length <= k.Length; //Operator
            else if (j.IsNumber || k.IsNumber)
                return j.Number <= k.Number; //Operator
            else if (j.IsDateTime && k.IsDateTime)
                return j.DateTime <= k.DateTime; //Operator
            else if (j.IsBoolean && k.IsBoolean)
                return !j.Boolean; //psudo operator
            else if (j.IsString && k.IsString)
            {
                int i = 0;
                while (i < j.String.Length && i < k.String.Length && j.String[i] == k.String[i])
                    i++;

                if (i < j.String.Length && i < k.String.Length)
                    return j.String[i] <= k.String[i]; //Operator
                else
                    return j.String.Length <= k.String.Length; //Operator
            }
            else
                return false;
        }

        /// <summary>
        /// Returns true if j == k.
        /// </summary>
        public static bool operator ==(JSON j, JSON k)
        {
            if (object.ReferenceEquals(j, null) || j.IsNull)
                return (object.ReferenceEquals(k, null) || k.IsNull);
            else if (object.ReferenceEquals(k, null) || k.IsNull)
                return (object.ReferenceEquals(j, null) || j.IsNull);
            else if (!j.IsString && !k.IsString)
            {
                if (j.Length != k.Length)
                    return false;

                for (int i = 0; i < j.Length; i++)
                    if (!(j[i] == k[i]))
                        return false;

                return true;
            }
            else if (j.IsNumber || k.IsNumber)
                return j.Number == k.Number; //Operator
            else if (j.IsDateTime && k.IsDateTime)
                return j.DateTime == k.DateTime; //Operator
            else if (j.IsString && k.IsString)
                return j.String == k.String; //Operator
            else
                return false;
        }

        /// <summary>
        /// Returns true if j != k.
        /// </summary>
        public static bool operator !=(JSON j, JSON k)
        {
            return !(j == k);
        }

        /// <summary>
        /// Returns true if the contents of this JSON matches exactly the contents of the given object.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, null))
                return this.IsNull;
            if (obj.GetType().Name == "JSON")
                return this == (JSON)obj;
            else
                return this == new JSON(obj);
        }

        /// <summary>
        /// Returns true if the given JSON is a reference to the same data as the current JSON.
        /// This is equivalent to object.ReferenceEquals(this, j);
        /// </summary>
        public bool Is(JSON j)
        {
            return object.ReferenceEquals(this, j);
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        public override int GetHashCode()
        {
            if(_arrayData != null)
                return _arrayData.GetHashCode();
            else if(_dictData != null)
                return _dictData.GetHashCode();
            else if(_stringData != null)
                return _stringData.GetHashCode();
            else
                return 0;
        }

        /// <summary>
        /// Returns an IEnumerator for the JSON.
        /// </summary>
        IEnumerator<JSON> IEnumerable<JSON>.GetEnumerator()
        {
            if (this.IsArray)
                foreach (JSON j in _arrayData)
                    yield return j;
            else if (this.IsDictionary)
                //foreach (KeyValuePair<string, JSON> kvp in _dictData) //TODO: _keys (Done?)
                foreach (string k in _keys)
                    //yield return kvp.Value;
                    yield return _dictData[k];
            else if (this.IsString)
                yield return this;
        }

        /// <summary>
        /// Returns an IEnumerator for the JSON.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            if (this.IsArray)
                foreach (JSON j in _arrayData)
                    yield return j;
            else if (this.IsDictionary)
                //foreach (KeyValuePair<string, JSON> kvp in _dictData) //TODO: _keys (Done?)
                foreach (string k in _keys)
                    //yield return kvp.Value;
                    yield return _dictData[k];
            else if (this.IsString)
                yield return this;
        }

        #endregion

        #region JSON Library

        /// <summary>
        /// Creates a JSON from a basic URL using a YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="url">select * from html where url=[url]</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromURL(string url)
        {
            return CreateFromURL(url, "", false);
        }

        /// <summary>
        /// Creates a JSON from a basic URL using a YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="url">select * from html where url=[url]</param>
        /// <param name="incDiag">Default: false, Set to true if you want to include the diagnostic information from YQL.</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromURL(string url, bool incDiag)
        {
            return CreateFromURL(url, "", incDiag);
        }

        /// <summary>
        /// Creates a JSON from a basic URL using a YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="url">select * from html where url=[url]</param>
        /// <param name="xpath">select * from html where url=[url] and xpath=[xpath]</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromURL(string url, string xpath)
        {
            return CreateFromURL(url, xpath, false);
        }

        /// <summary>
        /// Creates a JSON from a basic URL using a YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="url">select * from html where url=[url]</param>
        /// <param name="xpath">select * from html where url=[url] and xpath=[xpath]</param>
        /// <param name="incDiag">Default: false, Set to true if you want to include the diagnostic information from YQL.</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromURL(string url, string xpath, bool incDiag)
        {
            return CreateFromYQL($"select * from html where url=\"{ url }{ (string.IsNullOrEmpty(xpath) ? "" : $"\" and xpath=\"{ xpath }") }\"", incDiag);
        }

        /// <summary>
        /// Creates a JSON from a plain YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="query">eg. "select * from html where url=[url]"</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromYQL(string query)
        {
            return CreateFromYQL(query, false);
        }

        /// <summary>
        /// Creates a JSON from a plain YQL query.
        /// See http://developer.yahoo.com/yql/ for more information.
        /// </summary>
        /// <param name="query">eg. "select * from html where url=[url]"</param>
        /// <param name="incDiag">Default: false, Set to true if you want to include the diagnostic information from YQL.</param>
        [Obsolete("Yahoo have no longer support YQL.", true)]
        public static JSON CreateFromYQL(string query, bool incDiag)
        {
            query = System.Web.HttpUtility.UrlEncode(query);
            WebRequest req = WebRequest.Create($"http://query.yahooapis.com/v1/public/yql?q={ query }&format=json{ (incDiag ? "&diagnostics=true" : "") }");
            JSON j = new JSON();
            try
            {
                StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream());
                j = new JSON(reader.ReadToEnd());
            }
            catch (Exception e)
            {
                throw new JSONException($"YQL request failed: { e.Message }", e);
            }

            if (j["error"].Length > 0)
                throw new JSONException($"YQL parse failed: { j["error"]["description"].String }");
            return incDiag ? j : j[0]["results"];
        }



        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        public static JSON FromDataRow(DataRow dataRow)
        {
            return FromDataRow(dataRow, true, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        public static JSON FromDataRow(DataRow dataRow, bool ColsInTable)
        {
            return FromDataRow(dataRow, ColsInTable, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataRow(DataRow dataRow, string ValIfNull)
        {
            return FromDataRow(dataRow, true, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataRow(DataRow dataRow, char Delimeter)
        {
            return FromDataRow(dataRow, false, null, Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataRow(DataRow dataRow, bool ColsInTable, string ValIfNull)
        {
            return FromDataRow(dataRow, ColsInTable, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataRow(DataRow dataRow, string ValIfNull, char Delimeter)
        {
            return FromDataRow(dataRow, false, ValIfNull, Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataRow.
        /// </summary>
        /// <param name="dataRow">Input DataRow</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataRow(DataRow dataRow, bool ColsInTable, string ValIfNull, char Delimeter)
        {
            JSON j = new JSON();
            if (dataRow != null)
            {
                if (ColsInTable)
                {
                    j._arrayData = new JSON[dataRow.Table.Columns.Count];
                    for (int i = 0; i < dataRow.Table.Columns.Count; i++)
                    {
                        object val = dataRow[i];
                        j._arrayData[i] = new JSON((val == DBNull.Value) ? ValIfNull : val).SetIndex(i);
                    }
                }
                else
                {
                    j._dictData = new Dictionary<string, JSON>();
                    j._keys = new List<string>();
                    foreach (DataColumn dataCol in dataRow.Table.Columns)
                    {
                        object val = dataRow[dataCol.ColumnName];
                        if(Delimeter == 0)
                            j.Add(dataCol.ColumnName, new JSON((val == DBNull.Value) ? ValIfNull : val));
                        else
                            j.Add(dataCol.ColumnName, new JSON((val == DBNull.Value) ? ValIfNull : val), Delimeter);
                    }
                }
            }
            return j;
        }


        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        public static JSON FromDataTable(DataTable dataTable)
        {
            return FromDataTable(dataTable, true, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        public static JSON FromDataTable(DataTable dataTable, bool ColsInTable)
        {
            return FromDataTable(dataTable, ColsInTable, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataTable(DataTable dataTable, string ValIfNull)
        {
            return FromDataTable(dataTable, true, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataTable(DataTable dataTable, char Delimeter)
        {
            return FromDataTable(dataTable, false, null, Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataTable(DataTable dataTable, bool ColsInTable, string ValIfNull)
        {
            return FromDataTable(dataTable, ColsInTable, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataTable(DataTable dataTable, string ValIfNull, char Delimeter)
        {
            return FromDataTable(dataTable, false, ValIfNull, Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataTable.
        /// </summary>
        /// <param name="dataTable">Input DataTable</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataTable(DataTable dataTable, bool ColsInTable, string ValIfNull, char Delimeter)
        {
            JSON j = new JSON();
            if (dataTable != null)
            {
                j._dictData = new Dictionary<string, JSON>();
                j._keys = new List<string>();

                if (ColsInTable)
                {
                    j.Add("columns", EmptyArray());
                    foreach (DataColumn col in dataTable.Columns)
                        j["columns"].Add(col.ColumnName);
                }

                j.Add("rows", EmptyArray());
                foreach (DataRow row in dataTable.Rows)
                    j["rows"].Add(FromDataRow(row, ColsInTable, ValIfNull, Delimeter));

                //if (ColsInTable)
                //{
                //    j._dictData = new Dictionary<string, JSON>();
                //    j._keys = new List<string>();
                //    j.Add("columns", EmptyArray());
                //    foreach (DataColumn col in dataTable.Columns)
                //        j["columns"].Add(col.ColumnName);
                //    j.Add("rows", EmptyArray());
                //    foreach (DataRow row in dataTable.Rows)
                //        j["rows"].Add(FromDataRow(row, ColsInTable, ValIfNull, Delimeter));
                //}
                //else
                //{
                //    j._arrayData = new JSON[dataTable.Rows.Count];
                //    for (int i = 0; i < dataTable.Rows.Count; i++)
                //    {
                //        j._arrayData[i] = FromDataRow(dataTable.Rows[i], ColsInTable, ValIfNull, Delimeter).SetIndex(i).SetParent(j);
                //    }
                //}
            }
            return j;
        }


        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        public static JSON FromDataSet(DataSet dataSet)
        {
            return FromDataSet(dataSet, true, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        public static JSON FromDataSet(DataSet dataSet, bool ColsInTable)
        {
            return FromDataSet(dataSet, ColsInTable, null, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataSet(DataSet dataSet, string ValIfNull)
        {
            return FromDataSet(dataSet, true, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataSet(DataSet dataSet, char Delimeter)
        {
            return FromDataSet(dataSet, false, null,  Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        public static JSON FromDataSet(DataSet dataSet, bool ColsInTable, string ValIfNull)
        {
            return FromDataSet(dataSet, ColsInTable, ValIfNull, (char)0);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataSet(DataSet dataSet, string ValIfNull, char Delimeter)
        {
            return FromDataSet(dataSet, false, ValIfNull, Delimeter);
        }

        /// <summary>
        ///     Creates a JSON from the given DataSet.
        /// </summary>
        /// <param name="dataSet">Input DataSet</param>
        /// <param name="ColsInTable">Default: True. When true, column name information will be stored at the table level, so this will generate an array of row values. When false, this will create a dictionary using column names as keys.</param>
        /// <param name="ValIfNull">Value to use if DB data is null.</param>
        /// <param name="Delimeter">Path delimeter in column names used to separate out sub JSON objects. (Cols in rows only).</param>
        public static JSON FromDataSet(DataSet dataSet, bool ColsInTable, string ValIfNull, char Delimeter)
        {
            JSON j = new JSON();
            if (dataSet != null)
            {
                j._dictData = new Dictionary<string, JSON>();
                j._keys = new List<string>();
                foreach(DataTable table in dataSet.Tables)
                    j.Add(table.TableName, FromDataTable(table, ColsInTable, ValIfNull, Delimeter));
            }
            return j;
        }

        /// <summary>
        /// Decodes the given Base64 encoded string and returns it as a JSON object.
        /// </summary>
        /// <param name="encodedJSON">Base64 encoded JSON string.</param>
        public static JSON FromEncoded(string encodedJSON)
        {
            return new JSON(Cryptography.Crypto.Decode(encodedJSON));
        }

        /// <summary>
        /// Decrypts the given encrypted string and returns it as a JSON object.
        /// </summary>
        /// <param name="encryptedJSON">Encrypted JSON string.</param>
        public static JSON FromEncrypted(string encryptedJSON)
        {
            return new JSON(Cryptography.Crypto.Decrypt(encryptedJSON));
        }

        /// <summary>
        /// Decrypts the given encrypted string and returns it as a JSON object.
        /// </summary>
        /// <param name="encryptedJSON">Encrypted JSON string.</param>
        /// <param name="password">Default: ConfigurationManager.AppSettings["EncryptionKey"]. This key must match the key used to encrypt the text.</param>
        public static JSON FromEncrypted(string encryptedJSON, string password)
        {
            return new JSON(Cryptography.Crypto.Decrypt(encryptedJSON, password));
        }

        /// <summary>
        /// Creates an empty Array based JSON.
        /// </summary>
        public static JSON EmptyArray()
        {
            return new JSON(new JSON[0]);
        }

        /// <summary>
        /// Creates an empty Dictionary based JSON.
        /// </summary>
        public static JSON EmptyDictionary()
        {
            return new JSON(new Dictionary<string, JSON>());
        }


        //Persist same session cookie over all requests.
        /// <summary>
        /// This CookieContainer maintains the session for all HTTP calls. This can be shared with external calls, or overridden to change sessions.
        /// </summary>
        public static CookieContainer cookies;

        /// <summary>
        /// Creates a JSON from given json file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        public static JSON Load(string url)
        {
            return Load(url, null, null, 30000, new JSON(), null);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        public static JSON Load(string url, int timeout)
        {
            return Load(url, null, null, timeout, new JSON(), null);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        public static JSON Load(string url, JSON headers)
        {
            return Load(url, null, null, 30000, headers, null);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Load(string url, Action<JSON> callback)
        {
            return Load(url, null, null, 30000, new JSON(), callback);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        public static JSON Load(string url, int timeout, JSON headers)
        {
            return Load(url, null, null, timeout, headers, null);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Load(string url, JSON headers, Action<JSON> callback)
        {
            return Load(url, null, null, 30000, headers, callback);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Load(string url, int timeout, Action<JSON> callback)
        {
            return Load(url, null, null, timeout, new JSON(), callback);
        }

        /// <summary>
        /// Creates a JSON from given JSON file url.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="postData">When populated, this will become a POST request with the given "application/x-www-form-urlencoded" postData. (Default: null. When postData and jsonData is null, this will create a GET request.)</param>
        /// <param name="jsonData">When populated, this will become a POST request with the given "application/json" body. (Default: null. When postData and jsonData is null, this will create a GET request.)</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        /// <param name="method">HTTP Method to use for this request. When populated this will override the automatically selected method of "GET" (or "POST" if post data is supplied).  Valid methods include "GET", "POST", "PUT", "PATCH" and "DELETE".</param>
        public static JSON Load(string url, byte[] postData = null, JSON jsonData = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null, string method = null)
        {
            JSON j = new JSON();
            try
            {
                if (File.Exists(url)) //If url is local file path.
                    j = new JSON(File.ReadAllText(url));
                else if (!url.StartsWith("http") && HttpContext.Current != null && File.Exists(HttpContext.Current.Server.MapPath(url))) //If url points to a path on this server.
                    j = new JSON(File.ReadAllText(HttpContext.Current.Server.MapPath(url)));
                else if (url.StartsWith("http") || url.StartsWith("/") || url.StartsWith("~"))
                {
                    if (cookies == null)
                        cookies = new CookieContainer();

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                    //Setup request
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                    req.CookieContainer = cookies;
                    req.Timeout = timeout;
                    req.KeepAlive = false;

                    //Default headers
                    req.Accept = "application/json";

                    //Add headers
                    foreach (JSON header in JSON.Safe(headers))
                    {
                        if (headers.IsArray)
                            req.Headers.Add(header.String);
                        else if (headers.IsDictionary)
                            req.Headers.Add(header.Key, header.String);
                    }

                    if (postData != null)
                    {
                        req.Method = method ?? "POST";
                        req.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                        req.ContentLength = postData.Length;

                        using (Stream dataStream = req.GetRequestStream())
                            dataStream.Write(postData, 0, postData.Length);
                    }
                    else if(!JSON.IsNullOrEmpty(jsonData))
                    {
                        postData = Encoding.UTF8.GetBytes(jsonData.ToString());

                        req.Method = method ?? "POST";
                        req.ContentType = "application/json; charset=utf-8";
                        req.ContentLength = postData.Length;

                        using (Stream dataStream = req.GetRequestStream())
                            dataStream.Write(postData, 0, postData.Length);
                    }
                    else
                    {
                        req.Method = method ?? "GET";
                        req.ContentType = "application/json; charset=utf-8";
                        req.ContentLength = 0;
                    }

                    //Get response
                    if (callback == null)
                    {
                        using (WebResponse res = req.GetResponse())
                            using (Stream str = res.GetResponseStream())
                                using (StreamReader reader = new StreamReader(str))
                                    j = new JSON(reader.ReadToEnd());

                        //Clear request so that GC knows we're done with it.
                        req = null;
                        GC.Collect();
                    }
                    else //Async
                    {
                        Action call = () =>
                        {
                            req.BeginGetResponse(
                                new AsyncCallback((result) =>
                                {
                                    try
                                    {
                                        using (WebResponse res = ((HttpWebRequest)result.AsyncState).EndGetResponse(result))
                                            using (Stream str = res.GetResponseStream())
                                                using (StreamReader reader = new StreamReader(str))
                                                    callback(j.Add("response", new JSON(reader.ReadToEnd())));
                                    }
                                    catch (WebException e)
                                    {
                                        callback(new JSON(e));
                                    }
                                    catch (Exception e)
                                    {
                                        callback(new JSON(new JSONException($"Couldn't load JSON from: { url }", e)));
                                    }
                                }
                            ), req);
                        };

                        call.BeginInvoke(
                            new AsyncCallback((result) =>
                            {
                                Action action = (Action)result.AsyncState;
                                action.EndInvoke(result);
                            }
                        ), call);

                        j = new JSON("success", "true").Add("request", url).Add("sent", new JSON(DateTime.Now));
                    }
                }
                else
                    throw new FileNotFoundException("File not found.");
            }
            catch (WebException e)
            {
                j = new JSON(e);
            }
            catch (Exception e)
            {
                j = new JSON(new JSONException($"Couldn't load JSON from: { url }", e));
            }
            finally { }

            return j;
        }


        /// <summary>
        /// Creates a JSON from given API GET Request.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="parameters">A JSON dictionary containing parameters for the query string.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Get(string url, JSON parameters = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null)
        {
            return Load($"{ url }{ JSON.Safe(parameters).ToQueryString(url.Contains("?") ? "&" : "?")}", null, null, timeout, headers, callback, "GET");
        }

        /// <summary>
        /// Creates a JSON from given API POST request.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="formData">A JSON dictionary containing parameters which will be converted to UTF8 bytes and sent as POST data in "application/x-www-form-urlencoded" format.</param>
        /// <param name="jsonData">A JSON object containing data which will be sent as the POST body in "application/json" format.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Post(string url, JSON formData = null, JSON jsonData = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null)
        {
            if(!JSON.IsNullOrEmpty(jsonData))
                return Load(url, null, jsonData, timeout, headers, callback, "POST");
            else
                return Load(url, Encoding.UTF8.GetBytes(JSON.Safe(formData).ToQueryString("")), null, timeout, headers, callback, "POST");
        }

        /// <summary>
        /// Creates a JSON from given API PUT request.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="formData">A JSON dictionary containing parameters which will be converted to UTF8 bytes and sent as POST data in "application/x-www-form-urlencoded" format.</param>
        /// <param name="jsonData">A JSON object containing data which will be sent as the POST body in "application/json" format.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Put(string url, JSON formData = null, JSON jsonData = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null)
        {
            if (!JSON.IsNullOrEmpty(jsonData))
                return Load(url, null, jsonData, timeout, headers, callback, "PUT");
            else
                return Load(url, Encoding.UTF8.GetBytes(JSON.Safe(formData).ToQueryString("")), null, timeout, headers, callback, "PUT");
        }

        /// <summary>
        /// Creates a JSON from given API PATCH request.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="formData">A JSON dictionary containing parameters which will be converted to UTF8 bytes and sent as POST data in "application/x-www-form-urlencoded" format.</param>
        /// <param name="jsonData">A JSON object containing data which will be sent as the POST body in "application/json" format.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Patch(string url, JSON formData = null, JSON jsonData = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null)
        {
            if (!JSON.IsNullOrEmpty(jsonData))
                return Load(url, null, jsonData, timeout, headers, callback, "PATCH");
            else
                return Load(url, Encoding.UTF8.GetBytes(JSON.Safe(formData).ToQueryString("")), null, timeout, headers, callback, "PATCH");
        }
        
        /// <summary>
        /// Creates a JSON from given API DELETE request.
        /// </summary>
        /// <param name="url">URL of the json page or file.</param>
        /// <param name="formData">A JSON dictionary containing parameters which will be converted to UTF8 bytes and sent as POST data in "application/x-www-form-urlencoded" format.</param>
        /// <param name="jsonData">A JSON object containing data which will be sent as the POST body in "application/json" format.</param>
        /// <param name="timeout">Request timeout in milliseconds (Default: 30000).</param>
        /// <param name="headers">A dictionary of Key Value pairs, or Array of headers.</param>
        /// <param name="callback">If a callback Action is supplied Load will be executed asynchrounously. e.g. private void Callback(JSON response) {}</param>
        public static JSON Delete(string url, JSON formData = null, JSON jsonData = null, int timeout = 30000, JSON headers = null, Action<JSON> callback = null)
        {
            if (!JSON.IsNullOrEmpty(jsonData))
                return Load(url, null, jsonData, timeout, headers, callback, "DELETE");
            else
                return Load(url, Encoding.UTF8.GetBytes(JSON.Safe(formData).ToQueryString("")), null, timeout, headers, callback, "DELETE");
        }



        /// <summary>
        /// Returns true when the given JSON is either null or IsNull or IsEmpty.
        /// </summary>
        public static bool IsNullOrEmpty(JSON json)
        {
            return object.ReferenceEquals(json, null) || json.IsNull || json.IsEmpty;
        }

        /// <summary>
        /// Returns true when the given JSON is a reference to a valid JSON object.
        /// </summary>
        public static bool IsDefined(JSON json)
        {
            return !object.ReferenceEquals(json, null);
        }

        /// <summary>
        /// Returns the first not null and JSON from the supplied list of JSONs. If all given JSONs are null, this will return an empty JSON.
        /// </summary>
        public static JSON Safe(params JSON[] json)
        {
            foreach (JSON j in json)
            {
                if (j != null && !j.IsNull)
                {
                    return j;
                } 
            }

            return new JSON();
        }

        #endregion
    }

    /// <summary>
    /// JSONExceptions are returned when there is an error within a JSON object.
    /// </summary>
    public class JSONException : Exception
    {
        /// <summary>
        /// Represents an empty exception raised during a JSON process.
        /// </summary>
        public JSONException() { }
        /// <summary>
        /// Represents an exception raised during a JSON process.
        /// </summary>
        public JSONException(string message) : base(message) { }
        /// <summary>
        /// Represents an exception raised during a JSON process with a cause exception.
        /// </summary>
        public JSONException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Represents an exception based on a JSON error message. Pass the json["error"] when json.HasErrors() is true.
        /// </summary>
        public JSONException(JSON error) : base(error["message"].SafeString, error["innerError"].IsNull ? null : new JSONException(error["innerError"])) { }
    }

}