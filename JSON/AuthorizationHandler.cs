﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLibs.Web
{

    /// <summary>
    /// A collection of built in authorization handlers to simplify request authorization.
    /// </summary>
    public class AuthorizationHandler
    {
        private APIRequest _thisRequest;

        public AuthorizationHandler(APIRequest ThisRequest)
        {
            _thisRequest = ThisRequest;
        }

        /// <summary>
        /// This handler will add the "Authorization: Bearer {AccessToken}" header to the current request. 
        /// </summary>
        public void BearerToken(string AccessToken)
        {
            _thisRequest.Headers.Add("Authorization", $"Bearer {AccessToken}");
        }

        /// <summary>
        /// This handler will add the access_token={AccessToken} querystring parameter to the currend request.
        /// </summary>
        public void QueryToken(string AccessToken)
        {
            _thisRequest.QueryString.Add("access_token", AccessToken);
        }

        public void QueryString(string Parmeter, string AccessToken)
        {
            _thisRequest.QueryString.Add(Parmeter, AccessToken);
        }

        public void Form(string Parmeter, string AccessToken)
        {
            _thisRequest.Form.Add(Parmeter, AccessToken);
        }
    }
}
