﻿using QuickLibs.Javascript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLibs.Web
{
    public class APIParams
    {
        private JSON _Values;

        public JSON Values
        {
            get
            {
                return _Values;
            }
            set
            {
                _Values = value;
            }
        }

        public string this[string key]
        {
            get
            {
                return _Values[key].String;
            }
            set
            {
                _Values[key, true].String = value;
            }
        }


        public APIParams()
        {
            _Values = new JSON();
        }
        public APIParams(JSON values)
        {
            _Values = values;
        }

        /// <summary>
        /// Adds a new key value pair to this dictionary, OR replaces a value if the key already exists.
        /// </summary>
        public APIParams Add(string key, string value)
        {
            _Values[key, true].String = value;
            return this;
        }

        public APIParams Remove(string key)
        {
            _Values.Remove(key);
            return this;
        }

        public APIParams Clear()
        {
            _Values.Clear();
            return this;
        }

        public bool HasData()
        {
            return !JSON.IsNullOrEmpty(_Values);
        }
    }

}
