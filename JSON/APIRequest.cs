﻿using QuickLibs.Javascript;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickLibs.Web
{
    public class APIRequest
    {
        private Uri _BaseUri;
        private string _Url;
        private RequestMethod _Method;

        public CookieContainer Cookies;
        public APIParams Headers;
        public APIParams Form;
        public JSON Body;
        public APIParams QueryString;
        public APIParams RouteParameters;

        public AuthorizationHandler Auth;

        public int RequestTimeout = 30;
        public Action<JSON> Callback;
        public JSON Response;


        /// <summary>
        /// Create a new instance of the API Request.
        /// </summary>
        /// <param name="BaseUri">Base API URI</param>
        public APIRequest(string BaseUri)
        {
            _BaseUri = new Uri(BaseUri);
            _Method = RequestMethod.GET;

            RouteParameters = new APIParams();
            QueryString = new APIParams();
            Form = new APIParams();
            Body = new JSON();
            Headers = new APIParams();
            Cookies = new CookieContainer();

            Auth = new AuthorizationHandler(this);

            Callback = null;
        }

        ///// <summary>
        ///// Create a new instance of the API Request controller.
        ///// </summary>
        ///// <param name="Config">JSON Config Data.</param>
        //public APIRequest(JSON Config)
        //{
        //    baseUri = new Uri(Config["ApiUrl"].String).ToString(); //ensure valid url.
        //    endpoint = Config["Endpoint"].String;
        //    routeParameters = new APIParams(Config["RouteParams"]);
        //    queryString = new APIParams(Config["QueryString"]);
        //    postData = new APIParams(Config["PostData"]);
        //    payload = new APIParams(Config["Payload"]);
        //    headers = new APIParams(Config["Headers"]);
        //    method = "POST".Equals(Config["Method"].String, StringComparison.CurrentCultureIgnoreCase) ? "POST" : "GET";
        //    RequestTimeout = Config["Timeout"].IsInteger ? (int)Config["Timeout"].Integer : RequestTimeout;

        //    cookies = new CookieContainer();
        //    foreach (JSON cookie in Config["Cookies"])
        //        cookies.Add(new Cookie(cookie.Key, cookie.String, "", new Uri(baseUri).Host));

        //    ApplyConfig();
        //}

        //public JSON GetConfig()
        //{
        //    JSON config = new JSON()
        //        .Add("ApiUrl", baseUri)
        //        .Add("Endpoint", endpoint)
        //        .Add("RouteParams", routeParameters.Get())
        //        .Add("QueryString", queryString.Get())
        //        .Add("PostData", postData.Get())
        //        .Add("Payload", payload.Get())
        //        .Add("Headers", headers.Get())
        //        .Add("Cookies", new JSON())
        //        .Add("Method", method)
        //        .Add("Timeout", RequestTimeout.ToString());

        //    foreach (Cookie cookie in cookies.GetCookies(new Uri(baseUri)))
        //        config["Cookies"].Add(cookie.Name, cookie.Value);

        //    return config;
        //}

        //private void ApplyConfig()
        //{
        //    if (string.IsNullOrEmpty(baseUri))
        //        ts.TraceEvent(TraceEventType.Error, 0, "Required parameter \"ApiUrl\" is Missing.");
        //    if (endpoint == null)
        //        ts.TraceEvent(TraceEventType.Error, 0, "Required Parameter \"Endpoint\" is Missing.");
        //    if (!JSON.IsNullOrEmpty(payload.Get()) && !JSON.IsNullOrEmpty(postData.Get()))
        //        ts.TraceEvent(TraceEventType.Warning, 0, "Both \"Payload\" and \"PostData\" parameters are populated. \"PostData\" will be ignored.");

        //    if (false)
        //        throw new ApplicationException("Unable to process request. Some required configuration parameters were missing or invalid. See log for more details.");

        //    //Ensure API Url has exactly one trailing slash.
        //    baseUri = baseUri.TrimEnd('/', '\\') + "/";

        //    //Ensure endpoint has no leading slash.
        //    endpoint = endpoint.TrimStart('/', '\\');

        //    //Prepare url,
        //    url = baseUri + endpoint;
        //}


        /// <summary>
        /// Performs a GET request to the given endpoint and returns the JSON response.
        /// </summary>
        public JSON Get(string Endpoint = "")
        {
            _Method = RequestMethod.GET;
            return DoRequest(Endpoint);
        }

        /// <summary>
        /// Performs a POST request to the given endpoint and returns the JSON response.
        /// </summary>
        public JSON Post(string Endpoint = "")
        {
            _Method = RequestMethod.POST;
            return DoRequest(Endpoint);
        }

        /// <summary>
        /// Performs a PUT request to the given endpoint and returns the JSON response.
        /// </summary>
        public JSON Put(string Endpoint = "")
        {
            _Method = RequestMethod.PUT;
            return DoRequest(Endpoint);
        }

        /// <summary>
        /// Performs a PATCH request to the given endpoint and returns the JSON response.
        /// </summary>
        public JSON Patch(string Endpoint = "")
        {
            _Method = RequestMethod.PATCH;
            return DoRequest(Endpoint);
        }

        /// <summary>
        /// Performs a DELET request to the given endpoint and returns the JSON response.
        /// </summary>
        public JSON Delete(string Endpoint = "")
        {
            _Method = RequestMethod.DELETE;
            return DoRequest(Endpoint);
        }

        /// <summary>
        /// Performs a request to the given endpoint using the current RequestMethod and returns the JSON response.
        /// </summary>
        public JSON DoRequest(string Endpoint = "")
        {
            string RequestUri = new Uri(_BaseUri, Endpoint.TrimStart('\\','/')).ToString();

            //Replace RouteParameters
            Regex RouteRex = new Regex("{(.*?)}");
            foreach (Match match in RouteRex.Matches(RequestUri))
            {
                if (!RouteParameters.Values.Contains(match.Groups[1].Value))
                    Trace.TraceWarning("Route parameter \"" + match.Value + "\" is missing.");

                RequestUri = RequestUri.Replace(match.Value, RouteParameters[match.Groups[1].Value]);
            }

            //Append QeryString
            if (QueryString.HasData())
                RequestUri += QueryString.Values.ToQueryString(RequestUri.Contains("?") ? "&" : "?");

            //Log Request details
            Trace.TraceInformation($"Request URL: {RequestUri}");

            if (QueryString.HasData())
                Trace.TraceInformation($"Query String: { QueryString.Values.ToString() }");
            if (Form.HasData())
                Trace.TraceInformation($"Form: { Form.Values.ToString() }");
            if (Body.HasData())
                Trace.TraceInformation($"Body: { Body.ToString() }");
            if (Headers.HasData())
                Trace.TraceInformation($"Headers: { Headers.Values.ToString() }");

            JSON.cookies = Cookies;

            switch (_Method)
            {
                case RequestMethod.GET:
                default:
                    Response = JSON.Get(RequestUri, null, RequestTimeout * 1000, Headers.Values, Callback);
                    break;
                case RequestMethod.POST:
                    Response = JSON.Post(RequestUri, Form.Values, Body, RequestTimeout * 1000, Headers.Values, Callback);
                    break;
                case RequestMethod.PUT:
                    Response = JSON.Put(RequestUri, null, Body, RequestTimeout * 1000, Headers.Values, Callback);
                    break;
                case RequestMethod.PATCH:
                    Response = JSON.Patch(RequestUri, null, Body, RequestTimeout * 1000, Headers.Values, Callback);
                    break;
                case RequestMethod.DELETE:
                    Response = JSON.Delete(RequestUri, null, Body, RequestTimeout * 1000, Headers.Values, Callback);
                    break;
            }

            if (Response.HasErrors())
            {
                //If the raised exception includes a response, log the exception and return the response.
                if (!Response["response"].IsNull)
                {
                    Trace.TraceError(Response.Exception.ToString());
                    return Response;
                }

                //If other exception - return the exeption.
                //Response.Exception.Data.Add("APIRequest.Config", GetConfig().ToString());
                Response.Throw();
            }

            //Return the response.
            return Response;
        }
    }

    enum RequestMethod
    {
        GET,
        PUT,
        POST,
        PATCH,
        DELETE
    }
}
