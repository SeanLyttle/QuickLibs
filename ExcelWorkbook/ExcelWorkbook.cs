﻿using System;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using QuickLibs.Javascript;
using QuickLibs.Data;
using System.Data;
using QuickLibs.Utils;
using QuickLibs.MSOffice.Crypto;

namespace QuickLibs.MSOffice
{
    /// <summary>
    /// Generates a MS Office Excel (XLSX) file from a JSON definition.
    /// </summary>
    public class ExcelWorkbook
    {
        /// <summary>
        /// Reference to the inner ClosedXML.XLWorkbook.
        /// </summary>
        public XLWorkbook xlWorkbook;
        JSON WorkbookDef;

        /// <summary>
        /// Messages generated during generation of this Excel workbook will be logged to the consolie by default and not recorded.
        /// Use this interface to enable, disable and read from the loging output (e.g. Log.OutputLogging, Log.RecordLogging, Log.LOG).
        /// </summary>
        public Logging Log;

        /// <summary>
        /// Creates an empty Excel Workbook.
        /// </summary>
        public ExcelWorkbook()
        {
            Log = new Logging();
            WorkbookDef = new JSON();
            xlWorkbook = new XLWorkbook();
        }

        /// <summary>
        /// Creates an Excel Workbook based on the given JSON WorkbookDefinition.
        /// </summary>
        /// <param name="WorkbookDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        public ExcelWorkbook(JSON WorkbookDefinition)
        {
            Log = new Logging();
            xlWorkbook = new XLWorkbook();
            GenerateXLSX(WorkbookDefinition);
        }

        /// <summary>
        /// Creates an Excel Workbook based on the given DataSet.
        /// </summary>
        public ExcelWorkbook(DataSet dataSet)
        {
            Log = new Logging();
            xlWorkbook = new XLWorkbook();
            GenerateXLSX(new JSON(dataSet));
        }

        /// <summary>
        /// Creates an Excel Workbook based on the given DataTable.
        /// </summary>
        public ExcelWorkbook(DataTable dataTable)
        {
            Log = new Logging();
            xlWorkbook = new XLWorkbook();
            GenerateXLSX(new JSON(dataTable));
        }

        /// <summary>
        /// Creates an Excel Workbook and loads the contents of the given Excel document.
        /// </summary>
        /// <param name="filePath">Location of the Excel spreadsheet.</param>
        /// <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        public ExcelWorkbook(string filePath, bool ReadOnly = true)
            : this()
        {
            Load(filePath, ReadOnly);
        }

        /// <summary>
        /// Creates and Excel Workbook and loads the contents of the given password protected Excel document.
        /// </summary>
        /// <param name="filePath">Location of the password protected Excel spreadsheet.</param>
        /// <param name="password">Plaintext password to decrypt the file.</param>
        /// <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        public ExcelWorkbook(string filePath, string password, bool ReadOnly = true)
            : this()
        {
            Load(filePath, password, ReadOnly);
        }

        /// <summary>
        /// Creates an Excel Workbook and loads the contents of the given Excel document.
        /// </summary>
        /// <param name="fileStream">The File Stream of an Excel file.</param>
        public ExcelWorkbook(FileStream fileStream)
            : this()
        {
            Load(fileStream);
        }

        /// <summary>
        /// Creates and Excel Workbook and loads the contents of the given password protected Excel document.
        /// </summary>
        /// <param name="fileStream">The File Stream of a password protected Excel file.</param>
        /// <param name="password">Plaintext password to decrypt the file.</param>
        public ExcelWorkbook(FileStream fileStream, string password)
            : this()
        {
            Load(fileStream, password);
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~ExcelWorkbook()
        {
            Log = null;
            xlWorkbook = null;
            WorkbookDef = null;
        }

        /// <summary>
        /// Saves changes to the current Excel file.
        /// </summary>
        public void Save()
        {
            //Create a default sheet if none exist (Save requires at least one worksheet).
            if (xlWorkbook.Worksheets.Count == 0)
                xlWorkbook.AddWorksheet("Sheet1");
            xlWorkbook.Save();
        }

        /// <summary>
        /// Saves the current Excel file to the given filepath.
        /// </summary>
        public void Save(string filePath)
        {
            //Create a default sheet if none exist (Save requires at least one worksheet).
            if (xlWorkbook.Worksheets.Count == 0)
                xlWorkbook.AddWorksheet("Sheet1");
            xlWorkbook.SaveAs(filePath);
        }

        /// <summary>
        /// Saves the current Excel file to the given fileStream.
        /// </summary>
        public void Save(Stream fileStream)
        {
            //Create a default sheet if none exist (Save requires at least one worksheet).
            if (xlWorkbook.Worksheets.Count == 0)
                xlWorkbook.AddWorksheet("Sheet1");
            xlWorkbook.SaveAs(fileStream);
        }

        /// <summary>
        /// Loads an Excel file from the given filepath.
        /// </summary>
        /// <param name="filePath">Location of the Excel spreadsheet.</param>
        /// <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        public void Load(string filePath, bool ReadOnly = true)
        {
            try
            {
                if (filePath.EndsWith(".xls", StringComparison.CurrentCultureIgnoreCase))
                    GetFromOleDB(filePath);
                else if (ReadOnly)
                {
                    using (FileStream fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        xlWorkbook = new XLWorkbook(fs);
                }
                else
                    xlWorkbook = new XLWorkbook(filePath);
            }
            catch (Exception exp)
            {
                throw new FileLoadException("Unable to load Excel spreadsheet: " + filePath, exp); 
            }
        }

        /// <summary>
        /// Loads an Excel file from the given filepath.
        /// </summary>
        /// <param name="filePath">Location of the password protected Excel file.</param>
        /// <param name="password">Plaintext password to decrypt the file.</param>
        /// <param name="ReadOnly">When true, this process will open the file in read only mode. Use this to avoid file access issues when the spreadsheet is open elsewhere.</param>
        public void Load(string filePath, string password, bool ReadOnly = true)
        {
            try
            {
                if (ReadOnly)
                {
                    using (FileStream fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        using (OfficeCryptoStream ocs = new OfficeCryptoStream(fs, password))
                            xlWorkbook = new XLWorkbook(ocs);
                }
                else
                {
                    using (OfficeCryptoStream ocs = OfficeCryptoStream.Open(filePath, password))
                        xlWorkbook = new XLWorkbook(ocs);
                }
            }
            catch (Exception exp)
            {
                throw new FileLoadException("Unable to load password encnrypted Excel spreadsheet: " + filePath, exp);
            }
        }

        /// <summary>
        /// Loads an Excel file from the given file stream.
        /// </summary>
        /// <param name="fileStream">The File Stream of an Excel file.</param>
        public void Load(FileStream fileStream)
        {
            try
            {
                xlWorkbook = new XLWorkbook(fileStream);
            }
            catch (Exception exp)
            {
                throw new FileLoadException("Unable to load Excel spreadsheet from the given file stream.", exp);
            }
        }

        /// <summary>
        /// Loads an Excel file from the given file stream.
        /// </summary>
        /// <param name="fileStream">The File Stream of an Excel file.</param>
        /// <param name="password">Plaintext password to decrypt the file.</param>
        public void Load(FileStream fileStream, string password)
        {
            try
            {
                using (OfficeCryptoStream ocs = new OfficeCryptoStream(fileStream, password))
                    xlWorkbook = new XLWorkbook(ocs);
            }
            catch (Exception exp)
            {
                throw new FileLoadException("Unable to load password encnrypted Excel spreadsheet from the given file stream.", exp);
            }
        }

        private void GetFromOleDB(string filePath)
        {
            xlWorkbook = new XLWorkbook();

            QuickDB qDB = new QuickDB("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"");
            qDB.GetSchema();

            foreach (QuickDB qRow in qDB)
            {
                string sheetName = qRow["TABLE_NAME"].GetString();
                if (!sheetName.EndsWith("$"))
                    continue;

                qDB.Select("SELECT * FROM [" + sheetName + "]");
                //Serialise table and clean up blank column headings.
                JSON table = new JSON(qDB.GetTable());
                foreach(JSON j in table["columns"])
                {
                    if (string.Concat("F", j.Index + 1).Equals(j.String))
                        j.String = "'"; 
                };

                AddTable(new JSON(sheetName.TrimEnd('$'), table)[0]);
            }
        }

         /// <summary>
        /// Returns the current XL file as a MemoryStream.
        /// </summary>
        public MemoryStream ToMemoryStream()
        {
            MemoryStream ms = new MemoryStream();
            if (xlWorkbook.Worksheets.Count == 0)
                xlWorkbook.AddWorksheet("Sheet1");
            xlWorkbook.SaveAs(ms); //Save workbook to memory stream.
            ms.Position = 0; //Reset memory stream so we can start reading again from the beginning.
            return ms;
        }


        /// <summary>
        ///     Generates an XLSX file from the given JSON Workbook definition OR JSON DataSet or DataTable.
        /// </summary>
        /// <param name="WorkbookDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        public void GenerateXLSX(JSON WorkbookDefinition)
        {
            WorkbookDef = WorkbookDefinition;

            if (WorkbookDef.Contains("Sheets"))
            {
                foreach (JSON SheetDef in WorkbookDef["Sheets"])
                    AddSheet(SheetDef);
            }
            else if (WorkbookDef[0].Contains("columns") && WorkbookDef[0].Contains("rows"))
            {
                foreach (JSON table in WorkbookDef)
                    AddTable(table);
            }
            else if (WorkbookDef.Contains("columns") && WorkbookDef.Contains("rows"))
            {
                AddTable(WorkbookDef);
            }
        }

        /// <summary>
        /// Generates a new sheet based on the given JSON Sheet definition.
        /// </summary>
        public void AddSheet(JSON SheetDef)
        {
            //Keep track of relative positioning.
            int DeltaRow = 0;

            //Select existing or create new worksheet.
            IXLWorksheet Sheet = xlWorkbook.Worksheets.Where(w => w.Name ==SheetDef["Name"].SafeString).FirstOrDefault();
            if (Sheet == null)
                Sheet = xlWorkbook.AddWorksheet(SheetDef["Name"].SafeString);

            //Set all columns to default width.
            Sheet.Columns().Width = SheetDef["CellWidth"].IsNumber ? SheetDef["CellWidth"].Number : WorkbookDef["CellWidth"].IsNumber ? WorkbookDef["CellWidth"].Number : 12.14; //90px;

            //Alter specific ranges from the sheet definition.
            foreach (JSON RangeDef in SheetDef["Cells"])
            {
                IXLRange Range = Sheet.Range(RangeDef.Key);
                if (Range != null)
                {
                    if (Range.LastCell().Address.RowNumber < Sheet.LastCell().Address.RowNumber - DeltaRow)
                    {
                        //Shift current selection down if other previous elements used more than one row.
                        if (!RangeDef.Key.Contains("$"))
                            Range = Sheet.Range(Range.FirstCell().CellBelow(Math.Max(DeltaRow, 0)), Range.LastCell().CellBelow(Math.Max(DeltaRow, 0)));
                    }
                    else
                        Log.Info("The following range has not been shifted as it reaches the bottom of the sheet.");

                    Log.Info("Adding range: " + Range.RangeAddress.ToStringRelative() + "(" + RangeDef.Key + ")");

                    IXLCell Cell = Range.FirstCell();
                    IXLCell LastCell = Range.LastCell();

                    string NumFormat = RangeDef["Format"].String ?? SheetDef["Format"].String ?? WorkbookDef["Format"].String ?? "General";

                    //Set cell text/formula
                    if (RangeDef.IsString)
                        LastCell = PopulateCells(Range, RangeDef.String);
                    else if (RangeDef["Text"].IsString)
                        LastCell = PopulateCells(Range, RangeDef["Text"].String);
                    else if (RangeDef["DataSource"].IsInteger)
                    {
                        JSON DataTable = WorkbookDef["DataSources"][(int)RangeDef["DataSource"].Integer];
                        LastCell = PopulateCells(Cell, DataTable, (int)RangeDef["DataIndex"].Integer, NumFormat);
                    }
                    else if (RangeDef["DataSource"].IsString)
                    {
                        JSON DataTable = WorkbookDef["DataSources"][RangeDef["DataSource"].String];
                        LastCell = PopulateCells(Cell, DataTable, (int)RangeDef["DataIndex"].Integer, NumFormat);
                    }

                    //Update range if necessary.
                    if (Range.LastCell() != LastCell)
                        Range = Sheet.Range(Cell, LastCell);

                    //Add totals
                    if (!RangeDef["Totals"].IsNull && Range.RowCount() > 1)
                    {
                        //Set up defaults
                        string Formula = "=SUM()";
                        string TotalText = "Total";
                        int Space = 1;

                        //Or use defined values if given.
                        if (RangeDef["Totals"]["Formula"].IsString)
                            Formula = RangeDef["Totals"]["Formula"].String;
                        if (RangeDef["Totals"]["Text"].IsString)
                            TotalText = RangeDef["Totals"]["Text"].String;
                        if (RangeDef["Totals"]["Space"].IsInteger)
                            Space = (int)RangeDef["Totals"]["Space"].Integer;

                        if (Formula.StartsWith("="))
                            Formula = Formula.Substring(1);

                        int row = Range.LastCell().Address.RowNumber + Space + 1;

                        for (int col = Range.FirstCell().Address.ColumnNumber; col <= Range.LastCell().Address.ColumnNumber; col++)
                        {
                            IXLCell TotalCell = Sheet.Cell(row, col);
                            string TotalFormula = Formula.Replace("()", String.Concat("(", Sheet.Range(Range.FirstCell().Address.RowNumber + 1, col, Range.LastCell().Address.RowNumber, col).RangeAddress.ToString(), ")"));
                            TotalFormula = "=IF(ISERROR(" + TotalFormula + "),\"\"," + TotalFormula + ")"; //Add a littel bit of cosmetics to hide Excel errors.

                            //Use TitleText for first cell if available.
                            if (!string.IsNullOrEmpty(TotalText))
                            {
                                TotalFormula = TotalText;
                                TotalCell.Style.Font.SetBold();
                                TotalText = null;
                            }

                            TotalCell.Style.NumberFormat.SetFormat(RangeDef["Totals"]["Format"].String ?? NumFormat);

                            LastCell = PopulateCells(TotalCell, TotalFormula);
                        }
                        //Update the range again
                        Range = Sheet.Range(Cell, LastCell);
                    }

                    //Increment delta row if more than one row was added. Update last row used on this sheet.
                    if (RangeDef.IsString || RangeDef["Text"].IsString || RangeDef["DataSource"].IsString)
                        DeltaRow += Range.LastCell().Address.RowNumber - Range.FirstCell().Address.RowNumber;

                    Log.Info("Formatting range.");

                    //Apply formatting from definition.
                    if (RangeDef["ApplyToAll"].Boolean)
                        ApplyRangeFormatting(RangeDef, Range.Style);
                    else
                        ApplyRangeFormatting(RangeDef, Range.CellsUsed().Style);

                    if (RangeDef["IsHidden"].Boolean)
                        Sheet.Rows(Range.FirstCell().Address.RowNumber, Range.LastCell().Address.RowNumber).Hide();

                    if (RangeDef["Conditional"].IsArray)
                    {
                        foreach (JSON condition in RangeDef["Conditional"])
                        {
                            IXLStyle style = Sheet.Range(Range.FirstCell().CellBelow(), Range.LastCell()) //First row is always column headings, so skip it.
                                .AddConditionalFormat() //Add condition
                                .WhenIsTrue(FixRelativeFormula(condition["Formula"].SafeString, DeltaRow + 1, Sheet)); //Delta row +1 skip an extra row because we want to skip column headings.

                            //Apply formatting form definition.
                            ApplyRangeFormatting(condition, style);
                        }
                    }

                    if (RangeDef["Width"].IsNumber)
                        Sheet.Columns(Range.FirstCell().Address.ColumnNumber, Range.LastCell().Address.ColumnNumber).Width = RangeDef["Width"].Number;
                    else if (RangeDef["Width"].IsString && RangeDef["Width"].String.ToLower().Equals("auto"))
                        Sheet.Columns(Range.FirstCell().Address.ColumnNumber, Range.LastCell().Address.ColumnNumber).AdjustToContents();
                }
                else
                    Log.Info(string.Concat("Invalid cell reference '", RangeDef.Key, "' in sheet '", SheetDef["Name"].String, "'."));

            }
        }

        /// <summary>
        /// Generates a new sheet based on the given JSON data table. Use the JSON(DataTable dataTable) constructor to generate a JSON data table.
        /// </summary>
        public void AddTable(JSON DataTable)
        {
            //Select existing or create new worksheet.
            IXLWorksheet Sheet = xlWorkbook.Worksheets.Where(w => w.Name == (DataTable.Key ?? "Sheet " + (xlWorkbook.Worksheets.Count + 1).ToString())).FirstOrDefault();
            if (Sheet == null)
                Sheet = xlWorkbook.AddWorksheet(DataTable.Key ?? "Sheet " + (xlWorkbook.Worksheets.Count + 1).ToString());

            //Set all columns to default width.
            Sheet.Columns().Width = DataTable["CellWidth"].IsNumber ? DataTable["CellWidth"].Number : WorkbookDef["CellWidth"].IsNumber ? WorkbookDef["CellWidth"].Number : 12.14; //90px;

            string NumFormat = DataTable["Format"].String ?? WorkbookDef["Format"].String ?? "General";

            PopulateCells(Sheet.Cell("A1"), DataTable, (int)DataTable["DataIndex"].Integer, NumFormat);
        }

        /// <summary>
        /// Inserts data into a worksheet, starting at the specified cell, from a string ('\t' will move right to the next cell, '\n' will move down to the next row).
        /// </summary>
        private IXLCell PopulateCells(IXLCell cell, string value)
        {
            int cMin = cell.Address.ColumnNumber;
            int cMax = cMin;

            foreach (string row in value.Split('\n'))
            {
                foreach (string val in row.Split('\t'))
                {
                    if (!string.IsNullOrEmpty(cell.Value + cell.FormulaA1))
                        Log.Info(string.Concat("Cell ", cell.Address.ToStringRelative(true), " already contained some data (\"", cell.Value, cell.FormulaA1, "\"), which will be overritten with: ", val));

                    if (val.StartsWith("=") && new Regex(@"R[[]?-?\d*[\]]?C[[]?-?\d*[\]]?").IsMatch(val))
                        cell.FormulaR1C1 = val;
                    else if (val.StartsWith("="))
                        cell.FormulaA1 = val;
                    else
                        cell.Value = val;

                    cMax = Math.Max(cMax, cell.Address.ColumnNumber);
                    cell = cell.CellRight();
                }
                cell = cell.Worksheet.Cell(cell.Address.RowNumber + 1, cMin);
            }

            return cell.Worksheet.Cell(cell.Address.RowNumber - 1, cMax); ;
        }

        /// <summary>
        /// Inserts the same data into each cell in the given range ('\n' and '\t' will still change cells which will cause some data to be overridden!)
        /// </summary>
        private IXLCell PopulateCells(IXLRange range, string value)
        {
            IXLCell lastCell = range.FirstCell();
            foreach (IXLCell cell in range.Cells())
                lastCell = PopulateCells(cell, value);

            return lastCell;
        }

        /// <summary>
        /// Inserts data into a worksheet, starting at the specified cell, from a JSON table.
        /// </summary>
        private IXLCell PopulateCells(IXLCell Cell, JSON DataTable, int DataIndex, string NumFormat)
        {
            //Fetch data from SQL source if DataTable represents a SQL command.
            if (DataTable["ConnectionString"].IsString)
            {
                QuickDB qDB = new QuickDB(DataTable["ConnectionString"].String);

                foreach (JSON param in DataTable["Parameters"])
                    qDB.SetParameter(param.Key, param.String);

                qDB.Select(DataTable["Command"].SafeString);

                DataTable = new JSON(qDB.GetTable(DataIndex));
            }

            //Check that the table is not empty.
            if (DataTable.Length == 0)
                return Cell;

            IXLWorksheet sheet = Cell.Worksheet;
            int r = Cell.Address.RowNumber;
            int c = Cell.Address.ColumnNumber;

            //Column Headers
            foreach (JSON col in DataTable["columns"])
            {
                if (col.IsString)
                {
                    sheet.Cell(r, c).Value = col.SafeString.StartsWith("_") ? "" : ("'" + col.SafeString.Replace("_", " "));
                    sheet.Cell(r, c).Style.Font.SetBold(true);
                }
                else
                {
                    foreach (JSON cell in col)
                    {
                        IXLCell ValueCell = sheet.Cell(r++, c);
                        ValueCell.Value = cell.SafeString.StartsWith("_") ? "" : ("'" + cell.SafeString.Replace("_", " "));
                        ValueCell.Style.Font.SetBold(true);

                        if (c > 1 && ValueCell.Value.ToString() != "")
                        {
                            IXLCell CellLeft = ValueCell.CellLeft();
                            if (ValueCell.Value.ToString() == CellLeft.Value.ToString())
                            {
                                while (ValueCell.Value.ToString() == CellLeft.Value.ToString())
                                    CellLeft = CellLeft.CellLeft();

                                sheet.Range(CellLeft.CellRight(), ValueCell).Merge();
                                CellLeft.CellRight().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            }
                        }
                    }
                    r = Cell.Address.RowNumber;
                }
                c++;
            }

            r += DataTable["columns"][0].Length;
            int cMax = c;

            //Data Rows
            foreach (JSON row in DataTable["rows"])
            {
                c = Cell.Address.ColumnNumber;
                foreach (JSON col in row)
                {
                    JSON val = col.IsArray ? col[DataIndex % col.Length] : col; //DataIndex selects individual value from multi valued columns.

                    IXLCell ValueCell = sheet.Cell(r, c); //TODO: offer transpose?

                    //If header cell is blank - column contains row headers.
                    if (sheet.Cell(Cell.Address.RowNumber, c).Value.Equals(""))
                    {
                        ValueCell.Value = "'" + val.String; //"'" Ensure header cells are treated as text.
                        ValueCell.Style.Font.SetBold(true);

                        IXLCell CellAbove = ValueCell.CellAbove();
                        if (ValueCell.Value.ToString() == CellAbove.Value.ToString())
                        {
                            while (ValueCell.Value.ToString() == CellAbove.Value.ToString())
                                CellAbove = CellAbove.CellAbove();

                            sheet.Range(CellAbove.CellBelow(), ValueCell).Merge();
                            CellAbove.CellBelow().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        }
                    }
                    else
                    {
                        ValueCell.Value = val.String;// ?? "-";
                        ValueCell.Style.NumberFormat.SetFormat(NumFormat);
                        //if (ValueCell.Value.ToString().Equals("-") && !string.IsNullOrEmpty(NumFormat))
                        //    ValueCell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    }

                    if (ValueCell.Value.ToString().Contains(" "))
                        ValueCell.Style.Alignment.WrapText = true;

                    c++;
                }
                cMax = Math.Max(c, cMax);
                r++;
            }

            return sheet.Cell(--r, --cMax);
        }


        /// <summary>
        /// Applys formatting to the given cell range based on the given JSON definition.
        /// </summary>
        private void ApplyRangeFormatting(JSON styleDef, IXLStyle rangeStyle)
        {
            if (styleDef["Bold"].IsBoolean)
                rangeStyle.Font.SetBold(styleDef["Bold"].Boolean);
            if (styleDef["Color"].IsString && IsColor(styleDef["Color"].String))
                rangeStyle.Font.SetFontColor(GetColor(styleDef["Color"].String));
            if (styleDef["Background"].IsString && IsColor(styleDef["Background"].String))
                rangeStyle.Fill.SetBackgroundColor(GetColor(styleDef["Background"].String));
            if (styleDef["Format"].IsString)
                rangeStyle.NumberFormat.SetFormat(styleDef["Format"].String);
            if (styleDef["WrapText"].IsBoolean)
                rangeStyle.Alignment.WrapText = styleDef["WrapText"].Boolean;
            if (styleDef["Borders"].IsBoolean)
                if (styleDef["Borders"].Boolean)
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.Medium)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin);
                else
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.None)
                        .Border.SetInsideBorder(XLBorderStyleValues.None);
            else if (styleDef["Borders"].IsDictionary)
            {
                if (styleDef["Borders"]["Color"].IsString && IsColor(styleDef["Borders"]["Color"].String))
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.Medium)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetOutsideBorderColor(GetColor(styleDef["Borders"]["Color"].String))
                        .Border.SetInsideBorderColor(GetColor(styleDef["Borders"]["Color"].String));

                if (styleDef["Borders"]["Style"].IsString)
                    rangeStyle
                        .Border.SetOutsideBorder(XLBorderStyleValues.None)
                        .Border.SetInsideBorder(BorderStyleFromString(styleDef["Borders"]["Style"].String));


                if (styleDef["Borders"]["Top"].IsDictionary)
                {
                    if (styleDef["Borders"]["Top"]["Color"].IsString && IsColor(styleDef["Borders"]["Top"]["Color"].String))
                        rangeStyle.Border.SetTopBorderColor(GetColor(styleDef["Borders"]["Top"]["Color"].String));
                    if (styleDef["Borders"]["Top"]["Style"].IsString)
                        rangeStyle.Border.SetTopBorder(BorderStyleFromString(styleDef["Borders"]["Top"]["Style"].String));
                }
                if (styleDef["Borders"]["Bottom"].IsDictionary)
                {
                    if (styleDef["Borders"]["Bottom"]["Color"].IsString && IsColor(styleDef["Borders"]["Bottom"]["Color"].String))
                        rangeStyle.Border.SetBottomBorderColor(GetColor(styleDef["Borders"]["Bottom"]["Color"].String));
                    if (styleDef["Borders"]["Bottom"]["Style"].IsString)
                        rangeStyle.Border.SetBottomBorder(BorderStyleFromString(styleDef["Borders"]["Bottom"]["Style"].String));
                }
                if (styleDef["Borders"]["Left"].IsDictionary)
                {
                    if (styleDef["Borders"]["Left"]["Color"].IsString && IsColor(styleDef["Borders"]["Left"]["Color"].String))
                        rangeStyle.Border.SetLeftBorderColor(GetColor(styleDef["Borders"]["Left"]["Color"].String));
                    if (styleDef["Borders"]["Left"]["Style"].IsString)
                        rangeStyle.Border.SetLeftBorder(BorderStyleFromString(styleDef["Borders"]["Left"]["Style"].String));
                }
                if (styleDef["Borders"]["Right"].IsDictionary)
                {
                    if (styleDef["Borders"]["Right"]["Color"].IsString && IsColor(styleDef["Borders"]["Right"]["Color"].String))
                        rangeStyle.Border.SetRightBorderColor(GetColor(styleDef["Borders"]["Right"]["Color"].String));
                    if (styleDef["Borders"]["Right"]["Style"].IsString)
                        rangeStyle.Border.SetRightBorder(BorderStyleFromString(styleDef["Borders"]["Right"]["Style"].String));
                }
            }
        }

        /// <summary>
        /// Finds relative cell references in the given formula and shifts them down, based on previous entries.
        /// </summary>
        private string FixRelativeFormula(string formula, int deltaRow, IXLWorksheet sheet)
        {
            Regex rex = new Regex(@"(?:\b|\$)[a-zA-Z]+[0-9]+\b(?!\()(?=(?:[^""']*(?:""|')[^""']*(?:""|'))*[^""']*$)"); //Regex to find Excel cell references with a relative row number.
            foreach (Match reference in rex.Matches(formula))
            {
                formula = formula.Replace(reference.Value, sheet.Cell(reference.Value).CellBelow(Math.Max(deltaRow, 0)).Address.ToString()); //Loses fixed column relativity (not sure if this is an issue).
            }
            return formula;
        }

        /// <summary>
        /// Returns a valid border style based on the given string. (Default: None)
        /// </summary>
        private XLBorderStyleValues BorderStyleFromString(string borderStyle)
        {
            switch (borderStyle.ToLower())
            {
                case "hair":
                    return XLBorderStyleValues.Hair;
                case "thin":
                    return XLBorderStyleValues.Thin;
                case "medium":
                    return XLBorderStyleValues.Medium;
                case "thick":
                    return XLBorderStyleValues.Thick;
                case "double":
                    return XLBorderStyleValues.Double;
                case "dashed":
                    return XLBorderStyleValues.Dashed;
                case "dotted":
                    return XLBorderStyleValues.Dotted;
                case "dotdash":
                case "dashdot":
                    return XLBorderStyleValues.DashDot;
                case "dashdotdot":
                case "dotdashdot":
                case "dotdotdash":
                    return XLBorderStyleValues.DashDotDot;
                case "mediumdashed":
                    return XLBorderStyleValues.MediumDashed;
                case "mediumdotdash":
                case "mediumdashdot":
                    return XLBorderStyleValues.MediumDashDot;
                case "mediumdashdotdot":
                case "mediumdotdashdot":
                case "mediumdotdotdash":
                    return XLBorderStyleValues.MediumDashDotDot;
                default:
                    return XLBorderStyleValues.None;
            }
        }

        /// <summary>
        /// Returns true if the given string can be matched againsta a color.
        /// </summary>
        private bool IsColor(string color)
        {
            if (string.IsNullOrEmpty(color))
            {
                Log.Info(string.Concat("Template sheet definition contins an unrecognised color: ''"));
                return false;
            }

            //Remove tint if specified.
            color = color.Split(' ')[0].Split(',')[0].Trim();

            try
            {
                if (",accent1,accent2,accent3,accent4,accent5,accent6,background1,background2,followedhyperlink,hyperlink,text1,text2,dark1,dark2,light1,light2,".Contains(String.Concat(",", color.ToLower(), ",")))
                    return true;

                XLColor.FromHtml(color);
                return true;
            }
            catch (Exception e)
            {
                Log.Info(string.Concat("Template sheet definition contins an unrecognised color: ", color));
                return false;
            }
        }

        /// <summary>
        /// Returns a Color based on the given string ("Color Tint": where Color = any color string and positive Tint = brightness%  and negative Tint = darkness%)
        /// </summary>
        private XLColor GetColor(string color)
        {
            String colorPart = color.Split(' ')[0].Split(',')[0].Trim();

            int tint = 0;
            int.TryParse(color.Substring(colorPart.Length).Trim(), out tint);

            tint = Math.Min(Math.Max(tint, -100), 100); //Clip value if out of range.

            XLColor xlColor;

            switch (colorPart.ToLower())
            {
                case "accent1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent1, 1.0 * tint / 100);
                    break;
                case "accent2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent2, 1.0 * tint / 100);
                    break;
                case "accent3":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent3, 1.0 * tint / 100);
                    break;
                case "accent4":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent4, 1.0 * tint / 100);
                    break;
                case "accent5":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent5, 1.0 * tint / 100);
                    break;
                case "accent6":
                    xlColor = XLColor.FromTheme(XLThemeColor.Accent6, 1.0 * tint / 100);
                    break;
                case "background1":
                case "light1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Background1, 1.0 * tint / 100);
                    break;
                case "background2":
                case "light2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Background2, 1.0 * tint / 100);
                    break;
                case "followedhyperlink":
                    xlColor = XLColor.FromTheme(XLThemeColor.FollowedHyperlink, 1.0 * tint / 100);
                    break;
                case "hyperlink":
                    xlColor = XLColor.FromTheme(XLThemeColor.Hyperlink, 1.0 * tint / 100);
                    break;
                case "text1":
                case "dark1":
                    xlColor = XLColor.FromTheme(XLThemeColor.Text1, 1.0 * tint / 100);
                    break;
                case "text2":
                case "dark2":
                    xlColor = XLColor.FromTheme(XLThemeColor.Text2, 1.0 * tint / 100);
                    break;
                default:
                    xlColor = XLColor.FromHtml(colorPart);
                    break;
            }

            return xlColor;
        }

    }
}
