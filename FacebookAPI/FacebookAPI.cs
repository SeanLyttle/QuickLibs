﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Web.Configuration;
using System.Configuration;
using QuickLibs.Javascript;


namespace QuickLibs.Web.API.Facebook
{
    enum FBMethod
    {
        GET,
        POST,
        DELETE
    }

    /// <summary>
    /// Wrapper to handle the Facebook Graph API. 
    /// </summary>
    public class FBAPI
    {
        #region FBAPI

        private JSON _Response;
        private JSON _TokenData;

        /// <summary>
        /// The access token used to authenticate API calls.
        /// </summary>
        public string AccessToken;

        #region CONSTRUCTORS

        /// <summary>
        /// Create a new instance of the API, with public access only.
        /// </summary>
        public FBAPI() : this(null) { }

        /// <summary>
        /// Create a new instance of the API, using the given token to
        /// authenticate.
        /// </summary>
        /// <param name="token">The access token used for authentication</param>
        public FBAPI(string token)
        {
            _Response = new JSON();
            AccessToken = token;
        }

        #endregion

        private string GetUrl(string endpoint)
        {
            return $"https://graph.facebook.com/{ endpoint.Trim('/') }";
        }

        private JSON AddToken(JSON args)
        {
            if (args.Contains("override_token"))
                args.Add("access_token", args["override_token"].String).Remove("override_token");
            else if (!string.IsNullOrEmpty(AccessToken))
                args.Add("access_token", AccessToken);

            return args;
        }


        /// <summary>
        /// Makes a Facebook Graph API GET request.
        /// </summary>
        /// <param name="endpoint">The path for the call,
        ///   e.g. username/feed</param>
        public JSON Get(string endpoint)
        {
            return Get(endpoint, new JSON());
        }

        /// <summary>
        /// Makes a Facebook Graph API GET request.
        /// </summary>
        /// <param name="endpoint">The path for the call,
        ///   e.g. username/feed</param>
        /// <param name="fields">Specify a list of required API fields.</param>
        public JSON Get(string endpoint, params string[] fields)
        {
            string fieldList = string.Join(",", fields);
            return Get(endpoint, new JSON("fields", fieldList));
        }

        /// <summary>
        /// Makes a Facebook Graph API GET request.
        /// </summary>
        /// <param name="endpoint">The path for the call,
        ///   e.g. username/feed</param>
        /// <param name="args">JSON dictionary of key/value pairs that represents
        /// the key/value pairs for the request</param>
        public JSON Get(string endpoint, JSON args)
        {
            return _Response = JSON.Get(GetUrl(endpoint), AddToken(args));
        }

        /// <summary>
        /// Makes a Fascebook Graph API DELETE request.
        /// </summary>
        /// <param name="relativePath">The path for the call,
        /// e.g. username/feed</param>
        public JSON Delete(string endpoint)
        {
            return _Response = JSON.Delete(GetUrl(endpoint));
        }

        /// <summary>
        /// Makes a Facebook Graph API POST request.
        /// </summary>
        /// <param name="fbPath">The path for the call,
        ///   e.g. username/feed</param>
        /// <param name="args">A JSON dictionary of key=value pairs that
        /// will get passed as query arguments. These determine
        /// what will get set in the graph API.</param>
        public JSON Post(string endpoint, JSON args)
        {
            return _Response = JSON.Post(GetUrl(endpoint), AddToken(args));
        }

        /// <summary>
        /// Returns a non-expiring access token for the given page ID.
        /// The current access token must be for a user with Admin rights on the page with the given ID and have the 'pages_show_list' data-scope.
        /// Full details: https://developers.facebook.com/docs/pages/access-tokens
        /// </summary>
        /// <param name="PageID">The page ID you want an access token for.</param>
        /// <param name="AppID">The App ID/API Key for the FB authentication.</param>
        /// <param name="AppSecret">The App secret for the FB authentication app.</param>
        /// <returns>A page access token with no expiary date.</returns>
        public string GetPageToken(string PageID, string AppID, string AppSecret)
        {
            JSON Args = new JSON("client_id", AppID)
                            .Add("client_secret", AppSecret)
                            .Add("grant_type", "fb_exchange_token")
                            .Add("fb_exchange_token", AccessToken);

            Get("oauth/access_token", Args);

            if (_Response.HasErrors())
                return _Response["error"]["message"].String;

            string LongLivedToken = _Response["access_token"].SafeString; //Adopt new 'long-lived' access token. (valid for 60 days)

            Get("me/accounts", new JSON("override_token", LongLivedToken));

            if (_Response["data"].IsArray)
            {
                foreach (JSON page in _Response["data"])
                {
                    if (page["id"].SafeString.Equals(PageID))
                        return page["access_token"].SafeString;
                }
            }

            return null; //"The current user does not have admin rights for the given Page ID.";
        }

        #endregion

        #region output

        /// <summary>
        /// Gets the last Response JSON.
        /// </summary>
        public JSON Response
        {
            get
            {
                return _Response;
            }
        }

        /// <summary>
        /// Gets the contents of the latest response at given key, as a JSON.
        /// </summary>
        public JSON this[string key]
        {
            get
            {
                return _Response[key];
            }

        }

        /// <summary>
        /// Gets the contents of latest response at given index index, as a JSON.
        /// </summary>
        public JSON this[int index]
        {
            get
            {
                return _Response[index];
            }
        }


        /// <summary>
        /// Returns the result of a "debug_token" GET request passing the current AccessToken as "input_token".
        /// </summary>
        public JSON TokenData
        {
            get
            {
                if (_TokenData != null)
                    return _TokenData;

                //App Access Token used below found using the following get request for the Collage App: "https://graph.facebook.com/oauth/access_token?client_id={app-id}&client_secret={app-secret}&grant_type=client_credentials"
                //Or simply navigate to: https://developers.facebook.com/tools/accesstoken/
                _TokenData = Get("debug_token", new JSON("input_token", AccessToken).Add("override_token", "794621270630736|kaWJAcNHcT3j6c3XNjLuFOYt9JQ"))["data"]; //override_token = Collage App Access Token
                _TokenData["issued_at"].DateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(_TokenData["issued_at"].Integer);
                _TokenData["expires_at"].DateTime = (_TokenData["expires_at"].Integer > 0) ? new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(_TokenData["expires_at"].Integer) : DateTime.MaxValue;
                _TokenData.Add("user", Get(_TokenData["user_id"].SafeString));
                return _TokenData.Remove("user_id");
            }
        }

        /// <summary>
        /// Returns the last response as an Exception.
        /// </summary>
        public Exception Exception
        {
            get {
                return _Response.Exception;
            }
        }

        /// <summary>
        /// Returns true if the last response contain data.
        /// </summary>
        public bool HasData()
        {
            return _Response.HasData();
        }

        /// <summary>
        /// Returns true if this the last response contains an "error" message.
        /// </summary>
        public bool HasErrors()
        {
            return _Response.HasErrors();
        }

        /// <summary>
        /// If the last response HasErrors, this will throw the error as an exception. Otherwise, do nothing.
        /// </summary>
        public void Throw()
        {
            _Response.Throw();
        }


        #endregion
    }

    /// <summary>
    /// Extension to JSON to handle Facebook images.
    /// </summary>
    public static class JSONExtension
    {
        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json)
        {
            Image img = new Image();
            img.Height = Unit.Pixel((int)json["images"][0]["height"].Integer);
            img.Width = Unit.Pixel((int)json["images"][0]["width"].Integer);
            img.ImageUrl = json["images"][0]["source"].SafeString;
            img.ToolTip = json["name"].String;
            return img;
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json, int Square)
        {
            return json.ToImage(false, Square, Square, true);
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json, int Square, bool MaintainAspectRatio)
        {
            return json.ToImage(false, Square, Square, MaintainAspectRatio);
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json, int Width, int Height)
        {
            return json.ToImage(false, Width, Height, true);
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json, int Width, int Height, bool MaintainAspectRatio)
        {
            return json.ToImage(false, Width, Height, MaintainAspectRatio);
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        public static Image ToImage(this JSON json, bool MinDimentions, int Width, int Height)
        {
            return json.ToImage(MinDimentions, Width, Height, false);
        }

        /// <summary>
        /// Returns the best fit facebook image from the given dimentions
        /// </summary>
        private static Image ToImage(this JSON json, bool MinDimentions, int Width, int Height, bool MaintainAspectRatio)
        {
            Image img = json.ToImage();

            while (!json["images"].IsOutOfRange)
            {
                JSON ImgData = json["images"][true]; //iterate through img list

                //Find first image that is big enough to fill the given dimentions.
                if ((MaintainAspectRatio && ((ImgData["height"].Integer >= Height) || (ImgData["width"].Integer >= Width))) ||
                   (!MaintainAspectRatio && ((ImgData["height"].Integer >= Height) && (ImgData["width"].Integer >= Width))))
                {
                    if ((ImgData["height"].Integer < img.Height.Value) || (ImgData["width"].Integer < img.Width.Value)
                      || (img.Height.Value < Height) || (img.Width.Value < Width))
                    {
                        img.Height = Unit.Pixel((int)ImgData["height"].Integer);
                        img.Width = Unit.Pixel((int)ImgData["width"].Integer);
                        img.ImageUrl = ImgData["source"].SafeString;
                    }
                }
                else
                {
                    if ((ImgData["height"].Integer >= img.Height.Value) || (ImgData["width"].Integer >= img.Width.Value))
                    {
                        img.Height = Unit.Pixel((int)ImgData["height"].Integer);
                        img.Width = Unit.Pixel((int)ImgData["width"].Integer);
                        img.ImageUrl = ImgData["source"].SafeString;
                    }
                }

            }

            if (!MaintainAspectRatio)
            {
                if (!MinDimentions)
                {
                    img.Height = Unit.Pixel(Height);
                    img.Width = Unit.Pixel(Width);
                }
                else
                {
                    if ((1.0 * Width / Height) > (img.Width.Value / img.Height.Value))
                    {
                        img.Height = Unit.Pixel((int)Math.Round(img.Height.Value * Width / img.Width.Value));
                        img.Width = Unit.Pixel(Width);
                    }
                    else
                    {
                        img.Width = Unit.Pixel((int)Math.Round(img.Width.Value * Height / img.Height.Value));
                        img.Height = Unit.Pixel(Height);
                    }
                }
            }
            else
            {
                if ((1.0 * Width / Height) > (img.Width.Value / img.Height.Value))
                {
                    img.Width = Unit.Pixel((int)Math.Round(img.Width.Value * Height / img.Height.Value));
                    img.Height = Unit.Pixel(Height);
                }
                else
                {
                    img.Height = Unit.Pixel((int)Math.Round(img.Height.Value * Width / img.Width.Value));
                    img.Width = Unit.Pixel(Width);
                }
            }

            return img;
        }
    }
}
