﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using Drawing = DocumentFormat.OpenXml.Drawing;
using Charts = DocumentFormat.OpenXml.Drawing.Charts;
using QuickLibs.Javascript;
using QuickLibs.Utils;
using ClosedXML.Excel;

namespace QuickLibs.MSOffice
{
    /// <summary>
    /// Generates a MS Office Powerpoint (PPTX) file from a JSON definition.
    /// </summary>
    public class PowerpointPresentation
    {
        JSON presDef;
        string templateFilePath;
        MemoryStream ms;

        /// <summary>
        /// Messages generated during generation of this PowerPoint presentation will be logged to the consolie by default and not recorded.
        /// Use this interface to enable, disable and read from the loging output (e.g. Log.OutputLogging, Log.RecordLogging, Log.LOG).
        /// </summary>
        public Logging Log;

        /// <summary>
        /// Creates an empty Powerpoint Presentation.
        /// </summary>
        public PowerpointPresentation()
        {
            Log = new Logging();
        }

        /// <summary>
        /// Creates an empty Powerpoint Presentation.
        /// </summary>
        /// <param name="TemplateFilePath">Local file path to a template PPTX file.</param>
        public PowerpointPresentation(string TemplateFilePath)
        {
            Log = new Logging();
            templateFilePath = TemplateFilePath;
        }

        /// <summary>
        /// Creates a Powerpoint Presentation based on the given JSON PresentationDefinition.
        /// </summary>
        /// <param name="PresentationDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        public PowerpointPresentation(JSON PresentationDefinition)
        {
            Log = new Logging();
            GeneratePPTX(PresentationDefinition);
        }

        /// <summary>
        /// Creates a Powerpoint Presentation based on the given JSON PresentationDefinition and Template presentation.
        /// </summary>
        /// <param name="PresentationDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        /// <param name="TemplateFilePath">Local file path to a template PPTX file.</param>
        public PowerpointPresentation(JSON PresentationDefinition, string TemplateFilePath)
        {
            Log = new Logging();
            templateFilePath = TemplateFilePath;
            GeneratePPTX(PresentationDefinition);
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~PowerpointPresentation()
        {
            Log = null;
            presDef = null;
            templateFilePath = null;
            ms.Close();
            ms = null;
        }

        /// <summary>
        /// Saves the current Excel file to the given filepath.
        /// </summary>
        public void Save(string filePath)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                ms.Position = 0;
                fs.SetLength(ms.Length);
                ms.CopyTo(fs);
            }
        }

        /// <summary>
        /// Returns the current XL file as a MemoryStream.
        /// </summary>
        public MemoryStream ToMemoryStream()
        {
            ms.Position = 0; //Reset memory stream so we can start reading again from the beginning.
            return ms;
        }

        /// <summary>
        ///     Generates a PPTX file from the given JSON presentation data.
        /// </summary>
        /// <param name="PresentationDefinition">JSON definition. See full documentation for detailed structure requirements.</param>
        public void GeneratePPTX(JSON PresentationDefinition)
        {
            presDef = PresentationDefinition;

            //Log template file path.
            Log.Info(string.Concat("Using template file \"", templateFilePath, "\""));

            if (File.Exists(templateFilePath))
            {
                //Load the template file into the internal MemoryStream.
                using (FileStream fs = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read))
                {
                    ms = new MemoryStream((int)fs.Length);
                    fs.CopyTo(ms);
                    ms.Position = 0;
                }

                //Open the working file.
                PresentationDocument pptx = PresentationDocument.Open(ms, true);
                if (pptx == null)
                    throw new ArgumentNullException("PresentationDocument", "Unable to access PresentationDocument: " + templateFilePath);

                //Reference the presentation XML
                PresentationPart pptxPart = pptx.PresentationPart;

                try
                {
                    if (pptxPart == null)// || pptxPart.Presentation.SlideIdList == null)
                        throw new NullReferenceException("The template presentation document was empty.");

                    //Generate Slides from JSON slide data.
                    foreach (JSON slideDef in presDef["Slides"])
                        GenerateSlide(pptxPart, slideDef);

                    //Update Presentation Properties
                    pptx.PackageProperties.Title = ReplacePlaceholders(pptx.PackageProperties.Title ?? "<Title>"); //Title
                    pptx.PackageProperties.Subject = ReplacePlaceholders(pptx.PackageProperties.Subject ?? "<Subject>"); //Subject
                    pptx.PackageProperties.Category = ReplacePlaceholders(pptx.PackageProperties.Category ?? "<Category>"); //Category
                    pptx.PackageProperties.Keywords = ReplacePlaceholders(pptx.PackageProperties.Keywords ?? "<Tags>"); //Tags
                    pptx.PackageProperties.Description = ReplacePlaceholders(pptx.PackageProperties.Description ?? "<Comments>"); //Comments
                }
                catch (Exception exp)
                {
                    AddSlideToPresentation(pptxPart,
                        CreateErrorSlide(pptxPart, exp)
                    );
                }

                //Remove teplate slides
                RemoveTemplateSlides(pptxPart);

                // Save and close the modified presentation.
                pptxPart.Presentation.Save();
                pptx.Close(); //nb. failure to close document results in a corrupted file - alternatively, wrap the everything between open and close in a Using scope. (Took 3 horus to figure that out!)
            }
            else
                throw new FileNotFoundException("Template file does not exist: \"" + templateFilePath + "\"");
        }

        /// <summary>
        ///     Generates a single presentation slide based on JSON slide data and inserts it into the given Presentation.
        /// </summary>
        private void GenerateSlide(PresentationPart pptxPart, JSON slideDef)
        {
            try
            {
                // Fetch templateSlidePart
                SlidePart templateSlidePart = GetTemplateSlide(pptxPart, slideDef["Template"].SafeString);

                //Check template exists.
                if (templateSlidePart == null)
                    throw new InvalidDataException(string.Concat("Unable to generate slide: Slides[" + slideDef.Index + "]\nTemplate slide \"", slideDef["Template"].String, "\" does not exist."));

                //Create a new slide based on the given template if available.
                AddSlideToPresentation(pptxPart,
                    CreateTemplateSlide(pptxPart, templateSlidePart, slideDef)
                );
            }
            catch (Exception exp)
            {
                AddSlideToPresentation(pptxPart,
                    CreateErrorSlide(pptxPart, exp)
                );
            }
        }

        /// <summary>
        ///     Returns the template slide with the given name (if no name is given, the first template slide will be returned).
        /// </summary>
        private SlidePart GetTemplateSlide(PresentationPart pptxPart, string templateName)
        {
            Log.Info(string.Concat("Fetching template slide: '", templateName, "'"));
            //SlidePart lastSlide = null;

            if (pptxPart.Presentation.SlideIdList != null)
            {
                foreach (SlideId slide in pptxPart.Presentation.SlideIdList)
                {
                    SlidePart slidePart = (SlidePart)pptxPart.GetPartById(slide.RelationshipId);
                    if (slidePart.NotesSlidePart != null && slidePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template" + (String.IsNullOrEmpty(templateName) ? "" : "=" + templateName.ToLower())))
                        return slidePart;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds the given slide to the given presentation.
        /// This process will create a slide list if one does not already exist, and 
        /// </summary>
        private void AddSlideToPresentation(PresentationPart pptxPart, Slide slide)
        {
            //Fetch a reference to the presentation slides list and creat one if it's not already there.
            SlideIdList slideIdList = pptxPart.Presentation.SlideIdList;
            if (slideIdList == null)
                slideIdList = pptxPart.Presentation.SlideIdList = new SlideIdList();


            // Find the highest slide ID in the current list and pick the first template slide in the process.
            uint newSlideId = 256; //Minimum SlideID value must be less than or equal to 256.
            SlideId firstTemplate = null;

            foreach (SlideId slideId in slideIdList.ChildElements)
            {
                if (slideId.Id >= newSlideId)
                    newSlideId = slideId.Id + 1;

                SlidePart templatePart = (SlidePart)pptxPart.GetPartById(slideId.RelationshipId);
                if (firstTemplate == null && templatePart.NotesSlidePart != null && templatePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template"))
                    firstTemplate = slideId;
            }

            //If current slide has no loayout part defined, use the same layout as the first slide in the presentation. (Fixes CreateErrorSlide).
            if (slide.SlidePart.SlideLayoutPart == null)
                slide.SlidePart.AddPart(((SlidePart)pptxPart.GetPartById(((SlideId)(slideIdList.ChildElements[0])).RelationshipId)).SlideLayoutPart);

            //Insert the new slide into the slide list before the first template slide (if firstTemplate is null, this will Append instead).
            slideIdList.InsertBefore(
                new SlideId() { Id = newSlideId, RelationshipId = pptxPart.GetIdOfPart(slide.SlidePart) },
                firstTemplate
            );
        }

        /// <summary>
        ///     Removes all "_template" slides from the given presentation.
        /// </summary>
        private void RemoveTemplateSlides(PresentationPart pptxPart)
        {
            Log.Info("Removing template slides:");

            List<SlideId> templateSlides = new List<SlideId>();
            foreach (SlideId slide in pptxPart.Presentation.SlideIdList)
            {
                SlidePart slidePart = (SlidePart)pptxPart.GetPartById(slide.RelationshipId);
                if (slidePart.NotesSlidePart != null && slidePart.NotesSlidePart.NotesSlide.InnerText.ToLower().Contains("_template"))
                {
                    Log.Info(slidePart.NotesSlidePart.NotesSlide.Descendants<Drawing.Paragraph>().Where(p => p.InnerText.Contains("_template")).First().InnerText);
                    templateSlides.Add(slide);
                    pptxPart.DeletePart(slidePart);
                }
            }

            foreach (SlideId slide in templateSlides)
                slide.Remove();
        }

        /// <summary>
        /// Corrects the error that occurs in the Linux implementation.
        /// </summary>
        //private void RepairPPTX(string workingFilePath, string outputFilePath)
        //{
        //    //workingFilePath = Server.MapPath(workingFilePath);
        //    //outputFilePath = Server.MapPath(outputFilePath);

        //    //File.Copy(workingFilePath, outputFilePath + ".debug.zip", true);

        //    using (FileStream fs = new FileStream(workingFilePath, FileMode.Open))
        //    {
        //        Log.Info("Opening PPTX archive.");
        //        using (ZipArchive PPTX = new ZipArchive(fs, ZipArchiveMode.Update, true))
        //        {
        //            Log.Info("Searching for offending XML file.");
        //            ZipArchiveEntry presentationRels = PPTX.GetEntry("ppt/_rels/presentation.xml.rels");

        //            Log.Info("Opening offending XML file.");
        //            string xml = "";
        //            using (StreamReader sr = new StreamReader(presentationRels.Open()))
        //                xml = sr.ReadToEnd();
        //            presentationRels.Delete();

        //            Log.Info("Applying fix.");
        //            xml = xml.Substring(0, xml.IndexOf("</Relationships>") + 16); //Remove characters after the first root closing tag.

        //            Log.Info("Saving changes to XML file.");
        //            presentationRels = PPTX.CreateEntry("ppt/_rels/presentation.xml.rels");
        //            using (StreamWriter sw = new StreamWriter(presentationRels.Open()))
        //                sw.Write(xml);
        //        }
        //    }

        //    File.Copy(workingFilePath, outputFilePath, true);
        //    File.Delete(workingFilePath);
        //}


        //Create slides

        /// <summary>
        ///     Creates a new blank slide from scratch containing the error message.
        /// </summary>
        private Slide CreateErrorSlide(PresentationPart pptxPart, Exception exp)
        {
            //if (failOnError)
            //    throw exp;

            Log.Error("Creating error slide: " + exp.Message);

            //Slide contents:
            string TitleText = "Error generating slide";
            string BodyText = exp.Message;

            //Everything else is just generating the slide.
            uint drawingObjectId = 1;

            // Declare and instantiate a new slide.
            Slide slide = new Slide();
            slide.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");
            slide.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            slide.AddNamespaceDeclaration("p", "http://schemas.openxmlformats.org/presentationml/2006/main");

            slide.Append(
                new CommonSlideData(
                    new ShapeTree(
                        new NonVisualGroupShapeProperties(
                            new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "" },
                            new NonVisualGroupShapeDrawingProperties(),
                            new ApplicationNonVisualDrawingProperties()
                        ),
                        new GroupShapeProperties(
                            new Drawing.TransformGroup(
                                new Drawing.Offset() { X = 0L, Y = 0L },
                                new Drawing.Extents() { Cx = 0L, Cy = 0L },
                                new Drawing.ChildOffset() { X = 0L, Y = 0L },
                                new Drawing.ChildExtents() { Cx = 0L, Cy = 0L }
                            )
                        )
                    )
                ),
                new ColorMapOverride(
                    new Drawing.MasterColorMapping()
                )
            );


            // Declare and instantiate the title shape of the new slide.
            slide.CommonSlideData.ShapeTree.AppendChild(
                new Shape(
                    new NonVisualShapeProperties(
                        new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "Title" },
                        new NonVisualShapeDrawingProperties(
                            new Drawing.ShapeLocks() { NoGrouping = true }
                        ),
                        new ApplicationNonVisualDrawingProperties(
                            new PlaceholderShape() { Type = PlaceholderValues.Title }
                        )
                    ),
                    new ShapeProperties(
                        new Drawing.Transform2D(
                            new Drawing.Offset() { X = 287524L, Y = 332656L },
                            new Drawing.Extents() { Cx = 8568952L, Cy = 684076L }
                        )
                    ),
                    new TextBody(
                        new Drawing.BodyProperties(),
                        new Drawing.ListStyle(),
                        new Drawing.Paragraph(
                            new Drawing.Run(
                                new Drawing.RunProperties(
                                    new Drawing.SolidFill(
                                        new Drawing.RgbColorModelHex() { Val = "FF0000" } //RED
                                    )
                                )
                                { Dirty = false },
                                new Drawing.Text(TitleText)
                            )
                        )
                    )
                )
            );

            // Declare and instantiate the body shape of the new slide.
            slide.CommonSlideData.ShapeTree.AppendChild(
                new Shape(
                    new NonVisualShapeProperties(
                        new NonVisualDrawingProperties() { Id = drawingObjectId++, Name = "Content Placeholder" },
                        new NonVisualShapeDrawingProperties(
                            new Drawing.ShapeLocks() { NoGrouping = true }
                        ),
                        new ApplicationNonVisualDrawingProperties(
                            new PlaceholderShape() { Index = 1U }
                        )
                    ),
                    new ShapeProperties(
                        new Drawing.Transform2D(
                            new Drawing.Offset() { X = 287524L, Y = 1016732L },
                            new Drawing.Extents() { Cx = 8568952L, Cy = 5256584L }
                        )
                    ),
                    new TextBody(
                        new Drawing.BodyProperties(),
                        new Drawing.ListStyle(),
                        new Drawing.Paragraph(
                            new Drawing.Run(
                                new Drawing.RunProperties(
                                    new Drawing.SolidFill(
                                        new Drawing.RgbColorModelHex() { Val = "000000" } //BLACK
                                    )
                                )
                                { FontSize = 1200, Dirty = false },
                                new Drawing.Text(BodyText)
                            )
                        )
                    )
                )
            );


            // Create the slide part for the new slide.
            SlidePart slidePart = pptxPart.AddNewPart<SlidePart>();

            // Save the new slide part.
            slide.Save(slidePart);
            return slidePart.Slide;
        }

        /// <summary>
        ///     Create a new slide based on the contents of the given template slide.
        /// </summary>
        private Slide CreateTemplateSlide(PresentationPart pptxPart, SlidePart templateSlidePart, JSON slideDef)
        {
            Log.Info("Creating a new slide from template.");

            ChartPart chartPart = null;

            // Copy template to a new slide
            Slide slide = (Slide)templateSlidePart.Slide.CloneNode(true);

            // Create new slide part to copy into
            SlidePart slidePart = pptxPart.AddNewPart<SlidePart>();

            try
            {
                // Copy layout part
                slidePart.AddPart(templateSlidePart.SlideLayoutPart);

                // Copy the image parts only if they are reqired. TODO: Hanle multiple images better?

                JSON ImageFiles = new JSON("[" + slideDef["Images"].SafeString + "]");

                foreach (ImagePart part in templateSlidePart.ImageParts)
                {
                    //Grab next filepath from list.
                    string ImageFilePath = "/Resources/Images/" + ImageFiles[true].String;
                    ImagePartType ImageType = ImagePartType.Icon; //Use ICON as "Invalid Type"

                    //Ensure file exists
                    if (File.Exists(ImageFilePath))
                    {
                        switch (new FileInfo(ImageFilePath).Extension.ToLower())
                        {
                            case ".bmp": ImageType = ImagePartType.Bmp; break;
                            case ".emf": ImageType = ImagePartType.Emf; break;
                            case ".gif": ImageType = ImagePartType.Gif; break;
                            //case ".ico": ImageType = ImagePartType.Icon; break;
                            case ".jpg": ImageType = ImagePartType.Jpeg; break;
                            case ".jpeg": ImageType = ImagePartType.Jpeg; break;
                            case ".pcx": ImageType = ImagePartType.Pcx; break;
                            case ".png": ImageType = ImagePartType.Png; break;
                            case ".tif": ImageType = ImagePartType.Tiff; break;
                            case ".wmf": ImageType = ImagePartType.Wmf; break;
                            default:
                                Log.Error("Unsupported image file type: Slides[" + slideDef.Index + "] " + ImageFilePath);
                                break;
                        }
                    }


                    //If file exists and image type is valid, embed it - otherwise remove the placeholder image.
                    if (File.Exists(ImageFilePath) && ImageType != ImagePartType.Icon)
                    {
                        //Embed the image data and copy the reference from the template.
                        ImagePart newPart = slidePart.AddImagePart(ImageType, templateSlidePart.GetIdOfPart(part));
                        newPart.FeedData(new FileStream(ImageFilePath, FileMode.Open));
                    }
                    else
                    {
                        //Find and remove picture from the slide.
                        foreach (DocumentFormat.OpenXml.Presentation.Picture pic in slide.Descendants<DocumentFormat.OpenXml.Presentation.Picture>())
                            if (pic.Descendants<Drawing.Blip>().FirstOrDefault().Embed.Value == templateSlidePart.GetIdOfPart(part))
                                pic.Remove();
                    }
                }

                // Copy the chart parts
                foreach (ChartPart part in templateSlidePart.ChartParts)
                {
                    ChartPart newPart = slidePart.AddNewPart<ChartPart>(templateSlidePart.GetIdOfPart(part));
                    newPart.FeedData(part.GetStream());
                }

                // Copy the notes part
                NotesSlidePart notesPart = slidePart.AddNewPart<NotesSlidePart>(templateSlidePart.NotesSlidePart.ContentType, templateSlidePart.GetIdOfPart(templateSlidePart.NotesSlidePart));
                notesPart.FeedData(templateSlidePart.NotesSlidePart.GetStream());

                // and delete the "_template" reference
                List<Drawing.Paragraph> templatePara = new List<Drawing.Paragraph>();

                foreach (Drawing.Paragraph para in notesPart.NotesSlide.Descendants<Drawing.Paragraph>())
                    if (para.InnerText.ToLower().Contains("_template"))
                        templatePara.Add(para);

                foreach (Drawing.Paragraph para in templatePara)
                    para.Remove();

                // An empty NotesSlidePart causes an error, so remove it
                if (notesPart.NotesSlide.Descendants<Drawing.Run>().Count() == 0)
                    slidePart.DeletePart(slidePart.NotesSlidePart);

                // Fetch a reference to the first chart part on the templat slide if it exists. (Only handling for one chart per slide.)
                if (slidePart.ChartParts.Count() > 0)
                    chartPart = slidePart.ChartParts.First();

                //slidePart.DeletePart(chartPart);

                // Save the new slide part to link slidePart and Slide.
                slide.Save(slidePart);

                // Replace placeholders
                ReplaceSlidePlaceholders(slidePart.Slide, slideDef);

                //Generate Spreadsheet from data
                if (chartPart != null)
                {
                    ////Generate error slide if chartData contains errors.
                    //if (slideDef["ChartData"].HasErrors())
                    //    throw new InvalidDataException(string.Concat("Invalid Data: Slide[", slideDef.Index, "] ", slideDef["ChartData"].Exception.Message));

                    //Log warning if chart will be blank.
                    if (slideDef["Charts"].IsNull)
                        Log.Warning(string.Concat("Slide[", slideDef.Index, "] Title: ", slideDef["Title"].String, " - This template slide contains a chart, but no chart data was found."));

                    GenerateChart(chartPart, slideDef);
                }
                else if (!slideDef["Charts"].IsNull)
                    Log.Warning(string.Concat("[", slideDef.Index, "] Title: ", slideDef["Title"].String, " - This slide contains chart data, but no chart was found on the template slide."));
            }
            catch (Exception exp)
            {
                //clean up unfinished slide.
                pptxPart.DeletePart(slidePart);
                throw new Exception(string.Concat(exp.Message, " Slide[", slideDef.Index, "]"));
            }

            return slidePart.Slide;
        }

        /// <summary>
        ///     Find all placeholders ("&lt;name&gt;") within the given slide and replace them with the meta data of the same name (case sensitive).
        /// </summary>
        private void ReplaceSlidePlaceholders(Slide slide, JSON scopePlaceholders)
        {
            //Regular expression to find placeholder tags.
            Regex rex = new Regex("<([^>]*)>");
            //Iterate through the paragraphs in the slide AND the notes.
            foreach (Drawing.Paragraph para in slide.Descendants<Drawing.Paragraph>().Concat(slide.SlidePart.NotesSlidePart.NotesSlide.Descendants<Drawing.Paragraph>()))
            {
                if (rex.IsMatch(para.InnerText))
                {
                    //Fetch plain text from each paragraph (this could be fragmented over numerous Drawing.Text objects).
                    string txt = para.Descendants<Drawing.Text>().Aggregate("", (s, t) => s + t.Text);

                    //Replace placeholders.
                    txt = ReplacePlaceholders(txt, scopePlaceholders);

                    //Place the new text back into the first text object and delete the rest.
                    foreach (Drawing.Text text in para.Descendants<Drawing.Text>())
                        if (text == para.Descendants<Drawing.Text>().First())
                            text.Text = txt;
                        else
                            text.Text = ""; //Deleting text objects caused an error, so for now, I've cleared the text instead.
                }
            }
        }


        //Create Chart / Datasheets

        /// <summary>
        ///     Generates the given chart based on the given JSON chart data.
        /// </summary>
        private void GenerateChart(ChartPart chartPart, JSON slideDef)
        {
            JSON chartDef = slideDef["Charts"][0]; //Only supports one chart for now.
            JSON chartLayout = presDef["ChartLayouts"][chartDef["ChartLayout"].String];
            JSON colorScheme = presDef["ColorSchemes"][chartDef["ColorScheme"].String];
            JSON colorSubset = new JSON("[" + chartDef["ColorSubset"].SafeString + "]");
            JSON seriesAxes = new JSON("[" + chartDef["SeriesAxes"].SafeString + "]");
            JSON sheetsDef = presDef["DataSources"][chartDef["DataSource"].String];
            JSON primaryAxis = chartDef["PrimaryAxis"];
            JSON secondaryAxis = chartDef["SecondaryAxis"];
            string numFormat = chartDef["Format"].SafeString;
            bool transpose = chartDef["Transpose"].Boolean;
            string chartParams = chartDef["Params"].SafeString;

            #region Generate Chart

            /////////////////////////////////
            //       GENERATE CHART        //
            /////////////////////////////////

            Log.Info("Generating sheet data.");

            //Generate Sheet Data
            ExcelWorkbook xl = new ExcelWorkbook();
            //Copy logging preferences from this instance.
            xl.Log.LOG = Log.LOG;
            xl.Log.OutputLog = Log.OutputLog;
            xl.Log.VerboseOutput = Log.VerboseOutput;
            xl.Log.InfoColor = Log.InfoColor;
            xl.Log.WarningColor = Log.WarningColor;
            xl.Log.ErrorColor = Log.ErrorColor;
            xl.GenerateXLSX(sheetsDef);

            //Reference the chart sheet
            IXLWorksheet ChartSheet;
            try
            {
                if (chartDef["Sheet"].IsInteger)
                    ChartSheet = xl.xlWorkbook.Worksheet((int)chartDef["Sheet"].Integer);
                else if (chartDef["Sheet"].IsString)
                    ChartSheet = xl.xlWorkbook.Worksheet(chartDef["Sheet"].String);
                else
                    ChartSheet = xl.xlWorkbook.Worksheets.FirstOrDefault();
            }
            catch (Exception e)
            {
                ChartSheet = null;
            }

            //This should never happen as ExcelWorkbook will always have at least one sheet.
            if (ChartSheet == null)
                ChartSheet = xl.xlWorkbook.AddWorksheet("Chart Data");

            //Reference the cells containing chart data.
            IXLRange ChartRange;
            if (chartDef["Range"].IsString)
                ChartRange = ChartSheet.Range(chartDef["Range"].String);
            else
                ChartRange = ChartSheet.RangeUsed();

            Log.Info("Generating chart.");

            //Find chart reference in slide
            Charts.ChartSpace chartSpace = chartPart.ChartSpace;
            Dictionary<string, OpenXmlCompositeElement> charts = FindCharts(chartSpace);
            List<Charts.ValueAxis> axes = FindAxes(chartSpace);

            if (charts.Count == 0)
                throw new NullReferenceException("Chart missing or unsupported chart type found in template.");

            //Configure Axes
            if (axes.Count >= 1)
                ConfigureAxis(axes[0], primaryAxis);

            if (axes.Count >= 2)
                ConfigureAxis(axes[1], secondaryAxis);

            //Position objects on screen
            Charts.PlotArea PlotArea = chartSpace.Descendants<Charts.PlotArea>().FirstOrDefault();
            if (PlotArea != null)
            {
                if (chartLayout["PlotArea"]["Top"].IsNumber && chartLayout["PlotArea"]["Left"].IsNumber && chartLayout["PlotArea"]["Width"].IsNumber && chartLayout["PlotArea"]["Height"].IsNumber)
                    ApplyLayout(chartLayout["PlotArea"], PlotArea.GetFirstChild<Charts.Layout>());
                else if (chartLayout["PlotArea"].IsDictionary)
                    Log.Warning("Slide Schema > Charts > Plot Area definition is incomplete. Location has not been modified");
            }

            //If chart contains a legend - apply positioning
            Charts.Legend Legend = chartSpace.Descendants<Charts.Legend>().FirstOrDefault();
            if (Legend != null)
            {
                if (chartLayout["Legend"]["Top"].IsNumber && chartLayout["Legend"]["Left"].IsNumber && chartLayout["Legend"]["Width"].IsNumber && chartLayout["Legend"]["Height"].IsNumber)
                {
                    ApplyPosition(chartLayout["Legend"]["Position"].SafeString, Legend);
                    ApplyLayout(chartLayout["Legend"], Legend.GetFirstChild<Charts.Layout>());
                }
                else if (chartLayout["Legend"]["Position"].IsString)
                    ApplyPosition(chartLayout["Legend"]["Position"].String, chartSpace.Descendants<Charts.Legend>().FirstOrDefault());
                else if (chartLayout["Legend"].IsDictionary && !chartLayout["Legend"]["IsHidden"].IsBoolean)
                    Log.Warning("Slide Schema > Charts > Legend definition is incomplete. Location has not been modified");

                //Or remove the chart if legend should be hidden.
                if (chartLayout["Legend"]["IsHidden"].Boolean)
                    Legend.Remove();
                //else if (presDef["Params"]["SeriesCount"].Number <= 1) //Hide legend if only 1 series.
                //    Legend.Remove();
            }

            //NB: If the legend has been removed the title sometimes reappears - this removes it again.
            Charts.AutoTitleDeleted TitleBox = chartSpace.Descendants<Charts.AutoTitleDeleted>().FirstOrDefault();
            if (TitleBox != null)
                TitleBox.Remove();

            //Allow hidden cell to contain chart data.
            chartSpace.GetFirstChild<Charts.Chart>().GetFirstChild<Charts.PlotVisibleOnly>().Val = false;

            //Clear current cached chart data.
            List<OpenXmlCompositeElement> CachedElements = new List<OpenXmlCompositeElement>();
            //      Find child series elements in each chart.
            foreach (KeyValuePair<string, OpenXmlCompositeElement> chart in charts)
                foreach (OpenXmlElement child in chart.Value.ChildElements)
                    if (child.GetType().Name.Contains("Series"))
                        CachedElements.Add((OpenXmlCompositeElement)child);
            //      Remove found elements.
            foreach (OpenXmlCompositeElement child in CachedElements)
                child.Remove();


            Log.Info("Populating cached chart data.");

            uint iSer = 0;
            uint iCat = 0;
            int Row = 1;
            int Col = 1;
            int SeriesCount = 0;

            int ChartRows = ChartRange.RowCount() - 1; //TODO: "No Header Column"
            int ChartCols = ChartRange.ColumnCount() - 1; //TODO: "No Header Row"
            int ChartRow = ChartRange.FirstCell().Address.RowNumber;
            int ChartCol = ChartRange.FirstCell().Address.ColumnNumber;

            //Loop through each series (Could be on rows or columns)
            while (Row <= ChartRows && Col <= ChartCols) //For Each Series
            {
                //Add series to chart if included.
                //DO ONCE FOR EACH SERIES
                IXLCell SeriesCell;
                if (transpose)
                    SeriesCell = ChartSheet.Cell(ChartRow, ChartCol + Col); //Series on Cols
                else
                    SeriesCell = ChartSheet.Cell(ChartRow + Row, ChartCol); //Series on Rows

                //Don't add a series for gaps between series groups. (Blank series.)
                if (!string.IsNullOrEmpty(SeriesCell.Value.ToString()))
                {
                    //SELECT AXIS for this series.
                    //Get the correct chart (Primary and Secondary axis on different charts in powerpoint).
                    int SelectedAxis = seriesAxes.Length == 0 ? 0 : SeriesCount % seriesAxes.Length;

                    OpenXmlCompositeElement chart = charts[charts.Keys.First()]; //Default primary axis.
                    if (charts.Count > 0 && seriesAxes[SelectedAxis].Integer == 2) //2 = Secondary Axis - anything else uses primary.
                        chart = charts[charts.Keys.Last()]; //Secondary Axis

                    //SELECT COLOR for this series.
                    JSON SelectedColor = new JSON();
                    //If no color subset is defined, select incremental colour index by default. Otherwise use the subset set to select colours.
                    if (colorSubset.Length == 0)
                        SelectedColor = colorScheme[colorScheme.Length == 0 ? 0 : SeriesCount % colorScheme.Length];
                    else
                    {
                        JSON SelectedSubset = colorSubset[SeriesCount % colorSubset.Length];
                        if (colorScheme[0].IsDictionary) //One dimentional colour scheme.
                        {
                            if (SelectedSubset.IsInteger) //One dimentional Subset - EXPECTED ORDINAL BEHAVIOUR
                                SelectedColor = colorScheme[(int)SelectedSubset.Integer - 1]; //Zero based index.
                            else if (SelectedSubset.IsArray) //Two dimentional subset. - Unexpected behavour
                                SelectedColor = colorScheme[(int)SelectedSubset[1].Integer];
                            else //If all else fails - just use the next colour! - Unexpected behaviour
                                SelectedColor = colorScheme[colorScheme.Length == 0 ? 0 : SeriesCount % colorScheme.Length];
                        }
                        else //Two dimentional colour scheme.
                        {
                            if (SelectedSubset.IsInteger) //One dimentional Subset - EXPECTED NOMINAL BEHAVIOUR
                                SelectedColor = colorScheme[colorScheme.Length == 0 ? 0 : SeriesCount % colorScheme.Length][(int)SelectedSubset.Integer - 1]; //Use subset colour in incrementing groups.
                            else if (SelectedSubset.IsArray) //Two dimentional subset. - EXPECTED CLUSTERED BEHAVIOUR
                                SelectedColor = colorScheme[(int)SelectedSubset[0].Integer - 1][(int)SelectedSubset[1].Integer - 1]; //Use subset colour.
                            else //If all else fails - just use the next colour! - Unexpected behaviour
                                SelectedColor = colorScheme[colorScheme.Length == 0 ? 0 : SeriesCount % colorScheme.Length][0]; //Use first colour in each group
                        }
                    }

                    //Finally select the first available colour encoding.
                    string color = SelectedColor["MSColor"].String ?? SelectedColor["HTMLColor"].SafeString;

                    //Append a new chart series.
                    OpenXmlCompositeElement chartSeries = AppendSeries(chart, iSer, color, chartParams);
                    if (chartSeries == null)
                        throw new NullReferenceException("Chart series is unsupported."); //This should never happen - but would highligh missed types in coding.

                    if (Legend != null && SeriesCell.Value.ToString().StartsWith(" DT "))
                    {
                        Charts.Index idx = chartSeries.Descendants<Charts.Index>().FirstOrDefault();
                        //if(idx != null)
                        //    Legend.Append(
                        //        new Charts.LegendEntry(
                        //            new Charts.Index() { Val = idx.Val },//This doesn't work - The Legend Index bears no relation to the series index!?!
                        //            new Charts.Delete() { Val = true }
                        //        )
                        //    );
                    }


                    //Increment counter.
                    SeriesCount++;

                    //If more than one series
                    //if (ChartData.Length > 1 || ChartData[0].Length > 1) //TODO: Is this required?
                    {
                        //Add Series Labels
                        chartSeries.Append(
                            new Charts.SeriesText(
                                new Charts.StringReference(
                                    new Charts.Formula(SeriesCell.Address.ToStringRelative(true)),
                                    new Charts.StringCache(
                                        new Charts.PointCount() { Val = new UInt32Value(1U) },
                                        new Charts.StringPoint(
                                            new Charts.NumericValue(SeriesCell.Value.ToString())
                                        )
                                        { Index = new UInt32Value(0U) }
                                    )
                                )
                            )
                        );
                    }

                    //Dynamic data ranges
                    Charts.Formula strRange = new Charts.Formula(); //A2:A5
                    Charts.Formula numRange = new Charts.Formula(); //B2:B5, C2:C5, D2:D5 etc.

                    //Fixed data caches
                    Charts.StringCache strCache = new Charts.StringCache(new Charts.PointCount() { Val = new UInt32Value(0U) });
                    Charts.NumberingCache numCache = new Charts.NumberingCache(new Charts.FormatCode(SeriesCell.Style.NumberFormat.Format), new Charts.PointCount() { Val = new UInt32Value(0U) });

                    chartSeries.Append(
                        new Charts.CategoryAxisData(
                            new Charts.StringReference(
                                strRange,
                                strCache
                            )
                        ),
                        new Charts.Values(
                            new Charts.NumberReference(
                                numRange,
                                numCache
                            )
                        )
                    );

                    //Populate dynamic data ranges.
                    if (transpose) //Categories on Rows
                    {
                        strRange.Text = ChartSheet.Range(
                            ChartRow + 1,         //2
                            ChartCol,             //A
                            ChartRow + ChartRows, //5
                            ChartCol              //A
                        ).RangeAddress.ToStringRelative(true); //A2:A5

                        numRange.Text = ChartSheet.Range(
                            ChartRow + 1,         //2
                            ChartCol + Col,       //B .. C ..
                            ChartRow + ChartRows, //5
                            ChartCol + Col        //B .. C ..
                        ).RangeAddress.ToStringRelative(true); //B2:B5 .. C2:C5 ..
                    }
                    else //not transposed - Categories on Cols
                    {
                        strRange.Text = ChartSheet.Range(
                            ChartRow,             //1
                            ChartCol + 1,         //B
                            ChartRow,             //1
                            ChartCol + ChartCols  //E
                        ).RangeAddress.ToStringRelative(true); //B1:E1

                        numRange.Text = ChartSheet.Range(
                            ChartRow + Row,      //2 .. 3 ..
                            ChartCol + 1,        //B
                            ChartRow + Row,      //2 .. 3 ..
                            ChartCol + ChartCols //E
                        ).RangeAddress.ToStringRelative(true); //B2:E2 .. B3:E3 ..
                    }

                    iCat = 0;

                    //REPEAT FOR EACH VALUE IN THE SERIES
                    while (Row <= ChartRows && Col <= ChartCols) //For Each Category
                    {
                        IXLCell CategoryCell;
                        if (transpose) //Categories on Rows
                            CategoryCell = ChartSheet.Cell(ChartRow + Row, ChartCol); //B1, C1, D1...
                        else //Categories on Cols
                            CategoryCell = ChartSheet.Cell(ChartRow, ChartCol + Col); //A2, A3, A4...

                        IXLCell ValueCell;
                        ValueCell = ChartSheet.Cell(ChartRow + Row, ChartCol + Col); //B2,B3,B4 OR B2,C2,D2 ..

                        //Add chart cached data.
                        strCache.Append(
                            new Charts.StringPoint(
                                new Charts.NumericValue(CategoryCell.Value.ToString())
                            )
                            { Index = new UInt32Value(iCat) }
                        );

                        numCache.Append(
                            new Charts.NumericPoint(
                                new Charts.NumericValue(ValueCell.Value.ToString())
                            )
                            { Index = new UInt32Value(iCat) }
                        );

                        strCache.Descendants<Charts.PointCount>().First().Val++;
                        numCache.Descendants<Charts.PointCount>().First().Val++;
                        iCat++;

                        if (transpose)
                            Row++;
                        else
                            Col++;
                    }
                    iSer++;
                }//end if SeriesCell not empty

                if (transpose)
                    Row = Col / Col++; //Reset row to 1, increment col
                else
                    Col = Row / Row++; //Reset col to 1, increment row
            }//end While (For each series)

            #endregion

            //Debugging
            //xl.Save("test.xlsx");

            //Save spreadsheet and add it as an embedded part.
            EmbeddedPackagePart embeddedPart = chartPart.AddNewPart<EmbeddedPackagePart>("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "rId1");
            embeddedPart.FeedData(xl.ToMemoryStream()); //Load memory stream into embeded data part.

            // link the excel to the chart
            chartPart.ChartSpace.Descendants<Charts.ExternalData>().First().Id = chartPart.GetIdOfPart(embeddedPart);
            chartPart.ChartSpace.Save();
        }

        private void ConfigureAxis(Charts.ValueAxis axis, JSON config)
        {
            if (axis != null)
            {
                //Format
                if (config["Format"].IsString)
                    axis.NumberingFormat.FormatCode = config["Format"].String;

                //Range Max
                if ("auto".Equals(config["MaxValue"].String, StringComparison.CurrentCultureIgnoreCase) && axis.Scaling.MaxAxisValue != null)
                    axis.Scaling.MaxAxisValue.Remove();
                else if (config["MaxValue"].IsNumber && axis.Scaling.MaxAxisValue != null)
                    axis.Scaling.MaxAxisValue.Val = config["MaxValue"].Number;
                else if (config["MaxValue"].IsNumber)
                    axis.Scaling.MaxAxisValue = new Charts.MaxAxisValue() { Val = config["MaxValue"].Number };

                //Range Min
                if ("auto".Equals(config["MinValue"].String, StringComparison.CurrentCultureIgnoreCase) && axis.Scaling.MinAxisValue != null)
                    axis.Scaling.MinAxisValue.Remove();
                else if (config["MinValue"].IsNumber && axis.Scaling.MinAxisValue != null)
                    axis.Scaling.MinAxisValue.Val = config["MinValue"].Number;
                else if (config["MinValue"].IsNumber)
                    axis.Scaling.MinAxisValue = new Charts.MinAxisValue() { Val = config["MinValue"].Number };

                //Major Units (updates minor units too, but this can be overridden later)
                Charts.MajorUnit MajorUnit = axis.Descendants<Charts.MajorUnit>().FirstOrDefault();
                Charts.MinorUnit MinorUnit = axis.Descendants<Charts.MinorUnit>().FirstOrDefault();
                if ("auto".Equals(config["MajorUnit"].String, StringComparison.CurrentCultureIgnoreCase) && MajorUnit != null)
                {
                    MajorUnit.Remove();
                    if (MinorUnit != null)
                        MinorUnit.Remove();
                }
                else if (config["MajorUnit"].IsNumber && MajorUnit != null)
                {
                    MajorUnit.Val = config["MajorUnit"].Number;
                    if (MinorUnit != null)
                        MinorUnit.Val = config["MajorUnit"].Number / 2;
                }
                else if (config["MajorUnit"].IsNumber)
                {
                    axis.Append(new Charts.MajorUnit() { Val = config["MajorUnit"].Number });
                    if (MinorUnit != null)
                        MinorUnit.Val = config["MajorUnit"].Number / 2;
                    else
                        axis.Append(new Charts.MinorUnit() { Val = config["MajorUnit"].Number / 2 });
                }

                //Minor Units - if supplied, will override the default 1/2 major unit, above.
                if ("auto".Equals(config["MinorUnit"].String, StringComparison.CurrentCultureIgnoreCase) && MinorUnit != null)
                    MinorUnit.Remove();
                else if (config["MinorUnit"].IsNumber && MinorUnit != null)
                    MinorUnit.Val = config["MinorUnit"].Number;
                else if (config["MinorUnit"].IsNumber)
                    axis.Append(new Charts.MinorUnit() { Val = config["MinorUnit"].Number });

                //Colour - TODO: This section hasn't been fully tested. There's a chance under some circumstances this may cause
                if (config["Color"].IsString && IsColor(config["Color"].String))
                {
                    Drawing.SolidFill axisColor = axis.ChartShapeProperties.Descendants<Drawing.SolidFill>().FirstOrDefault();
                    axisColor.RemoveAllChildren();
                    axisColor.Append(GetXMLColor(config["Color"].String));

                    Drawing.SolidFill textColor = axis.TextProperties.Descendants<Drawing.SolidFill>().FirstOrDefault();
                    textColor.RemoveAllChildren();
                    textColor.Append(GetXMLColor(config["Color"].String));
                }
            }
        }

        private void ApplyPosition(string position, Charts.Legend legend)
        {
            if (legend != null)
            {
                Charts.LegendPosition legendPos = new Charts.LegendPosition();

                switch (position.ToUpper())
                {
                    case "TOP": legendPos.Val = Charts.LegendPositionValues.Top; break;
                    case "LEFT": legendPos.Val = Charts.LegendPositionValues.Left; break;
                    case "RIGHT": legendPos.Val = Charts.LegendPositionValues.Right; break;
                    case "BOTTOM": legendPos.Val = Charts.LegendPositionValues.Bottom; break;
                    case "TOPRIGHT": legendPos.Val = Charts.LegendPositionValues.TopRight; break;
                    default: break;
                }

                if (legendPos.Val != null)
                {
                    Log.Info("Applying legend position: " + position);
                    legend.LegendPosition = legendPos;
                }
                else
                    Log.Warning("Legend position not recognised: " + position);
            }
        }

        /// <summary>
        /// Sets the position of the given element using the given schema.
        /// </summary>
        private void ApplyLayout(JSON layoutSchema, Charts.Layout Layout)
        {
            if (Layout.ManualLayout == null) //Chart is using default layout, so create manual layout.
            {
                Log.Info("Generating " + Layout.Parent.GetType().Name + " layout.");

                Layout.Append(
                    new Charts.ManualLayout( //The order of creation is important: Left, Top, Width, Height.
                        Layout.Parent.GetType().Name.Contains("PlotArea") ? new Charts.LayoutTarget() { Val = Charts.LayoutTargetValues.Inner } : null,
                        new Charts.LeftMode() { Val = Charts.LayoutModeValues.Edge },
                        new Charts.TopMode() { Val = Charts.LayoutModeValues.Edge },
                        new Charts.Left() { Val = Math.Min(Math.Max(layoutSchema["Left"].Number, 0), 100) / 100 },
                        new Charts.Top() { Val = Math.Min(Math.Max(layoutSchema["Top"].Number, 0), 100) / 100 },
                        new Charts.Width() { Val = Math.Min(Math.Max(layoutSchema["Width"].Number, 0), 100) / 100 },
                        new Charts.Height() { Val = Math.Min(Math.Max(layoutSchema["Height"].Number, 0), 100) / 100 }
                    )
                );
            }
            else //Adjust existing manual layout values.
            {
                Log.Info("Updating " + Layout.Parent.GetType().Name + " layout.");

                Layout.ManualLayout.Top.Val = Math.Min(Math.Max(layoutSchema["Top"].Number, 0), 100) / 100;
                Layout.ManualLayout.Left.Val = Math.Min(Math.Max(layoutSchema["Left"].Number, 0), 100) / 100;
                Layout.ManualLayout.Width.Val = Math.Min(Math.Max(layoutSchema["Width"].Number, 0), 100) / 100;
                Layout.ManualLayout.Height.Val = Math.Min(Math.Max(layoutSchema["Height"].Number, 0), 100) / 100;
            }
        }

        /// <summary>
        /// Returns a list of all charts on the slide regardless of chart type.
        /// </summary>
        private Dictionary<string, OpenXmlCompositeElement> FindCharts(Charts.ChartSpace chartSpace)
        {
            Dictionary<string, OpenXmlCompositeElement> charts = new Dictionary<string, OpenXmlCompositeElement>();

            //Find all charts within the plot area.
            //Area3DChart, AreaChart, Bar3DChart, BarChart, BubbleChart, DoughnutChart, Line3DChart, LineChart, OfPieChart, Pie3DChart, PieChart, RadarChart, ScatterChart
            foreach (OpenXmlCompositeElement chart in chartSpace.Descendants<Charts.PlotArea>().FirstOrDefault().ChildElements.Where(e => e.GetType().Name.EndsWith("Chart")))
            {
                Log.Info("Found chart type: " + chart.GetType().Name);
                charts.Add(chart.GetType().Name + charts.Count.ToString(), chart);
            }

            return charts;
        }

        /// <summary>
        /// Returns a list of all axes on the slide regardless of chart type.
        /// </summary>
        private List<Charts.ValueAxis> FindAxes(Charts.ChartSpace chartSpace)
        {
            List<Charts.ValueAxis> axes = new List<Charts.ValueAxis>();

            //Find all axes within the plot area.
            foreach (Charts.ValueAxis axis in chartSpace.Descendants<Charts.PlotArea>().FirstOrDefault().Descendants<Charts.ValueAxis>())
            {
                axes.Add(axis);
                Log.Info("Found " + (axes.Count == 1 ? "primary" : "secondary") + " axis.");
            }

            return axes;
        }

        /// <summary>
        ///     Creates the relevant chart series within the given chart reglardless or chart type.
        /// </summary>
        private OpenXmlCompositeElement AppendSeries(OpenXmlCompositeElement chart, uint index, string color, string param)
        {
            Charts.ChartShapeProperties shapeProp = null;
            if (IsColor(color))
            {
                if (param.EndsWith("line", StringComparison.CurrentCultureIgnoreCase)) //As in "line" or smoothline"
                    shapeProp =
                        new Charts.ChartShapeProperties(
                            new Drawing.Outline(
                                new Drawing.SolidFill(
                                    GetXMLColor(color)
                                )
                            )
                        );
                else
                    shapeProp =
                        new Charts.ChartShapeProperties(
                            new Drawing.SolidFill(
                                GetXMLColor(color)
                            ),
                            new Drawing.Outline(
                                new Drawing.NoFill()
                            )
                        );
            }

            switch (chart.GetType().Name)
            {
                case "Area3DChart":
                case "AreaChart":
                    return chart.AppendChild(
                        new Charts.AreaChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                        )
                    );
                case "Bar3DChart":
                case "BarChart":
                    return chart.AppendChild(
                        new Charts.BarChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                        )
                    );
                case "BubbleChart":
                    return chart.AppendChild(
                        new Charts.BubbleChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                        )
                    );
                case "Line3DChart":
                case "LineChart":
                    if (param.EndsWith("line", StringComparison.CurrentCultureIgnoreCase)) //Line or SmoothLine
                    {
                        return chart.AppendChild(
                            new Charts.LineChartSeries(
                                new Charts.Index() { Val = new UInt32Value(index) },
                                new Charts.Order() { Val = new UInt32Value(index) },
                                shapeProp
                                ,
                                new Charts.InvertIfNegative() { Val = false },
                                new Charts.Marker(
                                    new Charts.Symbol() { Val = Charts.MarkerStyleValues.None }
                                ),
                                new Charts.Smooth() { Val = param.StartsWith("smooth", StringComparison.CurrentCultureIgnoreCase) }
                            )
                        );
                    }
                    else
                    {
                        return chart.AppendChild(
                            new Charts.LineChartSeries(
                                new Charts.Index() { Val = new UInt32Value(index) },
                                new Charts.Order() { Val = new UInt32Value(index) },
                                new Charts.ChartShapeProperties(
                                    new Drawing.Outline(
                                        new Drawing.NoFill()
                                    )
                                ),
                                new Charts.InvertIfNegative() { Val = false },
                                new Charts.Marker(
                                    new Charts.Symbol() { Val = Charts.MarkerStyleValues.Circle },
                                    new Charts.Size() { Val = 7 },
                                    shapeProp
                                ),
                                new Charts.Smooth() { Val = false }
                            )
                        );
                    }
                case "DoughnutChart":
                case "OfPieChart":
                case "Pie3DChart":
                case "PieChart":
                    return chart.AppendChild(
                        new Charts.PieChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                        )
                    );
                case "RadarChart":
                    return chart.AppendChild(
                        new Charts.RadarChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                        )
                    );
                case "ScatterChart":
                    return chart.AppendChild(
                        new Charts.ScatterChartSeries(
                            new Charts.Index() { Val = new UInt32Value(index) },
                            new Charts.Order() { Val = new UInt32Value(index) },
                            new Charts.Marker(
                                new Charts.Symbol() { Val = Charts.MarkerStyleValues.Circle },
                                new Charts.Size() { Val = 7 },
                                shapeProp,
                            new Charts.InvertIfNegative() { Val = false }
                            )
                        )
                    );
                default:
                    return null;
            }
        }


        //Common functions

        /// <summary>
        /// Returns true if the given string can be matched againsta a color.
        /// </summary>
        private bool IsColor(string color)
        {
            if (string.IsNullOrEmpty(color))
            {
                Log.Warning(string.Concat("Template sheet definition contins an unrecognised color: ''"));
                return false;
            }

            //Remove tint if specified.
            color = color.Split(' ')[0].Split(',')[0].Trim();

            try
            {
                if (",accent1,accent2,accent3,accent4,accent5,accent6,background1,background2,followedhyperlink,hyperlink,text1,text2,dark1,dark2,light1,light2,".Contains(string.Concat(",", color.ToLower(), ",")))
                    return true;

                XLColor.FromHtml(color);
                return true;
            }
            catch (Exception e)
            {
                Log.Warning(502, string.Concat("Template sheet definition contins an unrecognised color: ", color));
                return false;
            }
        }

        /// <summary>
        /// Returns an XML Color based on the given string ("Color Tint": where Color = any color string and positive Tint = brightness%  and negative Tint = darkness%)
        /// </summary>
        private OpenXmlElement GetXMLColor(string color)
        {
            String colorPart = color.Split(' ')[0].Split(',')[0].Trim();

            int tint = 0;
            int.TryParse(color.Substring(colorPart.Length).Trim(), out tint);

            tint = Math.Min(Math.Max(tint, -100), 100); //Clip value if out of range.

            OpenXmlElement xmlColor;

            switch (colorPart.ToLower())
            {
                case "accent1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent1 };
                    break;
                case "accent2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent2 };
                    break;
                case "accent3":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent3 };
                    break;
                case "accent4":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent4 };
                    break;
                case "accent5":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent5 };
                    break;
                case "accent6":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Accent6 };
                    break;
                case "background1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Background1 };
                    break;
                case "background2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Background2 };
                    break;
                case "followedhyperlink":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.FollowedHyperlink };
                    break;
                case "hyperlink":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Hyperlink };
                    break;
                case "text1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Text1 };
                    break;
                case "text2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Text2 };
                    break;
                case "dark1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Dark1 };
                    break;
                case "dark2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Dark2 };
                    break;
                case "light1":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Light1 };
                    break;
                case "light2":
                    xmlColor = new Drawing.SchemeColor() { Val = Drawing.SchemeColorValues.Light2 };
                    break;
                default:
                    xmlColor = new Drawing.RgbColorModelHex() { Val = colorPart.TrimStart('#') };
                    break;
            }

            //Apply tint / shade if available.
            if (tint > 0)
                xmlColor.Append(
                    new Drawing.LuminanceModulation() { Val = (100 - tint) * 1000 },
                    new Drawing.LuminanceOffset() { Val = tint * 1000 }
                );
            else if (tint < 0)
                xmlColor.Append(
                    new Drawing.LuminanceModulation() { Val = (100 + tint) * 1000 }
                );


            return xmlColor;
        }

        /// <summary>
        ///     Find all placeholders ("&lt;name&gt;") within the given string and replace them with the meta data of the same name (case sensitive).
        /// </summary>
        public string ReplacePlaceholders(string txt)
        {
            return ReplacePlaceholders(txt, null);
        }

        /// <summary>
        ///     Find all placeholders ("&lt;name&gt;") within the given string and replace them with the meta data of the same name (case sensitive).
        /// </summary>
        public string ReplacePlaceholders(string txt, JSON scopeDef)
        {
            txt = txt.Replace("&lt;", "<").Replace("&gt;", ">");
            //txt = txt.Replace("[", "<").Replace("]", ">"); //Works with square brackets too.

            //Regular expression to find placeholder tags.
            Regex rex = new Regex("<([^>]*)>");

            foreach (Match match in rex.Matches(txt))
            {
                int index;
                string key = match.Groups[1].Value;
                JSON source = scopeDef ?? presDef;
                JSON Dictionary = presDef["Dictionary"]; //TODO: Rename Dictionary? -- Shortcuts?
                if (key.StartsWith("."))
                {
                    key = key.TrimStart('.');
                    source = presDef;
                }

                if (Dictionary.Contains(key)) //If the Key matches an abbreviation in the dictionary replace it with the dictinary entry.
                    key = key.Replace(key, Dictionary[key].SafeString);

                while (key.Contains('.'))
                {
                    string keyPart = key.Split('.')[0];
                    if (Dictionary.Contains(keyPart)) //If the KeyPart matches an abbreviation in the dictionary replace it with the dictinary entry.
                        key = key.Replace(keyPart, Dictionary[keyPart].SafeString);
                    else
                    {
                        if (int.TryParse(keyPart, out index)) //If key is numeric use as index.
                            source = source[index];
                        else if (source.IsArray && keyPart.ToLower() == "first") //If source is array and key is "First", return the first element of array.
                            source = source[0];
                        else if (source.IsArray && keyPart.ToLower() == "last") //If source is array and key is "Last", return the last element of array.
                            source = source[-1];
                        else if (source.IsArray) //If source is array (and key is non-numeric) look in first element of array.
                            source = source[0][key];
                        else
                            source = source[key.Split('.')[0]]; //Otherwise just use the key.

                        key = key.Substring(key.IndexOf('.') + 1);
                    }

                    if (Dictionary.Contains(key)) //If the remaining Key matches an abbreviation in the dictionary replace it with the dictinary entry.
                        key = key.Replace(key, Dictionary[key].SafeString);
                }

                if (int.TryParse(key, out index)) //If key is numeric use as index.
                    txt = txt.Replace(match.Value, source[index].SafeString);
                else if (source.IsArray && key.ToLower() == "first") //If source is array and key is "First", return the first element of array.
                    txt = source[0].SafeString;
                else if (source.IsArray && key.ToLower() == "last") //If source is array and key is "Last", return the last element of array.
                    txt = source[-1].SafeString;
                else if (source.IsArray) //If source is array (and key is non-numeric) look in first element of array.
                    txt = txt.Replace(match.Value, source[0][key].String);
                else //Otherwise just use the key.
                    txt = txt.Replace(match.Value, source[key].String);
            }

            return txt.Replace(">", "&gt;").Replace("<", "&lt;");
        }
    }
}
